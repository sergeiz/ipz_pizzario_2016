/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.testapp;

import pizzario.model.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;


public class TestModelGenerator {

    public static PizzarioModel generateTestData ()
    {
        PizzarioModel m = new PizzarioModel();

        generateSizes( m );
        generateIngredients( m );
        generateProducts( m );
        generateCarts( m );
        generateOrders( m );
        generateAccounts( m );
        generateCookingAssignments( m );
        generateDeliveries( m );

        return m;
    }


    private static void generateSizes ( PizzarioModel m )
    {
        // ----

        Small  = new ProductSize( UUID.randomUUID(), "Small",  25, 300.0 );
        Medium = new ProductSize( UUID.randomUUID(), "Medium", 30, 600.0 );
        Large  = new ProductSize( UUID.randomUUID(), "Large",  35, 900.0 );

        // ----

        m.getProductSizes().add( Small );
        m.getProductSizes().add( Medium );
        m.getProductSizes().add( Large );
    }


    private static void generateIngredients ( PizzarioModel m )
    {
        Mozarella       = new Ingredient( UUID.randomUUID(), "Mozarella" );
        Pineaple        = new Ingredient( UUID.randomUUID(), "Pineaple" );
        Chicken         = new Ingredient( UUID.randomUUID(), "Chicken" );
        Saliami         = new Ingredient( UUID.randomUUID(), "Saliami" );
        Tomato          = new Ingredient( UUID.randomUUID(), "Tomato" );
        Kebab           = new Ingredient( UUID.randomUUID(), "Kebab" );
        Eggplant        = new Ingredient( UUID.randomUUID(), "Eggplant" );
        Onion           = new Ingredient( UUID.randomUUID(), "Onion" );
        Parsley         = new Ingredient( UUID.randomUUID(), "Parsley" );
        Bacon           = new Ingredient( UUID.randomUUID(), "Bacon" );
        Ham             = new Ingredient( UUID.randomUUID(), "Ham" );
        PickledCucumber = new Ingredient( UUID.randomUUID(), "Pickled Cucumber" );
        Mushroom        = new Ingredient( UUID.randomUUID(), "Mushroom" );

        m.getIngredients().add( Mozarella );
        m.getIngredients().add( Pineaple );
        m.getIngredients().add( Chicken );
        m.getIngredients().add( Saliami );
        m.getIngredients().add( Tomato );
        m.getIngredients().add( Kebab );
        m.getIngredients().add( Eggplant );
        m.getIngredients().add( Onion );
        m.getIngredients().add( Parsley );
        m.getIngredients().add( Bacon );
        m.getIngredients().add( Ham );
        m.getIngredients().add( PickledCucumber );
        m.getIngredients().add( Mushroom );
    }

    private static void generateProducts ( PizzarioModel m )
    {
        // ----

        Mafia = new Product( UUID.randomUUID(), "Mafia", "http://pizzario.com/images/mafia.jpg" );

        Mafia.defineRecipe( Small )
                .useIngredient( Mozarella, 25.0 )
                .useIngredient( Pineaple,  25.0 )
                .useIngredient( Chicken,   25.0 )
                .useIngredient( Saliami,   25.0 )
                .useIngredient( Tomato,    25.0 );

        Mafia.defineRecipe( Medium )
                .useIngredient( Mozarella, 50.0 )
                .useIngredient( Pineaple,  50.0 )
                .useIngredient( Chicken,   50.0 )
                .useIngredient( Saliami,   50.0 )
                .useIngredient( Tomato,    50.0 );

        Mafia.defineRecipe( Large )
                .useIngredient( Mozarella, 75.0 )
                .useIngredient( Pineaple,  75.0 )
                .useIngredient( Chicken,   75.0 )
                .useIngredient( Saliami,   75.0 )
                .useIngredient( Tomato,    75.0 );

        Mafia.setPrice( Small,  BigDecimal.valueOf(  73.00 ) );
        Mafia.setPrice( Medium, BigDecimal.valueOf(  99.00 ) );
        Mafia.setPrice( Large,  BigDecimal.valueOf( 125.00 ) );

        // ----

        Georgia = new Product( UUID.randomUUID(), "Georgia", "http://pizzario.com/images/georgia.jpg" );

        Georgia.defineRecipe( Small )
                .useIngredient( Kebab,     25.0 )
                .useIngredient( Mozarella, 25.0 )
                .useIngredient( Eggplant,  25.0 )
                .useIngredient( Onion,     25.0 )
                .useIngredient( Parsley,   10.0 )
                .useIngredient( Tomato,    25.0 );

        Georgia.defineRecipe( Medium )
                .useIngredient( Kebab,     50.0 )
                .useIngredient( Mozarella, 50.0 )
                .useIngredient( Eggplant,  50.0 )
                .useIngredient( Onion,     50.0 )
                .useIngredient( Parsley,   20.0 )
                .useIngredient( Tomato,    50.0 );

        Georgia.defineRecipe( Large )
                .useIngredient( Kebab,     75.0 )
                .useIngredient( Mozarella, 75.0 )
                .useIngredient( Eggplant,  75.0 )
                .useIngredient( Onion,     75.0 )
                .useIngredient( Parsley,   75.0 )
                .useIngredient( Tomato,    75.0 );

        Georgia.setPrice( Small,  BigDecimal.valueOf(  81.00 ) );
        Georgia.setPrice( Medium, BigDecimal.valueOf( 103.00 ) );
        Georgia.setPrice( Large,  BigDecimal.valueOf( 137.00 ) );

        // ----

        Cossack = new Product( UUID.randomUUID(), "Cossack", "http://pizzario.com/images/cosscack.jpg" );

        Cossack.defineRecipe( Small )
                .useIngredient( Mozarella,       25.0 )
                .useIngredient( Bacon,           25.0 )
                .useIngredient( Ham,             25.0 )
                .useIngredient( Onion,           25.0 )
                .useIngredient( PickledCucumber, 25.0 )
                .useIngredient( Mushroom,        25.0 );

        Cossack.defineRecipe( Medium )
                .useIngredient( Mozarella,       50.0 )
                .useIngredient( Bacon,           50.0 )
                .useIngredient( Ham,             50.0 )
                .useIngredient( Onion,           50.0 )
                .useIngredient( PickledCucumber, 50.0 )
                .useIngredient( Mushroom,        50.0 );

        Cossack.defineRecipe( Large )
                .useIngredient( Mozarella,       75.0 )
                .useIngredient( Bacon,           75.0 )
                .useIngredient( Ham,             75.0 )
                .useIngredient( Onion,           75.0 )
                .useIngredient( PickledCucumber, 75.0 )
                .useIngredient( Mushroom,        75.0 );

        Cossack.setPrice( Small,  BigDecimal.valueOf(  83.00 ) );
        Cossack.setPrice( Medium, BigDecimal.valueOf( 107.00 ) );
        Cossack.setPrice( Large,  BigDecimal.valueOf( 143.00 ) );

        // ----

        m.getProducts().add( Mafia );
        m.getProducts().add( Georgia );
        m.getProducts().add( Cossack );
    }


    private static void generateCarts ( PizzarioModel m )
    {
        cart = new ShoppingCart( UUID.randomUUID() );
        cart.addItem( new ProductItem( UUID.randomUUID(), Mafia,   Large,  1 ) );
        cart.addItem( new ProductItem( UUID.randomUUID(), Georgia, Small,  2 ) );
        cart.addItem( new ProductItem( UUID.randomUUID(), Cossack, Medium, 1 ) );

        cart.checkout();

        m.getShoppingCarts().add( cart );
    }


    private static void generateOrders ( PizzarioModel m )
    {
        order = new Order(
                UUID.randomUUID(),
                cart,
                new Contact( "Sumskaya 1", "123-45-67" ),
                LocalDateTime.now()
        );

        order.setDiscount( new Discount( BigDecimal.valueOf( 20.00 ) ) );
        order.confirm();

        m.getOrders().add( order );
    }


    private static void generateAccounts ( PizzarioModel m )
    {
        OperatorAccount wasya = new OperatorAccount( UUID.randomUUID(), "Wasya Pupkin", "wasya.pupkin@pizzario.com", "12345");
        m.getAccounts().add( wasya );

        wasya.getOrders().add( order );
    }


    private static void generateCookingAssignments ( PizzarioModel m )
    {
        for ( CookingAssignment ca : order.generateCookingAssignments()  )
            m.getCookings().add( ca );
    }


    private static void generateDeliveries ( PizzarioModel m )
    {
        m.getDeliveries().add( order.generateDelivery( "Ivan Vodilkin" ) );
    }

    private static ProductSize Large, Medium, Small;
    private static Ingredient Mozarella, Pineaple, Chicken, Saliami, Tomato, Kebab, Eggplant;
    private static Ingredient Onion, Parsley, Bacon, Ham, PickledCucumber, Mushroom;
    private static Product Mafia, Georgia, Cossack;
    private static ShoppingCart cart;
    private static Order order;

}
