/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.testapp;

import pizzario.repository.IRepository;
import pizzario.repository.jpa.RepositoryFactory;
import pizzario.utils.DomainEntity;

import javax.persistence.EntityManager;
import java.util.List;

public class ModelRestorer {

    public ModelRestorer ( EntityManager entityManager )
    {
        this.entityManager = entityManager;
    }

    public PizzarioModel Restore ()
    {
        PizzarioModel m = new PizzarioModel();

        restoreCollection( RepositoryFactory.makeProductSizeRepository( entityManager ),       m.getProductSizes() );
        restoreCollection( RepositoryFactory.makeIngredientRepository( entityManager ),        m.getIngredients() );
        restoreCollection( RepositoryFactory.makeProductRepository( entityManager ),           m.getProducts() );
        restoreCollection( RepositoryFactory.makeShoppingCartRepository( entityManager ),      m.getShoppingCarts() );
        restoreCollection( RepositoryFactory.makeOrderRepository( entityManager ),             m.getOrders() );
        restoreCollection( RepositoryFactory.makeAccountRepository( entityManager ),           m.getAccounts() );
        restoreCollection( RepositoryFactory.makeCookingAssignmentRepository( entityManager ), m.getCookings() );
        restoreCollection( RepositoryFactory.makeDeliveryRepository( entityManager ),          m.getDeliveries() );

        return m;
    }


    private < TEntity extends DomainEntity>
        void restoreCollection ( IRepository< TEntity > repository, List< TEntity > target )
    {
        for ( TEntity obj : repository.loadAll() )
            target.add( obj );
    }

    private EntityManager entityManager;

}


