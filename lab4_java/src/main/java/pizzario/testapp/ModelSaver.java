/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.testapp;

import pizzario.repository.IRepository;
import pizzario.repository.jpa.RepositoryFactory;
import pizzario.utils.DomainEntity;

import javax.persistence.EntityManager;
import java.util.List;

public class ModelSaver {
    
    public ModelSaver ( EntityManager entityManager )
    {
        this.entityManager = entityManager;
    }


    public void Save ( PizzarioModel model )
    {
        saveCollection( RepositoryFactory.makeProductSizeRepository( entityManager ),        model.getProductSizes()  );
        saveCollection( RepositoryFactory.makeIngredientRepository( entityManager ),         model.getIngredients()   );
        saveCollection( RepositoryFactory.makeProductRepository( entityManager ),            model.getProducts()      );
        saveCollection( RepositoryFactory.makeShoppingCartRepository( entityManager ),       model.getShoppingCarts() );
        saveCollection( RepositoryFactory.makeOrderRepository( entityManager ),              model.getOrders()        );
        saveCollection( RepositoryFactory.makeAccountRepository( entityManager ),            model.getAccounts()      );
        saveCollection( RepositoryFactory.makeCookingAssignmentRepository( entityManager ),  model.getCookings()      );
        saveCollection( RepositoryFactory.makeDeliveryRepository( entityManager ),           model.getDeliveries()    );
    }


    private < TRepository extends IRepository< TEntity >,
              TEntity extends DomainEntity>
        void saveCollection ( TRepository repository, List< TEntity > collection )
    {
        for ( TEntity obj : collection )
            repository.add( obj );

        repository.commit();
    }

    private EntityManager entityManager;
}
