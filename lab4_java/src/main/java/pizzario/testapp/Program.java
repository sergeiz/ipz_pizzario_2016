/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.testapp;

import pizzario.repository.jpa.PizzarioJPAContext;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Program {

    public static void main ( String[] args )
    {
        try
        {
            PizzarioModel model1 = TestModelGenerator.generateTestData();
            try ( PizzarioJPAContext jpaContext = new PizzarioJPAContext( "PizzarioCreate" ) )
            {
                ModelSaver saver = new ModelSaver( jpaContext.getEntityManager() );
                saver.Save( model1 );
            }

            try ( PizzarioJPAContext jpaContext = new PizzarioJPAContext( "PizzarioRead" ) )
            {
                ModelRestorer restorer = new ModelRestorer( jpaContext.getEntityManager() );
                PizzarioModel model2 = restorer.Restore();

                compareModels( model1, model2 );
            }
        }
        catch ( Exception e )
        {
            System.out.println( e.getClass().getName() );
            System.out.println( e.getMessage() );
            e.printStackTrace( System.out );
        }
    }

    private static void compareModels ( PizzarioModel model1, PizzarioModel model2 )
    {
        ByteArrayOutputStream writer1 = new ByteArrayOutputStream();
        ModelReporter reportGenerator1 = new ModelReporter( new PrintStream( writer1 ) );
        reportGenerator1.generateReport( model1 );

        ByteArrayOutputStream writer2 = new ByteArrayOutputStream();
        ModelReporter reportGenerator2 = new ModelReporter( new PrintStream( writer2 ) );
        reportGenerator2.generateReport( model2 );

        String report1Content = writer1.toString();
        String report2Content = writer2.toString();

        System.out.println( report1Content );

        System.out.println("================================================================");
        System.out.println();

        System.out.println( report2Content );

        System.out.println("================================================================");
        System.out.println();

        System.out.println( report1Content.equals( report2Content ) ? "PASSED: Models match" : "FAILED: Models mismatch");
    }

}
