/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.testapp;

import pizzario.model.*;

import java.util.ArrayList;
import java.util.List;

public class PizzarioModel {


    public List< ProductSize > getProductSizes () { return productSizes; }

    public List< Ingredient > getIngredients () { return ingredients; }

    public List< Product > getProducts () { return products; }

    public List< ShoppingCart > getShoppingCarts () { return shoppingCarts; }

    public List< Order > getOrders () { return orders; }

    public List< Account > getAccounts () { return accounts; }

    public List< CookingAssignment > getCookings () { return cookings; }

    public List< Delivery > getDeliveries () { return deliveries; }


    private List< ProductSize > productSizes = new ArrayList<>();

    private List< Ingredient > ingredients = new ArrayList<>();

    private List< Product > products = new ArrayList<>();

    private List< ShoppingCart > shoppingCarts = new ArrayList<>();

    private List< Order > orders = new ArrayList<>();

    private List< Account > accounts = new ArrayList<>();

    private List< CookingAssignment > cookings = new ArrayList<>();

    private List< Delivery > deliveries = new ArrayList<>();

}
