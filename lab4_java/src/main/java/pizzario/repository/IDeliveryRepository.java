/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository;

import pizzario.model.Delivery;

public interface IDeliveryRepository extends IRepository< Delivery > {}
