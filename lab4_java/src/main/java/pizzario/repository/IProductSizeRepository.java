/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository;

import pizzario.model.ProductSize;

public interface IProductSizeRepository extends IRepository< ProductSize > {}
