/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import pizzario.model.ShoppingCart;
import pizzario.repository.IShoppingCartRepository;

import javax.persistence.EntityManager;

class ShoppingCartRepository
        extends BasicRepository< ShoppingCart >
        implements IShoppingCartRepository
{

    public ShoppingCartRepository ( EntityManager entityManager ) {
        super( entityManager, ShoppingCart.class );
    }

}
