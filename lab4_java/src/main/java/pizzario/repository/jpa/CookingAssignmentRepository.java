/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import pizzario.model.CookingAssignment;
import pizzario.repository.ICookingAssignmentRepository;

import javax.persistence.EntityManager;

class CookingAssignmentRepository
        extends BasicRepository< CookingAssignment >
        implements ICookingAssignmentRepository
{

    public CookingAssignmentRepository ( EntityManager entityManager ) {
        super( entityManager, CookingAssignment.class );
    }

}
