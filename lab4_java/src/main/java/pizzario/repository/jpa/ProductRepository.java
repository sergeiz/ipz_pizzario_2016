/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import pizzario.model.Product;
import pizzario.repository.IProductRepository;

import javax.persistence.EntityManager;

class ProductRepository
        extends BasicRepository< Product >
        implements IProductRepository
{

    public ProductRepository ( EntityManager entityManager ) {
        super( entityManager, Product.class );
    }

}
