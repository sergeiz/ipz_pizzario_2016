/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import pizzario.model.Delivery;
import pizzario.repository.IDeliveryRepository;

import javax.persistence.EntityManager;

class DeliveryRepository
        extends BasicRepository< Delivery >
        implements IDeliveryRepository
{

    public DeliveryRepository ( EntityManager entityManager ) {
        super( entityManager, Delivery.class );
    }

}
