/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import pizzario.repository.*;

import javax.persistence.EntityManager;

public class RepositoryFactory {

    public static IProductSizeRepository makeProductSizeRepository ( EntityManager em ) {
        return new ProductSizeRepository( em );
    }

    public static IIngredientRepository makeIngredientRepository ( EntityManager em ) {
        return new IngredientRepository( em );
    }

    public static IProductRepository makeProductRepository ( EntityManager em ) {
        return new ProductRepository( em );
    }

    public static IShoppingCartRepository makeShoppingCartRepository ( EntityManager em ) {
        return new ShoppingCartRepository( em );
    }

    public static IOrderRepository makeOrderRepository ( EntityManager em ) {
        return new OrderRepository( em );
    }

    public static IAccountRepository makeAccountRepository ( EntityManager em ) {
        return new AccountRepository( em );
    }

    public static ICookingAssignmentRepository makeCookingAssignmentRepository ( EntityManager em ) {
        return new CookingAssignmentRepository( em );
    }

    public static IDeliveryRepository makeDeliveryRepository ( EntityManager em ) {
        return new DeliveryRepository( em );
    }

}
