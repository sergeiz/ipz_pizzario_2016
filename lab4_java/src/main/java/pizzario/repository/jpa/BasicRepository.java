/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import pizzario.repository.IRepository;
import pizzario.utils.DomainEntity;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;

abstract class BasicRepository< T extends DomainEntity> implements IRepository< T > {

    protected BasicRepository ( EntityManager entityManager, Class theClass )
    {
        this.entityManager = entityManager;
        this.theClass = theClass;
    }

    protected EntityManager getEntityManager ()
    {
        return this.entityManager;
    }

    @Override
    public void add ( T obj )
    {
        this.entityManager.persist( obj );
    }

    @Override
    public void delete ( T obj )
    {
        this.entityManager.remove( obj );
    }

    @Override
    public void commit ()
    {
        this.entityManager.flush();
    }

    @Override
    public Iterable< T > loadAll ()
    {
        CriteriaQuery< T > criteria = entityManager.getCriteriaBuilder().createQuery( theClass );
        criteria.select( criteria.from( theClass ) );
        return entityManager.createQuery( criteria ).getResultList();
    }

    @Override
    public T load ( long id )
    {
        return entityManager.find( theClass, id );
    }

    @Override
    public long count ()
    {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery< Long > criteriaQuery = criteriaBuilder.createQuery( Long.class );
        Root< T > root = criteriaQuery.from( theClass );
        EntityType< T > T_ = root.getModel();

        criteriaQuery.select( criteriaBuilder.countDistinct( root ) );
        return entityManager.createQuery( criteriaQuery ).getSingleResult();
    }


    private EntityManager entityManager;
    private Class< T > theClass;

}
