/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import pizzario.model.Account;
import pizzario.repository.IAccountRepository;

import javax.persistence.EntityManager;

class AccountRepository
        extends BasicRepository< Account >
        implements IAccountRepository
{

    public AccountRepository ( EntityManager entityManager ) {
        super( entityManager, Account.class );
    }

}
