/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import pizzario.model.Order;
import pizzario.repository.IOrderRepository;

import javax.persistence.EntityManager;

class OrderRepository
        extends BasicRepository< Order >
        implements IOrderRepository
{

    public OrderRepository ( EntityManager entityManager ) {
        super( entityManager, Order.class );
    }

}
