/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository;

import pizzario.model.ShoppingCart;

public interface IShoppingCartRepository extends IRepository< ShoppingCart > {}
