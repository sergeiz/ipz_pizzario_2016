/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository;

import pizzario.model.CookingAssignment;

public interface ICookingAssignmentRepository extends IRepository< CookingAssignment > {}
