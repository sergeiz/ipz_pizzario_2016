/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository;

import pizzario.model.Order;

public interface IOrderRepository extends IRepository< Order > {}
