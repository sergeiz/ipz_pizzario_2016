/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository;

import pizzario.utils.DomainEntity;

public interface IRepository< T extends DomainEntity> {

    long count ();

    T load ( long id );

    Iterable< T > loadAll ();

    void add ( T t );

    void delete ( T t );

    void commit ();

}
