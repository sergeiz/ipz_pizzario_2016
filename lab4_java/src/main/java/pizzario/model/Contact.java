/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.NonEmptyString;
import pizzario.utils.Value;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Embeddable
@Access( AccessType.PROPERTY )
public class Contact extends Value< Contact > {

    protected Contact () {}

    public Contact ( String address, String phone ) {
        setAddress( address );
        setPhone( phone );
    }

    public String getAddress () {
        return this.address.getValue();
    }

    private void setAddress ( String address ) {
        this.address.setValue( address );
    }

    public String getPhone () {
        return this.phone.getValue();
    }

    private void setPhone ( String phone ) {
        this.phone.setValue( phone );
    }

    @Override
    @Transient
    protected List< Object > getAttributesToIncludeInEqualityCheck () {
        return Arrays.asList( getAddress(), getPhone() );
    }

    private final NonEmptyString address = new NonEmptyString( "address" );

    private final NonEmptyString phone   = new NonEmptyString( "phone" );
}
