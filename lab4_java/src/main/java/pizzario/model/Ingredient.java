/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.DomainEntity;
import pizzario.utils.NonEmptyString;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Access( AccessType.PROPERTY )
public class Ingredient extends DomainEntity {

    protected Ingredient () {}

    public Ingredient ( UUID domainId, String name ) {
        super( domainId );
        setName( name );
    }

    public String getName () {
        return this.name.getValue();
    }

    public void setName ( String name ) {
        this.name.setValue( name );
    }

    @Override
    public String toString () {
        return String.format(
                "ID = %s\nName = %s",
                getDomainId(),
                getName()
        );
    }

    private NonEmptyString name = new NonEmptyString( "name" );

}
