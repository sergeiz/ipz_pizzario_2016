/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.DomainEntity;
import pizzario.utils.RequiredProperty;

import javax.persistence.*;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity( name = "CustomerOrder" )
public class Order extends DomainEntity {

    protected Order () {}

    public Order ( UUID domainId, ShoppingCart cart, Contact contact, LocalDateTime time ) {
        super( domainId );

        if ( cart.isModifiable() )
            throw new IllegalStateException( "Order: initializing with a modifiable cart" );

        this.items = new ArrayList<>( cart.getItems() );

        setContact( contact );
        setDiscount( new Discount() );
        setBasicCost( cart.cost() );

        this.placementTime = time;
        this.status = OrderStatus.Placed;

        this.unfinishedCookingsCount = 0;
        for ( ProductItem item : items )
            this.unfinishedCookingsCount += item.getQuantity();
    }

    public List< ProductItem > getItem () { return this.items; }

    public BigDecimal getBasicCost () { return this.basicCost; }

    private void setBasicCost ( BigDecimal basicCost ) { this.basicCost = basicCost; }

    @Access( AccessType.PROPERTY )
    @Embedded
    public Discount getDiscount () { return this.discount.getValue(); }

    public void setDiscount ( Discount discount ) { this.discount.setValue( discount );  }

    public BigDecimal getTotalCost () { return this.getDiscount().getDiscountedPrice( getBasicCost() ); }

    public OrderStatus getStatus () { return this.status; }

    public LocalDateTime getPlacementTime () { return this.placementTime; }

    @Access( AccessType.PROPERTY )
    @Embedded
    public Contact getContact () { return this.contact.getValue(); }

    private void setContact ( Contact contact ) { this.contact.setValue( contact ); }

    public int getUnfinishedCookingsCount () { return this.unfinishedCookingsCount; }


    @Override
    public String toString ()
    {
        StringBuilder itemsAsString = new StringBuilder();
        for ( ProductItem item : items ) {
            itemsAsString.append( item );
            itemsAsString.append( "\n\n" );
        }

        return String.format(
                "ID = %s\nContent:\n%sBasic cost = %s\nDiscount = %s\nTotal cost = %s\nStatus = %s\nPlaced = %s\nUnfinished = %d",
                getDomainId(),
                itemsAsString.toString(),
                NumberFormat.getCurrencyInstance().format( getBasicCost() ),
                NumberFormat.getPercentInstance().format( getDiscount().getPercent() ),
                NumberFormat.getCurrencyInstance().format( getTotalCost() ),
                getStatus(),
                getPlacementTime(),
                getUnfinishedCookingsCount()
        );
    }

    public void confirm ()
    {
        if ( this.status != OrderStatus.Placed )
            throw new IllegalStateException( "Order.confirm - can only run in Placed state");

        this.status = OrderStatus.Confirmed;
    }

    public void cancel ()
    {
        if ( this.status != OrderStatus.Placed )
            throw new IllegalStateException( "Order.cancel - can only run in Placed state");

        this.status = OrderStatus.Cancelled;
    }

    public void cookingAssignmentCompleted ()
    {
        if ( this.status == OrderStatus.Confirmed )
        {
            -- unfinishedCookingsCount;
            if ( unfinishedCookingsCount == 0 )
                this.status = OrderStatus.Delivering;
        }
        else
            throw new IllegalStateException( "Order.cookingAssignmentCompleted - can only happen in Confirmed state" );

    }

    public void deliveryCompleted ()
    {
        if ( this.status != OrderStatus.Delivering )
            throw new IllegalStateException( "Order.deliveryCompleted - can only happen in Delivering state");

        this.status = OrderStatus.Completed;
    }

    public List< CookingAssignment > generateCookingAssignments ()
    {
        List< CookingAssignment > cookingAssignments = new ArrayList<>();

        for ( ProductItem item : items )
        {
            for ( int i = 0; i < item.getQuantity(); i++ )
                cookingAssignments.add(
                        new CookingAssignment(
                                UUID.randomUUID(),
                                this,
                                item.getProduct(),
                                item.getSize()
                        )
                );
        }

        return cookingAssignments;
    }
    public Delivery generateDelivery ( String driverName )
    {
        return new Delivery( UUID.randomUUID(), driverName, this );
    }

    @OneToMany
    private List< ProductItem > items;

    @Transient
    private RequiredProperty< Contact > contact = new RequiredProperty<>( "contact" );

    @Transient
    private RequiredProperty< Discount > discount = new RequiredProperty<>( "discount" );

    private LocalDateTime placementTime;

    private BigDecimal basicCost;

    private OrderStatus status;

    private int unfinishedCookingsCount;

}
