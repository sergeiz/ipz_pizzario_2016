/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.DomainEntity;
import pizzario.utils.NonEmptyString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.*;

@Entity
@Access( AccessType.PROPERTY )
public class Product extends DomainEntity {

    protected Product () {}

    public Product ( UUID domainId, String name, String imageUrl ) {
        super( domainId );
        setName( name );
        setImageUrl( imageUrl );
    }

    public String getName () {
        return this.name.getValue();
    }

    public void setName ( String name ) {
        this.name.setValue( name );
    }

    public String getImageUrl () {
        return this.imageUrl;
    }

    public void setImageUrl ( String imageUrl ) {
        this.imageUrl = imageUrl;
    }

    public List< Recipe > getRecipes () { return this.recipes; }


    @Override
    public String toString () {

        StringBuilder recipesAsString = new StringBuilder();
        for ( Recipe r : recipes )
            recipesAsString.append( r );

        StringBuilder pricesAsString = new StringBuilder();
        for ( Map.Entry< ProductSize, BigDecimal > ppa : prices.entrySet() )
            pricesAsString.append(
                    String.format(
                            "%s = %s  ",
                            ppa.getKey().getName(),
                            NumberFormat.getCurrencyInstance().format( ppa.getValue() )
                    )
            );

        return String.format(
                "ID = %s\nName = %s\nImageUrl = %s\nRecipes:\n%sPrices: %s",
                getDomainId(),
                getName(),
                getImageUrl(),
                recipesAsString,
                pricesAsString
        );
    }

    public BigDecimal getPrice ( ProductSize size )
    {
        BigDecimal price = prices.get( size );
        if ( price != null )
            return price;

        throw new IllegalStateException( "Price for size " + size.getName() + " was not previously defined" );
    }

    public void setPrice ( ProductSize size, BigDecimal price )
    {
        prices.put( size, price );
    }

    public void removePrice ( ProductSize size )
    {
        BigDecimal price = prices.get( size );
        if ( price != null )
            prices.remove( size );

        else
            throw new IllegalStateException( "No price defined for size " + size.getName() );
    }


    public Recipe getRecipe ( ProductSize size )
    {
        Recipe recipe = findRecipe( size );
        if ( recipe != null )
            return recipe;

        throw new IllegalStateException( "Recipe for size " + size.getName() + " was not previously defined" );
    }

    public Recipe defineRecipe ( ProductSize size )
    {
        Recipe recipe = findRecipe( size );
        if ( recipe != null )
            throw new IllegalStateException( "Recipe for size " + size.getName() + " was already defined" );

        recipe = new Recipe( UUID.randomUUID(), this, size );
        recipes.add( recipe );
        return recipe;
    }

    public void removeRecipe ( ProductSize size )
    {
        Recipe recipe = findRecipe( size );
        if ( recipe == null )
            throw new IllegalStateException( "Recipe for size " + size.getName() + " was not defined" );

        recipes.remove( recipe );
    }

    private Recipe findRecipe ( ProductSize size ) {
        return recipes.stream()
                .filter( r -> r.getSize() == size )
                .findFirst().orElse( null )
                ;
    }

    private NonEmptyString name = new NonEmptyString( "name" );

    private String imageUrl;

    @OneToMany( cascade = CascadeType.ALL )
    @Access( AccessType.FIELD )
    private List< Recipe > recipes = new ArrayList<>();

    @ElementCollection
    @Access( AccessType.FIELD )
    private Map< ProductSize, BigDecimal > prices = new HashMap<>();
}
