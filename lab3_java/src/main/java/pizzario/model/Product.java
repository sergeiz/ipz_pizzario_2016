/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.DomainEntity;
import pizzario.utils.NonEmptyString;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Product extends DomainEntity {

    public Product ( UUID domainId, String name, String imageUrl ) {
        super( domainId );
        setName( name );
        setImageUrl( imageUrl );
    }

    public String getName () {
        return this.name.getValue();
    }

    public void setName ( String name ) {
        this.name.setValue( name );
    }

    public String getImageUrl () {
        return this.imageUrl;
    }

    public void setImageUrl ( String imageUrl ) {
        this.imageUrl = imageUrl;
    }

    public List< Recipe > getRecipes () { return this.recipes; }

    public List< ProductPriceAssignment > getPrices () { return this.getPrices(); }

    @Override
    public String toString () {

        StringBuilder recipesAsString = new StringBuilder();
        for ( Recipe r : recipes )
            recipesAsString.append( r );

        StringBuilder pricesAsString = new StringBuilder();
        for ( ProductPriceAssignment ppa : prices )
            pricesAsString.append(
                    String.format(
                            "%s = %s  ",
                            ppa.getSize().getName(),
                            NumberFormat.getCurrencyInstance().format( ppa.getPrice() )
                    )
            );

        return String.format(
                "ID = %s\nName = %s\nImageUrl = %s\nRecipes:\n%sPrices: %s",
                getDomainId(),
                getName(),
                getImageUrl(),
                recipesAsString,
                pricesAsString
        );
    }

    public BigDecimal getPrice ( ProductSize size )
    {
        ProductPriceAssignment assignment = findPriceAssignment( size );
        if ( assignment != null )
            return assignment.getPrice();

        throw new IllegalStateException( "Price for size " + size.getName() + " was not previously defined" );
    }

    public void setPrice ( ProductSize size, BigDecimal price )
    {
        ProductPriceAssignment assignment = findPriceAssignment( size );
        if ( assignment != null )
            assignment.setPrice( price );

        else
            prices.add( new ProductPriceAssignment( this, size, price ) );
    }

    public void removePrice ( ProductSize size )
    {
        ProductPriceAssignment assignment = findPriceAssignment( size );
        if ( assignment != null )
            prices.remove( assignment );

        else
            throw new IllegalStateException( "No price defined for size " + size.getName() );
    }

    private ProductPriceAssignment findPriceAssignment ( ProductSize size ) {
        return prices.stream()
                .filter( p -> p.getSize() == size )
                .findFirst().orElse( null )
                ;
    }

    public Recipe getRecipe ( ProductSize size )
    {
        Recipe recipe = findRecipe( size );
        if ( recipe != null )
            return recipe;

        throw new IllegalStateException( "Recipe for size " + size.getName() + " was not previously defined" );
    }

    public Recipe defineRecipe ( ProductSize size )
    {
        Recipe recipe = findRecipe( size );
        if ( recipe != null )
            throw new IllegalStateException( "Recipe for size " + size.getName() + " was already defined" );

        recipe = new Recipe( UUID.randomUUID(), this, size );
        recipes.add( recipe );
        return recipe;
    }

    public void removeRecipe ( ProductSize size )
    {
        Recipe recipe = findRecipe( size );
        if ( recipe == null )
            throw new IllegalStateException( "Recipe for size " + size.getName() + " was not defined" );

        recipes.remove( recipe );
    }

    private Recipe findRecipe ( ProductSize size ) {
        return recipes.stream()
                .filter( r -> r.getSize() == size )
                .findFirst().orElse( null )
                ;
    }

    private NonEmptyString name = new NonEmptyString( "name" );
    private String imageUrl;

    private List< Recipe > recipes = new ArrayList<>();
    private List< ProductPriceAssignment > prices = new ArrayList<>();
}
