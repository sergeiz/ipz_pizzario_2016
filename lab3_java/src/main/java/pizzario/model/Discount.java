/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.RangeProperty;
import pizzario.utils.Value;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Discount extends Value< Discount > {

    public Discount () {
        setPercent( BigDecimal.ZERO );
    }

    public Discount ( BigDecimal discount ) {
        setPercent( discount );
    }

    public BigDecimal getPercent () {
        return percent.getValue();
    }

    private void setPercent ( BigDecimal percent ) {
        this.percent.setValue( percent );
        this.derivedCoeffecient = HUNDRED.subtract( percent ).divide( HUNDRED );
    }

    @Override
    public String toString () {
        return this.getPercent().toString();
    }

    @Override
    protected List< Object > getAttributesToIncludeInEqualityCheck () {
        return Arrays.asList( getPercent() );
    }

    public BigDecimal getDiscountedPrice ( BigDecimal price ) {
        return price.multiply( derivedCoeffecient );
    }

    private static final BigDecimal HUNDRED = BigDecimal.valueOf( 100.00 );

    private RangeProperty< BigDecimal > percent =
            new RangeProperty<>(
                    "value", BigDecimal.ZERO, true, BigDecimal.valueOf( 100 ), true
            );

    private BigDecimal derivedCoeffecient;
}
