/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.RangeProperty;
import pizzario.utils.RequiredProperty;

public class IngredientRecipeInclusion {

    public IngredientRecipeInclusion ( Recipe recipe, Ingredient ingredient, double weight ) {
        setRecipe( recipe );
        setIngredient( ingredient );
        setWeight( weight );
    }

    public Recipe getRecipe () { return this.recipe.getValue(); }

    private void setRecipe ( Recipe recipe ) {
        this.recipe.setValue( recipe );
    }

    public Ingredient getIngredient () { return this.ingredient.getValue(); }

    private void setIngredient ( Ingredient ingredient ) {
        this.ingredient.setValue( ingredient );
    }

    public double getWeight () { return this.weight.getValue(); }

    public void setWeight ( double weight ) { this.weight.setValue( weight ); }


    private RequiredProperty< Recipe > recipe = new RequiredProperty<>( "recipe" );

    private RequiredProperty< Ingredient > ingredient = new RequiredProperty<>( "ingredient" );

    private RangeProperty< Double > weight =
            new RangeProperty< Double >(
                    "weight", Double.valueOf( 0.0 ), false, Double.MAX_VALUE, false
            );

}
