/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.RangeProperty;
import pizzario.utils.RequiredProperty;

import java.math.BigDecimal;

public class ProductPriceAssignment {

    public ProductPriceAssignment ( Product product, ProductSize size, BigDecimal price ) {
        setProduct( product );
        setSize( size );
        setPrice( price );
    }

    public Product getProduct () {
        return this.product.getValue();
    }

    private void setProduct ( Product product ) {
        this.product.setValue( product );
    }

    public ProductSize getSize () {
        return this.size.getValue();
    }

    private void setSize ( ProductSize size ) {
        this.size.setValue( size );
    }

    public BigDecimal getPrice () {
        return price.getValue();
    }

    public void setPrice ( BigDecimal price ) {
        this.price.setValue( price );
    }

    private RequiredProperty< Product > product = new RequiredProperty<>( "product" );

    private RequiredProperty< ProductSize > size = new RequiredProperty<>( "size" );

    private RangeProperty< BigDecimal > price =
            new RangeProperty<>(
                    "price", BigDecimal.ZERO, true, BigDecimal.valueOf( Long.MAX_VALUE ), false
            );

}
