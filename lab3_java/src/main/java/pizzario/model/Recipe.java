/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.DomainEntity;
import pizzario.utils.RequiredProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Recipe extends DomainEntity {

    public Recipe ( UUID domainId, Product product, ProductSize size ) {
        super( domainId );
        setProduct( product );
        setSize( size );
    }

    public Product getProduct () {
        return this.product.getValue();
    }

    private void setProduct ( Product product ) {
        this.product.setValue( product );
    }

    public ProductSize getSize () {
        return this.size.getValue();
    }

    private void setSize ( ProductSize size ) {
        this.size.setValue( size );
    }

    public double getTotalIngredientsWeight () {
        return ingredients.stream()
                .mapToDouble( inclusion -> inclusion.getWeight() )
                .sum()
                ;
    }

    public boolean usesIngredient ( Ingredient i )
    {
        IngredientRecipeInclusion inclusion = findInclusion( i );
        return inclusion != null;
    }

    public double getIngredientWeight ( Ingredient i )
    {
        IngredientRecipeInclusion inclusion = findInclusion( i );
        if ( inclusion != null )
            return inclusion.getWeight();

        throw new IllegalStateException( "Ingredient " + i.getName() + " not included to the recipe" );
    }

    public Recipe useIngredient ( Ingredient i, double weight )
    {
        IngredientRecipeInclusion inclusion = findInclusion( i );
        if ( inclusion != null )
            inclusion.setWeight( weight );

        else
            ingredients.add( new IngredientRecipeInclusion( this, i, weight ) );

        return this;
    }

    private IngredientRecipeInclusion findInclusion ( Ingredient ingredient ) {
        return ingredients.stream()
                .filter( incl -> incl.getIngredient() == ingredient )
                .findFirst().orElse( null );
    }

    @Override
    public String toString ()
    {
        StringBuilder ingredientsAsString = new StringBuilder();
        for ( IngredientRecipeInclusion i : ingredients )
            ingredientsAsString.append(
                    String.format( "\t\t%s (%fg)\n", i.getIngredient().getName(), i.getWeight() )
            );

        return String.format(
                "\tId = %s\n\tProduct = %s\n\tSize = %s\n\tIngredients:\n%s",
                getDomainId(),
                getProduct().getName(),
                getSize().getName(),
                ingredientsAsString
        );
    }

    private RequiredProperty< Product > product = new RequiredProperty<>( "product" );

    private RequiredProperty< ProductSize > size = new RequiredProperty<>( "size" );

    private List< IngredientRecipeInclusion > ingredients = new ArrayList<>();

}
