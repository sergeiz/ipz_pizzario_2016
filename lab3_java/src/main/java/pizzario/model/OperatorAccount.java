/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class OperatorAccount extends Account {

    public OperatorAccount ( UUID domainId, String name, String email, String password ) {
        super( domainId, name, email, password );
    }

    public List< Order > getOrders () { return this.orders; }

    @Override
    public String toString ()
    {
        StringBuilder b = new StringBuilder();
        b.append( super.toString() );
        b.append( "\n" );

        b.append( "Orders:\n" );
        for ( Order o : orders ) {
            b.append( String.format( "\t%s", o.getDomainId() ) );
        }

        return b.toString();
    }

    public void trackOrder ( Order order )
    {
        if ( order == null )
            throw new IllegalStateException( "order" );

        orders.add( order );
    }


    private List< Order > orders = new ArrayList<>();

}
