/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.DomainEntity;
import pizzario.utils.NonEmptyString;
import pizzario.utils.RegexString;

import java.util.UUID;

public class Account extends DomainEntity {

    public Account ( UUID domainId, String name, String email, String password  ) {
        super( domainId );
        setName( name );
        setEmail( email );
        setPassword( password );
    }

    public String getName () {
        return this.name.getValue();
    }

    public void setName ( String name ) {
        this.name.setValue( name );
    }

    public String getEmail () {
        return this.email.getValue();
    }

    public void setEmail ( String email ) {
        this.email.setValue( email );;
    }

    public String getPassword () {
        return this.password.getValue();
    }

    public void setPassword ( String password ) {
        this.password.setValue( password );
    }

    @Override
    public String toString () {
        return String.format(
                "ID = %s\nName = %s\nEmail = %s\nPassword = %s",
                getDomainId(),
                getName(),
                getEmail(),
                getPassword()
        );
    }

    public boolean CheckPassword ( String password )
    {
        if ( password == null )
            throw new IllegalArgumentException( "password" );

        return this.getPassword() == password;
    }


    private NonEmptyString name = new NonEmptyString( "name" );
    private RegexString email = new RegexString( "email", "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$" );
    private NonEmptyString password = new NonEmptyString( "password" );

}
