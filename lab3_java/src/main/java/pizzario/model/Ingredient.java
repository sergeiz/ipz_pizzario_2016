/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.DomainEntity;
import pizzario.utils.NonEmptyString;

import java.util.UUID;

public class Ingredient extends DomainEntity  {

    public Ingredient ( UUID domainId, String name ) {
        super( domainId );
        setName( name );
    }

    public String getName () {
        return this.name.getValue();
    }

    public void setName ( String name ) {
        this.name.setValue( name );
    }

    @Override
    public String toString () {
        return String.format(
                "ID = %s\nName = %s",
                getDomainId(),
                getName()
        );
    }

    private NonEmptyString name = new NonEmptyString( "name" );

}
