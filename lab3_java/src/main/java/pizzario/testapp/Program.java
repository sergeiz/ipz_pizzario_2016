/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.testapp;

public class Program {

    public static void main ( String[] args )
    {
        try
        {
            PizzarioModel model = TestModelGenerator.generateTestData();

            ModelReporter reporter = new ModelReporter( System.out );
            reporter.generateReport( model );
        }
        catch ( Exception e )
        {
            System.out.println( e.getClass().getName() );
            System.out.println( e.getMessage() );
            e.printStackTrace( System.out );
        }
    }

}
