/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.testapp;

import java.io.PrintStream;
import java.util.List;

public class ModelReporter {

    public ModelReporter ( PrintStream output )
    {
        this.output = output;
    }

    public void generateReport ( PizzarioModel model )
    {
        reportCollection( "Sizes",       model.getProductSizes()  );
        reportCollection( "Ingredients", model.getIngredients()   );
        reportCollection( "Products",    model.getProducts()      );
        reportCollection( "Carts",       model.getShoppingCarts() );
        reportCollection( "Orders",      model.getOrders()        );
        reportCollection( "Accounts",    model.getAccounts()      );
        reportCollection( "Cookings",    model.getCookings()      );
        reportCollection( "Deliveries",  model.getDeliveries()    );
    }

    private < T > void reportCollection ( String title, List< T > items )
    {
        output.format("==== %s ====\n\n", title );

        for ( T item : items )
        {
            output.print( item );
            output.print( "\n\n" );
        }

        output.println();
    }

    private PrintStream output;

}
