/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.utils;

import java.util.IllegalFormatException;
import java.util.regex.Pattern;

public class RegexString extends NonEmptyString {

    public RegexString ( String paramName, String pattern ) {
        super( paramName );

        if ( pattern == null )
            throw new IllegalArgumentException( "pattern" );

        this.pattern = pattern;
    }

    @Override
    protected void checkValue ( String value ) {

        super.checkValue( value );

        if ( ! Pattern.matches( pattern, value ) )
            throw new IllegalArgumentException( getParamName() );
    }

    private String pattern;
}
