<%--(C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine --%>
<%--@elvariable id="confirmedOrder" type="pizzario.dto.OrderDto"--%>
<tr>
    <td class="table-column-productname">
        ${confirmedOrder.domainId}
    </td>
    <td>
        ${confirmedOrder.placementTime}
    </td>
    <td>
        ${confirmedOrder.totalCost} &#x20b4;
    </td>
    <td>
        ${confirmedOrder.finishedCookingsCount} / ${confirmedOrder.totalCookingsCount}
    </td>
</tr>