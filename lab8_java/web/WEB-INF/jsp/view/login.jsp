<%--(C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine --%>

<spring:message code="title.login" var="loginTitle" />
<template:main htmlTitle="${loginTitle}" >

    <jsp:body>


        <div class="container content-wrap">

            <div class="row">

                <div class="col-xs-6 col-xs-offset-3" >

                    <c:if test="${param.containsKey('loginFailed')}">
                        <div class="alert alert-danger fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <spring:message code="error.login.failed" />
                        </div>
                    </c:if>
                    <c:if test="${param.containsKey('loggedOut')}">
                        <div class="alert alert-info fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <spring:message code="login.loggedOut" />
                        </div>
                    </c:if>

                    <p><spring:message code="login.instruction" /></p>

                    <form:form method="post" modelAttribute="loginForm" autocomplete="off" class="form-horizontal" >
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="inputEmail"><spring:message code="login.label.email" /></label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                    <input class="form-control" autocomplete="off" type="text" id="inputEmail" name="email" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="inputPassword"><spring:message code="login.label.password" /></label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input class="form-control" autocomplete="off" type="password" id="inputPassword" name="password" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-2">
                                <button type="submit" class="btn btn-success">
                                    <span class="glyphicon glyphicon-log-in"></span> <spring:message code="login.submit" />
                                </button>
                            </div>
                        </div>
                    </form:form>

                </div>

            </div>

        </div>

    </jsp:body>

</template:main>