<%--(C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine --%>

<%--@elvariable id="orderId" type="java.lang.String"--%>
<%--@elvariable id="status" type="java.lang.String"--%>
<%--@elvariable id="placed" type="java.lang.String"--%>
<%--@elvariable id="name" type="java.lang.String"--%>
<%--@elvariable id="address" type="java.lang.String"--%>
<%--@elvariable id="phone" type="java.lang.String"--%>
<%--@elvariable id="email" type="java.lang.String"--%>
<%--@elvariable id="discount" type="java.lang.String"--%>
<%--@elvariable id="cost" type="java.math.BigDecimal"--%>
<%--@elvariable id="comment" type="java.lang.String"--%>
<spring:message code="title.order" var="orderTitle" />
<template:main htmlTitle="${orderTitle}" >

    <jsp:body>

        <div class="container content-wrap">

            <div class="row">

                <div class="col-xs-10 col-xs-offset-1" >

                    <h2><spring:message code="order.head" /></h2>

                    <div>
                        <p><spring:message code="order.label.orderId" />: ${orderId}</p>
                        <p><spring:message code="order.label.status" />: ${status}</p>
                        <p><spring:message code="order.label.placed" />: ${placed}</p>
                        <p><spring:message code="order.label.name" />: ${name}</p>
                        <p><spring:message code="order.label.address" />: ${address}</p>
                        <p><spring:message code="order.label.phone" />: ${phone}</p>
                        <p><spring:message code="order.label.email" />: ${email}</p>
                        <p><spring:message code="order.label.discount" />: ${discount}%</p>
                        <p><spring:message code="order.label.cost" />: ${cost}&#x20b4;</p>
                        <p><spring:message code="order.label.comment" />: ${comment}</p>
                    </div>

                </div>

        </div>

    </jsp:body>

</template:main>