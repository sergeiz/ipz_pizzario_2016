<%--(C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine --%>
<spring:message code="title.operator" var="operatorTitle" />
<%--@elvariable id="unconfirmedOrders" type="java.util.List<pizzario.dto.OrderDto>"--%>
<%--@elvariable id="confirmedOrders" type="java.util.List<pizzario.dto.OrderDto>"--%>
<%--@elvariable id="ready4DeliveryOrders" type="java.util.List<pizzario.dto.OrderDto>"--%>
<template:main htmlTitle="${operatorTitle}" >

    <jsp:attribute name="headContent">
          <script src="<c:url value="/resources/js/operator.js" />"></script>
    </jsp:attribute>

    <jsp:body>

        <div class="container content-wrap">

            <div class="row">
                <h2>Unconfirmed Orders</h2>
            </div>

            <div class="row">

               <table class="table table-responsive table-striped">
                    <thead>
                    <tr>
                        <th>Order #</th>
                        <th>Placed</th>
                        <th>Basic Cost</th>
                        <th>Total Cost</th>
                        <th>Discount</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${unconfirmedOrders}" var="order" >
                        <tr>
                            <td class="table-column-productname">
                                ${order.domainId}
                            </td>
                            <td>
                                ${order.placementTime}
                            </td>
                            <td>
                                ${order.basicCost} &#x20b4;
                            </td>
                            <td>
                                ${order.totalCost} &#x20b4;
                            </td>
                            <td>
                                ${order.discountPercentage}%
                            </td>
                            <td>
                                <button class="btn btn-success" onclick="confirmOrder('${order.domainId}',this)" >Confirm</button>
                            </td>
                            <td>
                                <button class="btn btn-danger" onclick="cancelOrder('${order.domainId}',this)" >Cancel</button>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>

                <p><span id="unconfirmed-count">${unconfirmedOrders.size()}</span> order(s) totally</p>

            </div>

            <div class="row">
                <h2>Confirmed Orders</h2>
            </div>

            <div class="row">

                <table class="table table-responsive table-striped" id="tblConfirmed">
                    <thead>
                    <tr>
                        <th>Order #</th>
                        <th>Placed</th>
                        <th>Cost</th>
                        <th>Cooking</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${confirmedOrders}" var="order" >
                        <c:set var="confirmedOrder" value="${order}" scope="request"/>
                        <jsp:include page="operator_confirmedrow.jsp" />
                    </c:forEach>
                    </tbody>
                </table>

                <p><span id="confirmed-count">${confirmedOrders.size()}</span> order(s) totally</p>

            </div>

            <div class="row">
                <h2>Orders Ready for Delivery</h2>
            </div>

            <div class="row">

                <table class="table table-responsive table-striped">
                    <thead>
                    <tr>
                        <th>Order #</th>
                        <th>Placed</th>
                        <th>Cost</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${ready4DeliveryOrders}" var="order" >
                        <tr>
                            <td class="table-column-productname">
                                    ${order.domainId}
                            </td>
                            <td>
                                    ${order.placementTime}
                            </td>
                            <td>
                                    ${order.totalCost} &#x20b4;
                            </td>
                            <td>
                                <button class="btn btn-success">Delivered</button>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>

                <p><span id="ready-count">${ready4DeliveryOrders.size()}</span> order(s) totally</p>

            </div>

            <div class="row">

                <div class="col-xs-10 col-xs-offset-1" >

                    <form:form method="post" action="/logout" autocomplete="off" class="form-horizontal" >
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">
                                <span class="glyphicon glyphicon-log-out"></span> <spring:message code="logout.submit" />
                            </button>
                        </div>
                    </form:form>

                </div>

            </div>

        </div>

    </jsp:body>

</template:main>