/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

function incrementCounterSpan ( selectorString ) {
    var counter = $( selectorString );
    counter.text( parseInt( counter.text() ) + 1 );
}

function decrementCounterSpan ( selectorString ) {
    var counter = $( selectorString );
    counter.text( parseInt( counter.text() ) - 1 );
}

function cancelOrder ( orderId, element ) {

    $.post(
        "/operator/cancel",
        {
            orderId: orderId
        },
        function () {
            $( element ).closest( "tr" ).remove();

            decrementCounterSpan( '#unconfirmed-count' );

            $.toast( 'Order cancelled', 3000 );
        }
    ).fail( function() {
        alert( "Failed to cancel order" );
    });
}

function confirmOrder ( orderId, element ) {

    $.post(
        "/operator/confirm",
        {
            orderId: orderId
        },
        function () {

            var row = $( element ).closest( "tr" );
            row.remove();

            $.get(
                "/operator/confirmedrow",
                {
                    orderId: orderId
                },
                function( rowData )
                {
                    $( '#tblConfirmed' ).find( 'tbody' ).append( rowData );
                }
            );

            decrementCounterSpan( '#unconfirmed-count' );
            incrementCounterSpan( '#confirmed-count' );

            $.toast( 'Order confirmed', 3000 );
        }
    ).fail( function() {
        alert( "Failed to confirm order" );
    });
}

