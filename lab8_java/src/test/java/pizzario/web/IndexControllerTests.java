/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.web;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pizzario.web.constants.SessionKeys;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith( MockitoJUnitRunner.class )
public class IndexControllerTests
{
    @Before
    public void setup ()
    {
        this.mockMvc = MockMvcBuilders.standaloneSetup( theController ).build();
    }

    @Test
    public void test_Index () throws Exception
    {
        this.mockMvc.perform(
                get( "/" )
        )
                .andExpect( redirectedUrl( "/menu/" ) )
                ;
    }

    @Test
    public void test_SetLocale_NoLanguage () throws Exception
    {
        this.mockMvc.perform(
                post( "/setlocale/" )
        )
                .andExpect( status().is4xxClientError() )
                ;
    }

    @Test
    public void test_SetLocale_Ru () throws Exception
    {
        this.mockMvc.perform(
                post( "/setlocale/" )
                .param( "language", "ru" )
        )
                .andExpect( status().isOk() )
                .andExpect( request().sessionAttribute(
                        SessionKeys.LOCALE,
                        is( allOf(
                                hasProperty( "language", is( "ru" ) ),
                                hasProperty( "country", is( "RU" ) )
                        ) )
                ))
                ;
    }

    @Test
    public void test_SetLocale_Uk () throws Exception
    {
        this.mockMvc.perform(
                post( "/setlocale/" )
                        .param( "language", "uk" )
        )
                .andExpect( status().isOk() )
                .andExpect( request().sessionAttribute(
                        SessionKeys.LOCALE,
                        is( allOf(
                                hasProperty( "language", is( "uk" ) ),
                                hasProperty( "country", is( "UA" ) )
                        ) )
                ))
        ;
    }

    @Test
    public void test_SetLocale_En () throws Exception
    {
        this.mockMvc.perform(
                post( "/setlocale/" )
                        .param( "language", "en" )
        )
                .andExpect( status().isOk() )
                .andExpect( request().sessionAttribute(
                        SessionKeys.LOCALE,
                        is( allOf(
                                hasProperty( "language", is( "en" ) ),
                                hasProperty( "country", is( "" ) )
                        ) )
                ))
        ;
    }

    @Test
    public void test_SetLocale_Other_GivesEn () throws Exception
    {
        this.mockMvc.perform(
                post( "/setlocale/" )
                        .param( "language", "hr" )
        )
                .andExpect( status().isOk() )
                .andExpect( request().sessionAttribute(
                        SessionKeys.LOCALE,
                        is( allOf(
                                hasProperty( "language", is( "en" ) ),
                                hasProperty( "country", is( "" ) )
                        ) )
                ))
        ;
    }

    @InjectMocks
    private IndexController theController;

    private MockMvc mockMvc;
}
