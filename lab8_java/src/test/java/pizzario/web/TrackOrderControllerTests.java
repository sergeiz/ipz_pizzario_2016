/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.web;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pizzario.dto.OrderDto;
import pizzario.service.IOrderService;
import pizzario.web.constants.AttributeNames;
import pizzario.web.constants.ErrorKeys;
import pizzario.web.constants.SessionKeys;
import pizzario.web.constants.ViewNames;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith( MockitoJUnitRunner.class )
public class TrackOrderControllerTests
{
    @Before
    public void setup ()
    {
        this.testOrderId = UUID.randomUUID();

        this.testOrderDto = new OrderDto(
                testOrderId,
                "Ivan",
                "Sumskaya 1",
                "(050)123-45-67",
                "ivan@gmail.com",
                LocalDateTime.now(),
                "Placed",
                BigDecimal.valueOf( 100.00 ),
                BigDecimal.valueOf( 100.00 ),
                BigDecimal.valueOf(   0.00 ),
                "blablabla",
                0,
                1
        );

        this.mockMvc = MockMvcBuilders.standaloneSetup( theController ).build();
    }

    @Test
    public void test_NoOrderIdSpecified () throws Exception
    {
        this.mockMvc.perform(
                post( "/trackorder/" )
                        .param( "inputTrackOrderEmail", "ivan@gmail.com" )
        )
                .andExpect( status().is4xxClientError() )
        ;
    }

    @Test
    public void test_NoEmailSpecified () throws Exception
    {
        this.mockMvc.perform(
                post( "/trackorder/" )
                        .param( "inputTrackOrderId", testOrderId.toString() )
        )
                .andExpect( status().is4xxClientError() )
        ;
    }

    @Test
    public void test_OrderDoesNotExist () throws Exception
    {
        when( mockOrderService.findOrder( testOrderId ) ).thenReturn( null );

        this.mockMvc.perform(
                post( "/trackorder/" )
                        .param( "inputTrackOrderEmail", "ivan@gmail.com" )
                        .param( "inputTrackOrderId", testOrderId.toString() )
        )
                .andExpect( status().isOk() )
                .andExpect( view().name( ViewNames.Error ) )
                .andExpect( model().attribute( AttributeNames.ErrorView.Title,     ErrorKeys.OrderNotFoundTitle   ) )
                .andExpect( model().attribute( AttributeNames.ErrorView.Message,   ErrorKeys.OrderNotFoundMessage ) )
                .andExpect( model().attribute( AttributeNames.ErrorView.Arguments, testOrderId.toString() ) )
                ;
    }


    @Test
    public void test_OrderEmailMismatch () throws Exception
    {
        when( mockOrderService.findOrder( testOrderId ) ).thenReturn( testOrderDto );

        this.mockMvc.perform(
                post( "/trackorder/" )
                        .param( "inputTrackOrderEmail", "ivan-wrong@gmail.com" )
                        .param( "inputTrackOrderId", testOrderId.toString() )
        )
                .andExpect( status().isOk() )
                .andExpect( view().name( ViewNames.Error ) )
                .andExpect( model().attribute( AttributeNames.ErrorView.Title,     ErrorKeys.OrderAccessViolationTitle   ) )
                .andExpect( model().attribute( AttributeNames.ErrorView.Message,   ErrorKeys.OrderAccessViolationMessage ) )
                .andExpect( model().attribute( AttributeNames.ErrorView.Arguments, "ivan-wrong@gmail.com" ) )
        ;
    }

    @Test
    public void test_OrderFoundEmailMatched () throws Exception
    {
        when( mockOrderService.findOrder( testOrderId )).thenReturn( testOrderDto );

        this.mockMvc.perform(
                post( "/trackorder/" )
                        .param( "inputTrackOrderEmail", "ivan@gmail.com" )
                        .param( "inputTrackOrderId", testOrderId.toString() )
        )
                .andExpect( redirectedUrl( "/order/" ) )
                .andExpect( request().sessionAttribute( SessionKeys.ORDER_ID, testOrderId ) );
        ;
    }



    @Mock
    private IOrderService mockOrderService;

    @InjectMocks
    private TrackOrderController theController;

    private MockMvc mockMvc;

    private UUID testOrderId;
    private OrderDto testOrderDto;
}
