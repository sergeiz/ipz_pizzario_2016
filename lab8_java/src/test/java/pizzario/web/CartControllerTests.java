/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */
package pizzario.web;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pizzario.dto.ShoppingCartDto;
import pizzario.service.IShoppingCartService;
import pizzario.web.constants.AttributeNames;
import pizzario.web.constants.SessionKeys;
import pizzario.web.constants.ViewNames;

import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith( MockitoJUnitRunner.class )
public class CartControllerTests
{
    @Before
    public void setup ()
    {
        TestCartId    = UUID.randomUUID();
        TestProductId = UUID.randomUUID();
        TestSizeId    = UUID.randomUUID();

        TestCartDto = new ShoppingCartDto( TestCartId );

        this.mockMvc = MockMvcBuilders.standaloneSetup( theController ).build();
    }


    @Test
    public void testListNoCartYet () throws Exception
    {
        when( mockCartService.createNew() ).thenReturn( TestCartId );
        when( mockCartService.view( TestCartId ) ).thenReturn( TestCartDto );

        this.mockMvc.perform(
            get( "/cart/view" )
        )
            .andExpect( status().isOk() )
            .andExpect( view().name( ViewNames.Cart ))
            .andExpect( model().attribute( AttributeNames.CartView.Cart, TestCartDto ) )
            .andExpect( request().sessionAttribute( SessionKeys.CART_ID, TestCartId ) );

        verify( mockCartService, times( 1 ) )
                .createNew();
        verify( mockCartService, times( 1 ) )
                .view( any() );
    }


    @Test
    public void testListCartInSession () throws Exception
    {
        when( mockCartService.view( TestCartId ) ).thenReturn( TestCartDto );

        this.mockMvc.perform(
                get("/cart/view")
                .sessionAttr( SessionKeys.CART_ID, TestCartId )
        )
            .andExpect( status().isOk() )
            .andExpect( view().name( ViewNames.Cart ))
            .andExpect( model().attribute( AttributeNames.CartView.Cart, TestCartDto ) )
            .andExpect( request().sessionAttribute( SessionKeys.CART_ID, TestCartId ) );

        verify( mockCartService, never() )
                .createNew();
    }


    @Test
    public void testAddItem () throws Exception
    {
        when( mockCartService.view( TestCartId ) ).thenReturn( TestCartDto );

        this.mockMvc.perform(
            post("/cart/additem")
                .sessionAttr( SessionKeys.CART_ID, TestCartId )
                .param( "productId", TestProductId.toString() )
                .param( "sizeId",    TestSizeId.toString() )
        )
            .andExpect( status().isOk() )
            .andExpect( request().sessionAttribute( SessionKeys.CART_ID, TestCartId ) );

        verify( mockCartService, times( 1 ) )
                .addItem( TestCartId, TestProductId, TestSizeId );
    }

    @Test
    public void testSetItem () throws Exception
    {
        when( mockCartService.view( TestCartId ) ).thenReturn( TestCartDto );

        final int newQuantity = 3;

        this.mockMvc.perform(
            post( "/cart/setitem" )
                .sessionAttr( SessionKeys.CART_ID, TestCartId )
                .param( "productId",    TestProductId.toString() )
                .param( "sizeId",       TestSizeId.toString() )
                .param( "newQuantity",  Integer.toString( newQuantity ) )
        )
            .andExpect( status().isOk() )
            .andExpect( request().sessionAttribute( SessionKeys.CART_ID, TestCartId ) );

        verify( mockCartService, times( 1 ) )
                .setItem( TestCartId, TestProductId, TestSizeId, newQuantity );
    }


    @Test
    public void testRemoveItem () throws Exception
    {
        when( mockCartService.view( TestCartId ) ).thenReturn( TestCartDto );

        this.mockMvc.perform(
                post( "/cart/removeitem" )
                        .sessionAttr( SessionKeys.CART_ID, TestCartId )
                        .param( "productId",    TestProductId.toString() )
                        .param( "sizeId",       TestSizeId.toString() )
        )
                .andExpect( status().isOk() )
                .andExpect( request().sessionAttribute( SessionKeys.CART_ID, TestCartId ) );

        verify( mockCartService, times( 1 ) )
                .removeItem( TestCartId, TestProductId, TestSizeId );
    }

    @Test
    public void testClear () throws Exception
    {
        when( mockCartService.view( TestCartId ) ).thenReturn( TestCartDto );

        this.mockMvc.perform(
                post( "/cart/clear" )
                        .sessionAttr( SessionKeys.CART_ID, TestCartId )
        )
                .andExpect( status().isOk() )
                .andExpect( request().sessionAttribute( SessionKeys.CART_ID, TestCartId ) );

        verify( mockCartService, times( 1 ) )
                .clearItems( TestCartId );
    }


    @Mock
    private IShoppingCartService mockCartService;

    @InjectMocks
    private CartController theController;

    private MockMvc mockMvc;

    private UUID TestCartId;
    private UUID TestProductId, TestSizeId;
    private ShoppingCartDto TestCartDto;
}
