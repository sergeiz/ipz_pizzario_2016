/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.web;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pizzario.dto.OrderDto;
import pizzario.service.IOrderService;
import pizzario.web.constants.AttributeNames;
import pizzario.web.constants.ErrorKeys;
import pizzario.web.constants.SessionKeys;
import pizzario.web.constants.ViewNames;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith( MockitoJUnitRunner.class )
public class OrderControllerTests
{
    @Before
    public void setup ()
    {
        this.testOrderId = UUID.randomUUID();

        this.testOrderDto = new OrderDto(
                testOrderId,
                "Ivan",
                "Sumskaya 1",
                "(050)123-45-67",
                "ivan@gmail.com",
                LocalDateTime.now(),
                "Placed",
                BigDecimal.valueOf( 100.00 ),
                BigDecimal.valueOf( 100.00 ),
                BigDecimal.valueOf(   0.00 ),
                "blablabla",
                0,
                1
        );

        this.mockMvc = MockMvcBuilders.standaloneSetup( theController ).build();
    }


    @Test
    public void test_NoOrderInSession () throws Exception
    {
        this.mockMvc.perform(
                get( "/order/" )
        )
                .andExpect( status().isOk() )
                .andExpect( view().name( ViewNames.Error ) )
                .andExpect( model().attribute( AttributeNames.ErrorView.Title, ErrorKeys.NoOrderTitle ) )
                .andExpect( model().attribute( AttributeNames.ErrorView.Message, ErrorKeys.NoOrderMessage ) )
                ;
    }


    @Test
    public void test_OrderInSession () throws Exception
    {
        when( mockOrderService.view( testOrderId )).thenReturn( testOrderDto );

        this.mockMvc.perform(
                get( "/order/" )
                .sessionAttr( SessionKeys.ORDER_ID, testOrderId )
        )
                .andExpect( status().isOk() )
                .andExpect( view().name( ViewNames.Order ) )
                .andExpect( model().attribute( AttributeNames.OrderView.OrderId,  testOrderId.toString() ) )
                .andExpect( model().attribute( AttributeNames.OrderView.Status,   testOrderDto.getStatus() ) )
                .andExpect( model().attribute( AttributeNames.OrderView.Name,     testOrderDto.getCustomerName() ) )
                .andExpect( model().attribute( AttributeNames.OrderView.Address,  testOrderDto.getCustomerAddress() ) )
                .andExpect( model().attribute( AttributeNames.OrderView.Email,    testOrderDto.getContactEmail() ) )
                .andExpect( model().attribute( AttributeNames.OrderView.Phone,    testOrderDto.getContactPhone() ) )
                .andExpect( model().attribute( AttributeNames.OrderView.Discount, testOrderDto.getDiscountPercentage().setScale( 2 ).toString() ) )
                .andExpect( model().attribute( AttributeNames.OrderView.Cost,     testOrderDto.getTotalCost().setScale( 2 ).toString() ) )
                .andExpect( model().attribute( AttributeNames.OrderView.Comment,  testOrderDto.getComment() ) )
                .andExpect( model().attribute( AttributeNames.OrderView.Placed,   testOrderDto.getPlacementTime().format( OrderController.getDateTimeFormatter() ) ) )
                ;
    }


    @Mock
    private IOrderService mockOrderService;

    @InjectMocks
    private OrderController theController;

    private MockMvc mockMvc;

    private UUID testOrderId;
    private OrderDto testOrderDto;
}
