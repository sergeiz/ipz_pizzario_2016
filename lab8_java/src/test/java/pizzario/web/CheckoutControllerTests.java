/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.web;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pizzario.dto.ProductItemDto;
import pizzario.dto.ShoppingCartDto;
import pizzario.service.IOrderService;
import pizzario.service.IShoppingCartService;
import pizzario.web.constants.AttributeNames;
import pizzario.web.constants.SessionKeys;
import pizzario.web.constants.ViewNames;

import java.math.BigDecimal;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith( MockitoJUnitRunner.class )
public class CheckoutControllerTests
{
    @Before
    public void setup ()
    {
        TestCartId    = UUID.randomUUID();
        TestOrderId   = UUID.randomUUID();

        TestCartDto = new ShoppingCartDto( TestCartId );

        this.mockMvc = MockMvcBuilders.standaloneSetup( theController ).build();
    }


    @Test
    public void test_checkoutNoCart () throws Exception
    {
        when( mockCartService.createNew() ).thenReturn( TestCartId );
        when( mockCartService.view( TestCartId ) ).thenReturn( TestCartDto );

        this.mockMvc.perform(
                get( "/checkout/" )
        )
                .andExpect( status().isOk() )
                .andExpect( view().name( ViewNames.Checkout ) )
                .andExpect( model().attribute( AttributeNames.CartView.Cart, TestCartDto ) )
                .andExpect( request().sessionAttribute( SessionKeys.CART_ID, TestCartId ) );

        verify( mockCartService, times( 1 ) )
                .createNew();
        verify( mockCartService, times( 1 ) )
                .view( TestCartId );
    }


    @Test
    public void test_checkoutHavingCart () throws Exception
    {
        when( mockCartService.view( TestCartId ) ).thenReturn( TestCartDto );

        this.mockMvc.perform(
                get( "/checkout/" )
                .sessionAttr( SessionKeys.CART_ID, TestCartId )
        )
                .andExpect( status().isOk() )
                .andExpect( view().name( ViewNames.Checkout ))
                .andExpect( model().attribute( AttributeNames.CartView.Cart, TestCartDto ) )
                .andExpect( request().sessionAttribute( SessionKeys.CART_ID, TestCartId ) );

        verify( mockCartService, never() )
                .createNew();
        verify( mockCartService, times( 1 ) )
                .view( TestCartId );
    }


    @Test
    public void test_submitCheckoutEmptyCart () throws Exception
    {
        when( mockCartService.view( TestCartId ) ).thenReturn( TestCartDto );

        this.mockMvc.perform(
                post( "/checkout/" )
                    .sessionAttr( SessionKeys.CART_ID, TestCartId )
                    .param( "inputName", TestCustomerName )
                    .param( "inputAddress", TestAddress )
                    .param( "inputEmail", TestEmail )
                    .param( "inputPhone", TestPhone )
                    .param( "inputComment", TestComment )
        )
                .andExpect( redirectedUrl( "/checkout/" ) )
        ;
    }


    @Test
    public void test_submitCheckoutNonEmptyCart () throws Exception
    {
        when( mockCartService.view( TestCartId ) ).thenReturn( TestCartDto );
        TestCartDto.getItems().add( anItemDto() );

        when( mockOrderService.createNew(
                TestCustomerName,
                TestAddress,
                TestPhone,
                TestEmail,
                "",
                TestCartId ) ).thenReturn( TestOrderId );

        this.mockMvc.perform(
                post( "/checkout/" )
                        .sessionAttr( SessionKeys.CART_ID, TestCartId )
                        .param( "inputName", TestCustomerName )
                        .param( "inputAddress", TestAddress )
                        .param( "inputEmail", TestEmail )
                        .param( "inputPhone", TestPhone )
                        .param( "inputComment", TestComment )
        )
                .andExpect( redirectedUrl( "/order/" ) )
                .andExpect( request().sessionAttribute( SessionKeys.CART_ID, is( nullValue() ) ) )
                .andExpect( request().sessionAttribute( SessionKeys.ORDER_ID, TestOrderId ) )
        ;

        verify( mockCartService, times( 1 ) )
                .view( TestCartId );

        verify( mockCartService, times( 1 ) )
                .lock( TestCartId );

        verify( mockOrderService, times( 1 ) )
                .createNew( TestCustomerName, TestAddress, TestPhone, TestEmail, TestComment, TestCartId );
    }


    private ProductItemDto anItemDto ()
    {
        return new ProductItemDto(
                UUID.randomUUID(),
                UUID.randomUUID(),
                "Pizza",
                UUID.randomUUID(),
                "Small",
                1,
                BigDecimal.TEN,
                BigDecimal.TEN
        );
    }


    @Mock
    private IShoppingCartService mockCartService;

    @Mock
    private IOrderService mockOrderService;

    @InjectMocks
    private CheckoutController theController;

    private MockMvc mockMvc;

    private UUID TestCartId, TestOrderId;
    private ShoppingCartDto TestCartDto;

    private static final String TestCustomerName = "Wasya";
    private static final String TestAddress = "Sumskaya 1";
    private static final String TestPhone = "(050)123-45-67";
    private static final String TestEmail = "wasya@urk.net";
    private static final String TestComment = "";
}
