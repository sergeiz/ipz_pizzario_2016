/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.web;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pizzario.dto.AccountDto;
import pizzario.dto.OrderDto;
import pizzario.service.IOrderService;
import pizzario.web.constants.AttributeNames;
import pizzario.web.constants.ErrorKeys;
import pizzario.web.constants.ViewNames;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith( MockitoJUnitRunner.class )
public class OperatorControllerTests
{
    @Before
    public void setup ()
    {
        this.mockMvc = MockMvcBuilders.standaloneSetup( theController ).build();
    }


    @Test
    public void test_OperatorHome () throws Exception
    {
        UUID order1Id = UUID.randomUUID();
        UUID order2Id = UUID.randomUUID();
        UUID order3Id = UUID.randomUUID();

        List< UUID > unconfirmedOrderIds = Arrays.asList( order1Id );
        List< UUID > confirmedOrderIds   = Arrays.asList( order2Id );
        List< UUID > readyOrderIds       = Arrays.asList( order3Id );

        OrderDto dto1 = new OrderDto( order1Id );
        OrderDto dto2 = new OrderDto( order2Id );
        OrderDto dto3 = new OrderDto( order3Id );

        List< OrderDto > unconfirmedOrders = Arrays.asList( dto1 );
        List< OrderDto > confirmedOrders   = Arrays.asList( dto2 );
        List< OrderDto > readyOrders       = Arrays.asList( dto3 );

        when( mockOrderService.viewUnconfirmed() ).thenReturn( unconfirmedOrderIds );
        when( mockOrderService.viewConfirmed() ).thenReturn( confirmedOrderIds );
        when( mockOrderService.viewReady4Delivery() ).thenReturn( readyOrderIds );

        when( mockOrderService.view( order1Id )).thenReturn( dto1 );
        when( mockOrderService.view( order2Id )).thenReturn( dto2 );
        when( mockOrderService.view( order3Id )).thenReturn( dto3 );

        this.mockMvc.perform(
                get( "/operator/" )
        )
                .andExpect( status().isOk() )
                .andExpect( view().name( ViewNames.Operator ) )
                .andExpect( model().attribute( AttributeNames.OperatorView.UnconfirmedOrders,    unconfirmedOrders ) )
                .andExpect( model().attribute( AttributeNames.OperatorView.ConfirmedOrders,      confirmedOrders ) )
                .andExpect( model().attribute( AttributeNames.OperatorView.Ready4DeliveryOrders, readyOrders ) )
        ;
    }


    @Test
    public void test_ConfirmedRow () throws Exception
    {
        UUID orderId = UUID.randomUUID();
        OrderDto orderDto = new OrderDto( orderId );

        when( mockOrderService.view( orderId )).thenReturn( orderDto );

        this.mockMvc.perform(
                get( "/operator/confirmedrow/" )
                .param( "orderId", orderId.toString() )
        )
                .andExpect( status().isOk() )
                .andExpect( view().name( ViewNames.Operator_ConfirmedRow ) )
                .andExpect( model().attribute( AttributeNames.OperatorView.ConfirmedOrder, orderDto ) )
                ;
    }


    @Test
    public void test_SetOrderDiscount () throws Exception
    {
        UUID orderId = UUID.randomUUID();
        BigDecimal discount = BigDecimal.valueOf( 20.0 );

        this.mockMvc.perform(
                post( "/operator/setdiscount" )
                    .param( "orderId", orderId.toString() )
                    .param( "discount", discount.toString() )
        )
            .andExpect( status().isOk() )
            ;

        verify( mockOrderService, only() )
                .setDiscount( orderId, discount );
    }


    @Test
    public void test_ConfirmOrder () throws Exception
    {
        UUID orderId = UUID.randomUUID();

        AccountDto accountDto = new AccountDto( UUID.randomUUID(), "Wasya", "wasya@pizzario.com", "12345" );

        this.mockMvc.perform(
                post( "/operator/confirm" )
                        .param( "orderId", orderId.toString() )
                        .principal( accountDto )
        )
                .andExpect( status().isOk() )
        ;

        verify( mockOrderService, only() )
                .confirm( orderId, accountDto.getDomainId() );
    }



    @Test
    public void test_CancelOrder () throws Exception
    {
        UUID orderId = UUID.randomUUID();

        this.mockMvc.perform(
                post( "/operator/cancel" )
                        .param( "orderId", orderId.toString() )
        )
                .andExpect( status().isOk() )
        ;

        verify( mockOrderService, only() )
                .cancel( orderId );
    }

    @InjectMocks
    private OperatorController theController;

    @Mock
    private IOrderService mockOrderService;

    private MockMvc mockMvc;
}
