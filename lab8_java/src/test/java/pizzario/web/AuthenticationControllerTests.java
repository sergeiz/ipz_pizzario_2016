package pizzario.web;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import pizzario.dto.AccountDto;
import pizzario.web.constants.ViewNames;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith( MockitoJUnitRunner.class )
public class AuthenticationControllerTests
{
    @Before
    public void setup ()
    {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setViewClass( JstlView.class );
        resolver.setSuffix( ".jsp" );

        this.mockMvc = MockMvcBuilders.standaloneSetup( theController )
                        .setViewResolvers( resolver )
                        .build();
    }


    @Test
    public void test_Login_Get () throws Exception
    {
        SecurityContextHolder.getContext().setAuthentication( null );

        this.mockMvc.perform(
                get( "/login/" )
        )
                .andExpect( status().isOk() )
                .andExpect( view().name( ViewNames.Login ) )
        ;
    }


    @Test
    public void test_Login_AlreadyLogged () throws Exception
    {
        SecurityContextHolder.getContext().setAuthentication(
                new AccountDto(
                        UUID.randomUUID(),
                        "Wasya",
                        "wasya@pizzario.com",
                        "12345"
                )
        );

        this.mockMvc.perform(
                get( "/login" )
        )
                .andExpect( redirectedUrl( "/operator/" ) )
                ;
    }


    @InjectMocks
    private AuthenticationController theController;

    private MockMvc mockMvc;
}
