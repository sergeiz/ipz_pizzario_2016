/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.web;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pizzario.dto.ProductDto;
import pizzario.dto.ProductPricingDto;
import pizzario.dto.ProductSizeDto;
import pizzario.service.IProductService;
import pizzario.service.IProductSizeService;
import pizzario.web.constants.AttributeNames;
import pizzario.web.constants.ViewNames;

import java.math.BigDecimal;
import java.util.*;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith( MockitoJUnitRunner.class )
public class MenuControllerTests
{
    @Before
    public void setup ()
    {
        this.testProduct1Id = UUID.randomUUID();
        this.testProduct2Id = UUID.randomUUID();

        this.testSize1Id = UUID.randomUUID();
        this.testSize2Id = UUID.randomUUID();

        this.testProduct1Dto = new ProductDto(
                testProduct1Id,
                "MyPizza",
                "mypizza.jpg",
                "my pizza is good"
        );

        this.testProduct2Dto = new ProductDto(
                testProduct2Id,
                "TheirPizza",
                "theirpizza.jpg",
                "their pizza is not so good"
        );

        this.testProductSize1Dto = new ProductSizeDto(
                testSize1Id,
                "Small",
                "small.jpg",
                25,
                300
        );

        this.testProductSize2Dto = new ProductSizeDto(
                testSize2Id,
                "Large",
                "large.jpg",
                35,
                900
        );

        Map< UUID, BigDecimal > sizePrices = new TreeMap<>();
        sizePrices.put( testSize1Id, BigDecimal.valueOf( 100.00 ) );
        sizePrices.put( testSize2Id, BigDecimal.valueOf( 120.00 ) );

        Map< UUID, String > sizeNames = new TreeMap<>();
        sizeNames.put( testSize1Id, "Small" );
        sizeNames.put( testSize2Id, "Large" );

        this.testPricing1Dto = new ProductPricingDto(
                testProduct1Id, sizePrices, sizeNames
        );

        this.testPricing2Dto = new ProductPricingDto(
                testProduct2Id, sizePrices, sizeNames
        );

        this.testProducts = Arrays.asList( testProduct1Dto, testProduct2Dto );

        this.testProductPrices = new TreeMap<>();
        testProductPrices.put( testProduct1Id, sizePrices );
        testProductPrices.put( testProduct2Id, sizePrices );

        this.testSizes = Arrays.asList( testProductSize1Dto, testProductSize2Dto );

        this.mockMvc = MockMvcBuilders.standaloneSetup( theController ).build();
    }

    @Test
    public void test_MenuList () throws Exception
    {
        when( mockProductService.viewAll() ).thenReturn( Arrays.asList( testProduct1Id, testProduct2Id ) );
        when( mockProductService.view( testProduct1Id ) ).thenReturn( testProduct1Dto );
        when( mockProductService.view( testProduct2Id ) ).thenReturn( testProduct2Dto );
        when( mockProductService.viewPrices( testProduct1Id ) ).thenReturn( testPricing1Dto );
        when( mockProductService.viewPrices( testProduct2Id ) ).thenReturn( testPricing2Dto );

        when( mockProductSizeService.viewAll() ).thenReturn( Arrays.asList( testSize1Id, testSize2Id ) );
        when( mockProductSizeService.view( testSize1Id ) ).thenReturn( testProductSize1Dto );
        when( mockProductSizeService.view( testSize2Id ) ).thenReturn( testProductSize2Dto );

        this.mockMvc.perform(
                get( "/menu/" )
        )
                .andExpect( status().isOk() )
                .andExpect( view().name( ViewNames.Menu ) )
                .andExpect( model().attribute( AttributeNames.MenuView.Products, testProducts ) )
                .andExpect( model().attribute( AttributeNames.MenuView.ProductPrices, testProductPrices ) )
                .andExpect( model().attribute( AttributeNames.MenuView.Sizes, testSizes ) )
        ;
    }

    @Mock
    private IProductService mockProductService;

    @Mock
    private IProductSizeService mockProductSizeService;

    @InjectMocks
    private MenuController theController;

    private MockMvc mockMvc;

    private List< ProductDto > testProducts;
    private List< ProductSizeDto > testSizes;
    private Map< UUID, Map< UUID, BigDecimal > > testProductPrices;

    private UUID testProduct1Id, testProduct2Id;
    private UUID testSize1Id, testSize2Id;
    private ProductDto testProduct1Dto, testProduct2Dto;
    private ProductSizeDto testProductSize1Dto, testProductSize2Dto;
    private ProductPricingDto testPricing1Dto, testPricing2Dto;
}
