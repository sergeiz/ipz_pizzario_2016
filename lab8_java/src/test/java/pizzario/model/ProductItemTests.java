/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.UUID;

import static junit.framework.TestCase.assertSame;
import static org.junit.Assert.assertEquals;

public class ProductItemTests
{
    @Test
    public void test_DefaultConstructor ()
    {
        ProductItem pi = new ProductItem();

        assertEquals( pi.getDomainId(), null );
        assertEquals( pi.getProduct(), null );
        assertEquals( pi.getSize(), null );
        assertEquals( pi.getQuantity(), 0 );
        assertEquals( pi.getFixedPrice(), null );
    }


    @Test
    public void test_PublicConstructor () throws Exception
    {
        final UUID TEST_ID = UUID.randomUUID();
        final Product TEST_PRODUCT = new Product();
        final ProductSize TEST_PRODUCT_SIZE = new ProductSize();
        final int TEST_QUANTITY = 1;
        final BigDecimal TEST_PRICE = BigDecimal.valueOf( 100.00 );

        ProductItem pi = new ProductItem(
                TEST_ID,
                TEST_PRODUCT,
                TEST_PRODUCT_SIZE,
                TEST_QUANTITY,
                TEST_PRICE
        );

        assertEquals( pi.getDomainId(), TEST_ID );
        assertSame( pi.getProduct(), TEST_PRODUCT );
        assertSame( pi.getSize(), TEST_PRODUCT_SIZE );
        assertEquals( pi.getQuantity(), TEST_QUANTITY );
        assertEquals( pi.getFixedPrice(), TEST_PRICE );
    }


    @Test
    public void test_GetCost_Quantity_1 ()
    {
        ProductItem pi = new ProductItem(
                UUID.randomUUID(),
                new Product(),
                new ProductSize(),
                1,
                BigDecimal.valueOf( 50.00 )
        );

        assertEquals( pi.getCost(), BigDecimal.valueOf( 50.00 ) );
    }


    @Test
    public void test_GetCost_Quantity_3 ()
    {
        ProductItem pi = new ProductItem(
                UUID.randomUUID(),
                new Product(),
                new ProductSize(),
                3,
                BigDecimal.valueOf( 50.00 )
        );

        assertEquals( pi.getCost(), BigDecimal.valueOf( 150.00 ) );
    }
}
