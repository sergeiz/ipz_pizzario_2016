/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model.accounts;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.*;

public class AccountTests
{
    @Test
    public void test_DefaultConstructor ()
    {
        Account a = new Account();

        assertEquals( a.getDomainId(), null );
        assertEquals( a.getName(), null );
        assertEquals( a.getEmail(), null );
        assertEquals( a.getPassword(), null );
    }


    @Test
    public void test_PublicConstructor ()
    {
        final UUID TEST_ID = UUID.randomUUID();
        final String TEST_NAME = "Ivan";
        final String TEST_EMAIL = "ivan@gmail.com";
        final String TEST_PASSWORD = "12345";

        Account a = new Account(
                TEST_ID,
                TEST_NAME,
                TEST_EMAIL,
                TEST_PASSWORD
        );

        assertEquals( a.getDomainId(), TEST_ID );
        assertEquals( a.getName(), TEST_NAME );
        assertEquals( a.getEmail(), TEST_EMAIL );
        assertEquals( a.getPassword(), TEST_PASSWORD );
    }


    @Test
    public void test_SetName ()
    {
        Account a = new Account();
        a.setName( "Wasya" );
        assertEquals( a.getName(), "Wasya" );
    }


    @Test
    public void test_SetEmail ()
    {
        Account a = new Account();
        a.setEmail( "wasya@gmail.com" );
        assertEquals( a.getEmail(), "wasya@gmail.com" );
    }


    @Test
    public void test_SetPassword ()
    {
        Account a = new Account();
        a.setPassword( "12345" );
        assertEquals( a.getPassword(), "12345" );
        assertTrue( a.checkPassword( "12345" ) );
    }


    @Test
    public void test_Visitor ()
    {
        Account a = new Account();

        class SimpleVisitor implements AccountVisitor
        {
            public boolean visited;

            @Override
            public void visit ( Account account )
            {
                visited = true;
            }

            @Override
            public void visit ( OperatorAccount account )
            {
                fail();
            }
        }

        SimpleVisitor v = new SimpleVisitor();
        a.accept( v );

        assertTrue( v.visited );
    }

    @Test
    public void test_ExtractOrders ()
    {
        Account o = new Account();

        List< UUID > orderIdList = new ArrayList<>();
        OrdersExtractAccountVisitor v = new OrdersExtractAccountVisitor( orderIdList );
        o.accept( v );

        assertThat( orderIdList, empty() );
    }
}
