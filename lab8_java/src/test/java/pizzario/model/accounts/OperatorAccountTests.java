/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model.accounts;

import org.junit.Test;
import pizzario.model.Contact;
import pizzario.model.Order;
import pizzario.model.ProductItem;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class OperatorAccountTests
{
    @Test
    public void test_DefaultConstructor ()
    {
        OperatorAccount o = new OperatorAccount();

        assertEquals( o.getDomainId(), null );
        assertEquals( o.getName(), null );
        assertEquals( o.getEmail(), null );
        assertEquals( o.getPassword(), null );

        assertThat( o.getOrders(), empty() );
    }

    @Test
    public void test_PublicConstructor ()
    {
        final UUID TEST_ID = UUID.randomUUID();
        final String TEST_NAME = "Ivan";
        final String TEST_EMAIL = "ivan@gmail.com";
        final String TEST_PASSWORD = "12345";

        OperatorAccount o = new OperatorAccount(
                TEST_ID,
                TEST_NAME,
                TEST_EMAIL,
                TEST_PASSWORD
        );

        assertEquals( o.getDomainId(), TEST_ID );
        assertEquals( o.getName(), TEST_NAME );
        assertEquals( o.getEmail(), TEST_EMAIL );
        assertEquals( o.getPassword(), TEST_PASSWORD );

        assertThat( o.getOrders(), empty() );
    }

    @Test
    public void test_Visitor ()
    {
        OperatorAccount o = new OperatorAccount();

        class SimpleVisitor implements AccountVisitor
        {
            public boolean visited;

            @Override
            public void visit ( Account account )
            {
                fail();
            }

            @Override
            public void visit ( OperatorAccount account )
            {
                visited = true;
            }
        }

        SimpleVisitor v = new SimpleVisitor();
        o.accept( v );

        assertTrue( v.visited );
    }


    @Test
    public void test_AddOneOrder ()
    {
        OperatorAccount o = new OperatorAccount();

        Order order = buildOrder();
        o.trackOrder( order );

        assertThat( o.getOrders(), hasSize( 1 ) );
        assertThat( o.getOrders(), contains( order ) );
    }


    @Test
    public void test_AddTwoOrders ()
    {
        OperatorAccount o = new OperatorAccount();

        Order order1 = buildOrder();
        Order order2 = buildOrder();
        o.trackOrder( order1 );
        o.trackOrder( order2 );

        assertThat( o.getOrders(), hasSize( 2 ) );
        assertThat( o.getOrders(), containsInAnyOrder( order1, order2 ) );
    }


    @Test
    public void test_ExtractOrders ()
    {
        OperatorAccount o = new OperatorAccount();

        Order order1 = buildOrder();
        Order order2 = buildOrder();
        o.trackOrder( order1 );
        o.trackOrder( order2 );

        List< UUID > orderIdList = new ArrayList<>();
        OrdersExtractAccountVisitor v = new OrdersExtractAccountVisitor( orderIdList );
        o.accept( v );

        assertThat( orderIdList, hasSize( 2 ) );
        assertThat( orderIdList, containsInAnyOrder( order1.getDomainId(), order2.getDomainId() ) );
    }


    private Order buildOrder ()
    {
        return new Order(
                UUID.randomUUID(),
                new ArrayList< ProductItem >(),
                new Contact( "Ivan", "Some stree", "12345", "ivan@gmail.com" ),
                LocalDateTime.now()
        );
    }
}
