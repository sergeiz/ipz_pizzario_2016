/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import org.junit.Test;
import pizzario.exceptions.RemovingUnusedIngredientException;

import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;


public class RecipeTests
{
    @Test
    public void test_DefaultConstructor ()
    {
        Recipe r = new Recipe();

        assertEquals( r.getDomainId(), null );
        assertEquals( r.getProduct(), null );
        assertEquals( r.getSize(), null );
        assertThat( r.listUsedIngredients(), empty() );
    }


    @Test
    public void test_PublicConstructor ()
    {
        final UUID TEST_ID = UUID.randomUUID();
        final Product TEST_PRODUCT = new Product();
        final ProductSize TEST_SIZE = new ProductSize();

        Recipe r = new Recipe( TEST_ID, TEST_PRODUCT, TEST_SIZE );

        assertEquals( r.getDomainId(), TEST_ID );
        assertSame( r.getProduct(), TEST_PRODUCT );
        assertSame( r.getSize(), TEST_SIZE );
        assertThat( r.listUsedIngredients(), empty() );
    }


    @Test
    public void test_NoIngredientsInitially ()
    {
        Recipe r = new Recipe();
        assertThat( r.listUsedIngredients(), empty() );
    }


    @Test
    public void test_AddSingleIngredient ()
    {
        Recipe r = new Recipe();

        Ingredient ham = makeIngredient( "Ham" );
        r.useIngredient( ham, 100 );

        assertTrue( r.usesIngredient( ham ) );
        assertEquals( r.getIngredientWeight( ham ), 100 );

        assertThat( r.listUsedIngredients(), hasSize( 1 ) );
        assertThat( r.listUsedIngredients(), contains( ham ) );
    }


    @Test
    public void test_AddTwoIngredients ()
    {
        Ingredient ham = makeIngredient( "Ham" );
        Ingredient cheese = makeIngredient( "Cheese" );

        Recipe r = new Recipe();
        r.useIngredient( ham, 100 );
        r.useIngredient( cheese, 50 );

        assertTrue( r.usesIngredient( ham ) );
        assertTrue( r.usesIngredient( cheese ));

        assertEquals( r.getIngredientWeight( ham ), 100 );
        assertEquals( r.getIngredientWeight( cheese ), 50 );

        assertThat( r.listUsedIngredients(), hasSize( 2 ) );
        assertThat( r.listUsedIngredients(), containsInAnyOrder( ham, cheese) );
    }


    @Test
    public void test_UseSameIngredient ()
    {
        Ingredient ham = makeIngredient( "Ham" );
        Recipe r = new Recipe();
        r.useIngredient( ham, 100 );
        r.useIngredient( ham, 200 );

        assertTrue( r.usesIngredient( ham ) );
        assertEquals( r.getIngredientWeight( ham ), 200 );

        assertThat( r.listUsedIngredients(), hasSize( 1 ) );
        assertThat( r.listUsedIngredients(), contains( ham ) );
    }


    @Test
    public void test_QueryUnusedIngredient ()
    {
        Ingredient ham = makeIngredient( "Ham" );
        Recipe r = new Recipe();

        assertFalse( r.usesIngredient( ham ) );
        assertEquals( r.getIngredientWeight( ham ), 0 );
    }


    @Test
    public void test_RemoveUsedIngredient () throws Exception
    {
        Ingredient ham = makeIngredient( "Ham" );
        Recipe r = new Recipe();
        r.useIngredient( ham, 100 );
        r.removeIngredient( ham );

        assertFalse( r.usesIngredient( ham ) );
        assertEquals( r.getIngredientWeight( ham ), 0 );
        assertThat( r.listUsedIngredients(), empty() );
    }


    @Test( expected = RemovingUnusedIngredientException.class )
    public void test_RemoveUnusedIngredient () throws Exception
    {
        // exception transports product name
        Recipe r = new Recipe( UUID.randomUUID(), new Product(), null );

        Ingredient ham = makeIngredient( "Ham" );
        r.removeIngredient( ham );
    }


    private Ingredient makeIngredient ( String name )
    {
        return new Ingredient( UUID.randomUUID(), name );
    }
}
