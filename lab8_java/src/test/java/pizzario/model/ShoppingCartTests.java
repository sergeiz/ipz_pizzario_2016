/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import org.junit.Test;
import pizzario.exceptions.LockingEmptyCartException;
import pizzario.exceptions.UnmodifiableCartException;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class ShoppingCartTests
{
    @Test
    public void test_DefaultConstructor ()
    {
        ShoppingCart cart = new ShoppingCart();

        assertEquals( cart.getDomainId(), null );
        assertTrue( cart.isModifiable() );
        assertThat( cart.getItems(), empty() );
        assertTrue( wasRecentlyUpdated( cart ) );
    }


    @Test
    public void test_PublicConstructor ()
    {
        final UUID TEST_ID = UUID.randomUUID();
        ShoppingCart cart = new ShoppingCart( TEST_ID );

        assertEquals( cart.getDomainId(), TEST_ID );
        assertTrue( cart.isModifiable() );
        assertThat( cart.getItems(), empty() );
        assertTrue( wasRecentlyUpdated( cart ) );
    }


    @Test
    public void test_PlaceItem () throws Exception
    {
        ShoppingCart cart = new ShoppingCart();

        ProductItem item = makeProductItem( 1 );
        cart.placeItem( item );

        assertThat( cart.getItems(), hasSize( 1 ) );
        assertThat( cart.getItems(), contains( item ) );

        assertEquals(
                cart.findItemIndex( item.getProduct(), item.getSize() ),
                0
        );

        assertEquals(
                cart.getItemQuantity( item.getProduct(), item.getSize() ),
                1
        );
    }


    @Test
    public void test_QueryMissingItem () throws Exception
    {
        ShoppingCart cart = new ShoppingCart();
        ProductItem item = makeProductItem();

        assertEquals(
                cart.findItemIndex( item.getProduct(), item.getSize() ),
                -1
        );

        assertEquals(
                cart.getItemQuantity( item.getProduct(), item.getSize() ),
                0
        );
    }


    @Test
    public void test_PlaceTwoItems () throws Exception
    {
        ShoppingCart cart = new ShoppingCart();
        ProductItem item1 = makeProductItem();
        ProductItem item2 = makeProductItem();
        cart.placeItem( item1 );
        cart.placeItem( item2 );

        assertThat( cart.getItems(), hasSize( 2 ) );
        assertThat( cart.getItems(), containsInAnyOrder( item1, item2 ) );

        assertEquals(
                cart.findItemIndex( item1.getProduct(), item1.getSize() ),
                0
        );

        assertEquals(
                cart.findItemIndex( item2.getProduct(), item2.getSize() ),
                1
        );

        assertEquals(
                cart.getItemQuantity( item1.getProduct(), item1.getSize() ),
                1
        );

        assertEquals(
                cart.getItemQuantity( item2.getProduct(), item2.getSize() ),
                1
        );
    }


    @Test
    public void test_PlaceSameItemTwice () throws Exception
    {
        ShoppingCart cart = new ShoppingCart();
        ProductItem item = makeProductItem( 3 );
        cart.placeItem( item );
        cart.placeItem( item );

        assertThat( cart.getItems(), hasSize( 1 ) );
        assertThat( cart.getItems(), contains( item ) );

        assertEquals(
                cart.findItemIndex( item.getProduct(), item.getSize() ),
                0
        );

        assertEquals(
                cart.getItemQuantity( item.getProduct(), item.getSize() ),
                3
        );
    }


    @Test
    public void test_PlaceCheckout () throws Exception
    {
        ShoppingCart cart = new ShoppingCart();
        ProductItem item = makeProductItem();
        cart.placeItem( item );
        cart.checkout();

        assertFalse( cart.isModifiable() );
    }


    @Test( expected = LockingEmptyCartException.class )
    public void test_CheckoutEmpty () throws Exception
    {
        ShoppingCart cart = new ShoppingCart();
        cart.checkout();
    }


    @Test( expected = UnmodifiableCartException.class )
    public void test_PlaceOneMoreAfterCheckout () throws Exception
    {
        ShoppingCart cart = new ShoppingCart();

        ProductItem item = makeProductItem();
        cart.placeItem( item );
        cart.checkout();

        item = makeProductItem( );
        cart.placeItem( item );
    }


    @Test
    public void test_PlaceDrop () throws Exception
    {
        ShoppingCart cart = new ShoppingCart();

        ProductItem item = makeProductItem();
        cart.placeItem( item );
        cart.dropItem( 0 );

        assertThat( cart.getItems(), empty() );
        assertEquals( cart.findItemIndex( item.getProduct(), item.getSize() ), -1 );
    }


    @Test( expected = UnmodifiableCartException.class )
    public void test_DropAfterCheckout () throws Exception
    {
        ShoppingCart cart = new ShoppingCart();

        ProductItem item = makeProductItem();
        cart.placeItem( item );
        cart.checkout();
        cart.dropItem( 0 );
    }


    @Test
    public void test_ClearEmpty () throws Exception
    {
        ShoppingCart cart = new ShoppingCart();
        cart.clearItems();
        assertThat( cart.getItems(), empty() );
    }


    @Test
    public void test_CleanPlaced () throws Exception
    {
        ShoppingCart cart = new ShoppingCart();
        cart.placeItem( makeProductItem() );
        cart.placeItem( makeProductItem() );

        cart.clearItems();
        assertThat( cart.getItems(), empty() );
    }


    @Test( expected = UnmodifiableCartException.class )
    public void test_CleanAfterSubmit () throws Exception
    {
        ShoppingCart cart = new ShoppingCart();
        cart.placeItem( makeProductItem() );
        cart.checkout();
        cart.clearItems();
    }


    @Test
    public void test_Cost () throws Exception
    {
        ProductItem item1 = makeProductItem( 3, BigDecimal.valueOf( 50.00 ) );
        ProductItem item2 = makeProductItem( 1, BigDecimal.valueOf( 70.00 ) );

        ShoppingCart cart = new ShoppingCart();
        cart.placeItem( item1 );
        cart.placeItem( item2 );

        assertEquals( cart.cost(), BigDecimal.valueOf( 220.00 ) );
    }


    private ProductItem makeProductItem ()
    {
        return makeProductItem( 1 );
    }


    private ProductItem makeProductItem ( int quantity )
    {
        return makeProductItem( quantity, BigDecimal.valueOf( 100.00 ) );
    }


    private ProductItem makeProductItem ( int quantity, BigDecimal itemPrice )
    {
        ProductItem item = new ProductItem(
                UUID.randomUUID(),
                new Product(),
                new ProductSize(),
                quantity,
                itemPrice
        );

        return item;
    }



    private boolean wasRecentlyUpdated ( ShoppingCart cart )
    {
        return Duration.between(
                cart.getLastModified(),
                LocalDateTime.now()
        ).getSeconds() < 1;
    }
}
