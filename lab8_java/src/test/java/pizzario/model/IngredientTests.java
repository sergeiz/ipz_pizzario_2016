/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class IngredientTests
{
    @Test
    public void test_DefaultConstructor ()
    {
        Ingredient i = new Ingredient();
        assertEquals( i.getDomainId(), null );
        assertEquals( i.getName(), null );
    }


    @Test
    public void test_PublicConstructor ()
    {
        final UUID TEST_ID = UUID.randomUUID();
        final String TEST_NAME = "Ham";

        Ingredient i = new Ingredient(
                TEST_ID,
                TEST_NAME
        );

        assertEquals( i.getDomainId(), TEST_ID );
        assertEquals( i.getName(), TEST_NAME );
    }


    @Test
    public void test_SetName ()
    {
        Ingredient i = new Ingredient(
                UUID.randomUUID(),
                "Ham"
        );
        i.setName( "Cheese" );
        assertEquals( i.getName(), "Cheese" );
    }
}
