/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class ProductSizeTests
{
    @Test
    public void test_DefaultConstructor ()
    {
        ProductSize sz = new ProductSize();

        assertEquals( sz.getDomainId(), null );
        assertEquals( sz.getName(), null );
        assertEquals( sz.getImageUrl(), null );
        assertEquals( sz.getDiameter(), 0 );
        assertEquals( sz.getWeight(), 0 );
    }


    @Test
    public void test_PublicConstructor ()
    {
        final UUID TEST_ID = UUID.randomUUID();
        final String TEST_NAME = "Large";
        final String TEST_IMAGE_URL = "/resources/large.png";
        final int TEST_DIAMETER = 35;
        final int TEST_WEIGHT = 900;

        ProductSize sz = new ProductSize(
                TEST_ID,
                TEST_NAME,
                TEST_IMAGE_URL,
                TEST_DIAMETER,
                TEST_WEIGHT
        );

        assertEquals( sz.getDomainId(), TEST_ID );
        assertEquals( sz.getName(), TEST_NAME );
        assertEquals( sz.getImageUrl(), TEST_IMAGE_URL );
        assertEquals( sz.getDiameter(), TEST_DIAMETER );
        assertEquals( sz.getWeight(), TEST_WEIGHT );
    }


    @Test
    public void test_SetName ()
    {
        ProductSize size = new ProductSize();
        size.setName( "Large" );
        assertEquals( size.getName(), "Large" );
    }


    @Test
    public void test_SetImageUrl ()
    {
        ProductSize size = new ProductSize();
        size.setImageUrl( "other.png" );
        assertEquals( size.getImageUrl(), "other.png" );
    }


    @Test
    public void test_SetWeight ()
    {
        ProductSize size = new ProductSize();
        size.setWeight( 500 );
        assertEquals( size.getWeight(), 500 );
    }


    @Test
    public void test_SetDiameter ()
    {
        ProductSize size = new ProductSize();
        size.setDiameter( 70 );
        assertEquals( size.getDiameter(), 70 );
    }
}
