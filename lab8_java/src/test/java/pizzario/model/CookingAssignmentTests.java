/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import org.junit.Test;
import pizzario.exceptions.CookingLifecycleException;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class CookingAssignmentTests
{
    @Test
    public void test_DefaultConstructor ()
    {
        CookingAssignment ca = new CookingAssignment();

        assertEquals( ca.getDomainId(), null );
        assertEquals( ca.getOrder(), null );
        assertEquals( ca.getProduct(), null );
        assertEquals( ca.getSize(), null );
        assertEquals( ca.getStatus(), CookingStatus.Waiting );
    }


    @Test
    public void test_PublicConstructor ()
    {
        final UUID TEST_ID = UUID.randomUUID();
        final Order TEST_ORDER = new Order();
        final Product TEST_PRODUCT = new Product();
        final ProductSize TEST_SIZE = new ProductSize();

        CookingAssignment ca = new CookingAssignment(
                TEST_ID,
                TEST_ORDER,
                TEST_PRODUCT,
                TEST_SIZE
        );

        assertEquals( ca.getDomainId(), TEST_ID );
        assertSame( ca.getOrder(), TEST_ORDER );
        assertSame( ca.getProduct(), TEST_PRODUCT );
        assertSame( ca.getSize(), TEST_SIZE );
        assertEquals( ca.getStatus(), CookingStatus.Waiting );
    }


    @Test
    public void test_StartCooking () throws Exception
    {
        CookingAssignment ca = new CookingAssignment();
        ca.startCooking();

        assertEquals( ca.getStatus(), CookingStatus.InProgress );
    }


    @Test( expected = CookingLifecycleException.class )
    public void test_StartCooking_AlreadyStarted () throws Exception
    {
        CookingAssignment ca = new CookingAssignment();
        ca.startCooking();
        ca.startCooking();
    }


    @Test
    public void test_StartFinishCooking () throws Exception
    {
        CookingAssignment ca = new CookingAssignment();
        ca.startCooking();
        ca.finishCooking();

        assertEquals( ca.getStatus(), CookingStatus.Finished );
    }


    @Test( expected = CookingLifecycleException.class )
    public void test_FinishAlreadyFinished () throws Exception
    {
        CookingAssignment ca = new CookingAssignment();
        ca.startCooking();
        ca.finishCooking();
        ca.finishCooking();
    }
}
