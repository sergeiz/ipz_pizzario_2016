/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import org.junit.Test;
import pizzario.exceptions.OrderLifecycleException;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class OrderTests
{
    @Test
    public void test_DefaultConstructor ()
    {
        Order o = new Order();

        assertEquals( o.getDomainId(), null );
        assertEquals( o.getBasicCost(), BigDecimal.ZERO );
        assertEquals( o.getComment(), null );
        assertEquals( o.getDiscount().getPercent(), BigDecimal.ZERO );
        assertEquals( o.getContact(), null );
        assertEquals( o.getStatus(), OrderStatus.Placed );

        assertThat( o.getItems(), empty() );
    }

    @Test
    public void test_ConstructorWithOneItem ()
    {
        UUID orderId = UUID.randomUUID();
        ProductItem testItem = buildItem( BigDecimal.valueOf( 50.0 ), 1 );
        Contact contact = buildContact();
        LocalDateTime placementTime = LocalDateTime.now();

        Order o = new Order( orderId, Arrays.asList( testItem), contact, placementTime );

        assertEquals( o.getDomainId(), orderId );
        assertEquals( o.getComment(), null );

        assertEquals( o.getBasicCost(), BigDecimal.valueOf( 50.00 ) );
        assertEquals( o.getDiscount().getPercent(), BigDecimal.ZERO );
        assertEquals( o.getPlacementTime(), placementTime );
        assertSame( o.getContact(), contact );
        assertEquals( o.getStatus(), OrderStatus.Placed );

        assertEquals( o.getTotalCookingsCount(), 1 );
        assertEquals( o.getFinishedCookingsCount(), 0 );

        assertThat( o.getItems(), hasSize( 1 ) );
        assertThat( o.getItems(), contains( testItem ) );
    }


    @Test
    public void test_ConstructorWithTwoItems ()
    {
        UUID orderId = UUID.randomUUID();
        ProductItem testItem1 = buildItem( BigDecimal.valueOf( 50.00 ), 1 );
        ProductItem testItem2 = buildItem( BigDecimal.valueOf( 30.00 ), 3 );
        Contact contact = buildContact();
        LocalDateTime placementTime = LocalDateTime.now();

        Order o = new Order(
                orderId,
                Arrays.asList( testItem1, testItem2 ),
                contact,
                placementTime
        );

        assertEquals( o.getDomainId(), orderId );
        assertEquals( o.getComment(), null );

        assertEquals( o.getBasicCost(), BigDecimal.valueOf( 140.00 ) );
        assertEquals( o.getDiscount().getPercent(), BigDecimal.ZERO );
        assertEquals( o.getPlacementTime(), placementTime );
        assertSame( o.getContact(), contact );
        assertEquals( o.getStatus(), OrderStatus.Placed );

        assertEquals( o.getTotalCookingsCount(), 4 );
        assertEquals( o.getFinishedCookingsCount(), 0 );

        assertThat( o.getItems(), hasSize( 2 ) );
        assertThat( o.getItems(), containsInAnyOrder( testItem1, testItem2 ) );
    }


    @Test
    public void test_SetDiscount () throws Exception
    {
        Order o = new Order(
                UUID.randomUUID(),
                Arrays.asList( buildItem( BigDecimal.valueOf( 50.0 ), 1 ) ),
                buildContact(),
                LocalDateTime.now()
        );

        o.setDiscount( new Discount( BigDecimal.valueOf( 20.0 ) ) );

        assertEquals( o.getDiscount().getPercent(), BigDecimal.valueOf( 20.0 ) );
        assertEquals( o.getBasicCost(), BigDecimal.valueOf( 50.0 ) );
        assertEquals( o.getTotalCost().intValue(), 40 );
    }


    @Test( expected = OrderLifecycleException.class )
    public void test_SetDiscount_ConfirmedOrder () throws Exception
    {
        Order o = new Order(
                UUID.randomUUID(),
                Arrays.asList( buildItem( BigDecimal.valueOf( 50.0 ), 1 ) ),
                buildContact(),
                LocalDateTime.now()
        );

        o.confirm();

        o.setDiscount( new Discount( BigDecimal.valueOf( 20.0 ) ) );
    }


    @Test
    public void test_SetComment ()
    {
        Order o = new Order();
        o.setComment( "Test" );
        assertEquals( o.getComment(), "Test" );
    }


    @Test
    public void test_ConfirmOrder () throws OrderLifecycleException
    {
        Order o = new Order();
        o.confirm();

        assertEquals( o.getStatus(), OrderStatus.Confirmed );
    }


    @Test
    public void test_CancelPlacedOrder () throws OrderLifecycleException
    {
        Order o = new Order();
        o.cancel();

        assertEquals( o.getStatus(), OrderStatus.Cancelled );
    }


    @Test( expected = OrderLifecycleException.class )
    public void test_CancelConfirmedOrderNotPossible () throws OrderLifecycleException
    {
        Order o = new Order();
        o.confirm();
        o.cancel();
    }


    @Test( expected = OrderLifecycleException.class )
    public void test_ConfirmAlreadyConfirmed () throws OrderLifecycleException
    {
        Order o = new Order();
        o.confirm();
        o.confirm();
    }


    @Test
    public void test_CookingJustOne () throws OrderLifecycleException
    {
        Order o = new Order(
                UUID.randomUUID(),
                Arrays.asList( buildItem( BigDecimal.valueOf( 50.0 ), 1 ) ),
                buildContact(),
                LocalDateTime.now()
        );

        o.confirm();

        assertEquals( o.getFinishedCookingsCount(), 0 );

        o.cookingAssignmentCompleted();

        assertEquals( o.getFinishedCookingsCount(), 1 );

        assertEquals( o.getStatus(), OrderStatus.Delivering );
    }

    @Test
    public void test_CookingTwo () throws OrderLifecycleException
    {
        Order o = new Order(
                UUID.randomUUID(),
                Arrays.asList( buildItem( BigDecimal.valueOf( 50.0 ), 2 ) ),
                buildContact(),
                LocalDateTime.now()
        );

        o.confirm();

        assertEquals( o.getFinishedCookingsCount(), 0 );

        o.cookingAssignmentCompleted();

        assertEquals( o.getStatus(), OrderStatus.Confirmed );
        assertEquals( o.getFinishedCookingsCount(), 1 );

        o.cookingAssignmentCompleted();

        assertEquals( o.getStatus(), OrderStatus.Delivering );
        assertEquals( o.getFinishedCookingsCount(), 2 );
    }


    @Test( expected = OrderLifecycleException.class )
    public void test_TwoCookNotifications_ButOneItem () throws OrderLifecycleException
    {
        Order o = new Order(
                UUID.randomUUID(),
                Arrays.asList( buildItem( BigDecimal.valueOf( 50.0 ), 1 ) ),
                buildContact(),
                LocalDateTime.now()
        );

        o.cookingAssignmentCompleted();
        o.cookingAssignmentCompleted();
    }


    @Test( expected = OrderLifecycleException.class )
    public void test_CookingBeforeConfirmed () throws OrderLifecycleException
    {
        Order o = new Order(
                UUID.randomUUID(),
                Arrays.asList( buildItem( BigDecimal.valueOf( 50.0 ), 1 ) ),
                buildContact(),
                LocalDateTime.now()
        );

        o.cookingAssignmentCompleted();
    }


    @Test
    public void test_Delivery_AfterCooked () throws OrderLifecycleException
    {
        Order o = new Order(
                UUID.randomUUID(),
                Arrays.asList( buildItem( BigDecimal.valueOf( 50.0 ), 1 ) ),
                buildContact(),
                LocalDateTime.now()
        );

        o.confirm();
        o.cookingAssignmentCompleted();
        o.deliveryCompleted();

        assertEquals( o.getStatus(), OrderStatus.Completed );
    }


    @Test( expected = OrderLifecycleException.class )
    public void test_Delivery_AfterDelivered () throws OrderLifecycleException
    {
        Order o = new Order(
                UUID.randomUUID(),
                Arrays.asList( buildItem( BigDecimal.valueOf( 50.0 ), 1 ) ),
                buildContact(),
                LocalDateTime.now()
        );

        o.confirm();
        o.cookingAssignmentCompleted();
        o.deliveryCompleted();
        o.deliveryCompleted();
    }


    @Test( expected = OrderLifecycleException.class )
    public void test_Delivery_BeforeCooked () throws OrderLifecycleException
    {
        Order o = new Order(
                UUID.randomUUID(),
                Arrays.asList( buildItem( BigDecimal.valueOf( 50.0 ), 1 ) ),
                buildContact(),
                LocalDateTime.now()
        );

        o.confirm();
        o.deliveryCompleted();
    }


    @Test( expected = OrderLifecycleException.class )
    public void test_Delivery_BeforeConfirmed () throws OrderLifecycleException
    {
        Order o = new Order(
                UUID.randomUUID(),
                Arrays.asList( buildItem( BigDecimal.valueOf( 50.0 ), 1 ) ),
                buildContact(),
                LocalDateTime.now()
        );

        o.deliveryCompleted();
    }


    @Test
    public void test_GenerateCookingAssignment_ForSingleItem ()
    {
        ProductItem item = buildItem( BigDecimal.valueOf( 50.0 ), 1 );

        Order o = new Order(
                UUID.randomUUID(),
                Arrays.asList( item ),
                buildContact(),
                LocalDateTime.now()
        );

        List< CookingAssignment > assignments = o.generateCookingAssignments();

        assertThat( assignments, hasSize( 1 ) );

        CookingAssignment ca = assignments.get( 0 );
        assertSame( ca.getStatus(), CookingStatus.Waiting );
        assertSame( ca.getOrder(), o );
        assertSame( ca.getProduct(), item.getProduct() );
        assertSame( ca.getSize(), item.getSize() );
    }


    @Test
    public void test_GenerateCookingAssignment_ForTwoItems ()
    {
        ProductItem item1 = buildItem( BigDecimal.valueOf( 50.0 ), 1 );
        ProductItem item2 = buildItem( BigDecimal.valueOf( 60.0 ), 1 );

        Order o = new Order(
                UUID.randomUUID(),
                Arrays.asList( item1, item2 ),
                buildContact(),
                LocalDateTime.now()
        );

        List< CookingAssignment > assignments = o.generateCookingAssignments();

        assertThat( assignments, hasSize( 2 ) );

        CookingAssignment ca1 = assignments.get( 0 );
        assertEquals( ca1.getStatus(), CookingStatus.Waiting );
        assertSame( ca1.getOrder(), o );
        assertSame( ca1.getProduct(), item1.getProduct() );
        assertSame( ca1.getSize(), item1.getSize() );

        CookingAssignment ca2 = assignments.get( 1 );
        assertEquals( ca2.getStatus(), CookingStatus.Waiting );
        assertSame( ca2.getOrder(), o );
        assertSame( ca2.getProduct(), item2.getProduct() );
        assertSame( ca2.getSize(), item2.getSize() );
    }


    @Test
    public void test_GenerateDelivery ()
    {
        Order o = new Order();
        Delivery d = o.generateDelivery();

        assertSame( d.getOrder(), o );
        assertEquals( d.getStatus(), DeliveryStatus.Waiting );
    }


    private ProductItem buildItem ( BigDecimal price, int quantity )
    {
        return new ProductItem(
                UUID.randomUUID(),
                new Product(),
                new ProductSize(),
                quantity,
                price
        );
    }


    private Contact buildContact ()
    {
        Contact c = new Contact(
                "Ivan",
                "Sumskaya 1",
                "(050)123-45-67",
                "ivan@gmail.com"
        );

        return c;
    }
}
