/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ContactTests
{
    @Test
    public void test_DefaultConstructor ()
    {
        Contact c = new Contact();

        assertEquals( c.getName(), null );
        assertEquals( c.getEmail(), null );
        assertEquals( c.getAddress(), null );
        assertEquals( c.getPhone(), null );
    }


    @Test
    public void test_PublicConstructor ()
    {
        final String TEST_NAME = "Ivan";
        final String TEST_ADDRESS = "Sumskaya 1";
        final String TEST_PHONE = "12345";
        final String TEST_EMAIL = "ivan@gmail.com";

        Contact c = new Contact(
                TEST_NAME,
                TEST_ADDRESS,
                TEST_PHONE,
                TEST_EMAIL
        );

        assertEquals( c.getName(), TEST_NAME );
        assertEquals( c.getAddress(), TEST_ADDRESS );
        assertEquals( c.getPhone(), TEST_PHONE );
        assertEquals( c.getEmail(), TEST_EMAIL );
    }


    @Test
    public void test_Equals ()
    {
        Contact c1 = new Contact(
                "Ivan",
                "Sumskaya 1",
                "(050)123-45-67",
                "ivan@gmail.com"
        );

        Contact c2 = new Contact(
                "Ivan",
                "Sumskaya 1",
                "(050)123-45-67",
                "ivan@gmail.com"
        );

        assertEquals( c1, c2 );
    }


    @Test
    public void test_Equals_Diff_Name ()
    {
        Contact c1 = new Contact(
                "Ivan",
                "Sumskaya 1",
                "(050)123-45-67",
                "ivan@gmail.com"
        );

        Contact c2 = new Contact(
                "Petr",
                "Sumskaya 1",
                "(050)123-45-67",
                "ivan@gmail.com"
        );

        assertNotEquals( c1, c2 );
    }


    @Test
    public void test_Equals_Diff_Email ()
    {
        Contact c1 = new Contact(
                "Ivan",
                "Sumskaya 1",
                "(050)123-45-67",
                "ivan@gmail.com"
        );

        Contact c2 = new Contact(
                "Ivan",
                "Sumskaya 1",
                "(050)123-45-67",
                "ivan@ukr.net"
        );

        assertNotEquals( c1, c2 );
    }


    @Test
    public void test_Equals_Diff_Address ()
    {
        Contact c1 = new Contact(
                "Ivan",
                "Sumskaya 1",
                "(050)123-45-67",
                "ivan@gmail.com"
        );

        Contact c2 = new Contact(
                "Ivan",
                "Sumskaya 2",
                "(050)123-45-67",
                "ivan@gmail.com"
        );

        assertNotEquals( c1, c2 );
    }


    @Test
    public void test_Equals_Diff_Phone ()
    {
        Contact c1 = new Contact(
                "Ivan",
                "Sumskaya 1",
                "(050)123-45-67",
                "ivan@gmail.com"
        );

        Contact c2 = new Contact(
                "Ivan",
                "Sumskaya 1",
                "(097)123-45-67",
                "ivan@gmail.com"
        );

        assertNotEquals( c1, c2 );
    }
}
