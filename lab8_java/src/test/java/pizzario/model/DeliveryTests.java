/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import org.junit.Test;
import pizzario.exceptions.DeliveryLifecycleException;

import java.math.BigDecimal;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class DeliveryTests
{
    @Test
    public void test_DefaultConstructor ()
    {
        Delivery d = new Delivery();

        assertEquals( d.getDomainId(), null );
        assertEquals( d.getOrder(), null );
        assertEquals( d.getDriverName(), null );
        assertEquals( d.getCash2Collect(), null );
        assertEquals( d.getStatus(), DeliveryStatus.Waiting );
    }

    @Test
    public void test_PublicConstructor ()
    {
        final UUID TEST_ID = UUID.randomUUID();
        final Order TEST_ORDER = new Order();

        Delivery d = new Delivery(
                TEST_ID,
                TEST_ORDER
        );

        assertEquals( d.getDomainId(), TEST_ID );
        assertSame( d.getOrder(), TEST_ORDER );
        assertEquals( d.getDriverName(), null );
        assertEquals( d.getCash2Collect(), BigDecimal.ZERO );
        assertEquals( d.getStatus(), DeliveryStatus.Waiting );
    }


    @Test
    public void test_SetCash2Collect ()
    {
        Delivery d = new Delivery();
        d.setCash2Collect( BigDecimal.valueOf( 100.00 ) );
        assertEquals( d.getCash2Collect(), BigDecimal.valueOf(  100.00 ) );
    }


    @Test
    public void test_StartDelivery () throws Exception
    {
        Delivery d = new Delivery();
        d.startDelivery( "Fedor Petrovich" );

        assertEquals( d.getDriverName(), "Fedor Petrovich" );
        assertEquals( d.getStatus(), DeliveryStatus.InProgress );
    }


    @Test( expected = DeliveryLifecycleException.class )
    public void test_StartDelivery_AlreadyStarted () throws Exception
    {
        Delivery d = new Delivery();
        d.startDelivery( "Fedor Petrovich" );
        d.startDelivery( "Fedor Petrovich" );
    }


    @Test
    public void test_StartAndFinishDelivery () throws Exception
    {
        Delivery d = new Delivery();

        d.startDelivery( "Fedor Petrovich" );
        d.finishDelivery();

        assertEquals( d.getStatus(), DeliveryStatus.Delivered );
    }


    @Test( expected = DeliveryLifecycleException.class )
    public void test_FinishDelivery_WithoutStarting () throws Exception
    {
        Delivery d = new Delivery();
        d.finishDelivery();
    }


    @Test( expected = DeliveryLifecycleException.class )
    public void test_FinishDelivery_AlreadyFinished () throws Exception
    {
        Delivery d = new Delivery();
        d.startDelivery( "Fedor Petrovich" );
        d.finishDelivery();
        d.finishDelivery();
    }
}
