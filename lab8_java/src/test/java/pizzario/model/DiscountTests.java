/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class DiscountTests
{
    @Test
    public void test_DefaultConstructor ()
    {
        Discount d = new Discount();
        assertEquals( d.getPercent(), BigDecimal.ZERO );
    }

    @Test
    public void test_ConcreteConstructor ()
    {
        Discount d = new Discount( BigDecimal.valueOf( 20.0 ) );
        assertEquals( d.getPercent(), BigDecimal.valueOf( 20.0 ) );
    }

    @Test
    public void test_Equals ()
    {
        Discount d1 = new Discount( BigDecimal.valueOf( 20.0 ) );
        Discount d2 = new Discount( BigDecimal.valueOf( 20.0 ) );
        Discount d3 = new Discount( BigDecimal.valueOf( 20.1 ) );

        assertEquals( d1, d2 );
        assertNotEquals( d1, d3 );
    }

    @Test
    public void test_DiscountedPrice_0percent ()
    {
        Discount d = new Discount();
        assertEquals(
                d.getDiscountedPrice( BigDecimal.valueOf( 300.00 ) ),
                BigDecimal.valueOf( 300.00 )
        );
    }

    @Test
    public void test_DiscountedPrice_20percent ()
    {
        Discount d = new Discount( BigDecimal.valueOf( 20.0 ) );
        assertEquals(
                d.getDiscountedPrice( BigDecimal.valueOf( 300 ) ),
                BigDecimal.valueOf( 240.00 )
        );
    }

    @Test
    public void test_DiscountedPrice_100percent ()
    {
        Discount d = new Discount( BigDecimal.valueOf( 100.00 ));
        assertEquals(
                d.getDiscountedPrice( BigDecimal.valueOf( 300.00 ) ),
                BigDecimal.valueOf( 0.00 )
        );
    }

}
