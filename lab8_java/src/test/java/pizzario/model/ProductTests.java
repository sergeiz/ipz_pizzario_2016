/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import org.junit.Test;
import pizzario.exceptions.PriceUndefinedException;
import pizzario.exceptions.RecipeUndefinedException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class ProductTests
{
    @Test
    public void test_DefaultConstructor ()
    {
        Product p = new Product();

        assertEquals( p.getDomainId(), null );
        assertEquals( p.getName(), null );
        assertEquals( p.getImageUrl(), null );
    }


    @Test
    public void test_PublicConstructor ()
    {
        final UUID TEST_ID = UUID.randomUUID();
        final String TEST_NAME = "Milano";
        final String TEST_IMAGE_URL = "/resources/milano.png";

        Product p = new Product(
                TEST_ID,
                TEST_NAME,
                TEST_IMAGE_URL
        );

        assertEquals( p.getDomainId(), TEST_ID );
        assertEquals( p.getName(), TEST_NAME );
        assertEquals( p.getImageUrl(), TEST_IMAGE_URL );
    }


    @Test
    public void test_SetName ()
    {
        Product p = new Product();
        p.setName( "Georgia" );
        assertEquals( p.getName(), "Georgia" );
    }


    @Test
    public void test_SetImageUrl ()
    {
        Product p = new Product();
        p.setImageUrl( "georgia.png" );
        assertEquals( p.getImageUrl(), "georgia.png" );
    }


    @Test
    public void test_DescriptionWithoutRecipes ()
    {
        Product p = new Product();
        assertEquals( p.buildDescription(), "" );
    }


    @Test
    public void test_Description_RecipeNoIngredients ()
    {
        Product p = new Product();
        p.defineRecipe( makeSize() );
        assertEquals( p.buildDescription(), "" );
    }


    @Test
    public void test_DescriptionOneIngredient ()
    {
        Ingredient ham = makeIngredient( "Ham" );

        Product p = new Product();
        p.defineRecipe( makeSize() )
                .useIngredient( ham, 100 )
                ;

        assertEquals( p.buildDescription(), "Ham" );
    }


    @Test
    public void test_DescriptionTwoIngredients ()
    {
        Ingredient ham = makeIngredient( "Ham" );
        Ingredient cheese = makeIngredient( "Cheese" );

        Product p = new Product();
        p.defineRecipe( makeSize() )
                .useIngredient( ham, 100 )
                .useIngredient( cheese, 50 )
                ;

        assertEquals( p.buildDescription(), "Cheese • Ham" );
    }


    @Test
    public void test_DefineRecipeForSize () throws Exception
    {
        ProductSize sz = makeSize();
        Product p = new Product();
        Recipe r = p.defineRecipe( sz );
        assertSame( r, p.getRecipe( sz ) );

        List< Recipe > list = new ArrayList<>();
        p.listAvailableRecipes().forEach( list::add );
        assertThat( list, hasSize( 1 ) );
        assertThat( list, contains( r ) );
    }


    @Test
    public void test_DefineRecipeForSameSize () throws Exception
    {
        ProductSize sz = makeSize();
        Product p = new Product();
        Recipe r1 = p.defineRecipe( sz );
        Recipe r2 = p.defineRecipe( sz );
        assertSame( r1, r2 );
    }


    @Test
    public void test_DefineRecipeForTwoSizes () throws Exception
    {
        ProductSize sz1 = makeSize();
        ProductSize sz2 = makeSize();
        Product p = new Product();

        Recipe r1 = p.defineRecipe( sz1 );
        Recipe r2 = p.defineRecipe( sz2 );

        assertNotSame( r1, r2 );
        assertSame( p.getRecipe( sz1 ), r1 );
        assertSame( p.getRecipe( sz2 ), r2 );


        List< Recipe > list = new ArrayList<>();
        p.listAvailableRecipes().forEach( list::add );
        assertThat( list, hasSize( 2 ) );
        assertThat( list, containsInAnyOrder( r1, r2 ) );
    }


    @Test( expected = RecipeUndefinedException.class )
    public void test_GetRecipeUnregisteredSize () throws Exception
    {
        Product p = new Product();
        ProductSize sz = makeSize();
        p.getRecipe( sz );
    }


    @Test
    public void test_RemoveRegisteredRecipe () throws Exception
    {
        Product p = new Product();
        ProductSize sz = makeSize();
        p.defineRecipe( sz );
        p.removeRecipe( sz );
    }


    @Test( expected = RecipeUndefinedException.class )
    public void test_RemoveUnregisteredRecipe () throws Exception
    {
        Product p = new Product();
        p.removeRecipe( makeSize() );
    }


    @Test
    public void test_SetPrice_OneSize () throws Exception
    {
        Product p = new Product();
        ProductSize sz = makeSize();
        p.setPrice( sz, BigDecimal.valueOf( 100.00 ) );
        assertEquals( p.getPrice( sz ), BigDecimal.valueOf( 100.00 ) );

        List< ProductSize > sizes = new ArrayList<>();
        p.listAvailableSizes().forEach( sizes::add );
        assertThat( sizes, hasSize( 1 ) );
        assertThat( sizes, contains( sz ) );
    }


    @Test
    public void test_SetPrice_TwoSizes () throws Exception
    {
        Product p = new Product();
        ProductSize sz1 = makeSize();
        ProductSize sz2 = makeSize();
        p.setPrice( sz1, BigDecimal.valueOf( 100.00 ) );
        p.setPrice( sz2, BigDecimal.valueOf( 200.00 ) );
        assertEquals( p.getPrice( sz1 ), BigDecimal.valueOf( 100.00 ) );
        assertEquals( p.getPrice( sz2 ), BigDecimal.valueOf( 200.00 ) );

        List< ProductSize > sizes = new ArrayList<>();
        p.listAvailableSizes().forEach( sizes::add );
        assertThat( sizes, hasSize( 2 ) );
        assertThat( sizes, containsInAnyOrder( sz1, sz2 ) );
    }


    @Test
    public void test_SetPrice_SameSize () throws Exception
    {
        Product p = new Product();
        ProductSize sz = makeSize();
        p.setPrice( sz, BigDecimal.valueOf( 100.00 ) );
        p.setPrice( sz, BigDecimal.valueOf( 200.00 ) );
        assertEquals( p.getPrice( sz ), BigDecimal.valueOf( 200.00 ) );
    }


    @Test( expected = PriceUndefinedException.class )
    public void test_GetPrice_UnregisteredSize () throws Exception
    {
        Product p = new Product();
        p.getPrice( makeSize() );
    }


    @Test
    public void test_RemovePrice () throws Exception
    {
        Product p = new Product();

        ProductSize sz = makeSize();
        p.setPrice( sz, BigDecimal.valueOf( 100.00 ) );
        p.removePrice( sz );

        assertTrue( ! p.listAvailableSizes().iterator().hasNext() );
        assertTrue( ! p.listAvailablePrices().iterator().hasNext() );
    }


    @Test( expected = PriceUndefinedException.class )
    public void test_RemoveUnregisteredPrice () throws Exception
    {
        Product p = new Product();
        p.removePrice( makeSize() );
    }


    private Ingredient makeIngredient ( String name )
    {
        return new Ingredient( UUID.randomUUID(), name );
    }


    private ProductSize makeSize ()
    {
        return new ProductSize( UUID.randomUUID(), "Small", "small.png", 25, 300 );
    }
}
