/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import pizzario.dto.*;
import pizzario.exceptions.DuplicateNamedEntityException;
import pizzario.exceptions.ServiceValidationException;
import pizzario.service.IIngredientService;
import pizzario.service.IProductService;
import pizzario.service.IProductSizeService;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = pizzario.configuration.JUnitConfiguration.class )
@Transactional
public class ProductServiceTests
{
    @Test
    public void test_NoProductsByDefault ()
    {
        assertThat( productService.viewAll(), empty() );
    }


    @Test
    public void test_CreateSingleProduct () throws Exception
    {
        UUID productId = createProduct();

        assertEquals( productService.viewAll(), Arrays.asList( productId ) );

        assertEquals(
                productService.view( productId ),
                new ProductDto( productId, "Milano", "milano.png", "" )
        );
    }


    @Test
    public void test_CreateTwoProducts () throws Exception
    {
        UUID productMilano = createProduct();
        UUID productGeorgia = productService.create( "Georgia", "georgia.png" );

        assertThat( productService.viewAll(), hasSize( 2 ) );

        assertThat(
                productService.viewAll(),
                containsInAnyOrder( productMilano, productGeorgia )
        );

        assertEquals(
                productService.view( productMilano ),
                new ProductDto( productMilano, "Milano", "milano.png", "" )
        );

        assertEquals(
                productService.view( productGeorgia ),
                new ProductDto( productGeorgia, "Georgia", "georgia.png", "" )
        );
    }


    @Test( expected = DuplicateNamedEntityException.class )
    public void test_CreateDuplicateProduct () throws Exception
    {
        productService.create( "Milano", "milano.png" );
        productService.create( "Milano", "milano.png" );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_CreateProduct_BlankName () throws Exception
    {
        productService.create( "", "milano.png" );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_CreateProduct_BlankImageUrl () throws Exception
    {
        productService.create( "Milano", "" );
    }


    @Test
    public void test_RenameProduct () throws Exception
    {
        UUID productId = productService.create( "Milan", "milano.png" );
        productService.rename( productId, "Milano" );

        assertEquals(
                productService.view( productId ),
                new ProductDto( productId, "Milano", "milano.png", "" )
        );
    }


    @Test( expected = DuplicateNamedEntityException.class )
    public void test_RenameProduct_ToExisting () throws Exception
    {
        UUID productId = createProduct();
        productService.create( "Georgia", "georgia.png" );
        productService.rename( productId, "Georgia" );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_RenameProduct_ToBlank () throws Exception
    {
        UUID productId = createProduct();
        productService.rename( productId, "" );
    }


    @Test
    public void test_UpdateImageUrl () throws Exception
    {
        UUID productId = createProduct();
        productService.updateImageUrl( productId, "milano.png" );

        assertEquals(
                productService.view( productId ),
                new ProductDto( productId, "Milano", "milano.png", "" )
        );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_UpdateImageUrl_ToBlank () throws Exception
    {
        UUID productId = createProduct();
        productService.updateImageUrl( productId, "" );
    }


    @Test
    public void test_ViewPrices_WithoutDefining () throws Exception
    {
        UUID productId = createProduct();

        ProductPricingDto pricingDto = productService.viewPrices( productId );
        assertTrue( pricingDto.getPricesBySizeId().isEmpty() );
    }


    @Test
    public void test_DefinePrice () throws Exception
    {
        UUID productId = createProduct();
        UUID sizeId = createSmallSize();
        productService.definePrice( productId, sizeId, BigDecimal.valueOf( 100.00 ) );

        Map< UUID, BigDecimal > pricesBySize = new TreeMap<>();
        pricesBySize.put( sizeId, BigDecimal.valueOf( 100.00 ) );

        Map< UUID, String > sizesByName = new TreeMap<>();
        sizesByName.put( sizeId, "Small" );

        assertEquals(
                productService.viewPrices( productId ),
                new ProductPricingDto(
                        productId,
                        pricesBySize,
                        sizesByName
                )
        );

        assertEquals(
                productService.availableSizes( productId ),
                Arrays.asList(
                        new ProductSizeDto(
                                sizeId,
                                "Small",
                                "small.png",
                                25,
                                300
                        )
                )
        );
    }


    @Test
    public void test_DefinePrice_TwoSizes () throws Exception
    {
        UUID productId = createProduct();

        UUID sizeSmallId = createSmallSize();
        UUID sizeLargeId = productSizeService.create( "Large", "large.png", 35, 900 );

        productService.definePrice( productId, sizeSmallId, BigDecimal.valueOf( 100.00 ) );
        productService.definePrice( productId, sizeLargeId, BigDecimal.valueOf( 150.00 ) );

        Map< UUID, BigDecimal > pricesBySize = new HashMap<>();
        pricesBySize.put( sizeSmallId, BigDecimal.valueOf( 100.00 ) );
        pricesBySize.put( sizeLargeId, BigDecimal.valueOf( 150.00 ) );

        Map< UUID, String > sizesByName = new HashMap<>();
        sizesByName.put( sizeSmallId, "Small" );
        sizesByName.put( sizeLargeId, "Large" );

        assertEquals(
                productService.viewPrices( productId ),
                new ProductPricingDto(
                        productId,
                        pricesBySize,
                        sizesByName
                )
        );

        assertThat(
                productService.availableSizes( productId ),
                hasSize( 2 )
        );

        assertThat(
                productService.availableSizes( productId ),
                containsInAnyOrder(
                        new ProductSizeDto(
                                sizeSmallId,
                                "Small",
                                "small.png",
                                25,
                                300
                        ),
                        new ProductSizeDto(
                                sizeLargeId,
                                "Large",
                                "large.png",
                                35,
                                900
                        )
                )
        );
    }


    @Test
    public void test_DefineSamePriceTwice () throws Exception
    {
        UUID productId = createProduct();
        UUID sizeId = createSmallSize();
        productService.definePrice( productId, sizeId, BigDecimal.valueOf( 100.00 ) );
        productService.definePrice( productId, sizeId, BigDecimal.valueOf( 150.00 ) );

        assertEquals(
                productService.viewPrices( productId ).getPricesBySizeId().get( sizeId ),
                BigDecimal.valueOf( 150.00 )
        );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_DefinePrice_Zero () throws Exception
    {
        UUID productId = createProduct();
        UUID sizeId = createSmallSize();
        productService.definePrice( productId, sizeId, BigDecimal.ZERO );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_DefinePrice_Negative () throws Exception
    {
        UUID productId = createProduct();
        UUID sizeId = createSmallSize();
        productService.definePrice( productId, sizeId, BigDecimal.valueOf( -1 ) );
    }


    @Test
    public void test_UndefinePrice () throws Exception
    {
        UUID productId = createProduct();
        UUID sizeId = createSmallSize();
        productService.definePrice( productId, sizeId, BigDecimal.valueOf( 100.00 ) );
        productService.undefinePrice( productId, sizeId );

        assertThat( productService.availableSizes( productId ), empty() );
    }


    @Test
    public void test_AvailableRecipes_NotYetDefined () throws Exception
    {
        UUID productId = createProduct();
        assertThat(
                productService.availableRecipes( productId ),
                empty()
        );
    }


    @Test
    public void test_AddIngredient () throws Exception
    {
        UUID productId = createProduct();
        UUID sizeId = createSmallSize();
        UUID ingredientId = createHam();

        productService.defineRecipeIngredient( productId, sizeId, ingredientId, 100 );

        List< ProductRecipeDto > recipes = productService.availableRecipes( productId );
        assertThat( recipes, hasSize( 1 ) );

        Map< IngredientDto, Integer > usedIngredients = new HashMap<>();
        usedIngredients.put( new IngredientDto( ingredientId, "Ham" ), 100 );

        ProductRecipeDto recipeDto = recipes.get( 0 );
        assertEquals(
                recipeDto,
                new ProductRecipeDto( productId, sizeId, usedIngredients )
        );
    }


    @Test
    public void test_AddTwoIngredients () throws Exception
    {
        UUID productId = createProduct();
        UUID sizeId = createSmallSize();
        UUID ingredientHamId = createHam();
        UUID ingredientCheeseId = ingredientService.create( "Cheese" );

        productService.defineRecipeIngredient( productId, sizeId, ingredientHamId, 100 );
        productService.defineRecipeIngredient( productId, sizeId, ingredientCheeseId, 80 );

        Map< IngredientDto, Integer > usedIngredients = new HashMap<>();
        usedIngredients.put( new IngredientDto( ingredientHamId, "Ham" ), 100 );
        usedIngredients.put( new IngredientDto( ingredientCheeseId, "Cheese" ), 80 );

        ProductRecipeDto recipeDto = productService.viewRecipe( productId, sizeId );
        assertEquals(
                recipeDto,
                new ProductRecipeDto( productId, sizeId, usedIngredients )
        );
    }


    @Test
    public void test_AddSameIngredient() throws Exception
    {
        UUID productId = createProduct();
        UUID sizeId = createSmallSize();
        UUID ingredientHamId = createHam();

        productService.defineRecipeIngredient( productId, sizeId, ingredientHamId, 100 );
        productService.defineRecipeIngredient( productId, sizeId, ingredientHamId, 150 );

        Map< IngredientDto, Integer > usedIngredients = new HashMap<>();
        usedIngredients.put( new IngredientDto( ingredientHamId, "Ham" ), 150 );

        ProductRecipeDto recipeDto = productService.viewRecipe( productId, sizeId );
        assertEquals(
                recipeDto,
                new ProductRecipeDto( productId, sizeId, usedIngredients )
        );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_AddIngredient_NegativeWeight () throws Exception
    {
        UUID productId = createProduct();
        UUID sizeId = createSmallSize();
        UUID ingredientHamId = createHam();
        productService.defineRecipeIngredient( productId, sizeId, ingredientHamId, -1 );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_AddIngredient_ZeroWeight () throws Exception
    {
        UUID productId = createProduct();
        UUID sizeId = createSmallSize();
        UUID ingredientHamId = createHam();
        productService.defineRecipeIngredient( productId, sizeId, ingredientHamId, 0 );
    }


    @Test
    public void test_AddRemoveIngredient () throws Exception
    {
        UUID productId = createProduct();
        UUID sizeId = createSmallSize();
        UUID ingredientHamId = createHam();

        productService.defineRecipeIngredient( productId, sizeId, ingredientHamId, 100 );
        productService.removeRecipeIngredient( productId, sizeId, ingredientHamId );

        ProductRecipeDto recipeDto = productService.viewRecipe( productId, sizeId );
        assertEquals(
                recipeDto,
                new ProductRecipeDto( productId, sizeId, new HashMap< IngredientDto, Integer >(  ) )
        );
    }


    private UUID createProduct () throws DuplicateNamedEntityException
    {
        return productService.create( "Milano", "milano.png"  );
    }


    private UUID createSmallSize () throws DuplicateNamedEntityException
    {
        return productSizeService.create( "Small", "small.png", 25, 300 );
    }


    private UUID createHam () throws DuplicateNamedEntityException
    {
        return ingredientService.create( "Ham" );
    }


    @Inject
    private IProductService productService;

    @Inject
    private IProductSizeService productSizeService;

    @Inject
    private IIngredientService ingredientService;
}
