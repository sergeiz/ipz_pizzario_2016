/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import pizzario.dto.OrderDto;
import pizzario.exceptions.ModifiableCartException;
import pizzario.exceptions.OrderLifecycleException;
import pizzario.exceptions.ServiceUnresolvedEntityException;
import pizzario.exceptions.ServiceValidationException;
import pizzario.service.*;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = pizzario.configuration.JUnitConfiguration.class )
@Transactional
public class OrderServiceTests
{
    @Before
    public void setup () throws Exception
    {
        TEST_CART_ID = shoppingCartService.createNew();
        TEST_CART2_ID = shoppingCartService.createNew();

        TEST_PRODUCT_ID = productService.create( "Milano", "milano.png" );
        TEST_SIZE_ID = productSizeService.create( "Small", "small.png", 25, 300 );
        TEST_SIZE2_ID = productSizeService.create( "Large", "large.png", 35, 900 );

        TEST_OPERATOR_ID = accountService.createOperator( "Wasya", "wasya@pizzario.com", "12345" );

        productService.definePrice( TEST_PRODUCT_ID, TEST_SIZE_ID, BigDecimal.valueOf( PRICE ) );
        productService.definePrice( TEST_PRODUCT_ID, TEST_SIZE2_ID, BigDecimal.valueOf( PRICE2 ) );

        shoppingCartService.addItem( TEST_CART_ID, TEST_PRODUCT_ID, TEST_SIZE_ID );
        shoppingCartService.lock( TEST_CART_ID );

        shoppingCartService.addItem( TEST_CART2_ID, TEST_PRODUCT_ID, TEST_SIZE2_ID );
        shoppingCartService.lock( TEST_CART2_ID );
    }


    @Test
    public void test_NoOrdersInitially ()
    {
        assertThat( orderService.viewAll(), empty() );
    }


    @Test
    public void test_CreateOrder () throws Exception
    {
        UUID orderId = orderService.createNew(
                "Ivan",
                "Sumskaya 1",
                "(097)123-45-67",
                "ivan@gmail.com",
                "some comment",
                TEST_CART_ID
        );

        List< UUID > allOrders = orderService.viewAll();
        assertThat( allOrders, hasSize( 1 ) );
        assertThat( allOrders, hasItem( orderId ) );

        assertEquals( allOrders, orderService.viewUnconfirmed() );
        assertThat( orderService.viewConfirmed(), empty() );
        assertThat( orderService.viewReady4Delivery(), empty() );

        OrderDto orderDto = orderService.view( orderId );
        assertEquals( orderDto.getDomainId(), orderId );
        assertEquals( orderDto.getCustomerName(), "Ivan" );
        assertEquals( orderDto.getCustomerAddress(), "Sumskaya 1" );
        assertEquals( orderDto.getContactPhone(), "(097)123-45-67" );
        assertEquals( orderDto.getContactEmail(), "ivan@gmail.com" );
        assertEquals( orderDto.getComment(), "some comment" );
        assertEquals( orderDto.getTotalCookingsCount(), 1 );
        expectOrderCosts( orderDto, PRICE, NO_DISCOUNT, PRICE );

        assertThat( shoppingCartService.viewAll(), not( hasItem( TEST_CART_ID ) ) );

        assertEquals( orderDto, orderService.findOrder( orderId ) );
    }


    @Test
    public void test_CreateTwoOrders () throws Exception
    {
        UUID order1Id = orderService.createNew(
                "Ivan",
                "Sumskaya 1",
                "(097)123-45-67",
                "ivan@gmail.com",
                "some comment",
                TEST_CART_ID
        );

        UUID order2Id = orderService.createNew(
                "Petr",
                "Sumskaya 2",
                "(050)765-43-21",
                "petr@gmail.com",
                "another comment",
                TEST_CART2_ID
        );

        List< UUID > allOrders = orderService.viewAll();
        assertThat( allOrders, hasSize( 2 ) );
        assertThat( allOrders, hasItem( order1Id ) );
        assertThat( allOrders, hasItem( order2Id ) );

        List< UUID > unconfirmedOrders = orderService.viewUnconfirmed();
        assertThat( unconfirmedOrders, hasSize( 2 ) );
        assertThat( unconfirmedOrders, hasItem( order1Id ) );
        assertThat( unconfirmedOrders, hasItem( order2Id ) );

        assertThat( orderService.viewConfirmed(), empty() );
        assertThat( orderService.viewReady4Delivery(), empty() );


        OrderDto order1Dto = orderService.view( order1Id );
        assertEquals( order1Dto.getDomainId(), order1Id );
        assertEquals( order1Dto.getCustomerName(), "Ivan" );
        assertEquals( order1Dto.getCustomerAddress(), "Sumskaya 1" );
        assertEquals( order1Dto.getContactPhone(), "(097)123-45-67" );
        assertEquals( order1Dto.getContactEmail(), "ivan@gmail.com" );
        assertEquals( order1Dto.getComment(), "some comment" );
        assertEquals( order1Dto.getTotalCookingsCount(), 1 );
        expectOrderCosts( order1Dto, PRICE, NO_DISCOUNT, PRICE );

        OrderDto order2Dto = orderService.view( order2Id );
        assertEquals( order2Dto.getDomainId(), order2Id );
        assertEquals( order2Dto.getCustomerName(), "Petr" );
        assertEquals( order2Dto.getCustomerAddress(), "Sumskaya 2" );
        assertEquals( order2Dto.getContactPhone(), "(050)765-43-21" );
        assertEquals( order2Dto.getContactEmail(), "petr@gmail.com" );
        assertEquals( order2Dto.getComment(), "another comment" );
        assertEquals( order2Dto.getTotalCookingsCount(), 1 );
        expectOrderCosts( order2Dto, PRICE2, NO_DISCOUNT, PRICE2 );

        assertThat( shoppingCartService.viewAll(),
                allOf(
                        not( hasItem( TEST_CART_ID ) ),
                        not( hasItem( TEST_CART2_ID ) )
                )
        );

        assertEquals( order1Dto, orderService.findOrder( order1Id ) );
        assertEquals( order2Dto, orderService.findOrder( order2Id ) );
    }


    @Test( expected = ModifiableCartException.class )
    public void test_CreateOrder_WithEmptyCart () throws Exception
    {
        UUID EMPTY_CART_ID = shoppingCartService.createNew();
        orderService.createNew(
                CUSTOMER_NAME,
                CUSTOMER_ADDRESS,
                CUSTOMER_PHONE,
                CUSTOMER_EMAIL,
                "some comment",
                EMPTY_CART_ID
        );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_CreateOrder_BlankCustomer () throws Exception
    {
        orderService.createNew(
                "",
                CUSTOMER_ADDRESS,
                CUSTOMER_PHONE,
                CUSTOMER_EMAIL,
                "some comment",
                TEST_CART_ID
        );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_CreateOrder_BlankAddress () throws Exception
    {
        orderService.createNew(
                CUSTOMER_NAME,
                "",
                CUSTOMER_PHONE,
                CUSTOMER_EMAIL,
                "some comment",
                TEST_CART_ID
        );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_CreateOrder_BlankPhone () throws Exception
    {
        orderService.createNew(
                CUSTOMER_NAME,
                CUSTOMER_ADDRESS,
                "",
                CUSTOMER_EMAIL,
                "some comment",
                TEST_CART_ID
        );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_CreateOrder_BadPhone () throws Exception
    {
        orderService.createNew(
                CUSTOMER_NAME,
                CUSTOMER_ADDRESS,
                "DEFINITELY I'm NOT A PHONE NUMBER",
                CUSTOMER_EMAIL,
                "some comment",
                TEST_CART_ID
        );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_CreateOrder_BlankEmail () throws Exception
    {
        orderService.createNew(
                CUSTOMER_NAME,
                CUSTOMER_ADDRESS,
                CUSTOMER_PHONE,
                "",
                "some comment",
                TEST_CART_ID
        );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_CreateOrder_BadEmail () throws Exception
    {
        orderService.createNew(
                CUSTOMER_NAME,
                CUSTOMER_ADDRESS,
                CUSTOMER_PHONE,
                "DO I LOOK LIKE AN E-MAIL?",
                "some comment",
                TEST_CART_ID
        );
    }


    @Test
    public void test_CreateOrder_EmptyCommentIsFine () throws Exception
    {
        UUID orderId = orderService.createNew(
                CUSTOMER_NAME,
                CUSTOMER_ADDRESS,
                CUSTOMER_PHONE,
                CUSTOMER_EMAIL,
                "", /* empty */
                TEST_CART_ID
        );

        OrderDto orderDto = orderService.view( orderId );
        assertEquals( orderDto.getComment(), "" );
    }


    @Test
    public void test_FindMissingOrder ()
    {
        assertNull( orderService.findOrder( UUID.randomUUID() ) );
    }


    @Test
    @WithMockUser( authorities = "OPERATOR" )
    public void test_SetDiscount () throws Exception
    {
        UUID orderId = createSimpleOrder();
        orderService.setDiscount( orderId, BigDecimal.valueOf( 20.00 ) );

        OrderDto orderDto = orderService.view( orderId );
        BigDecimal basicCost = orderDto.getBasicCost();
        BigDecimal totalCost = orderDto.getTotalCost();

        assertEquals(
                basicCost.multiply( BigDecimal.valueOf( 0.80 ) ),
                totalCost
        );
    }


    @Test
    @WithMockUser( authorities = "OPERATOR" )
    public void test_SetFullDiscount () throws Exception
    {
        UUID orderId = createSimpleOrder();
        orderService.setDiscount( orderId, BigDecimal.valueOf( 100.00 ) );

        OrderDto orderDto = orderService.view( orderId );
        BigDecimal totalCost = orderDto.getTotalCost();

        assertEquals( totalCost.setScale( 2 ), BigDecimal.ZERO.setScale( 2 ) );
    }


    @Test
    @WithMockUser( authorities = "OPERATOR" )
    public void test_SetNoDiscount () throws Exception
    {
        UUID orderId = createSimpleOrder();
        orderService.setDiscount( orderId, BigDecimal.ZERO );

        OrderDto orderDto = orderService.view( orderId );
        BigDecimal basicCost = orderDto.getBasicCost();
        BigDecimal totalCost = orderDto.getTotalCost();

        assertEquals( basicCost, totalCost );
    }


    @Test( expected = ServiceValidationException.class )
    @WithMockUser( authorities = "OPERATOR" )
    public void test_SetNegativeDiscount () throws Exception
    {
        UUID orderId = createSimpleOrder();
        orderService.setDiscount( orderId, BigDecimal.valueOf( -0.01 ) );
    }


    @Test( expected = ServiceValidationException.class )
    @WithMockUser( authorities = "OPERATOR" )
    public void test_SetTooLargeDiscount () throws Exception
    {
        UUID orderId = createSimpleOrder();
        orderService.setDiscount( orderId, BigDecimal.valueOf( 100.01 ) );
    }


    @Test( expected = AccessDeniedException.class )
    @WithAnonymousUser
    public void test_SetDiscount_WithoutPermissions () throws Exception
    {
        UUID orderId = createSimpleOrder();
        orderService.setDiscount( orderId, BigDecimal.valueOf( 20.00 ) );
    }


    @Test
    @WithMockUser( authorities = "OPERATOR" )
    public void test_ConfirmOrder () throws Exception
    {
        UUID orderId = createSimpleOrder();
        orderService.confirm( orderId, TEST_OPERATOR_ID );

        OrderDto orderDto = orderService.view( orderId );
        assertEquals( orderDto.getStatus(), "Confirmed" );

        assertThat(
                accountService.viewAssociatedOrders( TEST_OPERATOR_ID ),
                hasItem( orderId )
        );

        List< UUID > confirmedOrders = orderService.viewConfirmed();
        assertThat( confirmedOrders, hasSize( 1 ) );
        assertThat( confirmedOrders, hasItem( orderId ) );

        assertThat( orderService.viewUnconfirmed(), empty() );
        assertThat( orderService.viewReady4Delivery(), empty() );
    }


    @Test( expected = OrderLifecycleException.class )
    @WithMockUser( authorities = "OPERATOR" )
    public void test_ConfirmOrderTwice () throws Exception
    {
        UUID orderId = createSimpleOrder();
        orderService.confirm( orderId, TEST_OPERATOR_ID );
        orderService.confirm( orderId, TEST_OPERATOR_ID );
    }


    @Test( expected = AccessDeniedException.class )
    @WithAnonymousUser
    public void test_ConfirmOrder_NoPermission () throws Exception
    {
        UUID orderId = createSimpleOrder();
        orderService.confirm( orderId, TEST_OPERATOR_ID );
    }


    @Test
    @WithMockUser( authorities = "OPERATOR" )
    public void test_CancelOrder () throws Exception
    {
        UUID orderId = createSimpleOrder();
        orderService.cancel( orderId );

        OrderDto orderDto = orderService.view( orderId );
        assertEquals( orderDto.getStatus(), "Cancelled" );
    }


    @Test( expected = OrderLifecycleException.class )
    @WithMockUser( authorities = "OPERATOR" )
    public void test_CancelConfirmed () throws Exception
    {
        UUID orderId = createSimpleOrder();
        orderService.confirm( orderId, TEST_OPERATOR_ID );
        orderService.cancel( orderId );
    }


    @Test( expected = AccessDeniedException.class )
    @WithAnonymousUser
    public void test_Cancel_NoPermission () throws Exception
    {
        UUID orderId = createSimpleOrder();
        orderService.cancel( orderId );
    }


    @Test( expected = ServiceUnresolvedEntityException.class )
    @WithMockUser( authorities = "OPERATOR" )
    public void test_ConfirmByUnknownUser () throws Exception
    {
        UUID orderId = createSimpleOrder();
        orderService.confirm( orderId, UUID.randomUUID() );
    }



    private UUID createSimpleOrder () throws Exception
    {
        return orderService.createNew(
                CUSTOMER_NAME,
                CUSTOMER_ADDRESS,
                CUSTOMER_PHONE,
                CUSTOMER_EMAIL,
                "",
                TEST_CART_ID
        );
    }


    private void expectOrderCosts ( OrderDto orderDto, double basicCost, double discountPercentage, double totalCost )
    {
        assertEquals(
                orderDto.getBasicCost().setScale( 2 ),
                BigDecimal.valueOf( basicCost ).setScale(  2  )
        );

        assertEquals(
                orderDto.getDiscountPercentage().setScale( 2 ),
                BigDecimal.valueOf( discountPercentage ).setScale(  2  )
        );

        assertEquals(
                orderDto.getTotalCost().setScale( 2 ),
                BigDecimal.valueOf( totalCost ).setScale(  2  )
        );
    }


    @Inject
    private IOrderService orderService;

    @Inject
    private IShoppingCartService shoppingCartService;

    @Inject
    private IProductService productService;

    @Inject
    private IProductSizeService productSizeService;

    @Inject
    private IAccountService accountService;


    private UUID TEST_CART_ID, TEST_CART2_ID;
    private UUID TEST_PRODUCT_ID;
    private UUID TEST_SIZE_ID, TEST_SIZE2_ID;
    private UUID TEST_OPERATOR_ID;

    private static double PRICE = 100.0, PRICE2 = 150.0;
    private static final double NO_DISCOUNT = 0.0;

    private static final String CUSTOMER_NAME    = "Ivan";
    private static final String CUSTOMER_ADDRESS = "Sumskaya 1";
    private static final String CUSTOMER_PHONE   = "(050)123-45-67";
    private static final String CUSTOMER_EMAIL   = "ivan@gmail.com";
}
