/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import pizzario.exceptions.DuplicateNamedEntityException;
import pizzario.exceptions.ServiceUnresolvedEntityException;
import pizzario.exceptions.ServiceValidationException;
import pizzario.service.IIngredientService;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.*;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = pizzario.configuration.JUnitConfiguration.class )
@Transactional
public class IngredientServiceTests
{
    @Test
    public void test_ServiceNoIngredientsByDefault ()
    {
        assertThat( ingredientService.viewAll(), empty() );
    }


    @Test
    public void test_CreateSingleIngredient () throws DuplicateNamedEntityException
    {
        UUID hamId = ingredientService.create( "Ham" );

        assertNotNull( ingredientService.view( hamId ) );
        assertEquals( ingredientService.viewAll(), Arrays.asList( hamId ) );
    }


    @Test
    public void test_CreateTwoIngredients () throws DuplicateNamedEntityException
    {
        UUID hamId = ingredientService.create( "Ham" );
        UUID cheeseId = ingredientService.create( "Cheese" );

        assertNotNull( ingredientService.view( hamId ) );
        assertNotNull( ingredientService.view( cheeseId ) );

        List< UUID > allIds = ingredientService.viewAll();
        assertTrue( allIds.containsAll( Arrays.asList( cheeseId, hamId ) ) );
        assertEquals( allIds.size(), 2 );
    }


    @Test( expected = DuplicateNamedEntityException.class )
    public void test_CreateSameSingleIngredient () throws DuplicateNamedEntityException
    {
        ingredientService.create( "Ham" );
        ingredientService.create( "Ham" );
    }


    @Test( expected = ServiceUnresolvedEntityException.class )
    public void test_ViewMissing ()
    {
        assertNull( ingredientService.view( UUID.randomUUID() ) );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_ViewNull ()
    {
        ingredientService.view( null );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_CreateNull () throws DuplicateNamedEntityException
    {
        ingredientService.create( null );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_CreateBlank () throws DuplicateNamedEntityException
    {
        ingredientService.create( "" );
    }


    @Test
    public void test_RenameIngredient () throws DuplicateNamedEntityException
    {
        UUID hamId = ingredientService.create( "Ham" );
        ingredientService.rename( hamId, "Shynka" );
        assertEquals( ingredientService.view( hamId ).getName(), "Shynka" );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_RenameWithNullId () throws DuplicateNamedEntityException
    {
        ingredientService.rename( null, "Ham" );
    }


    @Test( expected = ServiceUnresolvedEntityException.class )
    public void test_RenameMissingId () throws DuplicateNamedEntityException
    {
        UUID randomId = UUID.randomUUID();
        ingredientService.rename( randomId, "Ham" );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_RenameToNullName () throws DuplicateNamedEntityException
    {
        UUID hamId = ingredientService.create( "Ham" );
        ingredientService.rename( hamId, null );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_RenameToBlankName () throws DuplicateNamedEntityException
    {
        UUID hamId = ingredientService.create( "Ham" );
        ingredientService.rename( hamId, "" );
    }


    @Test( expected = DuplicateNamedEntityException.class )
    public void test_RenameToDuplicateIngredient () throws DuplicateNamedEntityException
    {
        UUID hamId = ingredientService.create( "Ham" );
        ingredientService.create( "Shynka" );
        ingredientService.rename( hamId, "Shynka" );
    }


    @Inject
    private IIngredientService ingredientService;
}
