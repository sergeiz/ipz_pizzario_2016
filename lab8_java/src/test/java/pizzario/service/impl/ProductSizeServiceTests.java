/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import pizzario.dto.ProductSizeDto;
import pizzario.exceptions.DuplicateNamedEntityException;
import pizzario.exceptions.ServiceValidationException;
import pizzario.service.IProductSizeService;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.UUID;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = pizzario.configuration.JUnitConfiguration.class )
@Transactional
public class ProductSizeServiceTests
{
    @Test
    public void test_ServiceNoSizesByDefault ()
    {
        assertThat( productSizeService.viewAll(), empty() );
    }


    @Test
    public void test_CreateSingleSize () throws DuplicateNamedEntityException
    {
        UUID smallId = productSizeService.create( "Small", "small.png", 25, 300 );

        assertEquals( productSizeService.viewAll(), Arrays.asList( smallId ) );

        assertEquals(
                productSizeService.view( smallId ),
                new ProductSizeDto( smallId, "Small", "small.png", 25, 300 )
        );
    }


    @Test
    public void test_CreateTwoSizes () throws DuplicateNamedEntityException
    {
        UUID smallId = productSizeService.create( "Small", "small.png", 25, 300 );
        UUID largeId = productSizeService.create( "Large", "large.png", 35, 900 );

        assertThat( productSizeService.viewAll(), hasSize( 2 ) );

        assertEquals(
                productSizeService.view( smallId ),
                new ProductSizeDto( smallId, "Small", "small.png", 25, 300 )
        );

        assertEquals(
                productSizeService.view( largeId ),
                new ProductSizeDto( largeId, "Large", "large.png", 35, 900 )
        );
    }


    @Test( expected = DuplicateNamedEntityException.class )
    public void test_CreateSameNameSize () throws Exception
    {
        productSizeService.create( "Small", "small.png", 25, 300 );
        productSizeService.create( "Small", "large.png", 35, 900 );
    }



    @Test( expected = ServiceValidationException.class )
    public void test_Create_BlankName () throws Exception
    {
        productSizeService.create( "", "small.png", 25, 300 );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_Create_BlankImageUrl () throws Exception
    {
        productSizeService.create( "Small", "", 25, 300 );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_Create_ZeroDiameter () throws Exception
    {
        productSizeService.create( "Small", "small.png", 0, 300 );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_Create_NegativeDiameter () throws Exception
    {
        productSizeService.create( "Small", "small.png", -1, 300 );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_Create_ZeroWeight () throws Exception
    {
        productSizeService.create( "Small", "small.png", 25, 0 );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_Create_NegativeWeight () throws Exception
    {
        productSizeService.create( "Small", "small.png", 25, -1 );
    }


    @Test
    public void test_Rename () throws Exception
    {
        UUID smallId = productSizeService.create( "Small", "small.png", 25, 300 );
        productSizeService.rename( smallId, "Smallest" );

        assertEquals( productSizeService.viewAll(), Arrays.asList( smallId ) );
        assertEquals(
                productSizeService.view( smallId ),
                new ProductSizeDto( smallId, "Smallest", "small.png", 25, 300 )
        );
    }


    @Test( expected = DuplicateNamedEntityException.class )
    public void test_RenameToExisting () throws Exception
    {
        UUID smallId = productSizeService.create( "Small", "small.png", 25, 300 );
        UUID largeId = productSizeService.create( "Large", "large.png", 35, 900 );
        productSizeService.rename( smallId, "Large" );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_RenameToBlank () throws Exception
    {
        UUID smallId = productSizeService.create( "Small", "small.png", 25, 300 );
        productSizeService.rename( smallId, "" );
    }


    @Test
    public void test_UpdateImage () throws Exception
    {
        UUID smallId = productSizeService.create( "Small", "small.png", 25, 300 );
        productSizeService.updateImageUrl( smallId, "small.jpg" );

        assertEquals(
                productSizeService.view( smallId ),
                new ProductSizeDto( smallId, "Small", "small.jpg", 25, 300 )
        );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_UpdateImage_ToBlank () throws Exception
    {
        UUID smallId = productSizeService.create( "Small", "small.png", 25, 300 );
        productSizeService.updateImageUrl( smallId, "" );
    }


    @Test
    public void test_ChangeDiameter () throws Exception
    {
        UUID smallId = productSizeService.create( "Small", "small.png", 25, 300 );
        productSizeService.changeDiameter( smallId, 30 );

        assertEquals(
                productSizeService.view( smallId ),
                new ProductSizeDto( smallId, "Small", "small.png", 30, 300 )
        );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_ChangeDiameter_ToZero () throws Exception
    {
        UUID smallId = productSizeService.create( "Small", "small.png", 25, 300 );
        productSizeService.changeDiameter( smallId, 0 );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_ChangeDiameter_ToNegative () throws Exception
    {
        UUID smallId = productSizeService.create( "Small", "small.png", 25, 300 );
        productSizeService.changeDiameter( smallId, -1 );
    }


    @Test
    public void test_ChangeWeight () throws Exception
    {
        UUID smallId = productSizeService.create( "Small", "small.png", 25, 300 );
        productSizeService.changeWeight( smallId, 450 );

        assertEquals(
                productSizeService.view( smallId ),
                new ProductSizeDto( smallId, "Small", "small.png", 25, 450 )
        );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_ChangeWeight_ToZero () throws Exception
    {
        UUID smallId = productSizeService.create( "Small", "small.png", 25, 300 );
        productSizeService.changeWeight( smallId, 0 );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_ChangeWeight_ToNegative () throws Exception
    {
        UUID smallId = productSizeService.create( "Small", "small.png", 25, 300 );
        productSizeService.changeWeight( smallId, -1 );
    }


    @Inject
    private IProductSizeService productSizeService;
}