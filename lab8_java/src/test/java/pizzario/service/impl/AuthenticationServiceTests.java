/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import pizzario.dto.AccountDto;
import pizzario.service.IAccountService;
import pizzario.service.IAuthenticationService;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = pizzario.configuration.JUnitConfiguration.class )
@Transactional
public class AuthenticationServiceTests
{
    @Test
    public void test_Authenticate_Existing () throws Exception
    {
        accountService.createOperator( "Wasya", "wasya@pizzario.com", "12345" );

        AccountDto accountDto = authenticationService.authenticate(
                new UsernamePasswordAuthenticationToken(
                        "wasya@pizzario.com",
                        "12345"
                )
        );

        assertEquals( accountDto.getName(), "Wasya" );
        assertEquals( accountDto.getEmail(), "wasya@pizzario.com" );
    }


    @Test
    public void test_Authenticate_Missing () throws Exception
    {
        AccountDto accountDto = authenticationService.authenticate(
                new UsernamePasswordAuthenticationToken(
                        "wasya@pizzario.com",
                        "12345"
                )
        );

        assertNull( accountDto );
    }


    @Test
    public void test_Authenticate_WrongPassword () throws Exception
    {
        accountService.createOperator( "Wasya", "wasya@pizzario.com", "12345" );

        AccountDto accountDto = authenticationService.authenticate(
                new UsernamePasswordAuthenticationToken(
                        "wasya@pizzario.com",
                        "54321"
                )
        );

        assertNull( accountDto );
    }


    @Inject
    private IAuthenticationService authenticationService;

    @Inject
    private IAccountService accountService;
}
