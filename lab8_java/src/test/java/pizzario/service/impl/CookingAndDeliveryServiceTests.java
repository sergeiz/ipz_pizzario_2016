/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import pizzario.dto.CookingAssignmentDto;
import pizzario.dto.DeliveryDto;
import pizzario.dto.OrderDto;
import pizzario.exceptions.CookingLifecycleException;
import pizzario.exceptions.DeliveryLifecycleException;
import pizzario.exceptions.OrderLifecycleException;
import pizzario.exceptions.ServiceValidationException;
import pizzario.service.*;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.junit.Assert.*;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = pizzario.configuration.JUnitConfiguration.class )
@Transactional
public class CookingAndDeliveryServiceTests
{
    @Before
    public void setup () throws Exception
    {
        TEST_PRODUCT_ID = productService.create( "Milano", "milano.png" );
        TEST_SIZE_ID = productSizeService.create( "Small", "small.png", 25, 300 );
        TEST_OPERATOR_ID = accountService.createOperator( "Wasya", "wasya@pizzario.com", "12345" );

        productService.definePrice( TEST_PRODUCT_ID, TEST_SIZE_ID, BigDecimal.valueOf( 100.00 ) );

        UUID cart1Id = shoppingCartService.createNew();
        shoppingCartService.addItem( cart1Id, TEST_PRODUCT_ID, TEST_SIZE_ID );
        shoppingCartService.lock( cart1Id );

        UUID cart2Id = shoppingCartService.createNew();
        shoppingCartService.setItem( cart2Id, TEST_PRODUCT_ID, TEST_SIZE_ID, 2 );
        shoppingCartService.lock( cart2Id );

        TEST_ORDER1_ID = orderService.createNew(
                "Ivan",
                "Sumskaya 1",
                "(050)123-45-67",
                "ivan@gmail.com",
                "",
                cart1Id
        );

        TEST_ORDER2_ID = orderService.createNew(
                "Ivan",
                "Sumskaya 1",
                "(050)123-45-67",
                "ivan@gmail.com",
                "",
                cart2Id
        );
    }


    @Test
    public void test_NoCookingsByDefault ()
    {
        assertThat( cookingService.viewAll(), empty() );
        assertThat( cookingService.viewCookedRightNow(), empty() );
        assertThat( cookingService.viewWaiting(), empty() );
    }


    @Test
    @WithMockUser( authorities = "OPERATOR" )
    public void test_CookingAddedAfterConfirmingOrder () throws Exception
    {
        orderService.confirm( TEST_ORDER1_ID, TEST_OPERATOR_ID );

        List< UUID > cookingIds = cookingService.viewAll();
        assertThat( cookingIds, hasSize( 1 ) );

        assertEquals( cookingIds, cookingService.viewWaiting() );

        assertThat( cookingService.viewCookedRightNow(), empty() );

        CookingAssignmentDto cookingDto = cookingService.view( cookingIds.get( 0 ) );
        expectCookingAssignment( cookingDto, TEST_ORDER1_ID, TEST_PRODUCT_ID, TEST_SIZE_ID, "Waiting" );

        List< UUID > orderCookingsIds = cookingService.viewOrderAssignments( TEST_ORDER1_ID );
        assertThat( orderCookingsIds, hasSize( 1 ) );
        assertThat( orderCookingsIds, hasItem( cookingIds.get( 0 ) ) );
    }


    @Test
    @WithMockUser( authorities = "OPERATOR" )
    public void test_CookingAddedAfterConfirmingOrderWith2Items () throws Exception
    {
        orderService.confirm( TEST_ORDER2_ID, TEST_OPERATOR_ID );

        List< UUID > cookingIds = cookingService.viewAll();
        assertThat( cookingIds, hasSize( 2 ) );

        List< UUID > waitingIds = cookingService.viewWaiting();
        assertThat( waitingIds, hasSize( 2 ) );
        assertThat(
                cookingIds,
                hasItems( cookingIds.get( 0 ), cookingIds.get( 1 ) )
        );

        assertThat( cookingService.viewCookedRightNow(), empty() );

        CookingAssignmentDto cooking1Dto = cookingService.view( cookingIds.get( 0 ) );
        expectCookingAssignment( cooking1Dto, TEST_ORDER2_ID, TEST_PRODUCT_ID, TEST_SIZE_ID, "Waiting" );

        CookingAssignmentDto cooking2Dto = cookingService.view( cookingIds.get( 1 ) );
        expectCookingAssignment( cooking2Dto, TEST_ORDER2_ID, TEST_PRODUCT_ID, TEST_SIZE_ID, "Waiting" );

        List< UUID > orderCookingsIds = cookingService.viewOrderAssignments( TEST_ORDER2_ID );
        assertThat( orderCookingsIds, hasSize( 2 ) );
        assertThat(
                orderCookingsIds,
                hasItems( orderCookingsIds.get( 0 ), orderCookingsIds.get( 1 ) )
        );
    }


    @Test
    @WithMockUser( authorities = "OPERATOR" )
    public void test_MarkCookingStarted () throws Exception
    {
        orderService.confirm( TEST_ORDER1_ID, TEST_OPERATOR_ID );

        UUID cookingId = cookingService.viewWaiting().get( 0 );
        cookingService.markCookingStarted( cookingId );

        CookingAssignmentDto cookingDto = cookingService.view( cookingId );
        expectCookingAssignment( cookingDto, TEST_ORDER1_ID, TEST_PRODUCT_ID, TEST_SIZE_ID, "InProgress" );
    }


    @Test( expected = CookingLifecycleException.class )
    @WithMockUser( authorities = "OPERATOR" )
    public void test_MarkCookingStartedTwiceOnSameAssignment () throws Exception
    {
        orderService.confirm( TEST_ORDER1_ID, TEST_OPERATOR_ID );

        UUID cookingId = cookingService.viewWaiting().get( 0 );
        cookingService.markCookingStarted( cookingId );
        cookingService.markCookingStarted( cookingId );
    }


    @Test
    @WithMockUser( authorities = "OPERATOR" )
    public void test_MarkOneOfTwoCookingsStarted () throws Exception
    {
        orderService.confirm( TEST_ORDER2_ID, TEST_OPERATOR_ID );

        UUID cooking1Id = cookingService.viewWaiting().get( 0 );
        UUID cooking2Id = cookingService.viewWaiting().get( 1 );

        cookingService.markCookingStarted( cooking1Id );

        CookingAssignmentDto cooking1Dto = cookingService.view( cooking1Id );
        expectCookingAssignment( cooking1Dto, TEST_ORDER2_ID, TEST_PRODUCT_ID, TEST_SIZE_ID, "InProgress" );

        CookingAssignmentDto cooking2Dto = cookingService.view( cooking2Id );
        expectCookingAssignment( cooking2Dto, TEST_ORDER2_ID, TEST_PRODUCT_ID, TEST_SIZE_ID, "Waiting" );
    }


    @Test
    @WithMockUser( authorities = "OPERATOR" )
    public void test_StartFinishTheOnlyCooking () throws Exception
    {
        orderService.confirm( TEST_ORDER1_ID, TEST_OPERATOR_ID );

        UUID cookingId = cookingService.viewWaiting().get( 0 );
        cookingService.markCookingStarted( cookingId );

        OrderDto orderDto = orderService.view( TEST_ORDER1_ID );
        assertEquals( orderDto.getStatus(), "Confirmed" );
        assertEquals( orderDto.getFinishedCookingsCount(), 0 );

        assertNull( deliveryService.findOrderDelivery( TEST_ORDER1_ID ) );

        cookingService.markCookingFinished( cookingId );

        orderDto = orderService.view( TEST_ORDER1_ID );
        assertEquals( orderDto.getStatus(), "Delivering" );
        assertEquals( orderDto.getFinishedCookingsCount(), 1 );

        CookingAssignmentDto cookingDto = cookingService.view( cookingId );
        expectCookingAssignment( cookingDto, TEST_ORDER1_ID, TEST_PRODUCT_ID, TEST_SIZE_ID, "Finished" );

        DeliveryDto deliveryDto = deliveryService.findOrderDelivery( TEST_ORDER1_ID );
        expectDelivery( deliveryDto, TEST_ORDER1_ID, null /* driver */, 100.00, "Waiting" );
    }


    @Test
    @WithMockUser( authorities = "OPERATOR" )
    public void test_StartFinishTwoCookings () throws Exception
    {
        orderService.confirm( TEST_ORDER2_ID, TEST_OPERATOR_ID );

        UUID cooking1Id = cookingService.viewWaiting().get( 0 );
        UUID cooking2Id = cookingService.viewWaiting().get( 1 );
        cookingService.markCookingStarted( cooking1Id );
        cookingService.markCookingStarted( cooking2Id );

        OrderDto orderDto = orderService.view( TEST_ORDER2_ID );
        assertEquals( orderDto.getStatus(), "Confirmed" );
        assertEquals( orderDto.getFinishedCookingsCount(), 0 );

        assertNull( deliveryService.findOrderDelivery( TEST_ORDER2_ID ) );

        cookingService.markCookingFinished( cooking1Id );

        orderDto = orderService.view( TEST_ORDER2_ID );
        assertEquals( orderDto.getStatus(), "Confirmed" );
        assertEquals( orderDto.getFinishedCookingsCount(), 1 );

        assertNull( deliveryService.findOrderDelivery( TEST_ORDER2_ID ) );

        cookingService.markCookingFinished( cooking2Id );


        orderDto = orderService.view( TEST_ORDER2_ID );
        assertEquals( orderDto.getStatus(), "Delivering" );
        assertEquals( orderDto.getFinishedCookingsCount(), 2 );

        CookingAssignmentDto cooking1Dto = cookingService.view( cooking1Id );
        expectCookingAssignment( cooking1Dto, TEST_ORDER2_ID, TEST_PRODUCT_ID, TEST_SIZE_ID, "Finished" );

        CookingAssignmentDto cooking2Dto = cookingService.view( cooking2Id );
        expectCookingAssignment( cooking2Dto, TEST_ORDER2_ID, TEST_PRODUCT_ID, TEST_SIZE_ID, "Finished" );

        DeliveryDto deliveryDto = deliveryService.findOrderDelivery( TEST_ORDER2_ID );
        expectDelivery( deliveryDto, TEST_ORDER2_ID, null /* driver */, 200.00, "Waiting" );
    }


    @Test( expected = CookingLifecycleException.class )
    @WithMockUser( authorities = "OPERATOR" )
    public void test_MarkCookingFinishedWithoutStarting () throws Exception
    {
        orderService.confirm( TEST_ORDER1_ID, TEST_OPERATOR_ID );

        UUID cookingId = cookingService.viewWaiting().get( 0 );
        cookingService.markCookingFinished( cookingId );
    }


    @Test( expected = CookingLifecycleException.class )
    @WithMockUser( authorities = "OPERATOR" )
    public void test_MarkCookingFinishedForSameItemTwice () throws Exception
    {
        orderService.confirm( TEST_ORDER1_ID, TEST_OPERATOR_ID );

        UUID cookingId = cookingService.viewWaiting().get( 0 );
        cookingService.markCookingStarted( cookingId );
        cookingService.markCookingFinished( cookingId );
        cookingService.markCookingFinished( cookingId );
    }


    @Test
    @WithMockUser( authorities = "OPERATOR" )
    public void test_StartDelivery () throws Exception
    {
        cookSimpleOrder();

        DeliveryDto deliveryDto = deliveryService.findOrderDelivery( TEST_ORDER1_ID );
        deliveryService.markStarted( deliveryDto.getDomainId(), "Fedor Petrovich" );

        deliveryDto = deliveryService.findOrderDelivery( TEST_ORDER1_ID );
        expectDelivery( deliveryDto, TEST_ORDER1_ID, "Fedor Petrovich", 100.00, "InProgress" );

        assertEquals(
                deliveryService.view( deliveryDto.getDomainId() ).getDomainId(),
                deliveryDto.getDomainId()
        );

        assertEquals( orderService.view( TEST_ORDER1_ID ).getStatus(), "Delivering" );

        List< UUID > deliveries = deliveryService.viewAll();
        assertThat( deliveries, hasSize( 1 ) );
        assertThat( deliveries, hasItem( deliveryDto.getDomainId() ) );

        List< UUID > inProgressIds = deliveryService.viewInProgress();
        assertThat( inProgressIds, hasSize( 1 ) );
        assertThat( inProgressIds, hasItem( deliveryDto.getDomainId() ) );

        assertThat( deliveryService.viewWaiting(), empty() );
    }


    @Test( expected = ServiceValidationException.class )
    @WithMockUser( authorities = "OPERATOR" )
    public void test_StartDelivery_BlankDriver () throws Exception
    {
        cookSimpleOrder();

        DeliveryDto deliveryDto = deliveryService.findOrderDelivery( TEST_ORDER1_ID );
        deliveryService.markStarted( deliveryDto.getDomainId(), "" );
    }


    @Test
    @WithMockUser( authorities = "OPERATOR" )
    public void test_StartFinishDelivery () throws Exception
    {
        cookSimpleOrder();

        DeliveryDto deliveryDto = deliveryService.findOrderDelivery( TEST_ORDER1_ID );
        deliveryService.markStarted( deliveryDto.getDomainId(), "Fedor Petrovich" );
        deliveryService.markDelivered( deliveryDto.getDomainId() );

        deliveryDto = deliveryService.findOrderDelivery( TEST_ORDER1_ID );
        expectDelivery( deliveryDto, TEST_ORDER1_ID, "Fedor Petrovich", 100.00, "Delivered" );

        assertEquals( orderService.view( TEST_ORDER1_ID ).getStatus(), "Completed" );

        assertThat( deliveryService.viewInProgress(), empty() );
        assertThat( deliveryService.viewWaiting(), empty() );
    }


    @Test( expected = DeliveryLifecycleException.class )
    @WithMockUser( authorities = "OPERATOR" )
    public void test_StartDeliveryTwice () throws Exception
    {
        cookSimpleOrder();

        DeliveryDto deliveryDto = deliveryService.findOrderDelivery( TEST_ORDER1_ID );
        deliveryService.markStarted( deliveryDto.getDomainId(), "Fedor Petrovich" );
        deliveryService.markStarted( deliveryDto.getDomainId(), "Fedor Petrovich" );
    }


    @Test( expected = DeliveryLifecycleException.class )
    @WithMockUser( authorities = "OPERATOR" )
    public void test_FinishDeliveryWithoutStarting () throws Exception
    {
        cookSimpleOrder();

        DeliveryDto deliveryDto = deliveryService.findOrderDelivery( TEST_ORDER1_ID );
        deliveryService.markDelivered( deliveryDto.getDomainId() );
    }


    @Test( expected = DeliveryLifecycleException.class )
    @WithMockUser( authorities = "OPERATOR" )
    public void test_FinishDeliveryTwice () throws Exception
    {
        cookSimpleOrder();

        DeliveryDto deliveryDto = deliveryService.findOrderDelivery( TEST_ORDER1_ID );
        deliveryService.markDelivered( deliveryDto.getDomainId() );
        deliveryService.markDelivered( deliveryDto.getDomainId() );
    }

    private void cookSimpleOrder () throws OrderLifecycleException, CookingLifecycleException
    {
        orderService.confirm( TEST_ORDER1_ID, TEST_OPERATOR_ID );

        UUID cookingId = cookingService.viewWaiting().get( 0 );
        cookingService.markCookingStarted( cookingId );
        cookingService.markCookingFinished( cookingId );
    }


    private void expectCookingAssignment (
            CookingAssignmentDto cookingDto,
            UUID orderId,
            UUID productid,
            UUID sizeId,
            String status
    )
    {
        assertEquals( cookingDto.getOrderId(), orderId );
        assertEquals( cookingDto.getProductId(), productid );
        assertEquals( cookingDto.getSizeId(), sizeId );
        assertEquals( cookingDto.getStatus(), status );
    }


    private void expectDelivery (
            DeliveryDto deliveryDto,
            UUID orderId,
            String driverName,
            double cash2Collect,
            String status
    )
    {
        assertEquals( deliveryDto.getOrderId(), orderId );
        assertEquals( deliveryDto.getDriverName(), driverName );
        assertEquals( deliveryDto.getCash2Collect(), BigDecimal.valueOf( cash2Collect ) );
        assertEquals( deliveryDto.getStatus(), status );
    }


    private UUID TEST_PRODUCT_ID, TEST_SIZE_ID, TEST_OPERATOR_ID, TEST_ORDER1_ID, TEST_ORDER2_ID;

    @Inject
    private ICookingAssignmentService cookingService;

    @Inject
    private IDeliveryService deliveryService;

    @Inject
    private IOrderService orderService;

    @Inject
    private IShoppingCartService shoppingCartService;

    @Inject
    private IProductService productService;

    @Inject
    private IProductSizeService productSizeService;

    @Inject
    private IAccountService accountService;
}
