/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import pizzario.dto.AccountDto;
import pizzario.exceptions.DuplicateNamedEntityException;
import pizzario.exceptions.ServiceValidationException;
import pizzario.service.IAccountService;

import javax.inject.Inject;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = pizzario.configuration.JUnitConfiguration.class )
@Transactional
public class AccountServiceTests
{
    @Test
    public void test_NoAccountsByDefault ()
    {
        assertThat( accountService.viewAll(), empty() );
    }


    @Test
    public void test_CreateOperator () throws Exception
    {
        UUID operatorId =
                accountService.createOperator(
                        "Wasya",
                        "wasya@pizzario.com",
                        "12345"
                );

        AccountDto accountDto = accountService.view( operatorId );
        assertEquals( accountDto.getDomainId(), operatorId );
        assertEquals( accountDto.getName(), "Wasya" );
        assertEquals( accountDto.getEmail(), "wasya@pizzario.com" );
        assertEquals( accountDto.getPassword(), "12345" );
    }


    @Test
    public void test_Create2Operators () throws Exception
    {
        UUID operator1Id =
                accountService.createOperator(
                        "Wasya",
                        "wasya@pizzario.com",
                        "12345"
                );

        UUID operator2Id =
                accountService.createOperator(
                        "Petya",
                        "petya@pizzario.com",
                        "54321"
                );

        AccountDto account1Dto = accountService.view( operator1Id );
        assertEquals( account1Dto.getDomainId(), operator1Id );
        assertEquals( account1Dto.getName(), "Wasya" );
        assertEquals( account1Dto.getEmail(), "wasya@pizzario.com" );
        assertEquals( account1Dto.getPassword(), "12345" );

        AccountDto account2Dto = accountService.view( operator2Id );
        assertEquals( account2Dto.getDomainId(), operator2Id );
        assertEquals( account2Dto.getName(), "Petya" );
        assertEquals( account2Dto.getEmail(), "petya@pizzario.com" );
        assertEquals( account2Dto.getPassword(), "54321" );

        List< UUID > accountIds = accountService.viewAll();
        assertThat( accountIds, hasSize( 2 ) );
        assertThat( accountIds, containsInAnyOrder( operator1Id, operator2Id ) );
    }


    @Test( expected = DuplicateNamedEntityException.class )
    public void test_CreateDuplicateEmailOperator () throws Exception
    {
        accountService.createOperator( "Wasya", "wasya@pizzario.com", "12345" );
        accountService.createOperator( "Petya", "wasya@pizzario.com", "12345" );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_CreateOperator_BlankName ()  throws Exception
    {
        accountService.createOperator( "", "wasya@pizzario.com", "12345" );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_CreateOperator_BlankEmail () throws Exception
    {
        accountService.createOperator( "Wasya", "", "12345" );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_CreateOperator_WrongEmail () throws Exception
    {
        accountService.createOperator( "Wasya", "NOT A GOOD EMAIL", "12345" );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_CreateOperator_BlankPassword () throws Exception
    {
        accountService.createOperator( "Wasya", "wasya@pizzario.com", "" );
    }


    @Test
    public void test_RenameOperator () throws Exception
    {
        UUID operatorId =
                accountService.createOperator(
                        "Wasya",
                        "wasya@pizzario.com",
                        "12345"
                );

        accountService.changeName( operatorId, "Petya" );

        assertEquals( accountService.view( operatorId ).getName(), "Petya" );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_RenameOperator_ToBlank () throws Exception
    {
        UUID operatorId =
                accountService.createOperator(
                        "Wasya",
                        "wasya@pizzario.com",
                        "12345"
                );

        accountService.changeName( operatorId, "" );
    }


    @Test
    public void test_ChangeEmail () throws Exception
    {
        UUID operatorId =
                accountService.createOperator(
                        "Wasya",
                        "wasya@pizzario.com",
                        "12345"
                );

        accountService.changeEmail( operatorId, "wasya2@pizzario.com" );

        assertEquals( accountService.view( operatorId ).getEmail(), "wasya2@pizzario.com" );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_ChangeEmail_ToBlank () throws Exception
    {
        UUID operatorId =
                accountService.createOperator(
                        "Wasya",
                        "wasya@pizzario.com",
                        "12345"
                );

        accountService.changeEmail( operatorId, "" );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_ChangeEmail_ToBad () throws Exception
    {
        UUID operatorId =
                accountService.createOperator(
                        "Wasya",
                        "wasya@pizzario.com",
                        "12345"
                );

        accountService.changeEmail( operatorId, "BAD EMAIL" );
    }


    @Test( expected = DuplicateNamedEntityException.class )
    public void test_ChangeEmail_ToExisting () throws Exception
    {
        accountService.createOperator( "Petya", "petya@pizzario.com", "12345" );
        UUID operatorId = accountService.createOperator( "Wasya", "wasya@pizzario.com", "12345" );

        accountService.changeEmail( operatorId, "petya@pizzario.com" );
    }


    @Test
    public void test_ChangePassword () throws Exception
    {
        UUID operatorId =
                accountService.createOperator(
                        "Wasya",
                        "wasya@pizzario.com",
                        "12345"
                );

        accountService.changePassword( operatorId, "54321" );

        assertEquals( accountService.view( operatorId ).getPassword(), "54321" );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_ChangePassword_ToBlank () throws Exception
    {
        UUID operatorId =
                accountService.createOperator(
                        "Wasya",
                        "wasya@pizzario.com",
                        "12345"
                );

        accountService.changePassword( operatorId, "" );
    }


    @Inject
    private IAccountService accountService;
}
