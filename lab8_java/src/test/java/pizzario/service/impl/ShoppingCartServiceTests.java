/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import pizzario.dto.ProductItemDto;
import pizzario.dto.ShoppingCartDto;
import pizzario.exceptions.LockingEmptyCartException;
import pizzario.exceptions.RemovingUnexistingCartItemException;
import pizzario.exceptions.ServiceValidationException;
import pizzario.exceptions.UnmodifiableCartException;
import pizzario.service.IProductService;
import pizzario.service.IProductSizeService;
import pizzario.service.IShoppingCartService;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = pizzario.configuration.JUnitConfiguration.class )
@Transactional
public class ShoppingCartServiceTests
{
    @Before
    public void setup () throws Exception
    {
        this.TEST_PRODUCT_ID = createProduct();
        this.TEST_SIZE_ID    = createSize();
        this.TEST_SIZE2_ID   = createAnotherSize();

        this.PRICE = 100.00;
        this.PRICE2 = 200.00;

        productService.definePrice( TEST_PRODUCT_ID, TEST_SIZE_ID, BigDecimal.valueOf( PRICE ) );
        productService.definePrice( TEST_PRODUCT_ID, TEST_SIZE2_ID, BigDecimal.valueOf( PRICE2 ) );
    }


    @Test
    public void test_CreateNew ()
    {
        UUID cartId = shoppingCartService.createNew();

        List< UUID > allCarts = shoppingCartService.viewAll();
        assertThat( allCarts, hasSize( 1 ) );
        assertThat( allCarts, hasItem( cartId ) );

        assertEquals(
                shoppingCartService.view( cartId ),
                new ShoppingCartDto( cartId )
        );
    }

    @Test
    public void test_CreateTwoCarts ()
    {
        UUID cartId1 = shoppingCartService.createNew();
        UUID cartId2 = shoppingCartService.createNew();

        List< UUID > allCarts = shoppingCartService.viewAll();
        assertThat( allCarts, hasSize( 2 ) );
        assertThat( allCarts, containsInAnyOrder( cartId1, cartId2 ) );
    }


    @Test
    public void test_AddItemOnce () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();

        shoppingCartService.addItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID );

        ShoppingCartDto cartDto = shoppingCartService.view( cartId );
        expectCartContent( cartDto, 1, PRICE );

        ProductItemDto itemDto = cartDto.getItems().get( 0 );
        expectProductItem( itemDto, TEST_PRODUCT_ID, TEST_SIZE_ID, 1 );
    }


    @Test
    public void test_AddItemTwice () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();

        shoppingCartService.addItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID );
        shoppingCartService.addItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID );

        ShoppingCartDto cartDto = shoppingCartService.view( cartId );
        expectCartContent( cartDto, 1, PRICE * 2 );

        ProductItemDto itemDto = cartDto.getItems().get( 0 );
        expectProductItem( itemDto, TEST_PRODUCT_ID, TEST_SIZE_ID, 2 );
    }


    @Test
    public void test_AddDifferentItems () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();

        shoppingCartService.addItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID );
        shoppingCartService.addItem( cartId, TEST_PRODUCT_ID, TEST_SIZE2_ID );

        ShoppingCartDto cartDto = shoppingCartService.view( cartId );
        expectCartContent( cartDto, 2, PRICE + PRICE2 );

        ProductItemDto item1Dto = cartDto.getItems().get( 0 );
        expectProductItem( item1Dto, TEST_PRODUCT_ID, TEST_SIZE_ID, 1 );

        ProductItemDto item2Dto = cartDto.getItems().get( 1 );
        expectProductItem( item2Dto, TEST_PRODUCT_ID, TEST_SIZE2_ID, 1 );
    }


    @Test
    public void test_SetItemOnce () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();

        shoppingCartService.setItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID, 5 );

        ShoppingCartDto cartDto = shoppingCartService.view( cartId );
        expectCartContent( cartDto, 1, PRICE * 5 );

        ProductItemDto itemDto = cartDto.getItems().get( 0 );
        expectProductItem( itemDto, TEST_PRODUCT_ID, TEST_SIZE_ID, 5 );
    }


    @Test
    public void test_SetItemTwice () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();

        shoppingCartService.setItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID, 4 );
        shoppingCartService.setItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID, 2 );

        ShoppingCartDto cartDto = shoppingCartService.view( cartId );
        expectCartContent( cartDto, 1, PRICE * 2 );

        ProductItemDto itemDto = cartDto.getItems().get( 0 );
        expectProductItem( itemDto, TEST_PRODUCT_ID, TEST_SIZE_ID, 2 );
    }


    @Test
    public void test_SetItemAfterAdd () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();

        shoppingCartService.addItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID );
        shoppingCartService.setItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID, 3 );

        ShoppingCartDto cartDto = shoppingCartService.view( cartId );
        expectCartContent( cartDto, 1, PRICE * 3 );

        ProductItemDto itemDto = cartDto.getItems().get( 0 );
        expectProductItem( itemDto, TEST_PRODUCT_ID, TEST_SIZE_ID, 3 );
    }


    @Test
    public void test_SetTwoItems () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();

        shoppingCartService.setItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID, 3 );
        shoppingCartService.setItem( cartId, TEST_PRODUCT_ID, TEST_SIZE2_ID, 2 );

        ShoppingCartDto cartDto = shoppingCartService.view( cartId );
        expectCartContent( cartDto, 2, PRICE * 3 + PRICE2 * 2 );

        ProductItemDto item1Dto = cartDto.getItems().get( 0 );
        expectProductItem( item1Dto, TEST_PRODUCT_ID, TEST_SIZE_ID, 3 );

        ProductItemDto item2Dto = cartDto.getItems().get( 1 );
        expectProductItem( item2Dto, TEST_PRODUCT_ID, TEST_SIZE2_ID, 2 );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_SetItem_NegativeQuantity () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();
        shoppingCartService.setItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID, -1 );
    }


    @Test( expected = ServiceValidationException.class )
    public void test_SetItem_ZeroQuantity () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();
        shoppingCartService.setItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID, 0 );
    }


    @Test
    public void test_AddRemoveItem () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();

        shoppingCartService.addItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID );
        shoppingCartService.removeItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID );

        ShoppingCartDto cartDto = shoppingCartService.view( cartId );
        expectEmptyCart( cartDto );
    }


    @Test
    public void test_Add2Items_Remove1 () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();

        shoppingCartService.addItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID );
        shoppingCartService.addItem( cartId, TEST_PRODUCT_ID, TEST_SIZE2_ID );
        shoppingCartService.removeItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID );

        ShoppingCartDto cartDto = shoppingCartService.view( cartId );
        expectCartContent( cartDto, 1, PRICE2 );

        ProductItemDto item1Dto = cartDto.getItems().get( 0 );
        expectProductItem( item1Dto, TEST_PRODUCT_ID, TEST_SIZE2_ID, 1 );
    }


    @Test( expected = RemovingUnexistingCartItemException.class )
    public void test_RemoveFromEmpty () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();
        shoppingCartService.removeItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID );
    }


    @Test( expected = RemovingUnexistingCartItemException.class )
    public void test_RemoveMissing() throws Exception
    {
        UUID cartId = shoppingCartService.createNew();
        shoppingCartService.addItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID );
        shoppingCartService.removeItem( cartId, TEST_PRODUCT_ID, TEST_SIZE2_ID );
    }


    @Test
    public void test_ClearEmpty () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();
        shoppingCartService.clearItems( cartId );

        ShoppingCartDto cartDto = shoppingCartService.view( cartId );
        expectEmptyCart( cartDto );
    }


    @Test
    public void test_ClearTheOnlyItem () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();
        shoppingCartService.addItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID );
        shoppingCartService.clearItems( cartId );

        ShoppingCartDto cartDto = shoppingCartService.view( cartId );
        expectEmptyCart( cartDto );
    }


    @Test
    public void test_ClearManyItems () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();
        shoppingCartService.addItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID );
        shoppingCartService.setItem( cartId, TEST_PRODUCT_ID, TEST_SIZE2_ID, 2 );
        shoppingCartService.clearItems( cartId );

        ShoppingCartDto cartDto = shoppingCartService.view( cartId );
        expectEmptyCart( cartDto );
    }


    @Test
    public void test_LockCart () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();
        shoppingCartService.addItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID );
        shoppingCartService.lock( cartId );

        ShoppingCartDto cartDto = shoppingCartService.view( cartId );
        expectCartContent( cartDto, 1, PRICE, LOCKED );
    }


    @Test( expected = LockingEmptyCartException.class )
    public void test_LockEmptyCart () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();
        shoppingCartService.lock( cartId );
    }


    @Test( expected = UnmodifiableCartException.class )
    public void test_Locked_AddItem () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();
        shoppingCartService.addItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID );
        shoppingCartService.lock( cartId );

        shoppingCartService.addItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID );
    }


    @Test( expected = UnmodifiableCartException.class )
    public void test_Locked_SetItem () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();
        shoppingCartService.addItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID );
        shoppingCartService.lock( cartId );

        shoppingCartService.setItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID, 2 );
    }


    @Test( expected = UnmodifiableCartException.class )
    public void test_Locked_RemoveItem () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();
        shoppingCartService.addItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID );
        shoppingCartService.lock( cartId );

        shoppingCartService.removeItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID );
    }


    @Test( expected = UnmodifiableCartException.class )
    public void test_Locked_ClearItems () throws Exception
    {
        UUID cartId = shoppingCartService.createNew();
        shoppingCartService.addItem( cartId, TEST_PRODUCT_ID, TEST_SIZE_ID );
        shoppingCartService.lock( cartId );

        shoppingCartService.clearItems( cartId );
    }


    private void expectProductItem ( ProductItemDto itemDto, UUID productId, UUID sizeId, int quantity )
    {
        assertThat(
                itemDto,
                allOf(
                        hasProperty( "productId", equalTo( productId ) ),
                        hasProperty( "sizeId",    equalTo( sizeId ) ),
                        hasProperty( "quantity",  equalTo( quantity ) )
                )
        );
    }

    private void expectEmptyCart ( ShoppingCartDto cartDto )
    {
        expectCartContent( cartDto, 0, 0.00 );
    }

    private void expectCartContent ( ShoppingCartDto cartDto, int nItems, double expectedCost )
    {
        expectCartContent( cartDto, nItems, expectedCost, false );
    }

    private void expectCartContent ( ShoppingCartDto cartDto, int nItems, double expectedCost, boolean locked )
    {
        assertThat( cartDto.getItems(), hasSize( nItems ) );
        assertEquals(
                cartDto.getTotalCost().setScale( 2 ),
                BigDecimal.valueOf( expectedCost ).setScale( 2 )
        );
        assertEquals( cartDto.isLocked(), locked );
    }


    private UUID createProduct () throws Exception
    {
        return productService.create( "Milano", "milano.png" );
    }


    private UUID createSize () throws Exception
    {
        return productSizeService.create( "Small", "small.png", 25, 300 );
    }


    private UUID createAnotherSize () throws Exception
    {
        return productSizeService.create( "Large", "large.png", 35, 900 );
    }


    @Inject
    private IShoppingCartService shoppingCartService;

    @Inject
    private IProductService productService;

    @Inject
    private IProductSizeService productSizeService;


    private UUID TEST_PRODUCT_ID, TEST_SIZE_ID, TEST_SIZE2_ID;
    private double PRICE, PRICE2;
    private static final boolean LOCKED = true;
}
