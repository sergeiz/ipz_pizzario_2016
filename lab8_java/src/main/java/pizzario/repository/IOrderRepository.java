/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository;

import pizzario.model.Order;

import java.util.List;
import java.util.UUID;

public interface IOrderRepository extends IRepository< Order >
{
    List< UUID > selectUnconfirmedIds ();

    List< UUID > selectConfirmedIds ();

    List< UUID > selectReady4DeliveryIds ();
}
