/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository;

import pizzario.model.ShoppingCart;

import java.time.LocalDateTime;
import java.util.List;

public interface IShoppingCartRepository extends IRepository< ShoppingCart >
{
    List< Long > selectAllCartIdsModifiedBefore ( LocalDateTime moment );
}
