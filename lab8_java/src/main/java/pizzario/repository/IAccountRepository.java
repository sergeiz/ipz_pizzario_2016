/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository;

import pizzario.model.accounts.Account;
import pizzario.model.accounts.OperatorAccount;

import java.util.UUID;

public interface IAccountRepository extends IRepository< Account >
{
    Account findByEmail ( String newEmail );

    OperatorAccount findOperatorByDomainId ( UUID operatorId );
}
