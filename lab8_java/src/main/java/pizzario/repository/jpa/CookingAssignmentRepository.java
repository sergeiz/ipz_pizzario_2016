/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import org.springframework.stereotype.Repository;
import pizzario.model.CookingAssignment;
import pizzario.model.Order;
import pizzario.repository.ICookingAssignmentRepository;

import javax.persistence.TypedQuery;
import java.util.List;
import java.util.UUID;


@Repository
class CookingAssignmentRepository
        extends BasicRepository< CookingAssignment >
        implements ICookingAssignmentRepository
{
    public CookingAssignmentRepository () {
        super( CookingAssignment.class );
    }


    @Override
    public List< UUID > selectCookingIds() {
        return getEntityManager().createQuery(
                "SELECT ca.domainId " +
                        "FROM CookingAssignment ca " +
                        "WHERE ca.status = pizzario.model.CookingStatus.InProgress",
                UUID.class
        ).getResultList();
    }


    @Override
    public List< UUID > selectWaitingIds() {
        return getEntityManager().createQuery(
                "SELECT ca.domainId " +
                        "FROM CookingAssignment ca " +
                        "WHERE ca.status = pizzario.model.CookingStatus.Waiting",
                UUID.class
        ).getResultList();
    }


    @Override
    public List< UUID > findOrderAssignments( Order o ) {
        TypedQuery< UUID > queryByOrder = getEntityManager().createQuery(
                "SELECT ca.domainId " +
                        "FROM CookingAssignment ca " +
                        "WHERE ca.order.databaseId = :orderId",

                UUID.class
        );
        queryByOrder.setParameter( "orderId", o.getDatabaseId() );
        return queryByOrder.getResultList();
    }
}
