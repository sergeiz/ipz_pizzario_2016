/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import org.springframework.stereotype.Repository;
import pizzario.model.Order;
import pizzario.repository.IOrderRepository;

import java.util.List;
import java.util.UUID;


@Repository
class OrderRepository
        extends BasicRepository< Order >
        implements IOrderRepository
{

    public OrderRepository () {
        super( Order.class );
    }


    @Override
    public List< UUID > selectUnconfirmedIds() {
        return getEntityManager().createQuery(
                "SELECT o.domainId " +
                        "FROM CustomerOrder o " +
                        "WHERE o.status = pizzario.model.OrderStatus.Placed",

                UUID.class

        ).getResultList();
    }


    @Override
    public List< UUID > selectConfirmedIds () {
        return getEntityManager().createQuery(
                "SELECT o.domainId " +
                        "FROM CustomerOrder o " +
                        "WHERE o.status = pizzario.model.OrderStatus.Confirmed",

                UUID.class

        ).getResultList();
    }


    @Override
    public List< UUID > selectReady4DeliveryIds() {
        return getEntityManager().createQuery(
                "SELECT o.domainId " +
                        "FROM CustomerOrder o " +
                        "WHERE o.status = pizzario.model.OrderStatus.Delivering",

                UUID.class

        ).getResultList();
    }
}
