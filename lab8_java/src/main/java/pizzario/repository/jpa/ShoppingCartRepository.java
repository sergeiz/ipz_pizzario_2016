/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import org.springframework.stereotype.Repository;
import pizzario.model.ShoppingCart;
import pizzario.repository.IShoppingCartRepository;

import javax.persistence.TypedQuery;
import java.time.LocalDateTime;
import java.util.List;


@Repository
class ShoppingCartRepository
        extends BasicRepository< ShoppingCart >
        implements IShoppingCartRepository
{
    public ShoppingCartRepository () {
        super( ShoppingCart.class );
    }


    @Override
    public List< Long > selectAllCartIdsModifiedBefore ( LocalDateTime moment )
    {
        TypedQuery< Long > queryByLastModifiedDate = getEntityManager().createQuery(

                "SELECT cart.databaseId " +
                        "FROM ShoppingCart cart " +
                        "WHERE cart.lastModified < :moment",

                Long.class
        );

        queryByLastModifiedDate.setParameter( "moment", moment);

        return queryByLastModifiedDate.getResultList();
    }
}
