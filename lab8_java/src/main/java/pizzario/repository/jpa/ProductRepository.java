/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import org.springframework.stereotype.Repository;
import pizzario.model.Product;
import pizzario.repository.IProductRepository;


@Repository
class ProductRepository
        extends NamedObjectRepository< Product >
        implements IProductRepository
{
    public ProductRepository () {
        super( Product.class );
    }
}
