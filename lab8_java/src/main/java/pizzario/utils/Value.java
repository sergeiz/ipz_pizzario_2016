/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.utils;

import java.util.List;

public abstract class Value< TValue extends Value< TValue > >
{

    @Override
    public boolean equals ( Object o )
    {

        if ( this.getClass() != o.getClass() )
            return false;

        List< Object > set1 = this.getAttributesToIncludeInEqualityCheck();
        List< Object > set2 = ( ( Value ) o ).getAttributesToIncludeInEqualityCheck();
        return set1.equals( set2 );
    }

    @Override
    public int hashCode ()
    {
        int hash = 17;
        for ( Object attr : this.getAttributesToIncludeInEqualityCheck() )
            hash = hash * 31 + ( ( attr == null ) ? 0 : attr.hashCode() );
        return hash;
    }


    protected abstract List< Object > getAttributesToIncludeInEqualityCheck ();

}
