/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.dto;

import org.mockito.cglib.core.Local;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class OrderDto extends DomainEntityDto< OrderDto >
{

    public OrderDto ( UUID domainId ) // useful for web-controller testing
    {
        super( domainId );

        this.customerName = "";
        this.customerAddress = "";
        this.contactPhone = "";
        this.contactEmail = "";
        this.comment = "";

        this.placementTime = LocalDateTime.now();
        this.status = "";
        this.basicCost = BigDecimal.ZERO;
        this.totalCost = BigDecimal.ZERO;
        this.discountPercentage = BigDecimal.ZERO;

        this.finishedCookingsCount = 0;
        this.totalCookingsCount = 0;
    }

    public OrderDto (
            UUID domainId,
            String customerName,
            String customerAddress,
            String contactPhone,
            String contactEmail,
            LocalDateTime placementTime,
            String status,
            BigDecimal basicCost,
            BigDecimal totalCost,
            BigDecimal discountPercentage,
            String comment,
            int finishedCookingsCount,
            int totalCookingsCount
    )
    {
        super( domainId );

        this.customerName = customerName;
        this.customerAddress = customerAddress;
        this.contactPhone = contactPhone;
        this.contactEmail = contactEmail;
        this.comment = comment;

        this.placementTime = placementTime;
        this.status = status;
        this.basicCost = basicCost;
        this.totalCost = totalCost;
        this.discountPercentage = discountPercentage;

        this.finishedCookingsCount = finishedCookingsCount;
        this.totalCookingsCount = totalCookingsCount;
    }

    public String getCustomerName () { return customerName; }

    public String getCustomerAddress ()
    {
        return customerAddress;
    }

    public String getContactPhone ()
    {
        return contactPhone;
    }

    public String getContactEmail () { return contactEmail; }

    public LocalDateTime getPlacementTime ()
    {
        return placementTime;
    }

    public String getStatus ()
    {
        return status;
    }

    public BigDecimal getBasicCost ()
    {
        return basicCost;
    }

    public BigDecimal getTotalCost ()
    {
        return totalCost;
    }

    public BigDecimal getDiscountPercentage ()
    {
        return discountPercentage;
    }

    public String getComment () { return comment; }

    public int getFinishedCookingsCount () { return finishedCookingsCount; }

    public int getTotalCookingsCount () { return totalCookingsCount; }

    @Override
    public String toString ()
    {
        return String.format(
                "ID = %s\nCustomer Name = %s\nAddress = %s\nPhone = %s\nEmail = %s\nBasic cost = %s\nDiscount = %s\nTotal cost = %s\nStatus = %s\nPlaced = %s\nComment = %s\nCookings = %s/%s\n",
                getDomainId(),
                getCustomerName(),
                getCustomerAddress(),
                getContactPhone(),
                getContactEmail(),
                NumberFormat.getCurrencyInstance().format( getBasicCost() ),
                NumberFormat.getPercentInstance().format( getDiscountPercentage() ),
                NumberFormat.getCurrencyInstance().format( getTotalCost() ),
                getStatus(),
                getPlacementTime(),
                getComment(),
                getFinishedCookingsCount(),
                getTotalCookingsCount()
        );
    }

    @Override
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList(
                getDomainId(),
                getCustomerName(),
                getCustomerAddress(),
                getContactPhone(),
                getContactEmail(),
                getPlacementTime(),
                getStatus(),
                getBasicCost(),
                getTotalCost(),
                getDiscountPercentage(),
                getComment(),
                getFinishedCookingsCount(),
                getTotalCookingsCount()
        );
    }

    private final String customerName;
    private final String customerAddress;
    private final String contactPhone;
    private final String contactEmail;

    private final LocalDateTime placementTime;
    private final String status;
    private final BigDecimal basicCost;
    private final BigDecimal totalCost;
    private final BigDecimal discountPercentage;
    private final String comment;

    private final int finishedCookingsCount;
    private final int totalCookingsCount;
}
