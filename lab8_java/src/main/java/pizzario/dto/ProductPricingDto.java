/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.dto;

import pizzario.utils.Value;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ProductPricingDto extends Value< ProductPricingDto >
{

    public ProductPricingDto (
            UUID productId,
            Map< UUID, BigDecimal > pricesBySizeId,
            Map< UUID, String > sizeNames
    )
    {
        this.productId = productId;
        this.pricesBySizeId = pricesBySizeId;
        this.sizeNames = sizeNames;
    }

    public UUID getProductId ()
    {
        return productId;
    }

    public Map< UUID, BigDecimal > getPricesBySizeId ()
    {
        return pricesBySizeId;
    }


    @Override
    public String toString ()
    {
        StringBuilder pricesAsString = new StringBuilder();
        pricesAsString.append( "Prices: " );

        for ( Map.Entry< UUID, BigDecimal > ppa : pricesBySizeId.entrySet() )
            pricesAsString.append(
                    String.format(
                            "%s = %s  ",
                            sizeNames.get( ppa.getKey() ),
                            NumberFormat.getCurrencyInstance().format( ppa.getValue() )
                    )
            );

        return pricesAsString.toString();
    }

    @Override
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        List< Object > list = new ArrayList<>();
        list.add( getProductId() );

        pricesBySizeId.entrySet().stream().forEach(
                entry ->
                {
                    list.add( entry.getKey() );
                    list.add( entry.getValue() );
                }
        );
        return list;
    }


    private final UUID productId;
    private final Map< UUID, BigDecimal > pricesBySizeId;
    private final Map< UUID, String > sizeNames;

}
