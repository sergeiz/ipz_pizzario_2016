/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.dto;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.*;

public class AccountDto
        extends DomainEntityDto< AccountDto >
        implements Authentication
{
    public AccountDto ( UUID domainId, String name, String email, String password )
    {
        super( domainId );
        this.name = name;
        this.email = email;
        this.password = password;
        this.authorities = new ArrayList<>();
    }

    public String getName ()
    {
        return name;
    }

    public String getEmail ()
    {
        return email;
    }

    public String getPassword () { return password; }

    @Override
    public String toString ()
    {
        return String.format(
                "ID = %s\nName = %s\nEmail = %s\nPassword = %s\n",
                getDomainId(),
                getName(),
                getEmail(),
                getPassword()
        );
    }

    @Override
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList( getDomainId(), getName(), getEmail(), getPassword() );
    }


    @Override
    public Collection< ? extends GrantedAuthority > getAuthorities ()
    {
        return authorities;
    }

    @Override
    public Object getCredentials ()
    {
        return getPassword();
    }

    @Override
    public Object getDetails ()
    {
        return getName();
    }

    @Override
    public Object getPrincipal ()
    {
        return getEmail();
    }

    @Override
    public boolean isAuthenticated ()
    {
        return authentificated;
    }

    @Override
    public void setAuthenticated ( boolean b ) throws IllegalArgumentException
    {
        authentificated = b;
    }

    public void addAuthority ( String authorityName )
    {
        authorities.add( new SimpleGrantedAuthority( authorityName ) );
    }


    private final String name;
    private final String email;
    private final String password;
    private List< GrantedAuthority > authorities;
    private boolean authentificated;
}
