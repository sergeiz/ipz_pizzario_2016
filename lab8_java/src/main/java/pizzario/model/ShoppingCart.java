/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.exceptions.LockingEmptyCartException;
import pizzario.exceptions.UnmodifiableCartException;
import pizzario.utils.DomainEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class ShoppingCart extends DomainEntity
{
    protected ShoppingCart () {}

    public ShoppingCart ( UUID domainId )
    {
        super( domainId );
    }

    public List< ProductItem > getItems ()
    {
        return items;
    }

    public int findItemIndex ( Product p, ProductSize sz )
    {
        for ( int i = 0; i < items.size(); i++ )
        {
            ProductItem item = items.get( i );
            if ( item.getProduct() == p && item.getSize() == sz )
                return i;
        }

        return -1;
    }

    public int getItemQuantity ( Product p, ProductSize sz )
    {
        int itemIndex = findItemIndex( p, sz );
        if ( itemIndex == -1 )
            return 0;

        return items.get( itemIndex ).getQuantity();
    }

    public void placeItem ( ProductItem item ) throws UnmodifiableCartException
    {
        if ( ! isModifiable() )
            throw new UnmodifiableCartException( getDomainId() );

        int existingItemIndex = findItemIndex( item.getProduct(), item.getSize() );
        if ( existingItemIndex != -1 )
            items.set( existingItemIndex, item );

        else
            items.add( item );

        setLastModified( LocalDateTime.now() );
    }

    public void dropItem ( int index ) throws UnmodifiableCartException
    {
        if ( ! isModifiable() )
            throw new UnmodifiableCartException( getDomainId() );

        items.remove( index );

        setLastModified( LocalDateTime.now() );
    }

    public void clearItems () throws UnmodifiableCartException
    {
        if ( ! isModifiable() )
            throw new UnmodifiableCartException( getDomainId() );

        items.clear();

        setLastModified( LocalDateTime.now() );
    }

    public boolean isModifiable ()
    {
        return this.modifiable;
    }

    public BigDecimal cost ()
    {
        return items.stream()
                .map( item -> item.getCost() )
                .reduce( BigDecimal.ZERO, BigDecimal::add );
    }

    public void checkout () throws LockingEmptyCartException
    {
        if ( items.isEmpty() )
            throw new LockingEmptyCartException( getDomainId() );

        this.modifiable = false;
        setLastModified( LocalDateTime.now() );
    }


    public LocalDateTime getLastModified () { return lastModified; }

    private void setLastModified ( LocalDateTime lastModified ) { this.lastModified = lastModified; }


    @OneToMany( cascade = CascadeType.PERSIST )
    private List< ProductItem > items = new ArrayList<>();

    private LocalDateTime lastModified = LocalDateTime.now();

    private boolean modifiable = true;
}
