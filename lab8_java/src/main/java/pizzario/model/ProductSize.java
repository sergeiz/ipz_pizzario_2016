/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.DomainEntity;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import java.util.UUID;

@Entity
@Access( AccessType.PROPERTY )
public class ProductSize extends DomainEntity
{
    protected ProductSize () {}

    public ProductSize ( UUID domainId, String name, String imageUrl, int diameter, int weight )
    {
        super( domainId );
        setName( name );
        setImageUrl( imageUrl );
        setDiameter( diameter );
        setWeight( weight );
    }

    public String getName ()
    {
        return this.name;
    }

    public void setName ( String name )
    {
        this.name = name;
    }

    public String getImageUrl () { return this.imageUrl; }

    public void setImageUrl ( String imageUrl ) { this.imageUrl = imageUrl; }

    public int getDiameter ()
    {
        return this.diameter;
    }

    public void setDiameter ( int diameter )
    {
        this.diameter = diameter;
    }

    public int getWeight ()
    {
        return this.weight;
    }

    public void setWeight ( int weight )
    {
        this.weight = weight;
    }



    private String name;

    private String imageUrl;

    private int diameter;

    private int weight;
}
