/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

public enum DeliveryStatus
{
    Waiting,
    InProgress,
    Delivered
}
