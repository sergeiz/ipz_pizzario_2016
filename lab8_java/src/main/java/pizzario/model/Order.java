/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.exceptions.OrderLifecycleException;
import pizzario.utils.DomainEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity( name = "CustomerOrder" )
public class Order extends DomainEntity
{
    protected Order () {}

    public Order ( UUID domainId, List< ProductItem > items, Contact contact, LocalDateTime time )
    {
        super( domainId );

        this.items.addAll( items );

        setContact( contact );

        this.placementTime = time;

        this.unfinishedCookingsCount = getTotalCookingsCount();
    }

    public List< ProductItem > getItems ()
    {
        return this.items;
    }

    public BigDecimal getBasicCost ()
    {
        return items.stream()
                .map( item -> item.getCost() )
                .reduce( BigDecimal.ZERO, BigDecimal::add );
    }


    @Access( AccessType.PROPERTY )
    @Embedded
    public Discount getDiscount ()
    {
        return this.discount;
    }

    public void setDiscount ( Discount discount ) throws OrderLifecycleException
    {
        if ( this.status != OrderStatus.Placed )
        {
            throw new OrderLifecycleException(
                    getDomainId(),
                    this.status.toString(),
                    OrderStatus.Placed.toString()
            );
        }
        this.discount = discount;
    }

    public BigDecimal getTotalCost ()
    {
        return this.getDiscount().getDiscountedPrice( getBasicCost() );
    }

    public OrderStatus getStatus ()
    {
        return this.status;
    }

    public LocalDateTime getPlacementTime ()
    {
        return this.placementTime;
    }

    @Access( AccessType.PROPERTY )
    @Embedded
    public Contact getContact ()
    {
        return this.contact;
    }

    private void setContact ( Contact contact )
    {
        this.contact = contact;
    }

    public String getComment () { return this.comment; }

    public void setComment ( String comment ) { this.comment = comment; }

    public int getFinishedCookingsCount () { return getTotalCookingsCount() - unfinishedCookingsCount; }

    public int getTotalCookingsCount () {
        return items.stream()
                .map( item -> item.getQuantity() )
                .reduce( 0, Integer::sum );
    }

    public void confirm () throws OrderLifecycleException
    {
        if ( this.status != OrderStatus.Placed )
        {
            throw new OrderLifecycleException(
                    getDomainId(),
                    this.status.toString(),
                    OrderStatus.Placed.toString()
            );
        }

        this.status = OrderStatus.Confirmed;
    }

    public void cancel () throws OrderLifecycleException
    {
        if ( this.status != OrderStatus.Placed )
        {
            throw new OrderLifecycleException(
                    getDomainId(),
                    this.status.toString(),
                    OrderStatus.Placed.toString()
            );
        }

        this.status = OrderStatus.Cancelled;
    }

    public void cookingAssignmentCompleted () throws OrderLifecycleException
    {
        if ( this.status == OrderStatus.Confirmed )
        {
            -- unfinishedCookingsCount;
            if ( unfinishedCookingsCount == 0 )
                this.status = OrderStatus.Delivering;
        }
        else
        {
            throw new OrderLifecycleException(
                    getDomainId(),
                    this.status.toString(),
                    OrderStatus.Confirmed.toString()
            );
        }

    }

    public void deliveryCompleted () throws OrderLifecycleException
    {
        if ( this.status != OrderStatus.Delivering )
        {
            throw new OrderLifecycleException(
                    getDomainId(),
                    this.status.toString(),
                    OrderStatus.Delivering.toString()
            );
        }

        this.status = OrderStatus.Completed;
    }

    public List< CookingAssignment > generateCookingAssignments ()
    {
        List< CookingAssignment > cookingAssignments = new ArrayList<>();

        for ( ProductItem item : items )
        {
            for ( int i = 0; i < item.getQuantity(); i++ )
            {
                cookingAssignments.add(
                        new CookingAssignment(
                                UUID.randomUUID(),
                                this,
                                item.getProduct(),
                                item.getSize()
                        )
                );
            }
        }

        return cookingAssignments;
    }


    public Delivery generateDelivery ()
    {
        return new Delivery( UUID.randomUUID(), this );
    }


    @OneToMany
    private List< ProductItem > items = new ArrayList<>();

    @Transient
    private Contact contact;

    @Transient
    private Discount discount = new Discount();

    private String comment;

    private LocalDateTime placementTime;

    private OrderStatus status = OrderStatus.Placed;

    private int unfinishedCookingsCount;
}
