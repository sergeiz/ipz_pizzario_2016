/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.exceptions.DeliveryLifecycleException;
import pizzario.exceptions.OrderLifecycleException;
import pizzario.utils.DomainEntity;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Access( AccessType.PROPERTY )
public class Delivery extends DomainEntity
{
    protected Delivery () {}

    public Delivery ( UUID domainId, Order order )
    {
        super( domainId );
        setOrder( order );
        setCash2Collect( order.getTotalCost() );
    }

    @ManyToOne
    public Order getOrder ()
    {
        return this.order;
    }

    private void setOrder ( Order order )
    {
        this.order = order;
    }


    public String getDriverName ()
    {
        return this.driverName;
    }

    private void setDriverName ( String driverName )
    {
        this.driverName = driverName;
    }


    public BigDecimal getCash2Collect ()
    {
        return cash2Collect;
    }

    public void setCash2Collect ( BigDecimal cash2Collect )
    {
        this.cash2Collect = cash2Collect;
    }



    public DeliveryStatus getStatus ()
    {
        return status;
    }

    private void setStatus ( DeliveryStatus status )
    {
        this.status = status;
    }


    public void startDelivery ( String driverName ) throws DeliveryLifecycleException
    {
        if ( this.status != DeliveryStatus.Waiting )
            throw DeliveryLifecycleException.makeStartWhenNotWaiting( getDomainId() );

        setDriverName( driverName );
        setStatus( DeliveryStatus.InProgress );
    }


    public void finishDelivery () throws DeliveryLifecycleException, OrderLifecycleException
    {
        if ( this.status != DeliveryStatus.InProgress )
            throw DeliveryLifecycleException.makeFinishWhenNotInProgress( getDomainId() );

        setStatus( DeliveryStatus.Delivered );
    }


    private String driverName;

    private Order order;

    private BigDecimal cash2Collect;

    private DeliveryStatus status = DeliveryStatus.Waiting;
}
