/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model.accounts;

import java.util.List;
import java.util.UUID;

public class OrdersExtractAccountVisitor implements AccountVisitor
{
    public OrdersExtractAccountVisitor ( List< UUID > orderIds )
    {
        this.orderIds = orderIds;
    }

    @Override
    public void visit ( Account a )
    {
        // Skip
    }

    @Override
    public void visit ( OperatorAccount a )
    {
        a.getOrders().stream()
            .map( o -> o.getDomainId() )
                .forEach( id -> orderIds.add( id ) );

    }

    private List< UUID > orderIds;
}
