/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.exceptions.PriceUndefinedException;
import pizzario.exceptions.RecipeUndefinedException;
import pizzario.utils.DomainEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Access( AccessType.PROPERTY )
public class Product extends DomainEntity
{
    protected Product () {}

    public Product ( UUID domainId, String name, String imageUrl )
    {
        super( domainId );
        setName( name );
        setImageUrl( imageUrl );
    }

    public String getName ()
    {
        return this.name;
    }

    public void setName ( String name )
    {
        this.name = name;
    }


    public String getImageUrl ()
    {
        return this.imageUrl;
    }

    public void setImageUrl ( String imageUrl )
    {
        this.imageUrl = imageUrl;
    }


    public String buildDescription () {
        if ( recipes.isEmpty() )
            return "";

        else
        {
            Recipe r = recipes.get( 0 );
            List< String > ingredientNames =
                    r.listUsedIngredients().stream()
                            .map( i -> i.getName() )
                            .sorted()
                            .collect( Collectors.toList() );

            return String.join( " • ", ingredientNames );
        }
    }

    public Iterable< ProductSize > listAvailableSizes ()
    {
        return prices.keySet();
    }

    public Iterable< Map.Entry< ProductSize, BigDecimal> > listAvailablePrices ()
    {
        return prices.entrySet();
    }

    public Iterable< Recipe > listAvailableRecipes ()
    {
        return recipes;
    }

    public BigDecimal getPrice ( ProductSize size ) throws PriceUndefinedException
    {
        BigDecimal price = prices.get( size );
        if ( price != null )
            return price;

        throw new PriceUndefinedException( this.getName(), size.getName() );
    }

    public void setPrice ( ProductSize size, BigDecimal price )
    {
        prices.put( size, price );
    }

    public void removePrice ( ProductSize size ) throws PriceUndefinedException
    {
        BigDecimal price = prices.get( size );
        if ( price != null )
            prices.remove( size );

        else
            throw new PriceUndefinedException( this.getName(), size.getName() );
    }

    public Recipe getRecipe ( ProductSize size ) throws RecipeUndefinedException
    {
        Recipe recipe = findRecipe( size );
        if ( recipe != null )
            return recipe;

        throw new RecipeUndefinedException( this.getName(), size.getName() );
    }

    public Recipe defineRecipe ( ProductSize size )
    {
        Recipe recipe = findRecipe( size );
        if ( recipe != null )
            return recipe;

        recipe = new Recipe( UUID.randomUUID(), this, size );

        recipes.add( recipe );
        return recipe;
    }

    public void removeRecipe ( ProductSize size ) throws RecipeUndefinedException
    {
        Recipe recipe = findRecipe( size );
        if ( recipe == null )
            throw new RecipeUndefinedException( this.getName(), size.getName() );

        recipes.remove( recipe );
    }

    private Recipe findRecipe ( ProductSize size )
    {
        return recipes.stream()
                .filter( r -> r.getSize() == size )
                .findFirst().orElse( null )
                ;
    }

    private String name;

    private String imageUrl;

    @OneToMany( cascade = CascadeType.ALL )
    @Access( AccessType.FIELD )
    private List< Recipe > recipes = new ArrayList<>();

    @ElementCollection
    @Access( AccessType.FIELD )
    private Map< ProductSize, BigDecimal > prices = new HashMap<>();

}
