/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.exceptions.RemovingUnusedIngredientException;
import pizzario.utils.DomainEntity;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

@Entity
@Access( AccessType.PROPERTY )
public class Recipe extends DomainEntity
{
    protected Recipe () {}

    public Recipe ( UUID domainId, Product product, ProductSize size )
    {
        super( domainId );
        setProduct( product );
        setSize( size );
    }

    @ManyToOne
    public Product getProduct ()
    {
        return this.product;
    }

    private void setProduct ( Product product )
    {
        this.product = product;
    }

    @ManyToOne
    public ProductSize getSize ()
    {
        return this.size;
    }

    private void setSize ( ProductSize size )
    {
        this.size = size;
    }

    public Set< Ingredient > listUsedIngredients ()
    {
        return ingredients.keySet();
    }

    public boolean usesIngredient ( Ingredient i )
    {
        return ingredients.containsKey( i );
    }

    public int getIngredientWeight ( Ingredient i )
    {
        Integer weight = ingredients.get( i );
        return ( weight != null ) ? weight : 0;
    }

    public Recipe useIngredient ( Ingredient i, int weight )
    {
        ingredients.put( i, weight );
        return this;
    }

    public void removeIngredient ( Ingredient i ) throws RemovingUnusedIngredientException
    {
        if ( usesIngredient( i ) )
            ingredients.remove( i );

        else
            throw new RemovingUnusedIngredientException( product.getName(), i.getName() );
    }


    private Product product;

    private ProductSize size;

    @ElementCollection
    @Access( AccessType.FIELD )
    private Map< Ingredient, Integer > ingredients = new HashMap<>();

}
