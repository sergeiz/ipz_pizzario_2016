/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service;

import org.springframework.security.access.prepost.PreAuthorize;
import pizzario.dto.CookingAssignmentDto;
import pizzario.exceptions.CookingLifecycleException;
import pizzario.exceptions.OrderLifecycleException;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

public interface ICookingAssignmentService extends IDomainEntityService< CookingAssignmentDto >
{
    List< UUID > viewCookedRightNow ();

    List< UUID > viewWaiting ();

    List< UUID > viewOrderAssignments ( @NotNull UUID orderId );

    @PreAuthorize( "hasAuthority('OPERATOR')" )
    void markCookingStarted ( @NotNull UUID cookingAssignmentId )
            throws CookingLifecycleException;

    @PreAuthorize( "hasAuthority('OPERATOR')" )
    void markCookingFinished ( @NotNull UUID cookingAssignmentId )
            throws CookingLifecycleException, OrderLifecycleException;
}
