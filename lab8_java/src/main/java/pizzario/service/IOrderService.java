/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.security.access.prepost.PreAuthorize;
import pizzario.dto.OrderDto;
import pizzario.exceptions.ModifiableCartException;
import pizzario.exceptions.OrderLifecycleException;
import pizzario.service.validation.Percentage;
import pizzario.service.validation.Phone;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public interface IOrderService extends IDomainEntityService< OrderDto >
{
    OrderDto findOrder ( @NotNull UUID orderId );

    List< UUID > viewUnconfirmed ();

    List< UUID > viewConfirmed ();

    List< UUID > viewReady4Delivery ();

    UUID createNew (
            @NotBlank String customerName,
            @NotBlank String deliveryAddress,
            @Phone String contactPhone,
            @NotBlank @Email String contactEmail,
            @NotNull String comment,
            @NotNull UUID cartId
    ) throws ModifiableCartException;

    @PreAuthorize( "hasAuthority('OPERATOR')" )
    void setDiscount (
            @NotNull UUID orderId,
            @Percentage BigDecimal discountPercent
    ) throws OrderLifecycleException;

    @PreAuthorize( "hasAuthority('OPERATOR')" )
    void confirm (
            @NotNull UUID orderId,
            @NotNull UUID operatorId
    ) throws OrderLifecycleException;

    @PreAuthorize( "hasAuthority('OPERATOR')" )
    void cancel ( @NotNull UUID orderId ) throws OrderLifecycleException;
}
