/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import pizzario.dto.AccountDto;


@Validated
public interface IAuthenticationService extends AuthenticationProvider
{
    @Override
    AccountDto authenticate ( Authentication authentication );
}