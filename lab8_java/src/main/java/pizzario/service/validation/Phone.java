/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.Pattern;
import java.lang.annotation.*;

@Documented
@Constraint( validatedBy = {} )
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Retention( RetentionPolicy.RUNTIME)
@ReportAsSingleViolation
@Pattern( regexp = "^((1-?)|(\\+1 ?))?\\(?(\\d{3})[)\\-.]?(\\d{3})[\\-.]?(\\d{2})[\\-.]?(\\d{2})$" )
public @interface Phone
{
    String message() default "Inappropriate phone number format";

    Class<?>[] groups() default {};

    Class<? extends Payload >[] payload() default {};
}
