/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import pizzario.dto.AccountDto;
import pizzario.exceptions.DuplicateNamedEntityException;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;


public interface IAccountService extends IDomainEntityService< AccountDto >
{
    List< UUID > viewAssociatedOrders ( @NotNull UUID accountId );

    UUID createOperator (
            @NotBlank String name,
            @NotBlank @Email String email,
            @NotBlank String password
    )  throws DuplicateNamedEntityException;

    void changeName (
            @NotNull UUID accountId,
            @NotBlank String newName
    );

    void changeEmail (
            @NotNull UUID accountId,
            @NotBlank @Email String newEmail
    ) throws DuplicateNamedEntityException;

    void changePassword (
            @NotNull UUID accountId,
            @NotBlank String newPassword
    );
}
