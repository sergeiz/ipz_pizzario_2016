/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pizzario.dto.IngredientDto;
import pizzario.exceptions.DuplicateNamedEntityException;
import pizzario.model.Ingredient;
import pizzario.repository.IIngredientRepository;
import pizzario.service.IIngredientService;

import javax.inject.Inject;
import java.util.List;
import java.util.UUID;


@Service
class IngredientService
        extends BasicService
        implements IIngredientService
{
    @Transactional
    @Override
    public List< UUID > viewAll ()
    {
        return ingredientRepository.selectAllDomainIds();
    }

    @Transactional
    @Override
    public IngredientDto view ( UUID ingredientId )
    {
        Ingredient i = resolveIngredient( ingredientId );
        return DtoBuilder.toDto( i );
    }

    @Transactional
    @Override
    public UUID create ( String name ) throws DuplicateNamedEntityException
    {
        if ( ingredientRepository.findByName( name ) != null )
            throw new DuplicateNamedEntityException( Ingredient.class, name );

        Ingredient i = new Ingredient( UUID.randomUUID(), name );
        ingredientRepository.add( i );
        return i.getDomainId();
    }

    @Transactional
    @Override
    public void rename ( UUID ingredientId, String newName ) throws DuplicateNamedEntityException
    {
        if ( ingredientRepository.findByName( newName ) != null )
            throw new DuplicateNamedEntityException( Ingredient.class, newName );

        Ingredient i = resolveIngredient( ingredientId );
        i.setName( newName );
    }

    private Ingredient resolveIngredient ( UUID ingredientId )
    {
        return resolveEntity( ingredientRepository, ingredientId );
    }

    @Inject
    private IIngredientRepository ingredientRepository;
}
