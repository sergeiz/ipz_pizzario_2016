/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pizzario.dto.ProductDto;
import pizzario.dto.ProductPricingDto;
import pizzario.dto.ProductRecipeDto;
import pizzario.dto.ProductSizeDto;
import pizzario.exceptions.DuplicateNamedEntityException;
import pizzario.exceptions.PriceUndefinedException;
import pizzario.exceptions.RecipeUndefinedException;
import pizzario.exceptions.RemovingUnusedIngredientException;
import pizzario.model.Ingredient;
import pizzario.model.Product;
import pizzario.model.ProductSize;
import pizzario.model.Recipe;
import pizzario.repository.IIngredientRepository;
import pizzario.repository.IProductRepository;
import pizzario.repository.IProductSizeRepository;
import pizzario.service.IProductService;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;


@Service
class ProductService
        extends BasicService
        implements IProductService
{
    @Transactional
    @Override
    public List< UUID > viewAll()
    {
        return productRepository.selectAllDomainIds();
    }


    @Transactional
    @Override
    public ProductDto view ( UUID productId )
    {
        Product product = resolveProduct( productId );
        return DtoBuilder.toDto( product );
    }


    @Transactional
    @Override
    public List< ProductSizeDto > availableSizes ( UUID productId )
    {
        Product product = resolveProduct( productId );

        List< ProductSizeDto > sizes = new ArrayList<>();
        for ( ProductSize size : product.listAvailableSizes() )
            sizes.add( DtoBuilder.toDto( size ) );

        return sizes;
    }


    @Transactional
    @Override
    public List< ProductRecipeDto > availableRecipes ( UUID productId )
    {
        Product product = resolveProduct( productId );

        List< ProductRecipeDto > recipes = new ArrayList<>();
        for ( Recipe recipe : product.listAvailableRecipes() )
            recipes.add( DtoBuilder.toDto( recipe ) );

        return recipes;
    }


    @Transactional
    @Override
    public ProductPricingDto viewPrices ( UUID productId )
    {
        Product product = resolveProduct( productId );

        Map< UUID, BigDecimal > pricesBySizeIds = new HashMap<>();
        Map< UUID, String > sizeNames = new HashMap<>();

        for ( Map.Entry< ProductSize, BigDecimal > priceEntry : product.listAvailablePrices() )
        {
            ProductSize size = priceEntry.getKey();

            pricesBySizeIds.put(
                    size.getDomainId(),
                    priceEntry.getValue()
            );

            sizeNames.put( size.getDomainId(), size.getName() );
        }

        return new ProductPricingDto( productId, pricesBySizeIds, sizeNames );
    }


    @Transactional
    @Override
    public ProductRecipeDto viewRecipe ( UUID productId, UUID sizeId ) throws RecipeUndefinedException
    {
        Product product = resolveProduct( productId );
        ProductSize size = resolveSize( sizeId );

        Recipe recipe = product.getRecipe( size );
        return DtoBuilder.toDto( recipe );
    }


    @Transactional
    @Override
    public UUID create ( String name, String imageUrl ) throws DuplicateNamedEntityException
    {
        if ( productRepository.findByName( name ) != null )
            throw new DuplicateNamedEntityException( Product.class, name );

        Product p = new Product( UUID.randomUUID(), name, imageUrl );
        productRepository.add( p );

        return p.getDomainId();
    }


    @Transactional
    @Override
    public void rename ( UUID productId, String newName ) throws DuplicateNamedEntityException
    {
        if ( productRepository.findByName( newName ) != null )
            throw new DuplicateNamedEntityException( Product.class, newName );

        Product p = resolveProduct( productId );
        p.setName( newName );
    }


    @Transactional
    @Override
    public void updateImageUrl ( UUID productId, String imageUrl )
    {
        Product p = resolveProduct( productId );
        p.setImageUrl( imageUrl );
    }


    @Transactional
    @Override
    public void definePrice ( UUID productId, UUID sizeId, BigDecimal price )
    {
        Product p = resolveProduct( productId );
        ProductSize sz = resolveSize( sizeId );
        p.setPrice( sz, price );
    }


    @Transactional
    @Override
    public void undefinePrice ( UUID productId, UUID sizeId ) throws PriceUndefinedException
    {
        Product p = resolveProduct( productId );
        ProductSize sz = resolveSize( sizeId );
        p.removePrice( sz );
    }


    @Transactional
    @Override
    public void defineRecipeIngredient ( UUID productId, UUID sizeId, UUID ingredientId, int weight )
    {
        Product p = resolveProduct( productId );
        ProductSize sz = resolveSize( sizeId );
        Ingredient i = resolveIngredient( ingredientId );

        p.defineRecipe( sz ).useIngredient( i, weight );
    }


    @Transactional
    @Override
    public void removeRecipeIngredient ( UUID productId, UUID sizeId, UUID ingredientId )
            throws RemovingUnusedIngredientException
    {
        Product p = resolveProduct( productId );
        ProductSize sz = resolveSize( sizeId );
        Ingredient i = resolveIngredient( ingredientId );
        p.defineRecipe( sz ).removeIngredient( i );
    }


    private Product resolveProduct ( UUID productId )
    {
        return resolveEntity( productRepository, productId );
    }

    private ProductSize resolveSize ( UUID sizeId )
    {
        return resolveEntity( productSizeRepository, sizeId );
    }

    private Ingredient resolveIngredient ( UUID ingredientId )
    {
        return resolveEntity( ingredientRepository, ingredientId );
    }

    @Inject
    private IProductRepository productRepository;

    @Inject
    private IProductSizeRepository productSizeRepository;

    @Inject
    private IIngredientRepository ingredientRepository;
}
