/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import pizzario.dto.*;
import pizzario.model.*;
import pizzario.model.accounts.Account;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class DtoBuilder
{

    public static AccountDto toDto ( Account account )
    {
        return new AccountDto(
                account.getDomainId(),
                account.getName(),
                account.getEmail(),
                account.getPassword()
        );
    }

    public static CookingAssignmentDto toDto ( CookingAssignment ca )
    {
        return new CookingAssignmentDto(
                ca.getDomainId(),
                ca.getOrder().getDomainId(),
                ca.getProduct().getDomainId(),
                ca.getProduct().getName(),
                ca.getSize().getDomainId(),
                ca.getSize().getName(),
                ca.getStatus().toString()
        );
    }

    public static DeliveryDto toDto ( Delivery delivery )
    {
        return new DeliveryDto(
                delivery.getDomainId(),
                delivery.getOrder().getDomainId(),
                delivery.getDriverName(),
                delivery.getStatus().toString(),
                delivery.getCash2Collect()
        );
    }

    public static IngredientDto toDto ( Ingredient ingredient )
    {
        return new IngredientDto( ingredient.getDomainId(), ingredient.getName() );
    }

    public static ProductDto toDto ( Product product )
    {
        return new ProductDto(
                product.getDomainId(),
                product.getName(),
                product.getImageUrl(),
                product.buildDescription()
        );
    }

    public static ProductSizeDto toDto ( ProductSize size )
    {
        return new ProductSizeDto(
                size.getDomainId(),
                size.getName(),
                size.getImageUrl(),
                size.getDiameter(),
                size.getWeight()
        );
    }

    public static ProductRecipeDto toDto ( Recipe recipe )
    {
        Map< IngredientDto, Integer > usedIngredients = new HashMap<>();
        for ( Ingredient i : recipe.listUsedIngredients() )
        {
            usedIngredients.put(
                    DtoBuilder.toDto( i ),
                    recipe.getIngredientWeight( i )
            );
        }
        return new ProductRecipeDto(
                recipe.getProduct().getDomainId(),
                recipe.getSize().getDomainId(),
                usedIngredients
        );
    }

    public static ProductItemDto toDto ( ProductItem item )
    {
        return new ProductItemDto(
                item.getDomainId(),
                item.getProduct().getDomainId(),
                item.getProduct().getName(),
                item.getSize().getDomainId(),
                item.getSize().getName(),
                item.getQuantity(),
                item.getFixedPrice(),
                item.getCost()
        );
    }

    public static ShoppingCartDto toDto ( ShoppingCart cart )
    {
        List< ProductItemDto > itemDtos = new ArrayList<>();
        cart.getItems().stream().forEach(
                item -> itemDtos.add( toDto( item ) )
        );
        return new ShoppingCartDto( cart.getDomainId(), itemDtos, cart.cost(), !cart.isModifiable() );
    }

    public static OrderDto toDto ( Order order )
    {
        return new OrderDto(
                order.getDomainId(),
                order.getContact().getName(),
                order.getContact().getAddress(),
                order.getContact().getPhone(),
                order.getContact().getEmail(),
                order.getPlacementTime(),
                order.getStatus().toString(),
                order.getBasicCost(),
                order.getTotalCost(),
                order.getDiscount().getPercent(),
                order.getComment(),
                order.getFinishedCookingsCount(),
                order.getTotalCookingsCount()
        );
    }
}
