/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pizzario.dto.OrderDto;
import pizzario.exceptions.ModifiableCartException;
import pizzario.exceptions.OrderLifecycleException;
import pizzario.exceptions.ServiceUnresolvedEntityException;
import pizzario.model.*;
import pizzario.model.accounts.OperatorAccount;
import pizzario.repository.IAccountRepository;
import pizzario.repository.ICookingAssignmentRepository;
import pizzario.repository.IOrderRepository;
import pizzario.repository.IShoppingCartRepository;
import pizzario.service.IOrderService;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;


@Service
class OrderService
        extends BasicService
        implements IOrderService
{
    @Transactional
    @Override
    public List< UUID > viewAll ()
    { 
       return orderRepository.selectAllDomainIds();
    }


    @Transactional
    @Override
    public List< UUID > viewUnconfirmed ()
    {
        return orderRepository.selectUnconfirmedIds();
    }


    @Transactional
    @Override
    public List< UUID > viewConfirmed ()
    {
        return orderRepository.selectConfirmedIds();
    }


    @Transactional
    @Override
    public List< UUID > viewReady4Delivery ()
    {
        return orderRepository.selectReady4DeliveryIds();
    }


    @Transactional
    @Override
    public OrderDto view ( UUID orderId )
    {
        Order o = resolveOrder( orderId );
        return DtoBuilder.toDto( o );
    }

    @Transactional
    @Override
    public OrderDto findOrder ( UUID orderId )
    {
        Order o = orderRepository.findByDomainId( orderId );
        if ( o != null )
            return DtoBuilder.toDto( o );

        else
            return null;
    }


    @Transactional
    @Override
    public UUID createNew (
            String customerName,
            String deliveryAddress,
            String contactPhone,
            String email,
            String comment,
            UUID cartId
    ) throws ModifiableCartException
    {
        ShoppingCart cart = resolveCart( cartId );
        if ( cart.isModifiable() )
            throw new ModifiableCartException( cart.getDomainId() );

        Contact contact = new Contact(
                customerName,
                deliveryAddress,
                contactPhone,
                email
        );

        Order o = new Order(
                UUID.randomUUID(),
                cart.getItems(),
                contact,
                LocalDateTime.now()
        );

        o.setComment( comment );

        orderRepository.add( o );

        shoppingCartRepository.delete( cart );

        return o.getDomainId();
    }


    @Transactional
    @Override
    public void setDiscount ( UUID orderId, BigDecimal discountPercent )
            throws OrderLifecycleException
    {
        Order o = resolveOrder( orderId );
        o.setDiscount( new Discount( discountPercent ) );
    }


    @Transactional
    @Override
    public void confirm ( UUID orderId, UUID operatorId ) throws OrderLifecycleException
    {
        Order o = resolveOrder( orderId );
        OperatorAccount operatorAccount = resolveOperator( operatorId );
        o.confirm();

        operatorAccount.trackOrder( o );

        for ( CookingAssignment ca : o.generateCookingAssignments() )
            cookingAssignmentRepository.add( ca );
    }


    @Transactional
    @Override
    public void cancel ( UUID orderId ) throws OrderLifecycleException
    {
        Order o = resolveOrder( orderId );
        o.cancel();
    }


    private Order resolveOrder ( UUID orderId )
    {
        return resolveEntity( orderRepository, orderId );
    }

    private ShoppingCart resolveCart ( UUID cartId )
    {
        return resolveEntity( shoppingCartRepository, cartId );
    }

    private OperatorAccount resolveOperator ( UUID operatorId )
    {
        OperatorAccount account = accountRepository.findOperatorByDomainId( operatorId );
        if ( account == null )
            throw new ServiceUnresolvedEntityException( OperatorAccount.class, operatorId );

        return account;
    }


    @Inject
    private IOrderRepository orderRepository;

    @Inject
    private IShoppingCartRepository shoppingCartRepository;

    @Inject
    private ICookingAssignmentRepository cookingAssignmentRepository;

    @Inject
    private IAccountRepository accountRepository;

}
