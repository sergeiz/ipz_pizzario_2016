/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pizzario.dto.CookingAssignmentDto;
import pizzario.exceptions.CookingLifecycleException;
import pizzario.exceptions.OrderLifecycleException;
import pizzario.model.CookingAssignment;
import pizzario.model.Delivery;
import pizzario.model.Order;
import pizzario.model.OrderStatus;
import pizzario.repository.ICookingAssignmentRepository;
import pizzario.repository.IDeliveryRepository;
import pizzario.repository.IOrderRepository;
import pizzario.service.ICookingAssignmentService;

import javax.inject.Inject;
import java.util.List;
import java.util.UUID;


@Service
class CookingAssignmentService
        extends BasicService
        implements ICookingAssignmentService
{
    @Transactional
    @Override
    public List< UUID > viewAll ()
    {
        return cookingAssignmentRepository.selectAllDomainIds();
    }


    @Transactional
    @Override
    public List< UUID > viewCookedRightNow()
    {
        return cookingAssignmentRepository.selectCookingIds();
    }


    @Transactional
    @Override
    public List< UUID > viewWaiting ()
    {
        return cookingAssignmentRepository.selectWaitingIds();
    }


    @Transactional
    @Override
    public CookingAssignmentDto view ( UUID cookingAssignmentId )
    {
        CookingAssignment cooking = resolveCooking( cookingAssignmentId );
        return DtoBuilder.toDto( cooking );
    }


    @Transactional
    @Override
    public List< UUID > viewOrderAssignments( UUID orderId )
    {
        Order o = resolveOrder( orderId );
        return cookingAssignmentRepository.findOrderAssignments( o );
    }


    @Transactional
    @Override
    public void markCookingStarted ( UUID cookingAssignmentId ) throws CookingLifecycleException
    {
        CookingAssignment cooking = resolveCooking( cookingAssignmentId );
        cooking.startCooking();
    }


    @Transactional
    @Override
    public void markCookingFinished ( UUID cookingAssignmentId )
            throws CookingLifecycleException, OrderLifecycleException
    {
        CookingAssignment cooking = resolveCooking( cookingAssignmentId );
        OrderStatus previousOrderStatus = cooking.getOrder().getStatus();

        cooking.finishCooking();

        cooking.getOrder().cookingAssignmentCompleted();

        OrderStatus newOrderStatus = cooking.getOrder().getStatus();
        if ( previousOrderStatus != newOrderStatus )
        {
            Delivery d = cooking.getOrder().generateDelivery();
            deliveryRepository.add( d );
        }
    }

    private CookingAssignment resolveCooking ( UUID cookingID )
    {
        return resolveEntity( cookingAssignmentRepository, cookingID );
    }

    private Order resolveOrder ( UUID orderId )
    {
        return resolveEntity( orderRepository, orderId );
    }


    @Inject
    private ICookingAssignmentRepository cookingAssignmentRepository;

    @Inject
    private IDeliveryRepository deliveryRepository;

    @Inject
    private IOrderRepository orderRepository;
}
