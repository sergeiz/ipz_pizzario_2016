/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pizzario.dto.AccountDto;
import pizzario.exceptions.DuplicateNamedEntityException;
import pizzario.model.accounts.Account;
import pizzario.model.accounts.AccountVisitor;
import pizzario.model.accounts.OperatorAccount;
import pizzario.model.accounts.OrdersExtractAccountVisitor;
import pizzario.repository.IAccountRepository;
import pizzario.service.IAccountService;
import pizzario.service.IAuthenticationService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Service
class AccountService
        extends BasicService
        implements IAccountService, IAuthenticationService
{
    @Transactional
    @Override
    public List< UUID > viewAll ()
    {
        return accountRepository.selectAllDomainIds();
    }


    @Transactional
    @Override
    public AccountDto view ( UUID accountId )
    {
        Account a = resolveAccount( accountId );
        return DtoBuilder.toDto( a );
    }


    @Transactional
    @Override
    public List< UUID > viewAssociatedOrders ( UUID accountId )
    {
        Account a = resolveAccount( accountId );

        List< UUID > orderIds = new ArrayList<>();
        OrdersExtractAccountVisitor visitor = new OrdersExtractAccountVisitor( orderIds );
        a.accept( visitor );

        return orderIds;
    }


    @Transactional
    @Override
    public UUID createOperator ( String name, String email, String password ) throws DuplicateNamedEntityException
    {
        Account a = accountRepository.findByEmail( email );
        if ( a != null )
            throw new DuplicateNamedEntityException( Account.class, email );

        OperatorAccount operator = new OperatorAccount( UUID.randomUUID(), name, email, password );
        accountRepository.add( operator );
        return operator.getDomainId();
    }


    @Transactional
    @Override
    public void changeName ( UUID accountId, String newName )
    {
        Account a = resolveAccount( accountId );
        a.setName( newName );
    }


    @Transactional
    @Override
    public void changeEmail ( UUID accountId, String newEmail ) throws DuplicateNamedEntityException
    {
        if ( accountRepository.findByEmail( newEmail ) != null )
            throw new DuplicateNamedEntityException( Account.class, newEmail );

        Account a = resolveAccount( accountId );
        a.setEmail( newEmail );
    }


    @Transactional
    @Override
    public void changePassword ( UUID accountId, String newPassword )
    {
        Account a = resolveAccount( accountId );
        a.setPassword( newPassword );
    }


    @Transactional
    @Override
    public AccountDto authenticate ( Authentication authentication )
    {
        UsernamePasswordAuthenticationToken credentials = ( UsernamePasswordAuthenticationToken ) authentication;
        String email = credentials.getPrincipal().toString();
        String password = credentials.getCredentials().toString();
        credentials.eraseCredentials();

        Account a = accountRepository.findByEmail( email );
        if ( a == null )
            return null;

        if ( ! a.checkPassword( password ) )
            return null;

        AccountDto dto = DtoBuilder.toDto( a );
        dto.setAuthenticated( true );

        a.accept( new AccountVisitor()
        {
            @Override
            public void visit ( Account account )
            {
                // Nothing
            }

            @Override
            public void visit ( OperatorAccount account )
            {
                dto.addAuthority( "OPERATOR" );
            }
        } );

        return dto;
    }


    @Override
    public boolean supports ( Class< ? > aClass )
    {
        return aClass == UsernamePasswordAuthenticationToken.class;
    }


    private Account resolveAccount ( UUID accountId )
    {
        return resolveEntity( accountRepository, accountId );
    }


    @Inject
    private IAccountRepository accountRepository;
}
