/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.security.access.prepost.PreAuthorize;
import pizzario.dto.DeliveryDto;
import pizzario.exceptions.DeliveryLifecycleException;
import pizzario.exceptions.OrderLifecycleException;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

public interface IDeliveryService extends IDomainEntityService< DeliveryDto >
{
    List< UUID > viewWaiting ();

    List< UUID > viewInProgress ();

    DeliveryDto findOrderDelivery ( @NotNull UUID orderId );

    @PreAuthorize( "hasAuthority('OPERATOR')" )
    void markStarted (
            @NotNull UUID deliveryId,
            @NotBlank String driverName
    ) throws DeliveryLifecycleException;

    @PreAuthorize( "hasAuthority('OPERATOR')" )
    void markDelivered ( @NotNull UUID deliveryId )
            throws DeliveryLifecycleException, OrderLifecycleException;
}
