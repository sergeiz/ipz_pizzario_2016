/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan( basePackages = "pizzario.testapp" )
public class TestAppReportConfiguration extends BaseRootConfiguration {

    @Override
    protected String getPersistenceUnitName() {
        return "PizzarioRead";
    }
}
