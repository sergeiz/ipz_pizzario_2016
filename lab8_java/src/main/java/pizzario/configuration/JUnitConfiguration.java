/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
public class JUnitConfiguration extends RootConfiguration {

    @Override
    protected String getPersistenceUnitName() {
        return "PizzarioJUnit";
    }

}
