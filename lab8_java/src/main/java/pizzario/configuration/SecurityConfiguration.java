/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.configuration;

import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import pizzario.service.IAuthenticationService;

import javax.inject.Inject;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
    prePostEnabled = true, order = 0, mode = AdviceMode.PROXY, proxyTargetClass = false
)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter
{
    @Override
    protected void configure ( AuthenticationManagerBuilder builder )
    {
        builder.authenticationProvider( this.authenticationService );
    }

    @Override
    public void configure ( WebSecurity security )
    {
        security.ignoring().antMatchers( "/resources/**" );
    }

    @Override
    protected void configure ( HttpSecurity security ) throws Exception
    {
        security
                .authorizeRequests()
                    .antMatchers( "/operator/**" ).hasAuthority( "OPERATOR" )
                .and().formLogin()
                    .loginPage( "/login" ).failureUrl( "/login?loginFailed" )
                    .usernameParameter( "email" )
                    .passwordParameter( "password" )
                    .permitAll()
                .and().logout()
                    .logoutUrl( "/logout" ).logoutSuccessUrl("/login?loggedOut")
                    .invalidateHttpSession( true )
                    .permitAll()
                .and().sessionManagement()
                    .sessionFixation().changeSessionId()
                .and()
                    .csrf()
                        .requireCsrfProtectionMatcher(
                            (r) -> false
                        );
    }

    @Inject
    private IAuthenticationService authenticationService;
}
