/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pizzario.dto.ProductDto;
import pizzario.dto.ProductPricingDto;
import pizzario.dto.ProductSizeDto;
import pizzario.service.IProductService;
import pizzario.service.IProductSizeService;
import pizzario.web.constants.AttributeNames;
import pizzario.web.constants.ViewNames;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;


@Controller
@RequestMapping( path = "/menu", method = RequestMethod.GET )
public class MenuController
{
    @RequestMapping( path = "/", method = RequestMethod.GET )
    public String handleRoot ( Map< String, Object > model )
    {
        model.put( AttributeNames.MenuView.Products,        getProductslist() );
        model.put( AttributeNames.MenuView.Sizes,           getProductSizesList() );
        model.put( AttributeNames.MenuView.ProductPrices,   getProductPricesMap() );

        return ViewNames.Menu;
    }


    private List< ProductDto > getProductslist ()
    {
        List< UUID > productIds = productService.viewAll();

        List< ProductDto > result = new ArrayList<>();
        for ( UUID productId : productIds )
            result.add( productService.view( productId ) );

        result.sort(
                ( ProductDto p1, ProductDto p2 ) -> p1.getName().compareTo( p2.getName() )
        );

        return result;
    }


    private List< ProductSizeDto > getProductSizesList ()
    {
        List< UUID > productSizeIds = productSizeService.viewAll();

        List< ProductSizeDto > result = new ArrayList<>();
        for ( UUID productSizeId : productSizeIds )
            result.add( productSizeService.view( productSizeId ) );

        result.sort(
                ( ProductSizeDto s1, ProductSizeDto s2 ) -> s1.getDiameter() - s2.getDiameter()
        );

        return result;
    }


    private Map< UUID, Map< UUID, BigDecimal > > getProductPricesMap ()
    {
        Map< UUID, Map< UUID, BigDecimal > > result = new TreeMap<>();

        for ( UUID productId : productService.viewAll() )
        {
            ProductPricingDto pricingDto = productService.viewPrices( productId );
            result.put( productId, pricingDto.getPricesBySizeId() );
        }

        return result;
    }


    @Inject
    private IProductService productService;

    @Inject
    private IProductSizeService productSizeService;
}
