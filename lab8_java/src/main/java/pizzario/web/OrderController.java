/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import pizzario.dto.OrderDto;
import pizzario.service.IOrderService;
import pizzario.web.constants.AttributeNames;
import pizzario.web.constants.ErrorKeys;
import pizzario.web.constants.ViewNames;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Map;

@Controller
@RequestMapping( "/order" )
public class OrderController
{
    @RequestMapping( path="/", method = RequestMethod.GET )
    public ModelAndView viewOrder ( HttpSession session, Map< String, Object > model )
    {
        SessionDataProvider sessionProvider = new SessionDataProvider( session );
        OrderDto orderDto = sessionProvider.currentOrder( orderService );
        if ( orderDto == null )
        {
            model.put( AttributeNames.ErrorView.Title,     ErrorKeys.NoOrderTitle   );
            model.put( AttributeNames.ErrorView.Message,   ErrorKeys.NoOrderMessage );
            model.put( AttributeNames.ErrorView.Arguments, "" );

            return new ModelAndView( ViewNames.Error, model );
        }

        model.put( AttributeNames.OrderView.OrderId,   orderDto.getDomainId().toString() );
        model.put( AttributeNames.OrderView.Status,    orderDto.getStatus() );
        model.put( AttributeNames.OrderView.Placed,    orderDto.getPlacementTime().format( getDateTimeFormatter() ) );
        model.put( AttributeNames.OrderView.Name,      orderDto.getCustomerName() );
        model.put( AttributeNames.OrderView.Address,   orderDto.getCustomerAddress() );
        model.put( AttributeNames.OrderView.Email,     orderDto.getContactEmail() );
        model.put( AttributeNames.OrderView.Phone,     orderDto.getContactPhone() );
        model.put( AttributeNames.OrderView.Discount,  orderDto.getDiscountPercentage().setScale( 2 ).toString() );
        model.put( AttributeNames.OrderView.Cost,      orderDto.getTotalCost().setScale( 2 ).toString() );
        model.put( AttributeNames.OrderView.Comment,   orderDto.getComment() );

        return new ModelAndView( ViewNames.Order, model );
    }

    static DateTimeFormatter getDateTimeFormatter ()
    {
        return DateTimeFormatter.ofLocalizedDateTime( FormatStyle.MEDIUM, FormatStyle.MEDIUM );
    }

    @Inject
    private IOrderService orderService;
}
