/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pizzario.dto.AccountDto;
import pizzario.dto.OrderDto;
import pizzario.exceptions.DomainLogicException;
import pizzario.service.IOrderService;
import pizzario.web.constants.AttributeNames;
import pizzario.web.constants.ViewNames;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;


@Controller
@RequestMapping( value = "/operator" )
public class OperatorController
{
    @RequestMapping( value = "/" )
    public String index ( Map< String, Object > model )
    {
        model.put( AttributeNames.OperatorView.UnconfirmedOrders, getUnconfirmedOrders() );
        model.put( AttributeNames.OperatorView.ConfirmedOrders, getConfirmedOrders() );
        model.put( AttributeNames.OperatorView.Ready4DeliveryOrders, getReady4DeliveryOrders() );
        return ViewNames.Operator;
    }

    @RequestMapping( value = "/confirmedrow", method = RequestMethod.GET )
    public String confirmedRow (
            @RequestParam UUID orderId,
            Map< String, Object > model
    )
    {
        model.put( AttributeNames.OperatorView.ConfirmedOrder, orderService.view( orderId ) );
        return ViewNames.Operator_ConfirmedRow;
    }

    @RequestMapping( value = "/setdiscount", method = RequestMethod.POST )
    public ResponseEntity setOrderDiscount (
            @RequestParam UUID orderId,
            @RequestParam BigDecimal discount
    )
            throws DomainLogicException
    {
        orderService.setDiscount( orderId, discount );
        return new ResponseEntity( HttpStatus.OK );
    }

    @RequestMapping( value = "/confirm", method = RequestMethod.POST )
    public ResponseEntity confirmOrder (
            @RequestParam UUID orderId,
            @AuthenticationPrincipal AccountDto operatorUser
    )
            throws DomainLogicException
    {
        orderService.confirm( orderId, operatorUser.getDomainId() );
        return new ResponseEntity( HttpStatus.OK );
    }

    @RequestMapping( value = "/cancel", method = RequestMethod.POST )
    public ResponseEntity cancelOrder (
            @RequestParam UUID orderId
    )
            throws DomainLogicException
    {
        orderService.cancel( orderId );
        return new ResponseEntity( HttpStatus.OK );
    }

    private List< OrderDto > getUnconfirmedOrders ()
    {
        return readOrders( orderService.viewUnconfirmed() );
    }

    private List< OrderDto > getConfirmedOrders ()
    {
        return readOrders( orderService.viewConfirmed() );
    }

    private List< OrderDto > getReady4DeliveryOrders ()
    {
        return readOrders( orderService.viewReady4Delivery() );
    }

    private List< OrderDto > readOrders ( List< UUID > orderIds )
    {
        return orderIds.stream()
                .map(
                        orderId -> orderService.view( orderId )
                )
                .collect( Collectors.toList() );
    }

    @Inject
    private IOrderService orderService;
}
