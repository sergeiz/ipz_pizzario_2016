package pizzario.web.constants;

public interface ErrorKeys
{
    String OrderNotFoundTitle = "error.ordernotfound.title";
    String OrderNotFoundMessage = "error.ordernotfound.message";

    String OrderAccessViolationTitle = "error.orderaccessviolation.title";
    String OrderAccessViolationMessage = "error.orderaccessviolation.message";

    String NoOrderTitle = "error.noorder.title";
    String NoOrderMessage = "error.noorder.message";

    String ServerFatalErrorTitle = "error.fatal.title";
    String ServerFatalErrorMessage = "error.fatal.message";

    String DomainLogicErrorTitle = "error.domain.title";
    String DomainLogicErrorMessage = "error.domain.message";

    String ValidationErrorTitle = "error.validation.title";
    String ValidationErrorMessage = "error.validation.message";

    String NoHandlerFoundTitle = "error.nohandler.title";
    String NoHandlerFoundMessage = "error.nohandler.message";
}
