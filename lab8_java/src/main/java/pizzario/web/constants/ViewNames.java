package pizzario.web.constants;

public interface ViewNames
{
    String Cart     = "cart";
    String Checkout = "checkout";
    String Menu     = "menu";
    String Order    = "order";
    String Error    = "error";
    String Login    = "login";

    String Operator = "operator";
    String Operator_ConfirmedRow = "operator_confirmedrow";
}