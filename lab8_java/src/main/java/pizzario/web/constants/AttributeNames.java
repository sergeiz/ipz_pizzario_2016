package pizzario.web.constants;

public interface AttributeNames
{
    interface CartView
    {
        String Cart = "cart";
    }

    interface CheckoutView
    {
        String Cart = "cart";
    }

    interface MenuView
    {
        String Products = "products";
        String Sizes = "sizes";
        String ProductPrices = "product_prices";
    }

    interface OrderView
    {
        String OrderId  = "orderId";
        String Status   = "status";
        String Placed   = "placed";
        String Name     = "name";
        String Address  = "address";
        String Email    = "email";
        String Phone    = "phone";
        String Discount = "discount";
        String Cost     = "cost";
        String Comment  = "comment";
    }

    interface ErrorView
    {
        String Title = "errorTitle";
        String Message = "errorMessage";
        String Arguments = "errorArguments";
    }

    interface OperatorView
    {
        String UnconfirmedOrders = "unconfirmedOrders";
        String ConfirmedOrders = "confirmedOrders";
        String Ready4DeliveryOrders = "ready4DeliveryOrders";

        String ConfirmedOrder = "confirmedOrder";
    }
}
