/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.web;

import pizzario.dto.OrderDto;
import pizzario.dto.ShoppingCartDto;
import pizzario.service.IOrderService;
import pizzario.service.IShoppingCartService;
import pizzario.web.constants.SessionKeys;

import javax.servlet.http.HttpSession;
import java.util.UUID;

class SessionDataProvider
{
    SessionDataProvider ( HttpSession session )
    {
        this.session = session;
    }

    ShoppingCartDto currentCart ( IShoppingCartService service )
    {
        UUID cartId = currentCartId( service );
        return service.view( cartId );
    }

    UUID currentCartId ( IShoppingCartService service )
    {
        UUID cartId = ( UUID ) session.getAttribute( SessionKeys.CART_ID );
        if ( cartId == null )
        {
            cartId = service.createNew();
            session.setAttribute( SessionKeys.CART_ID, cartId );
        }

        return cartId;
    }

    void resetCurrentCartId ()
    {
        session.removeAttribute( SessionKeys.CART_ID );
    }

    OrderDto currentOrder ( IOrderService service )
    {
        UUID orderId = currentOrderId( service );
        if ( orderId != null )
            return service.view( orderId );

        else
            return null;
    }

    UUID currentOrderId ( IOrderService service )
    {
        UUID orderId = ( UUID ) session.getAttribute( SessionKeys.ORDER_ID );
        return orderId;
    }

    void setCurrentOrderId ( UUID orderId )
    {
        session.setAttribute( SessionKeys.ORDER_ID, orderId );
    }


    private HttpSession session;
}
