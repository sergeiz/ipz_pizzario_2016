/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.web;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;
import pizzario.dto.AccountDto;
import pizzario.web.constants.ViewNames;

import java.util.Map;

@Controller
public class AuthenticationController
{
    @RequestMapping( value = "login", method = RequestMethod.GET )
    public ModelAndView login ( Map< String, Object > model )
    {
        if ( SecurityContextHolder.getContext().getAuthentication() instanceof AccountDto  )
            return new ModelAndView( new RedirectView( "/operator/" ) );

        return new ModelAndView( ViewNames.Login );
    }

}
