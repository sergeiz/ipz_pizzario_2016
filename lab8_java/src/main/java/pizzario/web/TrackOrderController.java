/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import pizzario.dto.OrderDto;
import pizzario.service.IOrderService;
import pizzario.web.constants.AttributeNames;
import pizzario.web.constants.ErrorKeys;
import pizzario.web.constants.ViewNames;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping( "/trackorder" )
public class TrackOrderController
{
    @RequestMapping( path="/", method = RequestMethod.POST )
    public ModelAndView submitTrackOrder ( HttpSession session,
                                           Map< String, Object > model,
                                           @RequestParam UUID inputTrackOrderId,
                                           @RequestParam String inputTrackOrderEmail )
    {
        OrderDto orderDto = orderService.findOrder( inputTrackOrderId );
        if ( orderDto == null )
        {
            model.put( AttributeNames.ErrorView.Title,      ErrorKeys.OrderNotFoundTitle );
            model.put( AttributeNames.ErrorView.Message,    ErrorKeys.OrderNotFoundMessage );
            model.put( AttributeNames.ErrorView.Arguments,  inputTrackOrderId.toString()  );

            return new ModelAndView( ViewNames.Error, model );
        }

        else if ( ! orderDto.getContactEmail().equalsIgnoreCase( inputTrackOrderEmail ) )
        {
            model.put( AttributeNames.ErrorView.Title,      ErrorKeys.OrderAccessViolationTitle   );
            model.put( AttributeNames.ErrorView.Message,    ErrorKeys.OrderAccessViolationMessage );
            model.put( AttributeNames.ErrorView.Arguments,  inputTrackOrderEmail  );

            return new ModelAndView( ViewNames.Error, model );
        }

        SessionDataProvider sessionProvider = new SessionDataProvider( session );
        sessionProvider.setCurrentOrderId( inputTrackOrderId );
        return new ModelAndView( "redirect:/order/" );
    }


    @Inject
    private IOrderService orderService;
}
