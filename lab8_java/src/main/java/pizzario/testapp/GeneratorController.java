/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.testapp;

import org.springframework.stereotype.Component;
import pizzario.exceptions.DomainLogicException;
import pizzario.service.*;

import javax.inject.Inject;

@Component
public class GeneratorController
{

    public void run ()
            throws DomainLogicException
    {
        TestModelGenerator generator = new TestModelGenerator();
        generator.generateIngredients( ingredientService );
        generator.generateSizes( productSizeService );
        generator.generateProducts( productService );
        generator.generateCarts( shoppingCartService );
        generator.generateOrders( orderService );
        generator.generateAccounts( accountService );
        generator.confirmOrders( orderService );
        generator.startCooking( cookingAssignmentService );
        generator.finishCooking( cookingAssignmentService );
        generator.deliver( deliveryService );
    }

    @Inject
    private IIngredientService ingredientService;

    @Inject
    private IProductSizeService productSizeService;

    @Inject
    private IProductService productService;

    @Inject
    private IShoppingCartService shoppingCartService;

    @Inject
    private IOrderService orderService;

    @Inject
    private IAccountService accountService;

    @Inject
    private ICookingAssignmentService cookingAssignmentService;

    @Inject
    private IDeliveryService deliveryService;
}
