/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.exceptions;

import java.util.UUID;

public class ModifiableCartException extends DomainLogicException
{
    public ModifiableCartException ( UUID cartId )
    {
        super( String.format( "Shopping cart #%s must be locked by this moment", cartId ) );
    }
}
