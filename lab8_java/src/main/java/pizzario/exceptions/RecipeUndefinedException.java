/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.exceptions;

public class RecipeUndefinedException extends DomainLogicException
{
    public RecipeUndefinedException ( String productName, String sizeName )
    {
        super( String.format(
                    "Recipe for size \"%s\" is undefined for product \"%S\"",
                    sizeName,
                    productName
        ) );
    }
}
