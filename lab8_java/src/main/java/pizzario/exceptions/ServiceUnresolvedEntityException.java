/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.exceptions;

import java.util.UUID;

public class ServiceUnresolvedEntityException extends ServiceValidationException
{
    public ServiceUnresolvedEntityException ( Class entityType, UUID entityId )
    {
        super( String.format( "Unresolved entity #%s of type %s", entityId, entityType.getName() ) );
    }
}
