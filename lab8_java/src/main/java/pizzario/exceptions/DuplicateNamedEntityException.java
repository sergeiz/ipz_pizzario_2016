/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.exceptions;

public class DuplicateNamedEntityException extends DomainLogicException
{
    public DuplicateNamedEntityException ( Class c, String name )
    {
        super( String.format( "Duplicate %s object called \"%s\"", c.getName(), name ) );
    }
}
