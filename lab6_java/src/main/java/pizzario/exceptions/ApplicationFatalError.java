/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.exceptions;

public class ApplicationFatalError extends RuntimeException
{
    public ApplicationFatalError ()
    {
        super( "Application fatal error. Please contact your administrator." );
    }
}
