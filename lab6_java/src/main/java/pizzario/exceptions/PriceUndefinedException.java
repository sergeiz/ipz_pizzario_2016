/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.exceptions;

public class PriceUndefinedException extends DomainLogicException
{
    public PriceUndefinedException ( String productName, String sizeName )
    {
        super( String.format(
                "Price for size \"%s\" is undefined for product \"%s\"",
                sizeName,
                productName ) );
    }
}
