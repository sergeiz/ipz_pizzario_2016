/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.exceptions;

import java.util.UUID;

public class CookingLifecycleException extends DomainLogicException
{
    private CookingLifecycleException ( String message )
    {
        super( message );
    }

    public static CookingLifecycleException makeStartWhenNotWaiting ( UUID cookingAssignmentId )
    {
        return new CookingLifecycleException(
                String.format(
                        "Cooking #%s may be started in Waiting state only" ,
                        cookingAssignmentId
                )
        );
    }

    public static CookingLifecycleException makeFinishWhenNotInProgress ( UUID cookingAssignmentId )
    {
        return new CookingLifecycleException(
                String.format(
                        "Сooking #%s may be finished in InProgress state only",
                        cookingAssignmentId
                )
        );
    }
}
