/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.exceptions;

import java.util.UUID;

public class UnmodifiableCartException extends DomainLogicException
{
    public UnmodifiableCartException ( UUID cartId )
    {
        super( String.format( "Attempted to modify a locked shopping cart #%s", cartId ) );
    }
}
