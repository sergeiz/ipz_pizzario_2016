/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.exceptions;

public class ServiceValidationException extends RuntimeException
{
    public ServiceValidationException ( String message )
    {
        super( message );
    }
}
