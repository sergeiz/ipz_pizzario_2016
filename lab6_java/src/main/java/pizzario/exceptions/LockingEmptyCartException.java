/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.exceptions;

import java.util.UUID;

public class LockingEmptyCartException extends DomainLogicException
{
    public LockingEmptyCartException ( UUID cartId )
    {
        super( String.format( "Attempted to lock empty cart #%s", cartId ) );
    }
}
