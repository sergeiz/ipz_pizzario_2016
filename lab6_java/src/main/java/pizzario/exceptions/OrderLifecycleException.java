/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.exceptions;

import java.util.UUID;

public class OrderLifecycleException extends DomainLogicException
{
    public OrderLifecycleException ( UUID orderId, String currentStatus, String expectedStatus )
    {
        super( String.format(
                "Order #%s must have %s status, while %s is a current status",
                orderId,
                expectedStatus,
                currentStatus ) );
    }
}
