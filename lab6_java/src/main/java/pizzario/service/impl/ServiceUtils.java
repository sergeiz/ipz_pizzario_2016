/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import pizzario.exceptions.ServiceUnresolvedEntityException;
import pizzario.repository.IRepository;
import pizzario.utils.DomainEntity;

import java.util.UUID;

final class ServiceUtils
{

    private ServiceUtils () {}

    static < TEntity extends DomainEntity > TEntity
        resolveEntity ( IRepository< TEntity > repository, UUID domainId )
    {

        TEntity entity = repository.findByDomainId( domainId );
        if ( entity != null )
            return entity;

        throw new ServiceUnresolvedEntityException( repository.getEntityClass(), domainId );
    }

}
