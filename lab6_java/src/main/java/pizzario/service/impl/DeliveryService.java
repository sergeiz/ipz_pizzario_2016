/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pizzario.dto.DeliveryDto;
import pizzario.exceptions.DeliveryLifecycleException;
import pizzario.exceptions.OrderLifecycleException;
import pizzario.model.Delivery;
import pizzario.model.Order;
import pizzario.repository.IDeliveryRepository;
import pizzario.repository.IOrderRepository;
import pizzario.service.IDeliveryService;

import javax.inject.Inject;
import java.util.List;
import java.util.UUID;


@Service
class DeliveryService implements IDeliveryService 
{

    protected DeliveryService () {}


    @Transactional
    @Override
    public List< UUID > viewAll ()
    { 
       return deliveryRepository.selectAllDomainIds();
    }


    @Transactional
    @Override
    public List< UUID > viewWaiting ()
    {
        return deliveryRepository.selectWaitingIds();
    }


    @Transactional
    @Override
    public List< UUID > viewInProgress ()
    {
        return deliveryRepository.selectInProgressIds();
    }


    @Transactional
    @Override
    public DeliveryDto view ( UUID deliveryId ) 
    {
        Delivery delivery = resolveDelivery( deliveryId );
        return DtoBuilder.toDto( delivery );
    }


    @Transactional
    @Override
    public DeliveryDto findOrderDelivery ( UUID orderId )
    {
        Order o = resolveOrder( orderId );
        Delivery delivery = deliveryRepository.findByOrder( o );
        return DtoBuilder.toDto( delivery );
    }


    @Transactional
    @Override
    public void markStarted ( UUID deliveryId, String driverName ) throws DeliveryLifecycleException
    {
        Delivery delivery = resolveDelivery( deliveryId );
        delivery.startDelivery( driverName );
    }


    @Transactional
    @Override
    public void markDelivered ( UUID deliveryId )
            throws DeliveryLifecycleException, OrderLifecycleException
    {
        Delivery delivery = resolveDelivery( deliveryId );
        delivery.finishDelivery();
    }


    private Delivery resolveDelivery ( UUID deliveryId )
    {
        return ServiceUtils.resolveEntity( deliveryRepository, deliveryId );
    }

    private Order resolveOrder ( UUID orderId )
    {
        return ServiceUtils.resolveEntity( orderRepository, orderId );
    }


    @Inject
    private IDeliveryRepository deliveryRepository;

    @Inject
    private IOrderRepository orderRepository;
}
