/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pizzario.dto.AccountDto;
import pizzario.exceptions.DuplicateNamedEntityException;
import pizzario.model.Order;
import pizzario.model.accounts.Account;
import pizzario.model.accounts.OperatorAccount;
import pizzario.model.accounts.OrdersExtractAccountVisitor;
import pizzario.repository.IAccountRepository;
import pizzario.repository.IOrderRepository;
import pizzario.service.IAccountService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Service
class AccountService implements IAccountService {

    protected AccountService () {}


    @Transactional
    @Override
    public List< UUID > viewAll ()
    {
        return accountRepository.selectAllDomainIds();
    }


    @Transactional
    @Override
    public AccountDto view ( UUID accountId )
    {
        Account a = resolveAccount( accountId );
        return DtoBuilder.toDto( a );
    }


    @Transactional
    @Override
    public AccountDto identify ( String email, String password )
    {
        Account a = accountRepository.findByEmail( email );
        if ( a == null )
            return null;

        if ( ! a.checkPassword( password ) )
            return null;

        return new AccountDto( a.getDomainId(), a.getName(), a.getEmail() );
    }


    @Transactional
    @Override
    public List< UUID > viewAssociatedOrders ( UUID accountId )
    {
        Account a = resolveAccount( accountId );

        List< UUID > orderIds = new ArrayList<>();
        OrdersExtractAccountVisitor visitor = new OrdersExtractAccountVisitor( orderIds );
        a.accept( visitor );

        return orderIds;
    }


    @Transactional
    @Override
    public UUID createOperator ( String name, String email, String password ) throws DuplicateNamedEntityException
    {
        Account a = accountRepository.findByEmail( email );
        if ( a != null )
            throw new DuplicateNamedEntityException( Account.class, email );

        OperatorAccount operator = new OperatorAccount( UUID.randomUUID(), name, email, password );
        accountRepository.add( operator );
        return operator.getDomainId();
    }


    @Transactional
    @Override
    public void changeName ( UUID accountId, String newName )
    {
        Account a = resolveAccount( accountId );
        a.setName( newName );
    }


    @Transactional
    @Override
    public void changeEmail ( UUID accountId, String newEmail ) throws DuplicateNamedEntityException
    {
        Account a = accountRepository.findByEmail( newEmail );
        if ( a != null )
            throw new DuplicateNamedEntityException( Account.class, newEmail );

        a.setEmail( newEmail );
    }


    @Transactional
    @Override
    public void changePassword ( UUID accountId, String oldPassword, String newPassword )
    {
        Account a = resolveAccount( accountId );
        a.setPassword( newPassword );
    }


    private Account resolveAccount ( UUID accountId )
    {
        return ServiceUtils.resolveEntity( accountRepository, accountId );
    }


    private Order resolveOrder ( UUID orderId )
    {
        return ServiceUtils.resolveEntity( orderRepository, orderId );
    }


    @Inject
    private IAccountRepository accountRepository;

    @Inject
    private IOrderRepository orderRepository;
}
