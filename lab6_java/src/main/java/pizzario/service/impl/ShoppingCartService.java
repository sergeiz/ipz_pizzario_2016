/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pizzario.dto.ShoppingCartDto;
import pizzario.exceptions.LockingEmptyCartException;
import pizzario.exceptions.PriceUndefinedException;
import pizzario.exceptions.RemovingUnexistingCartItemException;
import pizzario.exceptions.UnmodifiableCartException;
import pizzario.model.Product;
import pizzario.model.ProductItem;
import pizzario.model.ProductSize;
import pizzario.model.ShoppingCart;
import pizzario.repository.IProductRepository;
import pizzario.repository.IProductSizeRepository;
import pizzario.repository.IShoppingCartRepository;
import pizzario.service.IShoppingCartService;

import javax.inject.Inject;
import java.util.List;
import java.util.UUID;


@Service
public class ShoppingCartService implements IShoppingCartService
{

    protected ShoppingCartService () {}


    @Transactional
    @Override
    public List< UUID > viewAll () 
    { 
       return shoppingCartRepository.selectAllDomainIds();
    }


    @Transactional
    @Override
    public List< UUID > viewOpen()
    {
        return shoppingCartRepository.selectAllOpenCartIds();
    }


    @Transactional
    @Override
    public ShoppingCartDto view ( UUID cartId )
    {
        ShoppingCart cart = resolveCart( cartId );
        return DtoBuilder.toDto( cart );
    }


    @Transactional
    @Override
    public UUID createNew ()
    {
        ShoppingCart cart = new ShoppingCart( UUID.randomUUID() );
        shoppingCartRepository.add( cart );
        return cart.getDomainId();
    }


    @Transactional
    @Override
    public void setItem ( UUID cartId, UUID productId, UUID sizeId, int quantity )
            throws UnmodifiableCartException, PriceUndefinedException
    {
        ShoppingCart cart = resolveCart( cartId );
        Product product   = resolveProduct( productId );
        ProductSize size  = resolveSize( sizeId );

        cart.placeItem(
            new ProductItem( UUID.randomUUID(), product, size, quantity )
        );
    }


    @Transactional
    @Override
    public void removeItem ( UUID cartId, UUID productId, UUID sizeId )
            throws UnmodifiableCartException, RemovingUnexistingCartItemException
    {
        ShoppingCart cart = resolveCart( cartId );
        Product product   = resolveProduct( productId );
        ProductSize size  = resolveSize( sizeId );

        int itemIndex = cart.findItemIndex( product, size );
        if ( itemIndex != -1 )
            cart.dropItem( itemIndex );

        else
            throw new RemovingUnexistingCartItemException( cartId, product.getName(), size.getName() );
    }


    @Transactional
    @Override
    public void clearItems ( UUID cartId ) throws UnmodifiableCartException
    {
        ShoppingCart cart = resolveCart( cartId );
        cart.clearItems();
    }


    @Transactional
    @Override
    public void lock ( UUID cartId ) throws LockingEmptyCartException
    {
        ShoppingCart cart = resolveCart( cartId );
        cart.checkout();
    }


    private ShoppingCart resolveCart ( UUID cartId )
    {
        return ServiceUtils.resolveEntity( shoppingCartRepository, cartId );
    }

    private Product resolveProduct ( UUID productId )
    {
        return ServiceUtils.resolveEntity( productRepository, productId );
    }

    private ProductSize resolveSize ( UUID sizeId )
    {
        return ServiceUtils.resolveEntity( productSizeRepository, sizeId );
    }


    @Inject
    private IShoppingCartRepository shoppingCartRepository;

    @Inject
    private IProductRepository productRepository;

    @Inject
    private IProductSizeRepository productSizeRepository;
}
