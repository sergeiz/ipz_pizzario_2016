/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pizzario.dto.OrderDto;
import pizzario.exceptions.ModifiableCartException;
import pizzario.exceptions.OrderLifecycleException;
import pizzario.exceptions.ServiceUnresolvedEntityException;
import pizzario.model.*;
import pizzario.model.accounts.OperatorAccount;
import pizzario.repository.IAccountRepository;
import pizzario.repository.ICookingAssignmentRepository;
import pizzario.repository.IOrderRepository;
import pizzario.repository.IShoppingCartRepository;
import pizzario.service.IOrderService;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;


@Service
class OrderService implements IOrderService 
{

    protected OrderService () {}

    @Transactional
    @Override
    public List< UUID > viewAll ()
    { 
       return orderRepository.selectAllDomainIds();
    }


    @Transactional
    @Override
    public List< UUID > viewUnconfirmed ()
    {
        return orderRepository.selectUnconfirmedIds();
    }


    @Transactional
    @Override
    public List< UUID > viewReady4Delivery ()
    {
        return orderRepository.selectReady4DeliveryIds();
    }


    @Transactional
    @Override
    public OrderDto view ( UUID orderId )
    {
        Order o = resolveOrder( orderId );
        return DtoBuilder.toDto( o );
    }


    @Transactional
    @Override
    public UUID createNew ( String deliveryAddress, String contactPhone, UUID cartId ) throws ModifiableCartException
    {
        ShoppingCart cart = resolveCart( cartId );
        Order o = new Order(
                UUID.randomUUID(),
                cart,
                new Contact( deliveryAddress, contactPhone ),
                LocalDateTime.now()
        );

        orderRepository.add( o );
        return o.getDomainId();
    }


    @Transactional
    @Override
    public void setDiscount ( UUID orderId, BigDecimal discountPercent )
    {
        Order o = resolveOrder( orderId );
        o.setDiscount( new Discount( discountPercent ) );
    }


    @Transactional
    @Override
    public void confirm ( UUID orderId, UUID operatorId ) throws OrderLifecycleException
    {
        Order o = resolveOrder( orderId );
        OperatorAccount operatorAccount = resolveOperator( operatorId );
        o.confirm();

        operatorAccount.trackOrder( o );

        for ( CookingAssignment ca : o.generateCookingAssignments() )
            cookingAssignmentRepository.add( ca );
    }


    @Transactional
    @Override
    public void cancel ( UUID orderId ) throws OrderLifecycleException
    {
        Order o = resolveOrder( orderId );
        o.cancel();
    }


    private Order resolveOrder ( UUID orderId )
    {
        return ServiceUtils.resolveEntity( orderRepository, orderId );
    }

    private ShoppingCart resolveCart ( UUID cartId )
    {
        return ServiceUtils.resolveEntity( shoppingCartRepository, cartId );
    }

    private OperatorAccount resolveOperator ( UUID operatorId )
    {
        OperatorAccount account = accountRepository.findOperatorByDomainId( operatorId );
        if ( account == null )
            throw new ServiceUnresolvedEntityException( OperatorAccount.class, operatorId );

        return account;
    }


    @Inject
    private IOrderRepository orderRepository;

    @Inject
    private IShoppingCartRepository shoppingCartRepository;

    @Inject
    private ICookingAssignmentRepository cookingAssignmentRepository;

    @Inject
    private IAccountRepository accountRepository;

}
