/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pizzario.dto.ProductSizeDto;
import pizzario.exceptions.DuplicateNamedEntityException;
import pizzario.model.ProductSize;
import pizzario.repository.IProductSizeRepository;
import pizzario.service.IProductSizeService;

import javax.inject.Inject;
import java.util.List;
import java.util.UUID;


@Service
class ProductSizeService implements IProductSizeService
{

    protected ProductSizeService () {}


    @Transactional
    @Override
    public List< UUID > viewAll()
    {
        return productSizeRepository.selectAllDomainIds();
    }


    @Transactional
    @Override
    public ProductSizeDto view ( UUID sizeId )
    {
        ProductSize size = resolveSize( sizeId );
        return DtoBuilder.toDto( size );
    }


    @Transactional
    @Override
    public UUID create ( String name, int diameter, double weight ) throws DuplicateNamedEntityException
    {
        if ( productSizeRepository.findByName( name ) != null )
            throw new DuplicateNamedEntityException( ProductSize.class, name );

        ProductSize size = new ProductSize( UUID.randomUUID(), name, diameter, weight );
        productSizeRepository.add( size );
        return size.getDomainId();
    }


    @Transactional
    @Override
    public void rename ( UUID sizeId, String newName ) throws DuplicateNamedEntityException
    {
        if ( productSizeRepository.findByName( newName ) != null )
            throw new DuplicateNamedEntityException( ProductSize.class, newName );

        ProductSize size = resolveSize( sizeId );
        size.setName( newName );
    }


    @Transactional
    @Override
    public void changeDiameter ( UUID sizeId, int newDiameter )
    {
        ProductSize size = resolveSize( sizeId );
        size.setDiameter( newDiameter );
    }


    @Transactional
    @Override
    public void changeWeight ( UUID sizeId, double weight )
    {
        ProductSize size = resolveSize( sizeId );
        size.setWeight( weight );
    }


    private ProductSize resolveSize ( UUID sizeId )
    {
        return ServiceUtils.resolveEntity( productSizeRepository, sizeId );
    }

    @Inject
    private IProductSizeRepository productSizeRepository;
}
