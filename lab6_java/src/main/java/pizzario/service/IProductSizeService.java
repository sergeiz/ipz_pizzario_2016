/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service;

import org.hibernate.validator.constraints.NotBlank;
import pizzario.dto.ProductSizeDto;
import pizzario.exceptions.DuplicateNamedEntityException;
import pizzario.service.validation.Diameter;
import pizzario.service.validation.Weight;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public interface IProductSizeService extends IDomainEntityService< ProductSizeDto >
{
    UUID create (
            @NotBlank  String name,
            @Diameter int diameter,
            @Weight double weight
    ) throws DuplicateNamedEntityException;

    void rename (
            @NotNull UUID sizeId,
            @NotBlank String newName
    ) throws DuplicateNamedEntityException;

    void changeDiameter (
            @NotNull UUID sizeId,
            @Diameter int newDiameter
    );

    void changeWeight (
            @NotNull UUID sizeId,
            @Weight double weight
    );
}
