/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.Min;
import java.lang.annotation.*;

@Documented
@Constraint( validatedBy = {} )
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Retention( RetentionPolicy.RUNTIME)
@Min( 1 )
@ReportAsSingleViolation
public @interface Diameter
{
    String message() default "Diameter value must be positive";

    Class<?>[] groups() default {};

    Class<? extends Payload >[] payload() default {};
}
