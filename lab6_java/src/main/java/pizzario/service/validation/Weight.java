/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.DecimalMin;
import java.lang.annotation.*;

@Documented
@Constraint( validatedBy = {} )
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Retention( RetentionPolicy.RUNTIME)
@DecimalMin( value = "0.00", inclusive = false )
@ReportAsSingleViolation
public @interface Weight
{
    String message() default "Weight value must be above zero";

    Class<?>[] groups() default {};

    Class<? extends Payload >[] payload() default {};
}
