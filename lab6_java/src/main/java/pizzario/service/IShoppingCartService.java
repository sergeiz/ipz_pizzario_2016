/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service;

import pizzario.dto.ShoppingCartDto;
import pizzario.exceptions.LockingEmptyCartException;
import pizzario.exceptions.PriceUndefinedException;
import pizzario.exceptions.RemovingUnexistingCartItemException;
import pizzario.exceptions.UnmodifiableCartException;
import pizzario.service.validation.Quantity;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

public interface IShoppingCartService extends IDomainEntityService< ShoppingCartDto >
{
    List< UUID > viewOpen ();

    UUID createNew ();

    void setItem (
            @NotNull UUID cartId,
            @NotNull UUID productId,
            @NotNull UUID sizeId,
            @Quantity int quantity
    ) throws UnmodifiableCartException, PriceUndefinedException;

    void removeItem (
            @NotNull UUID cartId,
            @NotNull UUID productId,
            @NotNull UUID sizeId
    ) throws UnmodifiableCartException, RemovingUnexistingCartItemException;

    void clearItems ( @NotNull UUID cartId ) throws UnmodifiableCartException;

    void lock ( @NotNull UUID cartId ) throws LockingEmptyCartException;
}
