/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service;

import pizzario.dto.CookingAssignmentDto;
import pizzario.exceptions.CookingLifecycleException;
import pizzario.exceptions.OrderLifecycleException;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

public interface ICookingAssignmentService extends IDomainEntityService< CookingAssignmentDto >
{
    List< UUID > viewCookedRightNow ();

    List< UUID > viewWaiting ();

    List< CookingAssignmentDto > viewOrderAssignments ( @NotNull UUID orderId );

    void markCookingStarted ( @NotNull UUID cookingAssignmentId )
            throws CookingLifecycleException;

    void markCookingFinished ( @NotNull UUID cookingAssignmentId )
            throws CookingLifecycleException, OrderLifecycleException;
}
