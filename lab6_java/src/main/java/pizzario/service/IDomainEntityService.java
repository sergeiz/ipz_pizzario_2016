/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service;

import org.springframework.validation.annotation.Validated;
import pizzario.dto.DomainEntityDto;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Validated
public interface IDomainEntityService< TDto extends DomainEntityDto >
{
    List< UUID > viewAll ();

    TDto view ( @NotNull UUID domainId );
}
