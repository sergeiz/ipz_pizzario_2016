/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.dto;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class ProductItemDto extends DomainEntityDto< ProductItemDto >
{

    public ProductItemDto (
            UUID domainId,
            UUID productId,
            String productName,
            UUID sizeId,
            String sizeName,
            int quantity,
            BigDecimal fixedPrice
    )
    {
        super( domainId );

        this.productId = productId;
        this.productName = productName;
        this.sizeId = sizeId;
        this.sizeName = sizeName;
        this.quantity = quantity;
        this.fixedPrice = fixedPrice;
    }

    public UUID getProductId ()
    {
        return productId;
    }

    public String getProductName ()
    {
        return productName;
    }

    public UUID getSizeId ()
    {
        return sizeId;
    }

    public String getSizeName ()
    {
        return sizeName;
    }

    public int getQuantity ()
    {
        return quantity;
    }

    public BigDecimal getFixedPrice ()
    {
        return fixedPrice;
    }

    @Override
    public String toString ()
    {
        return String.format(
                "\tProduct = %s\n\tSize = %s\n\tQuantity = %d\n\tFixedPrice = %s",
                getProductName(),
                getSizeName(),
                getQuantity(),
                NumberFormat.getCurrencyInstance().format( getFixedPrice() )
        );
    }

    @Override
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList(
                getDomainId(),
                getProductId(),
                getProductName(),
                getSizeId(),
                getSizeName(),
                getQuantity(),
                getFixedPrice()
        );
    }

    private final UUID productId;
    private final String productName;
    private final UUID sizeId;
    private final String sizeName;
    private final int quantity;
    private final BigDecimal fixedPrice;
}
