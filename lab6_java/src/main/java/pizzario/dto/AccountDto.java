/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class AccountDto extends DomainEntityDto< AccountDto >
{

    public AccountDto ( UUID domainId, String name, String email )
    {
        this( domainId, name, email, new ArrayList< UUID >() );
    }

    public AccountDto ( UUID domainId, String name, String email, List< UUID > managedOrders )
    {
        super( domainId );

        this.name = name;
        this.email = email;
        this.managedOrders = managedOrders;
    }

    public String getName ()
    {
        return name;
    }

    public String getEmail ()
    {
        return email;
    }

    public List< UUID > getManagedOrders () { return managedOrders; }

    @Override
    public String toString ()
    {
        StringBuilder ordersKeysAsString = new StringBuilder();
        for ( UUID orderId : managedOrders )
        {
            ordersKeysAsString.append( orderId );
            ordersKeysAsString.append( '\n' );
        }

        return String.format(
                "ID = %s\nName = %s\nEmail = %s\nOrders:\n%s",
                getDomainId(),
                getName(),
                getEmail(),
                ordersKeysAsString
        );
    }

    @Override
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        List< Object > list = Arrays.asList( getDomainId(), getName(), getEmail() );
        list.addAll( managedOrders );
        return list;
    }

    private final String name;
    private final String email;
    private final List< UUID > managedOrders;
}
