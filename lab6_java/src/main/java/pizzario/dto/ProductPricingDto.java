/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.dto;

import pizzario.utils.Value;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ProductPricingDto extends Value< ProductPricingDto >
{

    public ProductPricingDto (
            UUID productId,
            Map< ProductSizeDto, BigDecimal > prices
    )
    {
        this.productId = productId;
        this.prices = prices;
    }

    public UUID getProductId ()
    {
        return productId;
    }

    public Map< ProductSizeDto, BigDecimal > getPrices ()
    {
        return prices;
    }


    @Override
    public String toString ()
    {
        StringBuilder pricesAsString = new StringBuilder();
        pricesAsString.append( "Prices: " );

        for ( Map.Entry< ProductSizeDto, BigDecimal > ppa : prices.entrySet() )
            pricesAsString.append(
                    String.format(
                            "%s = %s  ",
                            ppa.getKey().getName(),
                            NumberFormat.getCurrencyInstance().format( ppa.getValue() )
                    )
            );

        return pricesAsString.toString();
    }

    @Override
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        List< Object > list = Arrays.asList( getProductId() );
        prices.entrySet().stream().forEach(
                entry ->
                {
                    list.add( entry.getKey() );
                    list.add( entry.getValue() );
                }
        );
        return list;
    }


    private final UUID productId;
    private final Map< ProductSizeDto, BigDecimal > prices;

}
