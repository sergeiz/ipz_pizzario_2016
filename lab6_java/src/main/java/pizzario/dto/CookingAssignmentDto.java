/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.dto;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class CookingAssignmentDto extends DomainEntityDto< CookingAssignmentDto >
{

    public CookingAssignmentDto ( UUID domainId,
                                  UUID orderId,
                                  UUID productId,
                                  String productName,
                                  UUID sizeId,
                                  String sizeName,
                                  String status )
    {
        super( domainId );

        this.orderId = orderId;
        this.productId = productId;
        this.productName = productName;
        this.sizeId = sizeId;
        this.sizeName = sizeName;
        this.status = status;
    }

    public UUID getOrderId ()
    {
        return orderId;
    }

    public UUID getProductId ()
    {
        return productId;
    }

    public String getProductName ()
    {
        return productName;
    }

    public UUID getSizeId ()
    {
        return sizeId;
    }

    public String getSizeName ()
    {
        return sizeName;
    }

    public String getStatus ()
    {
        return status;
    }

    @Override
    public String toString ()
    {
        return String.format(
                "ID = %s\nOrder # = %s\nProduct = %s\nSize = %s\nStatus = %s",
                getDomainId(),
                getOrderId(),
                getProductName(),
                getSizeName(),
                getStatus()
        );
    }

    @Override
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList(
                getDomainId(),
                getProductId(), getProductName(),
                getSizeId(), getSizeName(),
                getStatus()
        );
    }

    private final UUID orderId;
    private final UUID productId;
    private final String productName;
    private final UUID sizeId;
    private final String sizeName;
    private final String status;

}
