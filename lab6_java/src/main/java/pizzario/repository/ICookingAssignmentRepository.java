/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository;

import pizzario.model.CookingAssignment;
import pizzario.model.Order;

import java.util.List;
import java.util.UUID;

public interface ICookingAssignmentRepository extends IRepository< CookingAssignment >
{

    List< UUID > selectCookingIds ();

    List< UUID > selectWaitingIds ();

    List< CookingAssignment > findOrderAssignments ( Order o );
}
