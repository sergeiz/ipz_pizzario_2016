/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository;

import pizzario.model.Delivery;
import pizzario.model.Order;

import java.util.List;
import java.util.UUID;

public interface IDeliveryRepository extends IRepository< Delivery >
{

    List< UUID > selectWaitingIds ();

    List< UUID > selectInProgressIds ();

    Delivery findByOrder ( Order o );
}
