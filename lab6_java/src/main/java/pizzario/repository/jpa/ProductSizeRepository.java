/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import org.springframework.stereotype.Repository;
import pizzario.model.ProductSize;
import pizzario.repository.IProductSizeRepository;


@Repository
class ProductSizeRepository
        extends NamedObjectRepository< ProductSize >
        implements IProductSizeRepository
{
    public ProductSizeRepository () {
        super( ProductSize.class );
    }
}
