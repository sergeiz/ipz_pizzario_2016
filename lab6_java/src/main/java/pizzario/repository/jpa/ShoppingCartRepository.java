/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import org.springframework.stereotype.Repository;
import pizzario.model.ShoppingCart;
import pizzario.repository.IShoppingCartRepository;

import java.util.List;
import java.util.UUID;


@Repository
class ShoppingCartRepository
        extends BasicRepository< ShoppingCart >
        implements IShoppingCartRepository
{
    public ShoppingCartRepository () {
        super( ShoppingCart.class );
    }


    @Override
    public List< UUID > selectAllOpenCartIds() {
        return getEntityManager().createQuery(

                "SELECT cart.domainId " +
                        "FROM ShoppingCart cart " +
                        "WHERE cart.modifiable = false",

                UUID.class

        ).getResultList();
    }
}
