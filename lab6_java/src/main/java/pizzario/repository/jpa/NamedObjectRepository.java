/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import pizzario.utils.DomainEntity;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;


abstract class NamedObjectRepository< T extends DomainEntity > extends BasicRepository< T >
{

    protected NamedObjectRepository ( Class< T > theClass ) {
        super( theClass );
    }


    public T findByName ( String name ) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery< T > criteria = cb.createQuery( getEntityClass() );
        Root< T > root = criteria.from( getEntityClass() );
        criteria.select( root )
                .where(
                        cb.equal(
                                root.get( "name" ),
                                name
                        )
                );

        List< T > results = getEntityManager().createQuery( criteria ).getResultList();
        return ( results.size() == 1 ) ? results.get( 0 ) : null;
    }
}
