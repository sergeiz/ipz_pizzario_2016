/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository;

import pizzario.utils.DomainEntity;

import java.util.List;
import java.util.UUID;


public interface IRepository< T extends DomainEntity >
{
    Class< T > getEntityClass ();

    T load ( long id );

    List< T > loadAll ();

    void add ( T t );

    void delete ( T t );

    T findByDomainId ( UUID domainId );

    List< UUID > selectAllDomainIds ();
}
