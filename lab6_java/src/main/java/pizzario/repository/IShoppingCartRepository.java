/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository;

import pizzario.model.ShoppingCart;

import java.util.List;
import java.util.UUID;

public interface IShoppingCartRepository extends IRepository< ShoppingCart >
{
    List< UUID > selectAllOpenCartIds ();
}
