/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.exceptions.PriceUndefinedException;
import pizzario.utils.DomainEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Access( AccessType.PROPERTY )
public class ProductItem extends DomainEntity
{

    protected ProductItem () {}

    public ProductItem ( UUID domainId, Product product, ProductSize size, int quantity )
            throws PriceUndefinedException
    {

        super( domainId );

        setProduct( product );
        setSize( size );
        setQuantity( quantity );
        setFixedPrice( product.getPrice( size ) );
    }

    @ManyToOne
    public Product getProduct ()
    {
        return product;
    }

    private void setProduct ( Product product )
    {
        this.product = product;
    }

    @ManyToOne
    public ProductSize getSize ()
    {
        return size;
    }

    private void setSize ( ProductSize size )
    {
        this.size = size;
    }

    public int getQuantity ()
    {
        return this.quantity;
    }

    public void setQuantity ( int quantity )
    {
        this.quantity = quantity;
    }

    public BigDecimal getFixedPrice ()
    {
        return this.fixedPrice;
    }

    public void setFixedPrice ( BigDecimal fixedPrice )
    {
        this.fixedPrice = fixedPrice;
    }

    @Transient
    public BigDecimal getCost ()
    {
        return getFixedPrice().multiply( BigDecimal.valueOf( getQuantity() ) );
    }


    private Product product;

    private ProductSize size;

    private int quantity;

    private BigDecimal fixedPrice;
}
