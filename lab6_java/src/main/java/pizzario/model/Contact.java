/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.Value;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.util.Arrays;
import java.util.List;

@Embeddable
@Access( AccessType.PROPERTY )
public class Contact extends Value< Contact >
{

    protected Contact () {}

    public Contact ( String address, String phone )
    {
        setAddress( address );
        setPhone( phone );
    }

    public String getAddress () { return this.address; }

    private void setAddress ( String address )
    {
        this.address = address;
    }

    public String getPhone ()
    {
        return this.phone;
    }

    private void setPhone ( String phone )
    {
        this.phone = phone;
    }

    @Override
    @Transient
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList( getAddress(), getPhone() );
    }

    private String address;

    private String phone;
}
