/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.exceptions.RemovingUnusedIngredientException;
import pizzario.utils.DomainEntity;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Entity
@Access( AccessType.PROPERTY )
public class Recipe extends DomainEntity
{

    protected Recipe () {}

    public Recipe ( UUID domainId, Product product, ProductSize size )
    {
        super( domainId );
        setProduct( product );
        setSize( size );
    }

    @ManyToOne
    public Product getProduct ()
    {
        return this.product;
    }

    private void setProduct ( Product product )
    {
        this.product = product;
    }

    @ManyToOne
    public ProductSize getSize ()
    {
        return this.size;
    }

    private void setSize ( ProductSize size )
    {
        this.size = size;
    }

    @Transient
    public double getTotalIngredientsWeight ()
    {
        return ingredients.values().stream()
                .mapToDouble( value -> value.doubleValue() )
                .sum()
                ;
    }

    public Iterable< Ingredient > listUsedIngredients ()
    {
        return ingredients.keySet();
    }

    public boolean usesIngredient ( Ingredient i )
    {
        return ingredients.containsKey( i );
    }

    public double getIngredientWeight ( Ingredient i )
    {
        Double weight = ingredients.get( i );
        return ( weight != null ) ? weight : 0.0;
    }

    public Recipe useIngredient ( Ingredient i, double weight )
    {
        ingredients.put( i, weight );
        return this;
    }

    public void removeIngredient ( Ingredient i ) throws RemovingUnusedIngredientException
    {
        if ( usesIngredient( i ) )
            ingredients.remove( i );

        else
            throw new RemovingUnusedIngredientException( product.getName(), i.getName() );
    }


    private Product product;

    private ProductSize size;

    @ElementCollection
    @Access( AccessType.FIELD )
    private Map< Ingredient, Double > ingredients = new HashMap<>();

}
