/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.DomainEntity;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import java.util.UUID;

@Entity
@Access( AccessType.PROPERTY )
public class ProductSize extends DomainEntity
{

    protected ProductSize () {}

    public ProductSize ( UUID domainId, String name, int diameter, double weight )
    {
        super( domainId );
        setName( name );
        setDiameter( diameter );
        setWeight( weight );
    }


    public String getName ()
    {
        return this.name;
    }

    public void setName ( String name )
    {
        this.name = name;
    }

    public int getDiameter ()
    {
        return this.diameter;
    }

    public void setDiameter ( int diameter )
    {
        this.diameter = diameter;
    }

    public double getWeight ()
    {
        return this.weight;
    }

    public void setWeight ( double weight )
    {
        this.weight = weight;
    }



    private String name;

    private int diameter;

    private double weight;
}
