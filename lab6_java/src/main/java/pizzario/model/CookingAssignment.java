/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.exceptions.CookingLifecycleException;
import pizzario.exceptions.OrderLifecycleException;
import pizzario.utils.DomainEntity;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.UUID;

@Entity
@Access( AccessType.PROPERTY )
public class CookingAssignment extends DomainEntity
{

    protected CookingAssignment () {}

    public CookingAssignment ( UUID domainID, Order order, Product product, ProductSize size )
    {
        super( domainID );

        setOrder( order );
        setProduct( product );
        setSize( size );

        status = CookingStatus.Waiting;
    }

    @ManyToOne
    public Order getOrder ()
    {
        return this.order;
    }

    private void setOrder ( Order order )
    {
        this.order = order;
    }

    @ManyToOne
    public Product getProduct ()
    {
        return this.product;
    }

    private void setProduct ( Product product )
    {
        this.product = product;
    }

    @ManyToOne
    public ProductSize getSize ()
    {
        return this.size;
    }

    private void setSize ( ProductSize size )
    {
        this.size = size;
    }

    public CookingStatus getStatus ()
    {
        return this.status;
    }

    private void setStatus ( CookingStatus status )
    {
        this.status = status;
    }

    public void startCooking () throws CookingLifecycleException
    {
        if ( this.status != CookingStatus.Waiting )
            throw CookingLifecycleException.makeStartWhenNotWaiting( getDomainId() );

        this.status = CookingStatus.InProgress;
    }

    public void finishCooking () throws CookingLifecycleException, OrderLifecycleException
    {
        if ( this.status != CookingStatus.InProgress )
            throw CookingLifecycleException.makeFinishWhenNotInProgress( getDomainId() );

        this.status = CookingStatus.Finished;

        getOrder().cookingAssignmentCompleted();
    }


    private Order order;

    private Product product;

    private ProductSize size;

    private CookingStatus status;

}
