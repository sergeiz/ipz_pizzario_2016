/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.exceptions.ModifiableCartException;
import pizzario.exceptions.OrderLifecycleException;
import pizzario.utils.DomainEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity( name = "CustomerOrder" )
public class Order extends DomainEntity
{

    protected Order () {}

    public Order ( UUID domainId, ShoppingCart cart, Contact contact, LocalDateTime time ) throws ModifiableCartException
    {
        super( domainId );

        if ( cart.isModifiable() )
            throw new ModifiableCartException( cart.getDomainId() );

        this.items = new ArrayList<>( cart.getItems() );

        setContact( contact );
        setDiscount( new Discount() );
        setBasicCost( cart.cost() );

        this.placementTime = time;
        this.status = OrderStatus.Placed;

        this.unfinishedCookingsCount = 0;
        for ( ProductItem item : items )
            this.unfinishedCookingsCount += item.getQuantity();
    }

    public List< ProductItem > getItems ()
    {
        return this.items;
    }

    public BigDecimal getBasicCost ()
    {
        return this.basicCost;
    }

    private void setBasicCost ( BigDecimal basicCost )
    {
        this.basicCost = basicCost;
    }

    @Access( AccessType.PROPERTY )
    @Embedded
    public Discount getDiscount ()
    {
        return this.discount;
    }

    public void setDiscount ( Discount discount )
    {
        this.discount = discount;
    }

    public BigDecimal getTotalCost ()
    {
        return this.getDiscount().getDiscountedPrice( getBasicCost() );
    }

    public OrderStatus getStatus ()
    {
        return this.status;
    }

    public LocalDateTime getPlacementTime ()
    {
        return this.placementTime;
    }

    @Access( AccessType.PROPERTY )
    @Embedded
    public Contact getContact ()
    {
        return this.contact;
    }


    private void setContact ( Contact contact )
    {
        this.contact = contact;
    }

    public int getUnfinishedCookingsCount ()
    {
        return this.unfinishedCookingsCount;
    }

    public void confirm () throws OrderLifecycleException
    {
        if ( this.status != OrderStatus.Placed )
        {
            throw new OrderLifecycleException(
                    getDomainId(),
                    this.status.toString(),
                    OrderStatus.Placed.toString()
            );
        }

        this.status = OrderStatus.Confirmed;
    }

    public void cancel () throws OrderLifecycleException
    {
        if ( this.status != OrderStatus.Placed )
        {
            throw new OrderLifecycleException(
                    getDomainId(),
                    this.status.toString(),
                    OrderStatus.Placed.toString()
            );
        }

        this.status = OrderStatus.Cancelled;
    }

    public void cookingAssignmentCompleted () throws OrderLifecycleException
    {
        if ( this.status == OrderStatus.Confirmed )
        {
            -- unfinishedCookingsCount;
            if ( unfinishedCookingsCount == 0 )
                this.status = OrderStatus.Delivering;
        }
        else
        {
            throw new OrderLifecycleException(
                    getDomainId(),
                    this.status.toString(),
                    OrderStatus.Confirmed.toString()
            );
        }

    }

    public void deliveryCompleted () throws OrderLifecycleException
    {
        if ( this.status != OrderStatus.Delivering )
        {
            throw new OrderLifecycleException(
                    getDomainId(),
                    this.status.toString(),
                    OrderStatus.Delivering.toString()
            );
        }

        this.status = OrderStatus.Completed;
    }

    public List< CookingAssignment > generateCookingAssignments ()
    {
        List< CookingAssignment > cookingAssignments = new ArrayList<>();

        for ( ProductItem item : items )
        {
            for ( int i = 0; i < item.getQuantity(); i++ )
                cookingAssignments.add(
                        new CookingAssignment(
                                UUID.randomUUID(),
                                this,
                                item.getProduct(),
                                item.getSize()
                        )
                );
        }

        return cookingAssignments;
    }
    public Delivery generateDelivery ()
    {
        return new Delivery( UUID.randomUUID(), this );
    }

    @OneToMany
    private List< ProductItem > items;

    @Transient
    private Contact contact;

    @Transient
    private Discount discount;

    private LocalDateTime placementTime;

    private BigDecimal basicCost;

    private OrderStatus status;

    private int unfinishedCookingsCount;

}
