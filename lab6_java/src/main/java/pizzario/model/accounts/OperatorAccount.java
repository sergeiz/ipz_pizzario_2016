/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model.accounts;

import pizzario.model.Order;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class OperatorAccount extends Account
{

    protected OperatorAccount () {}

    public OperatorAccount ( UUID domainId, String name, String email, String password )
    {
        super( domainId, name, email, password );
    }

    public List< Order > getOrders ()
    {
        return this.orders;
    }

    public void trackOrder ( Order order )
    {
        orders.add( order );
    }


    public void accept ( AccountVisitor visitor )
    {
        visitor.visit( this );
    }

    @OneToMany
    private List< Order > orders = new ArrayList<>();

}
