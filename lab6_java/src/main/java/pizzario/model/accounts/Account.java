/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model.accounts;

import pizzario.utils.DomainEntity;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import java.util.UUID;

@Entity
@Access( AccessType.PROPERTY )
public class Account extends DomainEntity
{

    protected Account () {}

    public Account ( UUID domainId, String name, String email, String password )
    {
        super( domainId );
        setName( name );
        setEmail( email );
        setPassword( password );
    }

    public String getName ()
    {
        return this.name;
    }

    public void setName ( String name ) { this.name = name; }

    public String getEmail ()
    {
        return this.email;
    }

    public void setEmail ( String email )
    {
        this.email = email;
    }

    public String getPassword ()
    {
        return this.password;
    }

    public void setPassword ( String password )
    {
        this.password = password;
    }


    public boolean checkPassword ( String password )
    {
        return this.getPassword() == password;
    }

    public void accept ( AccountVisitor visitor )
    {
        visitor.visit( this );
    }

    private String name;

    private String email;

    private String password;
}
