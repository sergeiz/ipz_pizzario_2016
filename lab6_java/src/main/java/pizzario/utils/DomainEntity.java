/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.utils;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@MappedSuperclass
public abstract class DomainEntity
{

    protected DomainEntity () {}

    protected DomainEntity ( UUID domainId )
    {
        this.domainId = domainId;
    }

    public UUID getDomainId ()
    {
        return this.domainId;
    }

    public long getDatabaseId ()
    {
        return this.databaseId;
    }

    public void setDatabaseId ( long databaseId )
    {
        this.databaseId = databaseId;
    }

    @Override
    public boolean equals ( Object o )
    {
        if ( o.getClass() != this.getClass() )
            return false;

        DomainEntity otherDomainEntity = ( DomainEntity ) o;
        return domainId.equals( otherDomainEntity.getDomainId() );
    }

    @Override
    public int hashCode ()
    {
        return domainId.hashCode();
    }

    @Column( length = 32, unique = true, nullable = false )
    @Type( type = "uuid-binary" )
    private UUID domainId;

    @Id
    @GeneratedValue
    private long databaseId;
}
