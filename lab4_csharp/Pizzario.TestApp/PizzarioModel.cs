﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System;
using System.Collections.Generic;

namespace Pizzario.TestApp
{
    class PizzarioModel
    {
        public ICollection< ProductSize > ProductSizes { get; private set; }

        public ICollection< Ingredient > Ingredients { get; private set; }

        public ICollection< Product > Products { get; private set; }

        public ICollection< ShoppingCart > ShoppingCarts { get; private set; }

        public ICollection< Order > Orders { get; private set; }

        public ICollection< Account > Accounts { get; private set; }

        public ICollection< CookingAssignment > Cookings { get; private set; }

        public ICollection< Delivery > Deliveries { get; private set; }


        public PizzarioModel ()
        {
            this.ProductSizes  = new List< ProductSize >();
            this.Ingredients   = new List< Ingredient >();
            this.Products      = new List< Product >();
            this.ShoppingCarts = new List< ShoppingCart >();
            this.Orders        = new List< Order >();
            this.Accounts      = new List< Account >();
            this.Cookings      = new List< CookingAssignment >();
            this.Deliveries    = new List< Delivery >();
        }

    }
}
