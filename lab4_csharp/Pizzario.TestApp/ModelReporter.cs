﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.IO;
using System.Collections.Generic;

namespace Pizzario.TestApp
{
    class ModelReporter
    {
        public ModelReporter ( TextWriter output )
        {
            this.output = output;
        }

        public void GenerateReport ( PizzarioModel model )
        {
            ReportCollection( "Sizes", model.ProductSizes );
            ReportCollection( "Ingredients", model.Ingredients );
            ReportCollection( "Products", model.Products );
            ReportCollection( "Carts", model.ShoppingCarts );
            ReportCollection( "Orders", model.Orders );
            ReportCollection( "Accounts", model.Accounts );
            ReportCollection( "Cookings", model.Cookings );
            ReportCollection( "Deliveries", model.Deliveries );
        }

        private void ReportCollection< T > ( string title, ICollection< T > items )
        {
            output.WriteLine("==== {0} ==== ", title );
            output.WriteLine();

            foreach ( var item in items )
            {
                output.WriteLine( item );
                output.WriteLine();
            }

            output.WriteLine();
        }

        private TextWriter output;
    }
}
