﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Repository.EntityFramework;

using System;
using System.IO;
using System.Text;

namespace Pizzario.TestApp
{
    class Program
    {
        static void Main ( string[] args )
        {
            try
            {
                PizzarioModel model1 = TestModelGenerator.GenerateTestData();
                using ( var dbContext = new PizzarioDbContext() )
                {
                    ModelSaver saver = new ModelSaver( dbContext );
                    saver.Save( model1 );
                }

                using ( var dbContext = new PizzarioDbContext() )
                {
                    ModelRestorer restorer = new ModelRestorer( dbContext );
                    PizzarioModel model2 = restorer.Restore();
                    
                    CompareModels( model1, model2 );
                }
            }
            catch ( Exception e )
            {
                Console.WriteLine( e.GetType().FullName );
                Console.WriteLine( e.Message );
                Console.WriteLine( e.StackTrace );
            }
        }

        private static void CompareModels ( PizzarioModel model1, PizzarioModel model2 )
        {
            StringWriter writer1 = new StringWriter( new StringBuilder() );
            var reportGenerator1 = new ModelReporter( writer1 );
            reportGenerator1.GenerateReport( model1 );

            StringWriter writer2 = new StringWriter( new StringBuilder() );
            var reportGenerator2 = new ModelReporter( writer2 );
            reportGenerator2.GenerateReport( model2 );

            String report1Content = writer1.ToString();
            String report2Content = writer2.ToString();

            Console.WriteLine( report1Content );

            Console.WriteLine("================================================================");
            Console.WriteLine();

            Console.WriteLine( report2Content );

            Console.WriteLine("================================================================");
            Console.WriteLine();

            Console.WriteLine( report1Content.Equals( report2Content ) ? "PASSED: Models match" : "FAILED: Models mismatch");
        }
    }
}
