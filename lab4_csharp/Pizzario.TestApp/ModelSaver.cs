﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;
using Pizzario.Repository;
using Pizzario.Repository.EntityFramework;

using System.Collections.Generic;

namespace Pizzario.TestApp
{
    class ModelSaver
    {
        public ModelSaver ( PizzarioDbContext dbContext )
        {
            this.dbContext = dbContext;
        }


        public void Save ( PizzarioModel model )
        {
            SaveCollection( RepositoryFactory.MakeProductSizeRepository( dbContext ),        model.ProductSizes );
            SaveCollection( RepositoryFactory.MakeIngredientRepository( dbContext ),         model.Ingredients );
            SaveCollection( RepositoryFactory.MakeProductRepository( dbContext ),            model.Products );
            SaveCollection( RepositoryFactory.MakeShoppingCartRepository( dbContext ),       model.ShoppingCarts );
            SaveCollection( RepositoryFactory.MakeOrderRepository( dbContext ),              model.Orders );
            SaveCollection( RepositoryFactory.MakeAccountRepository( dbContext ),            model.Accounts );
            SaveCollection( RepositoryFactory.MakeCookingAssignmentRepository( dbContext ),  model.Cookings );
            SaveCollection( RepositoryFactory.MakeDeliveryRepository( dbContext ),           model.Deliveries );
        }


        private void SaveCollection< TRepository, TEntity > ( TRepository repository, ICollection< TEntity > collection )
            where TRepository : IRepository< TEntity >
            where TEntity : Utils.Entity
        {
            foreach ( TEntity obj in collection )
                repository.Add( obj );

            repository.Commit();
        }

        private PizzarioDbContext dbContext;
    }
}
