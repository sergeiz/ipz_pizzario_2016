﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;
using Pizzario.Repository;
using Pizzario.Repository.EntityFramework;

using System.Collections.Generic;
using System.Data.Entity;

namespace Pizzario.TestApp
{
    class ModelRestorer
    {
        public ModelRestorer ( PizzarioDbContext dbContext )
        {
            this.dbContext = dbContext;
        }

        public PizzarioModel Restore ()
        {
            PizzarioModel m = new PizzarioModel();

            RestoreCollection( RepositoryFactory.MakeProductSizeRepository( dbContext ),       m.ProductSizes );
            RestoreCollection( RepositoryFactory.MakeIngredientRepository( dbContext ),        m.Ingredients );
            RestoreCollection( RepositoryFactory.MakeProductRepository( dbContext ),           m.Products );
            RestoreCollection( RepositoryFactory.MakeShoppingCartRepository( dbContext ),      m.ShoppingCarts );
            RestoreCollection( RepositoryFactory.MakeOrderRepository( dbContext ),             m.Orders );
            RestoreCollection( RepositoryFactory.MakeAccountRepository( dbContext ),           m.Accounts );
            RestoreCollection( RepositoryFactory.MakeCookingAssignmentRepository( dbContext ), m.Cookings );
            RestoreCollection( RepositoryFactory.MakeDeliveryRepository( dbContext ),          m.Deliveries );

            return m;
        }

        private PizzarioDbContext dbContext;


        private void RestoreCollection< TEntity >( IRepository< TEntity > repository, ICollection<TEntity> target)
            where TEntity : Utils.Entity
        {
            foreach ( TEntity obj in repository.LoadAll() )
                target.Add( obj );
        }
    }
}
