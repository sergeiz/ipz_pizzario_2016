﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System;


namespace Pizzario.TestApp
{
    static class TestModelGenerator
    {
        public static PizzarioModel GenerateTestData ()
        {
            PizzarioModel m = new PizzarioModel();

            GenerateSizes( m );
            GenerateIngredients( m );
            GenerateProducts( m );
            GenerateCarts( m );
            GenerateOrders( m );
            GenerateAccounts( m );
            GenerateCookingAssignments( m );
            GenerateDeliveries( m );

            return m;
        }


        private static void GenerateSizes ( PizzarioModel m )
        {
            // ----

            Small = new ProductSize( Guid.NewGuid(), "Small", 25, 300.0 );
            Medium = new ProductSize( Guid.NewGuid(), "Medium", 30, 600.0 );
            Large = new ProductSize( Guid.NewGuid(), "Large", 35, 900.0 );

            // ----

            m.ProductSizes.Add( Small );
            m.ProductSizes.Add( Medium );
            m.ProductSizes.Add( Large );
        }


        private static void GenerateIngredients ( PizzarioModel m )
        {
            Mozarella       = new Ingredient( Guid.NewGuid(), "Mozarella" );
            Pineaple        = new Ingredient( Guid.NewGuid(), "Pineaple" );
            Chicken         = new Ingredient( Guid.NewGuid(), "Chicken" );
            Saliami         = new Ingredient( Guid.NewGuid(), "Saliami" );
            Tomato          = new Ingredient( Guid.NewGuid(), "Tomato" );
            Kebab           = new Ingredient( Guid.NewGuid(), "Kebab" );
            Eggplant        = new Ingredient( Guid.NewGuid(), "Eggplant" );
            Onion           = new Ingredient( Guid.NewGuid(), "Onion" );
            Parsley         = new Ingredient( Guid.NewGuid(), "Parsley" );
            Bacon           = new Ingredient( Guid.NewGuid(), "Bacon" );
            Ham             = new Ingredient( Guid.NewGuid(), "Ham" );
            PickledCucumber = new Ingredient( Guid.NewGuid(), "Pickled Cucumber" );
            Mushroom        = new Ingredient( Guid.NewGuid(), "Mushroom" );

            m.Ingredients.Add( Mozarella );
            m.Ingredients.Add( Pineaple );
            m.Ingredients.Add( Chicken );
            m.Ingredients.Add( Saliami );
            m.Ingredients.Add( Tomato );
            m.Ingredients.Add( Kebab );
            m.Ingredients.Add( Eggplant );
            m.Ingredients.Add( Onion );
            m.Ingredients.Add( Parsley );
            m.Ingredients.Add( Bacon );
            m.Ingredients.Add( Ham );
            m.Ingredients.Add( PickledCucumber );
            m.Ingredients.Add( Mushroom );
        }

        private static void GenerateProducts ( PizzarioModel m )
        {
            // ----

            Mafia = new Product( Guid.NewGuid(), "Mafia" );
            Mafia.ImageUrl = "http://pizzario.com/images/mafia.jpg";

            Mafia.DefineRecipe( Small )
                .UseIngredient( Mozarella, 25.0 )
                .UseIngredient( Pineaple,  25.0 )
                .UseIngredient( Chicken,   25.0 )
                .UseIngredient( Saliami,   25.0 )
                .UseIngredient( Tomato,    25.0 );

            Mafia.DefineRecipe( Medium )
                .UseIngredient( Mozarella, 50.0 )
                .UseIngredient( Pineaple,  50.0 )
                .UseIngredient( Chicken,   50.0 )
                .UseIngredient( Saliami,   50.0 )
                .UseIngredient( Tomato,    50.0 );

            Mafia.DefineRecipe( Large )
                .UseIngredient( Mozarella, 75.0 )
                .UseIngredient( Pineaple,  75.0 )
                .UseIngredient( Chicken,   75.0 )
                .UseIngredient( Saliami,   75.0 )
                .UseIngredient( Tomato,    75.0 );

            Mafia.SetPrice( Small,   73.00M );
            Mafia.SetPrice( Medium,  99.00M );
            Mafia.SetPrice( Large,  125.00M );

            // ----

            Georgia = new Product( Guid.NewGuid(), "Georgia" );
            Georgia.ImageUrl = "http://pizzario.com/images/georgia.jpg";

            Georgia.DefineRecipe( Small )
                .UseIngredient( Kebab,     25.0 )
                .UseIngredient( Mozarella, 25.0 )
                .UseIngredient( Eggplant,  25.0 )
                .UseIngredient( Onion,     25.0 )
                .UseIngredient( Parsley,   10.0 )
                .UseIngredient( Tomato,    25.0 );

            Georgia.DefineRecipe( Medium )
                .UseIngredient( Kebab,     50.0 )
                .UseIngredient( Mozarella, 50.0 )
                .UseIngredient( Eggplant,  50.0 )
                .UseIngredient( Onion,     50.0 )
                .UseIngredient( Parsley,   20.0 )
                .UseIngredient( Tomato,    50.0 );

            Georgia.DefineRecipe( Large )
                .UseIngredient( Kebab,     75.0 )
                .UseIngredient( Mozarella, 75.0 )
                .UseIngredient( Eggplant,  75.0 )
                .UseIngredient( Onion,     75.0 )
                .UseIngredient( Parsley,   75.0 )
                .UseIngredient( Tomato,    75.0 );

            Georgia.SetPrice( Small,   81.00M );
            Georgia.SetPrice( Medium, 103.00M );
            Georgia.SetPrice( Large,  137.00M );

            // ----

            Cossack = new Product( Guid.NewGuid(), "Cossack" );
            Cossack.ImageUrl = "http://pizzario.com/images/cosscack.jpg";

            Cossack.DefineRecipe( Small )
                .UseIngredient( Mozarella,       25.0 )
                .UseIngredient( Bacon,           25.0 )
                .UseIngredient( Ham,             25.0 )
                .UseIngredient( Onion,           25.0 )
                .UseIngredient( PickledCucumber, 25.0 )
                .UseIngredient( Mushroom,        25.0 );

            Cossack.DefineRecipe( Medium )
                .UseIngredient( Mozarella,       50.0 )
                .UseIngredient( Bacon,           50.0 )
                .UseIngredient( Ham,             50.0 )
                .UseIngredient( Onion,           50.0 )
                .UseIngredient( PickledCucumber, 50.0 )
                .UseIngredient( Mushroom,        50.0 );

            Cossack.DefineRecipe( Large )
                .UseIngredient( Mozarella,       75.0 )
                .UseIngredient( Bacon,           75.0 )
                .UseIngredient( Ham,             75.0 )
                .UseIngredient( Onion,           75.0 )
                .UseIngredient( PickledCucumber, 75.0 )
                .UseIngredient( Mushroom,        75.0 );

            Cossack.SetPrice( Small,   83.00M );
            Cossack.SetPrice( Medium, 107.00M );
            Cossack.SetPrice( Large,  143.00M );

            // ----

            m.Products.Add( Mafia );
            m.Products.Add( Georgia );
            m.Products.Add( Cossack );
        }


        private static void GenerateCarts ( PizzarioModel m )
        {
            cart = new ShoppingCart( Guid.NewGuid() );
            cart.AddItem( new ProductItem( Guid.NewGuid(), Mafia,   Large,  1 ) );
            cart.AddItem( new ProductItem( Guid.NewGuid(), Georgia, Small,  2 ) );
            cart.AddItem( new ProductItem( Guid.NewGuid(), Cossack, Medium, 1 ) );

            cart.Checkout();

            m.ShoppingCarts.Add( cart );
        }


        private static void GenerateOrders ( PizzarioModel m )
        {
            order = new Order( 
                Guid.NewGuid(), 
                cart,
                new Contact( "Sumskaya 1", "123-45-67" ), 
                DateTime.Now 
            );

            order.SetDiscount( new Discount( 20.00M ) );
            order.Confirm();

            m.Orders.Add( order );
        }
        

        private static void GenerateAccounts ( PizzarioModel m )
        {
            OperatorAccount wasya = new OperatorAccount( Guid.NewGuid(), "Wasya Pupkin", "wasya.pupkin@pizzario.com", "12345");
            m.Accounts.Add( wasya );

            wasya.Orders.Add( order );
        }


        private static void GenerateCookingAssignments ( PizzarioModel m )
        {
            var orderAssignments = order.GenerateCookingAssignments();
            foreach ( CookingAssignment ca in orderAssignments )
                m.Cookings.Add( ca );
        }


        private static void GenerateDeliveries ( PizzarioModel m )
        {
            m.Deliveries.Add( order.GenerateDelivery( "Ivan Vodilkin" ) );
        }

        private static ProductSize Large, Medium, Small;
        private static Ingredient Mozarella, Pineaple, Chicken, Saliami, Tomato, Kebab, Eggplant;
        private static Ingredient Onion, Parsley, Bacon, Ham, PickledCucumber, Mushroom;
        private static Product Mafia, Georgia, Cossack;
        private static ShoppingCart cart;
        private static Order order;
    }

}
