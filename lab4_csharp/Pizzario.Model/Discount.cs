﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Model
{
    public class Discount : Utils.Value< Discount >
    {

        public decimal Percent
        {
            get { return _percent.Value; }
            private set { _percent.Value = value; }
        }

        public Discount ()
        {
            this._percent.Value = 0.0M;
        }

        public Discount ( decimal value )
        {
            this._percent.Value = value;
        }

        public override string ToString()
        {
            return Percent.ToString();
        }

        public decimal GetDiscountedPrice ( decimal price )
        {
            return price * ( 100.00M - Percent) / 100.00M;
        }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new object[] { Percent };
        }

        private readonly Utils.RangeProperty< decimal > _percent = 
            new Utils.RangeProperty<decimal>( "value", 0.0M, true, 100.0M, true );
    }
}
