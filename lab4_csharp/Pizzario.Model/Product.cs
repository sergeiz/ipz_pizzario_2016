﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Pizzario.Model
{
    public class Product : Utils.Entity
    {
        public string Name
        {
            get {  return _name.Value; }
            set { _name.Value = value; }
        }

        public string ImageUrl { get; set; }

        public virtual ICollection< Recipe > Recipes { get; private set; }

        public virtual ICollection< ProductPriceAssignment > Prices { get; private set; }

        protected Product() {} 

        public Product ( Guid domainId, string name )
            :   base( domainId )
        {
            this.Name = name;
            this.ImageUrl = "";
            this.Prices = new List< ProductPriceAssignment >();
            this.Recipes = new HashSet< Recipe >();
        }

        public override string ToString()
        {
            StringBuilder pricesAsString = new StringBuilder();
            foreach ( var assignment in Prices )
                pricesAsString.AppendFormat( "{0} = {1}  ", assignment.Size.Name, assignment.Price );

            StringBuilder recipesAsString = new StringBuilder();
            foreach ( var recipe in Recipes )
                recipesAsString.AppendLine( recipe.ToString() );

            return string.Format(
                       "ID = {0}\nName = {1}\nImageUrl = {2}\nRecipes:\n{3}\nPrices: {4}",
                       DomainId,
                       Name,
                       ImageUrl,
                       recipesAsString.ToString(),
                       pricesAsString.ToString()
                   );
        }

        public decimal GetPrice ( ProductSize size )
        {
            var assignment = Prices.Where( p => p.Size == size ).FirstOrDefault();
            if ( assignment != null )
                   return assignment.Price;

            throw new InvalidOperationException( "Price for size " + size.Name + " was not previously defined" );
        }

        public void SetPrice ( ProductSize size, decimal price )
        {
            var assignment = Prices.Where(p => p.Size == size).FirstOrDefault();
            if ( assignment != null )
            {
                assignment.Price = price;
                return;
            }

            Prices.Add( new ProductPriceAssignment( this, size, price ) );
        }

        public void RemovePrice ( ProductSize size )
        {
            var priceAssignment = Prices.Where( a => a.Size == size ).FirstOrDefault();
            if ( priceAssignment != null )
                Prices.Remove( priceAssignment );

            else
                throw new InvalidOperationException( "No price defined for size " + size.Name );
        }

        public Recipe GetRecipe ( ProductSize size )
        {
            var recipe = Recipes.Where( r => r.Size == size ).FirstOrDefault();
            if ( recipe != null )
                return recipe;

            throw new InvalidOperationException( "Recipe for size " + size.Name + " was not previously defined" );
        }

        public Recipe DefineRecipe ( ProductSize size )
        {
            var recipe = Recipes.Where( r => r.Size == size ).FirstOrDefault();
            if ( recipe != null )
                throw new InvalidOperationException( "Recipe for size " + size.Name + " was already defined" );

            recipe = new Recipe( Guid.NewGuid(), this, size );
            Recipes.Add( recipe );
            return recipe;
        }

        public void RemoveRecipe ( ProductSize size )
        {
            var recipe = Recipes.Where( r => r.Size == size ).FirstOrDefault();
            if ( recipe == null )
                throw new InvalidOperationException( "Recipe for size " + size.Name + " was not defined" );

            Recipes.Remove( recipe );
        }

        private Utils.NonEmptyString _name = new Utils.NonEmptyString( "name" );
    }
}
