﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Model
{
    public class ProductItem : Utils.Entity
    {
        public virtual Product SelectedProduct
        {
            get { return _product.Value; }
            private set { _product.Value = value; }
        }
    
        public virtual ProductSize Size
        {
            get { return _size.Value; }
            private set { _size.Value = value; }
        }

        public decimal FixedPrice {
            get {  return _fixedPrice.Value; }
            private set { _fixedPrice.Value = value; }
        }

        public int Quantity
        {
            get { return _quantity.Value; }
            private set { _quantity.Value = value; }
        }

        public decimal Cost
        {
            get
            {
                return FixedPrice * Quantity;
            }
        }

        protected ProductItem () {}

        public ProductItem ( Guid domainId, Product product, ProductSize size, int quantity )
            :   base( domainId )
        {
            if ( quantity <= 0 )
                throw new Exception( "ProductItem: non-positive quantity" );

            this._product.Value = product;
            this._size.Value = size;

            this._fixedPrice.Value = product.GetPrice( size );
            this._quantity.Value = quantity;
        }

        public override string ToString()
        {
            return string.Format(
                       "Product = {0}, Size = {1}, Price = {2}, Quantity = {3}",
                       SelectedProduct.Name,
                       Size.Name,
                       FixedPrice,
                       Quantity
                   );
        }


        private readonly Utils.RequiredProperty< Product > _product =
            new Utils.RequiredProperty< Product >( "product" );

        private readonly Utils.RequiredProperty< ProductSize > _size =
            new Utils.RequiredProperty< ProductSize >( "size" );

        private readonly Utils.RangeProperty< int > _quantity =
            new Utils.RangeProperty< int >( "quantity", 0, false, int.MaxValue, true );

        private readonly Utils.RangeProperty< decimal > _fixedPrice =
            new Utils.RangeProperty< decimal >( "fixedPrice", 0, true, decimal.MaxValue, true );
    }
}
