﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;
using System.Text;

namespace Pizzario.Model
{
    public class OperatorAccount : Account
    {
        public virtual ICollection< Order > Orders { get; private set; }

        protected OperatorAccount() {}

        public OperatorAccount ( Guid id, string name, string email, string passwordHash )
            :   base( id, name, email, passwordHash )
        {
            this.Orders = new List< Order >();
        }

        public override string ToString()
        {
            StringBuilder b = new StringBuilder();
            b.AppendLine( base.ToString() );

            b.AppendLine( "Orders: " );
            foreach ( var order in Orders )
                b.AppendFormat("\t{0}\n", order.DomainId );

            return b.ToString();
        }


        public void TrackOrder ( Order order )
        {
            if ( order == null )
                throw new ArgumentNullException( "order" );

            Orders.Add( order );
        }
    }
}
