﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

namespace Pizzario.Model
{
    public enum DeliveryStatus
    {
        Waiting,
        InProgress,
        Delivered
    }
}
