﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;
using System.Text;

namespace Pizzario.Model
{
    public class ShoppingCart : Utils.Entity
    {
        public virtual IList< ProductItem > Items { get; private set; } 

        public bool Modifiable { get; private set; }

        public decimal Cost
        {
            get
            {
                decimal totalCost = 0;
                foreach ( var item in Items )
                    totalCost += item.Cost;
                return totalCost;
            }
        }

        protected ShoppingCart() {}


        public ShoppingCart ( Guid domainId )
            :   base( domainId )
        {
            this.Items = new List< ProductItem >();
            this.Modifiable = true;
        }


        public override string ToString ()
        {
            StringBuilder b = new StringBuilder();

            b.AppendFormat( "Id = {0}\n", DomainId );

            b.AppendLine( "Content:" );
            foreach( var item in Items )
            {
                b.Append( "\t" );
                b.Append( item );
                b.AppendLine();
            }

            b.AppendFormat( "Modifiable = {0}\n", Modifiable );

            return b.ToString();
        }


        public int FindItemIndex ( Product product, ProductSize size )
        {
            for ( int i = 0; i < Items.Count; i++ )
            {
                ProductItem item = Items[i];
                if ( item.SelectedProduct == product && item.Size == size )
                    return i;
            }

            return -1;
        }


        public void AddItem ( ProductItem item )
        { 
            if ( ! Modifiable )
                throw new InvalidOperationException( "ShoppingCart.AddItem: unmodifiable cart" );

            int existingItemIndex = FindItemIndex( item.SelectedProduct, item.Size);
            if ( existingItemIndex != -1 )
                throw new InvalidOperationException( "ShoppingCart.AddItem: duplicate product-size pair added" );

            Items.Add( item );
        }


        public void UpdateItem ( int index, ProductItem item )
        {
            if ( ! Modifiable )
                throw new InvalidOperationException( "ShoppingCart.UpdateItem: unmodifiable cart" );

            Items[ index ] = item;
        }


        public void DropItem ( int index )
        {
            if ( ! Modifiable )
                throw new InvalidOperationException( "ShoppingCart.DropItem: unmodifiable cart" );

            Items.RemoveAt( index );
        }


        public void ClearItems ()
        {
            if ( ! Modifiable )
                throw new InvalidOperationException( "ShoppingCart.ClearItems: unmodifiable cart" );

            Items.Clear();
        }


        public void Checkout ()
        {
            if ( Items.Count == 0 )
                throw new InvalidOperationException( "ShoppingCart.Lock: locking empty cart" );

            Modifiable = false;
        }
    }
}
