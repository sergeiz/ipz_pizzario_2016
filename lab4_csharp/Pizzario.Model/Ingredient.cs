﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Model
{
    public class Ingredient : Utils.Entity
    {
        public string Name
        {
            get { return _name.Value; }
            private set { _name.Value = value; }
        }

        protected Ingredient () {}

        public Ingredient ( Guid domainId, string name )
            :   base( domainId )
        {
            this._name.Value = name;
        }

        public override string ToString()
        {
            return string.Format( "{0}:\n\tId = {1}", Name, DomainId );
        }

        private readonly Utils.NonEmptyString _name = new Utils.NonEmptyString( "name" );
    }
}
