﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

namespace Pizzario.Repository.EntityFramework.Configurations
{
    class OperatorAccountConfiguration : BasicEntityConfiguration< OperatorAccount >
    {
        public OperatorAccountConfiguration ()
        {
            Property( a => a.Name ).IsRequired();
            Property( a => a.Email ).IsRequired();
            Property( a => a.Password ).IsRequired();

            HasMany( a => a.Orders ).WithOptional();
        }
    }
}
