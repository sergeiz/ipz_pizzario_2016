﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Repository.EntityFramework.Configurations
{
    abstract class BasicValueConfiguration< TValue > : ComplexTypeConfiguration< TValue > 
        where TValue : class
    {
        protected BasicValueConfiguration ()
        {
        }
    }
}
