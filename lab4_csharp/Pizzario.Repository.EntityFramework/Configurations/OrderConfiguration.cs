﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

namespace Pizzario.Repository.EntityFramework.Configurations
{
    class OrderConfiguration : BasicEntityConfiguration< Order >
    {
        public OrderConfiguration ()
        {
            HasMany< ProductItem >( o => o.Items ).WithOptional();
        }
    }
}
