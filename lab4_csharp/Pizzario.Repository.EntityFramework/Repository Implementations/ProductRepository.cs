﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

namespace Pizzario.Repository.EntityFramework
{
    public class ProductRepository : BasicRepository< Product >, IProductRepository
    {
        public ProductRepository ( PizzarioDbContext dbContext )
            :   base( dbContext, dbContext.Products )
        {
        }
    }
}
