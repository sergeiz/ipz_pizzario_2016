﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System.Data.Entity;
using System.Linq;

namespace Pizzario.Repository.EntityFramework
{
    public abstract class BasicRepository< T > where T : Pizzario.Utils.Entity
    {
        protected BasicRepository ( PizzarioDbContext dbContext, DbSet< T > dbSet )
        {
            this.dbContext = dbContext;
            this.dbSet     = dbSet;
        }

        protected PizzarioDbContext GetDBContext ()
        {
            return this.dbContext;
        }

        protected DbSet GetDBSet ()
        {
            return this.dbSet;
        }

        public void Add ( T obj )
        {
            dbSet.Add( obj );
        }

        public void Delete ( T obj )
        {
            dbSet.Remove( obj );
        }

        public void Commit ()
        {
            dbContext.ChangeTracker.DetectChanges();
            dbContext.SaveChanges();
        }

        public IQueryable< T > LoadAll ()
        {
            return dbSet;
        }

        public T Load ( int id )
        {
            return dbSet.Find( id );
        }

        public int Count ()
        {
            return dbSet.Count();
        }


        private PizzarioDbContext dbContext;
        private DbSet< T > dbSet;
    }
}
