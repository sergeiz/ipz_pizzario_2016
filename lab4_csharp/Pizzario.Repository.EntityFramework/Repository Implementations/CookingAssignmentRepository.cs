﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

namespace Pizzario.Repository.EntityFramework
{
    public class CookingAssignmentRepository : BasicRepository< CookingAssignment >, ICookingAssignmentRepository
    {
        public CookingAssignmentRepository ( PizzarioDbContext dbContext )
            :   base( dbContext, dbContext.CookingAssignments )
        {
        }
    }
}
