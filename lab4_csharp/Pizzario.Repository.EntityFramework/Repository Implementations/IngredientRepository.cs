﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

namespace Pizzario.Repository.EntityFramework
{
    public class IngredientRepository : BasicRepository< Ingredient >, IIngredientRepository
    {
        public IngredientRepository ( PizzarioDbContext dbContext )
            :   base( dbContext, dbContext.Ingredients )
        {
        }
    }
}
