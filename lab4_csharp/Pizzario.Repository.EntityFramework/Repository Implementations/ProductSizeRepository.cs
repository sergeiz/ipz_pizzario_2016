﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

namespace Pizzario.Repository.EntityFramework
{
    public class ProductSizeRepository : BasicRepository< ProductSize >, IProductSizeRepository
    {
        public ProductSizeRepository( PizzarioDbContext dbContext )
            :   base( dbContext, dbContext.ProductSizes )
        {
        }
    }
}
