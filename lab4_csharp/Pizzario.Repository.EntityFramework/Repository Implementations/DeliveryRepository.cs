﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

namespace Pizzario.Repository.EntityFramework
{
    public class DeliveryRepository : BasicRepository< Delivery >, IDeliveryRepository
    {
        public DeliveryRepository ( PizzarioDbContext dbContext )
            :   base( dbContext, dbContext.Deliveries )
        {
        }
    }
}
