﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

namespace Pizzario.Repository.EntityFramework
{
    public class ShoppingCartRepository : BasicRepository< ShoppingCart >, IShoppingCartRepository
    {
        public ShoppingCartRepository ( PizzarioDbContext dbContext )
            :   base( dbContext, dbContext.ShoppingCarts )
        {
        }
    }
}
