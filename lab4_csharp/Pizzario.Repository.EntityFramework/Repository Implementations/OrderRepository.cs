﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;


namespace Pizzario.Repository.EntityFramework
{
    public class OrderRepository : BasicRepository< Order >, IOrderRepository
    {
        public OrderRepository ( PizzarioDbContext dbContext )
            :   base( dbContext, dbContext.Orders )
        {
        }
    }
}
