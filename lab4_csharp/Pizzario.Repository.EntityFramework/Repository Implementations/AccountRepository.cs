﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

namespace Pizzario.Repository.EntityFramework
{
    public class AccountRepository : BasicRepository< Account >, IAccountRepository
    {
        public AccountRepository ( PizzarioDbContext dbContext )
            :   base( dbContext, dbContext.Accounts )
        {
        }
    }
}
