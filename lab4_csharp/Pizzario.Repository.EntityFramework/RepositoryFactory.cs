﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

namespace Pizzario.Repository.EntityFramework
{
    public static class RepositoryFactory
    {
        public static IProductSizeRepository MakeProductSizeRepository ( PizzarioDbContext dbContext )
        {
            return new ProductSizeRepository( dbContext );
        }

        public static IIngredientRepository MakeIngredientRepository ( PizzarioDbContext dbContext )
        {
            return new IngredientRepository( dbContext );
        }

        public static IProductRepository MakeProductRepository ( PizzarioDbContext dbContext )
        {
            return new ProductRepository( dbContext );
        }

        public static IShoppingCartRepository MakeShoppingCartRepository ( PizzarioDbContext dbContext )
        {
            return new ShoppingCartRepository( dbContext );
        }

        public static IOrderRepository MakeOrderRepository ( PizzarioDbContext dbContext )
        {
            return new OrderRepository( dbContext );
        }

        public static IAccountRepository MakeAccountRepository ( PizzarioDbContext dbContext )
        {
            return new AccountRepository( dbContext );
        }


        public static ICookingAssignmentRepository MakeCookingAssignmentRepository ( PizzarioDbContext dbContext )
        {
            return new CookingAssignmentRepository( dbContext );
        }

        public static IDeliveryRepository MakeDeliveryRepository ( PizzarioDbContext dbContext )
        {
            return new DeliveryRepository( dbContext );
        }
    }
}
