﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Utils;

using System.Linq;

namespace Pizzario.Repository
{
    public interface IRepository< T > where T : Utils.Entity
    {
        int Count ();

        T Load ( int id );

        IQueryable< T > LoadAll ();

        void Add ( T t );

        void Delete ( T t );

        void Commit ();
    }
}
