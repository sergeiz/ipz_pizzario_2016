﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

namespace Pizzario.Repository
{
    public interface IProductRepository : IRepository< Product >
    {
    }
}
