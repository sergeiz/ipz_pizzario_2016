/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.Value;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.util.Arrays;
import java.util.List;

@Embeddable
@Access( AccessType.PROPERTY )
public class Contact extends Value< Contact >
{

    protected Contact () {}

    public Contact ( String name, String address, String phone, String email )
    {
        setName( name );
        setAddress( address );
        setPhone( phone );
        setEmail( email );
    }

    public String getName () { return this.name; }

    public void setName ( String name ) { this.name = name; }

    public String getAddress () { return this.address; }

    private void setAddress ( String address )
    {
        this.address = address;
    }

    public String getPhone ()
    {
        return this.phone;
    }

    private void setPhone ( String phone )
    {
        this.phone = phone;
    }

    public String getEmail () { return this.email; }

    public void setEmail ( String email ) { this.email = email; }

    @Override
    @Transient
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList( getName(), getAddress(), getPhone(), getEmail() );
    }

    private String name;

    private String address;

    private String phone;

    private String email;
}
