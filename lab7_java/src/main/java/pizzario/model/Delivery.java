/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.exceptions.DeliveryLifecycleException;
import pizzario.exceptions.OrderLifecycleException;
import pizzario.utils.DomainEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Access( AccessType.PROPERTY )
public class Delivery extends DomainEntity
{

    protected Delivery () {}

    public Delivery ( UUID domainId, Order order )
    {
        super( domainId );

        setOrder( order );

        status = DeliveryStatus.Waiting;
    }

    public String getDriverName ()
    {
        return this.driverName;
    }

    protected void setDriverName ( String driverName )
    {
        this.driverName = driverName;
    }

    @ManyToOne
    public Order getOrder ()
    {
        return order;
    }

    private void setOrder ( Order order )
    {
        this.order = order;
    }

    @Transient
    public BigDecimal getCash2Collect ()
    {
        return getOrder().getTotalCost();
    }

    public DeliveryStatus getStatus ()
    {
        return status;
    }

    private void setStatus ( DeliveryStatus status )
    {
        this.status = status;
    }

    public void startDelivery ( String driverName ) throws DeliveryLifecycleException
    {
        if ( this.status != DeliveryStatus.Waiting )
            throw DeliveryLifecycleException.makeStartWhenNotWaiting( getDomainId() );

        this.driverName = driverName;
        this.status = DeliveryStatus.InProgress;
    }

    public void finishDelivery () throws DeliveryLifecycleException, OrderLifecycleException
    {
        if ( this.status != DeliveryStatus.InProgress )
            throw DeliveryLifecycleException.makeFinishWhenNotInProgress( getDomainId() );

        this.status = DeliveryStatus.Delivered;

        getOrder().deliveryCompleted();
    }

    private String driverName;

    private Order order;

    private DeliveryStatus status;
}
