/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service;

import org.hibernate.validator.constraints.NotBlank;
import pizzario.dto.IngredientDto;
import pizzario.exceptions.DuplicateNamedEntityException;

import javax.validation.constraints.NotNull;
import java.util.UUID;


public interface IIngredientService extends IDomainEntityService< IngredientDto > {

    UUID create ( @NotBlank String name ) throws DuplicateNamedEntityException;

    void rename (
            @NotNull UUID ingredientId,
            @NotBlank String newName
    ) throws DuplicateNamedEntityException;

}
