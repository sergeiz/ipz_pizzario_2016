/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;
import pizzario.dto.ProductDto;
import pizzario.dto.ProductPricingDto;
import pizzario.dto.ProductRecipeDto;
import pizzario.dto.ProductSizeDto;
import pizzario.exceptions.DuplicateNamedEntityException;
import pizzario.exceptions.PriceUndefinedException;
import pizzario.exceptions.RecipeUndefinedException;
import pizzario.exceptions.RemovingUnusedIngredientException;
import pizzario.service.validation.Price;
import pizzario.service.validation.Weight;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public interface IProductService extends IDomainEntityService< ProductDto >
{
    List< ProductSizeDto > availableSizes ( @NotNull UUID productId );

    List< ProductRecipeDto > availableRecipes ( @NotNull UUID productId );

    ProductPricingDto viewPrices ( @NotNull UUID productId );

    ProductRecipeDto viewRecipe (
            @NotNull UUID productId,
            @NotNull UUID sizeId
    ) throws RecipeUndefinedException;

    UUID create (
            @NotBlank String name,
            @NotBlank String imageUrl
    ) throws DuplicateNamedEntityException;

    void rename (
            @NotNull UUID productId,
            @NotBlank String newName
    ) throws DuplicateNamedEntityException;

    void updateImageUrl (
            @NotNull UUID productId,
            @URL String imageUrl
    );

    void definePrice (
            @NotNull UUID productId,
            @NotNull UUID sizeId,
            @Price BigDecimal price
    );

    void undefinePrice (
            @NotNull UUID productd,
            @NotNull UUID sizeId
    ) throws PriceUndefinedException;

    void defineRecipeIngredient (
            @NotNull UUID productId,
            @NotNull UUID sizeId,
            @NotNull UUID ingredientId,
            @Weight double weight
    );

    void removeRecipeIngredient (
            @NotNull UUID productId,
            @NotNull UUID sizeId,
            @NotNull UUID ingredientId
    ) throws RemovingUnusedIngredientException;
}
