/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.dto;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class OrderDto extends DomainEntityDto< OrderDto >
{

    public OrderDto (
            UUID domainId,
            String customerName,
            String customerAddress,
            String contactPhone,
            String contactEmail,
            LocalDateTime placementTime,
            String status,
            List< ProductItemDto > items,
            BigDecimal basicCost,
            BigDecimal totalCost,
            BigDecimal discountPercentage,
            String comment
    )
    {
        super( domainId );

        this.customerName = customerName;
        this.customerAddress = customerAddress;
        this.contactPhone = contactPhone;
        this.contactEmail = contactEmail;
        this.comment = comment;

        this.placementTime = placementTime;
        this.status = status;
        this.items = items;
        this.basicCost = basicCost;
        this.totalCost = totalCost;
        this.discountPercentage = discountPercentage;
    }

    public String getCustomerName () { return customerName; }

    public String getCustomerAddress ()
    {
        return customerAddress;
    }

    public String getContactPhone ()
    {
        return contactPhone;
    }

    public String getContactEmail () { return contactEmail; }

    public LocalDateTime getPlacementTime ()
    {
        return placementTime;
    }

    public String getStatus ()
    {
        return status;
    }

    public List< ProductItemDto > viewItems ()
    {
        return items;
    }

    public BigDecimal getBasicCost ()
    {
        return basicCost;
    }

    public BigDecimal getTotalCost ()
    {
        return totalCost;
    }

    public BigDecimal getDiscountPercentage ()
    {
        return discountPercentage;
    }

    public String getComment () { return comment; }

    @Override
    public String toString ()
    {
        StringBuilder itemsAsString = new StringBuilder();
        for ( ProductItemDto item : items )
        {
            itemsAsString.append( item );
            itemsAsString.append( "\n\n" );
        }

        return String.format(
                "ID = %s\nCustomer Name = %s\nAddress = %s\nPhone = %s\nEmail = %s\nContent:\n%sBasic cost = %s\nDiscount = %s\nTotal cost = %s\nStatus = %s\nPlaced = %s\nComment = %s\n",
                getDomainId(),
                getCustomerName(),
                getCustomerAddress(),
                getContactPhone(),
                getContactEmail(),
                itemsAsString.toString(),
                NumberFormat.getCurrencyInstance().format( getBasicCost() ),
                NumberFormat.getPercentInstance().format( getDiscountPercentage() ),
                NumberFormat.getCurrencyInstance().format( getTotalCost() ),
                getStatus(),
                getPlacementTime(),
                getComment()
        );
    }

    @Override
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        List< Object > list = Arrays.asList(
                getDomainId(),
                getCustomerName(),
                getCustomerAddress(),
                getContactPhone(),
                getContactEmail(),
                getPlacementTime(),
                getStatus(),
                getBasicCost(),
                getTotalCost(),
                getDiscountPercentage(),
                getComment()
        );
        list.addAll( items );
        return list;
    }

    private final String customerName;
    private final String customerAddress;
    private final String contactPhone;
    private final String contactEmail;

    private final LocalDateTime placementTime;
    private final String status;
    private final List< ProductItemDto > items;
    private final BigDecimal basicCost;
    private final BigDecimal totalCost;
    private final BigDecimal discountPercentage;
    private final String comment;
}
