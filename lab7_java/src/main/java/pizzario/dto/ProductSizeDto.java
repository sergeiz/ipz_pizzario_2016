/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.dto;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class ProductSizeDto extends DomainEntityDto< ProductSizeDto >
{

    public ProductSizeDto ( UUID domainId, String name, String imageUrl, int diameter, int weight )
    {
        super( domainId );

        this.name = name;
        this.imageUrl = imageUrl;
        this.diameter = diameter;
        this.weight = weight;
    }

    public String getName ()
    {
        return name;
    }

    public String getImageUrl () { return imageUrl; }

    public int getDiameter ()
    {
        return diameter;
    }

    public int getWeight ()
    {
        return weight;
    }

    @Override
    public String toString ()
    {
        return String.format(
                "ID = %s\nName = %s\nImage = %s\nDiameter = %d\nWeight = %d",
                getDomainId(),
                getName(),
                getImageUrl(),
                getDiameter(),
                getWeight()
        );
    }

    @Override
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList(
                getDomainId(),
                getName(),
                getImageUrl(),
                getDiameter(),
                getWeight()
        );
    }

    private final String name;
    private final String imageUrl;
    private final int diameter;
    private final int weight;
}
