/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.dto;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class IngredientDto extends DomainEntityDto< IngredientDto >
{

    public IngredientDto ( UUID domainId, String name )
    {
        super( domainId );
        this.name = name;
    }

    public String getName ()
    {
        return name;
    }

    @Override
    public String toString ()
    {
        return String.format(
                "ID = %s\nName = %s",
                getDomainId(),
                getName()
        );
    }

    @Override
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList( getDomainId(), getName() );
    }

    private final String name;

}
