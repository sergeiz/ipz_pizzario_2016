/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.dto;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class ShoppingCartDto extends DomainEntityDto< ShoppingCartDto >
{

    public ShoppingCartDto (
            UUID domainId,
            List< ProductItemDto > items,
            BigDecimal totalCost,
            boolean locked
    )
    {
        super( domainId );

        this.items = items;
        this.totalCost = totalCost;
        this.locked = locked;
    }

    public List< ProductItemDto > getItems ()
    {
        return items;
    }

    public BigDecimal getTotalCost () { return totalCost; }

    public boolean isLocked ()
    {
        return locked;
    }

    @Override
    public String toString ()
    {
        StringBuilder b = new StringBuilder();

        b.append( String.format( "Id = %s\n", getDomainId() ) );

        b.append( "Content:\n" );
        for ( ProductItemDto item : items )
        {
            b.append( item );
            b.append( "\n\n" );
        }

        b.append( String.format( "Total = %s\n", NumberFormat.getCurrencyInstance().format( getTotalCost() ) ) );
        b.append( String.format( "Locked = %s\n", isLocked() ) );

        return b.toString();
    }

    @Override
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        List< Object > list = Arrays.asList( getDomainId(), isLocked(), getTotalCost() );
        list.addAll( items );
        return list;
    }

    private List< ProductItemDto > items;
    private BigDecimal totalCost;
    private boolean locked;

}
