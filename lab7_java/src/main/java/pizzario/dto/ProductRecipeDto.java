/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.dto;

import pizzario.utils.Value;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ProductRecipeDto extends Value< ProductRecipeDto >
{

    public ProductRecipeDto (
            UUID productId,
            UUID sizeId,
            Map< IngredientDto, Double > usedIngredients
    )
    {
        this.productId = productId;
        this.sizeId = sizeId;
        this.usedIngredients = usedIngredients;
    }

    public UUID getProductId ()
    {
        return productId;
    }

    public UUID getSizedId ()
    {
        return sizeId;
    }

    public Map< IngredientDto, Double > getUsedIngredients ()
    {
        return usedIngredients;
    }

    @Override
    public String toString ()
    {
        StringBuilder ingredientsAsString = new StringBuilder();
        for ( Map.Entry< IngredientDto, Double > entry : usedIngredients.entrySet() )
            ingredientsAsString.append(
                    String.format( "\t\t%s (%fg)\n", entry.getKey().getName(), entry.getValue() )
            );

        return String.format(
                "Recipe:\n\t\tIngredients:\n%s",
                ingredientsAsString
        );
    }

    @Override
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        List< Object > list = Arrays.asList( getProductId(), getSizedId() );
        usedIngredients.entrySet().stream().forEach(
                entry ->
                {
                    list.add( entry.getKey() );
                    list.add( entry.getValue() );
                }
        );
        return list;
    }


    private UUID productId;
    private UUID sizeId;
    private Map< IngredientDto, Double > usedIngredients;

}
