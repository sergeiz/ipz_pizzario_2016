/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.dto;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class ProductDto extends DomainEntityDto< ProductDto >
{

    public ProductDto ( UUID domainId, String name, String imageUrl, String description )
    {
        super( domainId );

        this.name = name;
        this.imageUrl = imageUrl;
        this.description = description;
    }

    public String getName ()
    {
        return name;
    }

    public String getImageUrl ()
    {
        return imageUrl;
    }

    public String getDescription () { return description; }


    @Override
    public String toString ()
    {
        return String.format(
                "ID = %s\nName = %s\nImageUrl = %s\nDescription = %s",
                getDomainId(),
                getName(),
                getImageUrl(),
                getDescription()
        );
    }

    @Override
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList( getDomainId(), getName(), getImageUrl(), getDescription() );
    }

    private final String name;
    private final String imageUrl;
    private final String description;
}
