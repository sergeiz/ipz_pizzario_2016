/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.dto;

import pizzario.utils.Value;

import java.util.UUID;


public abstract class DomainEntityDto< TConcreteDto extends Value< TConcreteDto > >
        extends Value< TConcreteDto >
{
    protected DomainEntityDto ( UUID domainId )
    {
        this.domainId = domainId;
    }

    public UUID getDomainId ()
    {
        return domainId;
    }

    private final UUID domainId;
}
