/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.exceptions;

import java.util.UUID;

public class RemovingUnexistingCartItemException extends DomainLogicException
{
    public RemovingUnexistingCartItemException ( UUID cartId, String productName, String sizeName )
    {
        super( String.format(
                "Removing item \"%s\" (%s) that does not exist in cart #%s",
                productName,
                sizeName,
                cartId ) );
    }
}
