/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.exceptions;

public class RemovingUnusedIngredientException extends DomainLogicException
{
    public RemovingUnusedIngredientException ( String productName, String ingredientName )
    {
        super( String.format(
                "Removing unregistered ingredient \"%s\" from product \"%s\"",
                ingredientName,
                productName ) );
    }
}
