/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.exceptions;

import java.util.UUID;

public class DeliveryLifecycleException extends DomainLogicException
{
    private DeliveryLifecycleException ( String message )
    {
        super( message );
    }

    public static DeliveryLifecycleException makeStartWhenNotWaiting ( UUID deliveryId )
    {
        return new DeliveryLifecycleException(
                String.format(
                        "Delivery #%s may be started in Waiting state only",
                        deliveryId
                )
        );
    }

    public static DeliveryLifecycleException makeFinishWhenNotInProgress ( UUID deliveryId )
    {
        return new DeliveryLifecycleException(
                String.format(
                        "Delivery #%s may be finished in InProgress state only",
                        deliveryId
                )
        );
    }
}
