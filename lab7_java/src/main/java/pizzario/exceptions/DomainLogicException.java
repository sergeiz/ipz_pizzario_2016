/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.exceptions;

public class DomainLogicException extends Exception
{
    public DomainLogicException ( String message )
    {
        super( message );
    }
}
