/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import org.springframework.stereotype.Repository;
import pizzario.model.accounts.Account;
import pizzario.model.accounts.OperatorAccount;
import pizzario.repository.IAccountRepository;

import javax.persistence.TypedQuery;
import java.util.List;
import java.util.UUID;


@Repository
class AccountRepository
        extends BasicRepository< Account >
        implements IAccountRepository
{
    public AccountRepository () {
        super( Account.class );
    }


    @Override
    public Account findByEmail ( String email ) 
    {
        TypedQuery< Account > queryByEmail = getEntityManager().createQuery(
                "SELECT a " +
                        "FROM Account a " +
                        "WHERE a.email = :email",

                Account.class
        );

        queryByEmail.setParameter( "email", email );
        List< Account > results = queryByEmail.getResultList();
        return ( results.size() == 1 ) ? results.get( 0 ) : null;
    }


    @Override
    public OperatorAccount findOperatorByDomainId ( UUID operatorId )
    {
        Account account = findByDomainId( operatorId );
        if ( account == null || !( account instanceof OperatorAccount ) )
            return null;

        return ( OperatorAccount ) account;
    }
}
