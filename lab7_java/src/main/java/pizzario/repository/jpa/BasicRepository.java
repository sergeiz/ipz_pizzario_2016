/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import pizzario.repository.IRepository;
import pizzario.utils.DomainEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.UUID;


abstract class BasicRepository< T extends DomainEntity> implements IRepository< T >
{


    protected BasicRepository ( Class< T > theClass ) {
        this.theClass = theClass;
    }


    @Override
    public Class< T > getEntityClass ()
    {
        return theClass;
    }


    @Override
    public void add ( T obj )
    {
        this.entityManager.persist( obj );
    }


    @Override
    public void delete ( T obj )
    {
        this.entityManager.remove( obj );
    }


    @Override
    public void delete ( long id ) {
        CriteriaBuilder cb  = entityManager.getCriteriaBuilder();
        CriteriaDelete< T > query = cb.createCriteriaDelete( theClass );
        Root< T > root = query.from( theClass );
        query.where(
                cb.equal(
                        root.get( "databaseId" ),
                        cb.parameter( Long.class, "databaseId")
                )
        );

        entityManager.createQuery( query )
                .setParameter( "databaseId", id )
                .executeUpdate();
    }


    @Override
    public List< T > loadAll ()
    {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery< T > criteria = cb.createQuery( theClass );
        criteria.select( criteria.from( theClass ) );
        return entityManager.createQuery( criteria ).getResultList();
    }


    @Override
    public T load ( long id )
    {
        return entityManager.find( theClass, id );
    }


    @Override
    public T findByDomainId ( UUID domainId ) {

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteria = cb.createQuery(theClass);
        Root<T> root = criteria.from(theClass);
        criteria.select(root)
                .where(
                        cb.equal(
                                root.get("domainId"),
                                cb.parameter(UUID.class, "domainId")
                        )
                );

        TypedQuery< T > queryByDomainId = entityManager.createQuery(criteria);
        queryByDomainId.setParameter( "domainId", domainId );
        List< T > results = queryByDomainId.getResultList();
        return ( results.size() == 1 ) ? results.get( 0 ) : null;
    }


    @Override
    public List< UUID > selectAllDomainIds () {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery< UUID > criteria = cb.createQuery( UUID.class );
        Root< T > root = criteria.from( theClass );
        criteria.select( root.get( "domainId" ) );
        return entityManager.createQuery( criteria ).getResultList();
    }


    protected EntityManager getEntityManager ()
    {
        return this.entityManager;
    }


    @PersistenceContext
    private EntityManager entityManager;

    private Class< T > theClass;
}
