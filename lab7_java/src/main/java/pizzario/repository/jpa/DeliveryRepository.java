/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import org.springframework.stereotype.Repository;
import pizzario.model.Delivery;
import pizzario.model.Order;
import pizzario.repository.IDeliveryRepository;

import javax.persistence.TypedQuery;
import java.util.List;
import java.util.UUID;


@Repository
class DeliveryRepository
        extends BasicRepository< Delivery >
        implements IDeliveryRepository
{
    public DeliveryRepository () {
        super( Delivery.class );
    }


    @Override
    public List< UUID > selectWaitingIds() {
        return getEntityManager().createQuery(
                "SELECT d.domainId " +
                        "FROM Delivery d " +
                        "WHERE d.status = pizzario.model.DeliveryStatus.Waiting",

                UUID.class
        ).getResultList();
    }


    @Override
    public List< UUID > selectInProgressIds() {
        return getEntityManager().createQuery(
                "SELECT d.domainId " +
                        "FROM Delivery d " +
                        "WHERE d.status = pizzario.model.DeliveryStatus.InProgress",

                UUID.class
        ).getResultList();
    }


    @Override
    public Delivery findByOrder ( Order o ) {
        TypedQuery< Delivery > queryByOrder = getEntityManager().createQuery(
                "SELECT d " +
                        "FROM Delivery d " +
                        "WHERE d.order.databaseId = :orderId",

                getEntityClass()
        );

        queryByOrder.setParameter( "orderId", o.getDatabaseId() );
        List< Delivery > results = queryByOrder.getResultList();
        return ( results.size() == 1 ) ? results.get( 0 ) : null;
    }
}
