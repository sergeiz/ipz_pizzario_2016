/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import org.springframework.stereotype.Repository;
import pizzario.model.Ingredient;
import pizzario.repository.IIngredientRepository;


@Repository
class IngredientRepository
        extends NamedObjectRepository< Ingredient >
        implements IIngredientRepository
{
    public IngredientRepository () {
        super( Ingredient.class );
    }
}
