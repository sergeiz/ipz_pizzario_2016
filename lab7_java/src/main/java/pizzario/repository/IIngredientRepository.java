/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository;

import pizzario.model.Ingredient;

public interface IIngredientRepository extends IRepository< Ingredient >
{
    Ingredient findByName ( String name );
}
