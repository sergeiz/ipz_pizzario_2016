/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.web;

import pizzario.dto.OrderDto;
import pizzario.dto.ShoppingCartDto;
import pizzario.service.IOrderService;
import pizzario.service.IShoppingCartService;
import pizzario.web.constants.SessionKeys;

import javax.servlet.http.HttpSession;
import java.util.UUID;

public final class SessionDataProvider
{
    public static ShoppingCartDto currentCart ( IShoppingCartService service, HttpSession session )
    {
        UUID cartId = currentCartId( service, session );
        return service.view( cartId );
    }

    public static UUID currentCartId ( IShoppingCartService service, HttpSession session )
    {
        UUID cartId = ( UUID ) session.getAttribute( SessionKeys.CART_ID );
        if ( cartId == null )
        {
            cartId = service.createNew();
            session.setAttribute( SessionKeys.CART_ID, cartId );
        }

        return cartId;
    }

    public static void resetCurrentCartId ( HttpSession session )
    {
        session.removeAttribute( SessionKeys.CART_ID );
    }

    public static OrderDto currentOrder ( IOrderService service, HttpSession session )
    {
        UUID orderId = currentOrderId( service, session );
        if ( orderId != null )
            return service.view( orderId );

        else
            return null;
    }

    public static UUID currentOrderId ( IOrderService service, HttpSession session )
    {
        UUID orderId = ( UUID ) session.getAttribute( SessionKeys.ORDER_ID );
        return orderId;
    }

    public static void setCurrentOrderId ( HttpSession session, UUID orderId )
    {
        session.setAttribute( SessionKeys.ORDER_ID, orderId );
    }
}
