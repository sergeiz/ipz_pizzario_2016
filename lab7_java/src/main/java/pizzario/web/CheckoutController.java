/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.web;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;
import pizzario.dto.ShoppingCartDto;
import pizzario.exceptions.DomainLogicException;
import pizzario.service.IOrderService;
import pizzario.service.IShoppingCartService;
import pizzario.web.constants.AttributeNames;
import pizzario.web.constants.ViewNames;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping( "/checkout" )
public class CheckoutController
{
    @RequestMapping( path="/", method = RequestMethod.GET )
    public String startCheckout ( HttpSession session, Map< String, Object > model )
    {
        ShoppingCartDto cartDto = SessionDataProvider.currentCart( cartService, session );
        model.put( AttributeNames.CheckoutView.Cart, cartDto );
        return ViewNames.Checkout;
    }


    @RequestMapping( path="/", method = RequestMethod.POST )
    public View submitCheckout ( HttpSession session,
                                 Map< String, Object > model,
                                 @RequestParam String inputName,
                                 @RequestParam String inputAddress,
                                 @RequestParam String inputEmail,
                                 @RequestParam String inputPhone,
                                 @RequestParam String inputComment )

            throws DomainLogicException
    {
        ShoppingCartDto cartDto = SessionDataProvider.currentCart( cartService, session );
        if ( cartDto.getItems().isEmpty() )
            throw new HttpClientErrorException( HttpStatus.BAD_REQUEST );

        cartService.lock( cartDto.getDomainId() );

        UUID orderId = orderService.createNew(
                            inputName,
                            inputAddress,
                            inputPhone,
                            inputEmail,
                            inputComment,
                            cartDto.getDomainId()
                        );

        SessionDataProvider.setCurrentOrderId( session, orderId );
        SessionDataProvider.resetCurrentCartId( session );

        return new RedirectView( "/order/" );
    }


    @Inject
    private IShoppingCartService cartService;

    @Inject
    private IOrderService orderService;
}
