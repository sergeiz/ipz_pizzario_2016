/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pizzario.exceptions.DomainLogicException;
import pizzario.service.IShoppingCartService;
import pizzario.web.constants.AttributeNames;
import pizzario.web.constants.ViewNames;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.UUID;


@Controller
@RequestMapping( "/cart" )
public class CartController
{
    @RequestMapping( value="/view", method = RequestMethod.GET )
    public String viewCart ( HttpSession session, Map< String, Object > model )
    {
        model.put(
                AttributeNames.CartView.Cart,
                SessionDataProvider.currentCart( cartService, session )
        );

        return ViewNames.Cart;
    }


    @RequestMapping( value = "/additem", method = RequestMethod.POST )
    public ResponseEntity addItem (
            HttpSession session,
            @RequestParam UUID productId,
            @RequestParam UUID sizeId
    )
            throws DomainLogicException
    {
        UUID cartId = SessionDataProvider.currentCartId( cartService, session );

        cartService.addItem( cartId, productId, sizeId );
        return new ResponseEntity( HttpStatus.CREATED );
    }


    @RequestMapping( value = "/setitem", method = RequestMethod.POST )
    public ResponseEntity setItem (
            HttpSession session,
            @RequestParam UUID productId,
            @RequestParam UUID sizeId,
            @RequestParam int newQuantity
    )
            throws DomainLogicException
    {
        UUID cartId = SessionDataProvider.currentCartId( cartService, session );

        cartService.setItem( cartId, productId, sizeId, newQuantity );
        return new ResponseEntity( HttpStatus.OK );
    }


    @RequestMapping( value = "/removeitem", method = RequestMethod.POST )
    public ResponseEntity removeItem (
            HttpSession session,
            @RequestParam UUID productId,
            @RequestParam UUID sizeId
    )
            throws DomainLogicException
    {
        UUID cartId = SessionDataProvider.currentCartId( cartService, session );

        cartService.removeItem( cartId, productId, sizeId);
        return new ResponseEntity( HttpStatus.OK );
    }


    @RequestMapping( value = "/clear", method = RequestMethod.POST )
    public ResponseEntity clear ( HttpSession session )
            throws DomainLogicException
    {
        UUID cartId = SessionDataProvider.currentCartId( cartService, session );

        cartService.clearItems( cartId );
        return new ResponseEntity( HttpStatus.OK );
    }


    @Inject
    private IShoppingCartService cartService;
}
