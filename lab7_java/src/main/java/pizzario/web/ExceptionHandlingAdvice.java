/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.web;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import pizzario.exceptions.ApplicationFatalError;
import pizzario.exceptions.DomainLogicException;
import pizzario.exceptions.ServiceValidationException;
import pizzario.web.constants.AttributeNames;
import pizzario.web.constants.ErrorKeys;
import pizzario.web.constants.ViewNames;


@ControllerAdvice
public class ExceptionHandlingAdvice
{
    @ExceptionHandler( ApplicationFatalError.class )
    public ModelAndView handle ( ApplicationFatalError e )
    {
        ModelAndView mav = new ModelAndView( ViewNames.Error );
        mav.addObject( AttributeNames.ErrorView.Title,      ErrorKeys.ServerFatalErrorTitle );
        mav.addObject( AttributeNames.ErrorView.Message,    ErrorKeys.ServerFatalErrorMessage );
        mav.addObject( AttributeNames.ErrorView.Arguments,  "" );

        return mav;
    }

    @ExceptionHandler( DomainLogicException.class )
    public ModelAndView handle ( DomainLogicException e )
    {
        ModelAndView mav = new ModelAndView( ViewNames.Error );
        mav.addObject( AttributeNames.ErrorView.Title,      ErrorKeys.DomainLogicErrorTitle );
        mav.addObject( AttributeNames.ErrorView.Message,    ErrorKeys.DomainLogicErrorMessage );
        mav.addObject( AttributeNames.ErrorView.Arguments,  e.getMessage() );

        return mav;
    }

    @ExceptionHandler( ServiceValidationException.class )
    public ModelAndView handle ( ServiceValidationException e )
    {
        ModelAndView mav = new ModelAndView( ViewNames.Error );
        mav.addObject( AttributeNames.ErrorView.Title,      ErrorKeys.ValidationErrorTitle );
        mav.addObject( AttributeNames.ErrorView.Message,    ErrorKeys.ValidationErrorMessage );
        mav.addObject( AttributeNames.ErrorView.Arguments,  e.getMessage() );

        return mav;
    }

    @ExceptionHandler( NoHandlerFoundException.class )
    public ModelAndView handle ( NoHandlerFoundException e )  {
        ModelAndView mav = new ModelAndView( ViewNames.Error );
        mav.addObject( AttributeNames.ErrorView.Title,      ErrorKeys.NoHandlerFoundTitle );
        mav.addObject( AttributeNames.ErrorView.Message,    ErrorKeys.NoHandlerFoundMessage );
        mav.addObject( AttributeNames.ErrorView.Arguments,  e.getMessage() );
        return mav;
    }
}