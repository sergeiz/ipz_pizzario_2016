package pizzario.web.constants;

public final class ViewNames
{
    public static final String Cart     = "cart";
    public static final String Checkout = "checkout";
    public static final String Menu     = "menu";
    public static final String Order    = "order";
    public static final String Error    = "error";
}