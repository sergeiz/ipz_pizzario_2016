package pizzario.web.constants;

public final class AttributeNames
{
    public static final class CartView
    {
        public static final String Cart = "cart";
    }

    public static final class CheckoutView
    {
        public static final String Cart = "cart";
    }

    public static final class MenuView
    {
        public static final String Products = "products";
        public static final String Sizes = "sizes";
        public static final String ProductPrices = "product_prices";
    }

    public static final class OrderView
    {
        public static final String OrderId  = "orderId";
        public static final String Status   = "status";
        public static final String Placed   = "placed";
        public static final String Name     = "name";
        public static final String Address  = "address";
        public static final String Email    = "email";
        public static final String Phone    = "phone";
        public static final String Discount = "discount";
        public static final String Cost     = "cost";
        public static final String Comment  = "comment";
    }

    public static final class ErrorView
    {
        public static final String Title = "errorTitle";
        public static final String Message = "errorMessage";
        public static final String Arguments = "errorArguments";
    }
}
