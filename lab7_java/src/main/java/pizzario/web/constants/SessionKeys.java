package pizzario.web.constants;

import org.springframework.web.servlet.i18n.SessionLocaleResolver;

public final class SessionKeys
{
    public static final String ORDER_ID = "ORDER_ID";
    public static final String CART_ID  = "CART_ID";
    public static final String LOCALE = SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME;
}
