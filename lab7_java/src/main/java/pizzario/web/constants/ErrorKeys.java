package pizzario.web.constants;

public final class ErrorKeys
{
    public static final String OrderNotFoundTitle = "error.ordernotfound.title";
    public static final String OrderNotFoundMessage = "error.ordernotfound.message";

    public static final String OrderAccessViolationTitle = "error.orderaccessviolation.title";
    public static final String OrderAccessViolationMessage = "error.orderaccessviolation.message";

    public static final String NoOrderTitle = "error.noorder.title";
    public static final String NoOrderMessage = "error.noorder.message";

    public static final String ServerFatalErrorTitle = "error.fatal.title";
    public static final String ServerFatalErrorMessage = "error.fatal.message";

    public static final String DomainLogicErrorTitle = "error.domain.title";
    public static final String DomainLogicErrorMessage = "error.domain.message";

    public static final String ValidationErrorTitle = "error.validation.title";
    public static final String ValidationErrorMessage = "error.validation.message";

    public static final String NoHandlerFoundTitle = "error.nohandler.title";
    public static final String NoHandlerFoundMessage = "error.nohandler.message";
}
