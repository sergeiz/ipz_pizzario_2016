/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.testapp;

import pizzario.dto.DomainEntityDto;
import pizzario.dto.ProductRecipeDto;
import pizzario.service.*;

import java.io.PrintStream;
import java.util.UUID;


public class ModelReporter
{


    public ModelReporter ( PrintStream output ) {
        this.output = output;
    }


    public void reportIngredients ( IIngredientService ingredientService )
    {
        reportCollection( "Ingredients", ingredientService );
    }


    public void reportSizes ( IProductSizeService sizeService )
    {
        reportCollection( "Sizes", sizeService );
    }


    public void reportProducts ( IProductService productService, IProductSizeService sizeService ) {
        output.format("==== %s ====\n\n", "Products" );

        for ( UUID productId : productService.viewAll() )
        {
            output.println( productService.view( productId ) );
            output.println( productService.viewPrices( productId ) );
            output.print( "\n" );

            for ( ProductRecipeDto recipeView : productService.availableRecipes( productId ) )
            {
                output.format( "%s\n\t", sizeService.view( recipeView.getSizedId() ) );
                output.println( recipeView );
            }
            output.print( "\n\n" );
        }

        output.println();
    }


    public void reportCarts ( IShoppingCartService cartService )
    {
        reportCollection( "Carts", cartService );
    }


    public void reportOrders ( IOrderService orderService )
    {
        reportCollection( "Orders", orderService );
    }


    public void reportAccounts ( IAccountService accountService )
    {
        reportCollection( "Accounts", accountService );
    }


    public void reportCookings ( ICookingAssignmentService cookingService )
    {
        reportCollection( "Cookings", cookingService );
    }


    public void reportDeliveries ( IDeliveryService deliveryService )
    {
        reportCollection( "Deliveries", deliveryService );
    }


    private < TDto extends DomainEntityDto >
    void reportCollection ( String title, IDomainEntityService< TDto > service )
    {
        output.format("==== %s ====\n\n", title );

        for ( UUID itemUuid : service.viewAll() )
        {
            output.print( service.view( itemUuid ) );
            output.print( "\n\n" );
        }

        output.println();
    }


    private PrintStream output;

}
