/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.testapp;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pizzario.configuration.TestAppGenerateConfiguration;
import pizzario.configuration.TestAppReportConfiguration;


public class Program {

    public static void main ( String[] args )
    {
        try
        {
            try ( ConfigurableApplicationContext generateContext = initGenerateContext() )
            {
                GeneratorController generatorController = generateContext.getBean( GeneratorController.class );
                generatorController.run();
            }

            try ( ConfigurableApplicationContext reportContext = initReportContext() ) {

                ReportController reportController = reportContext.getBean( ReportController.class );
                reportController.run();
            }
        }

        catch ( Exception e )
        {
            System.out.println( e.getClass().getName() );
            System.out.println( e.getMessage() );
        }
    }


    private static ConfigurableApplicationContext initGenerateContext () {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(
                        TestAppGenerateConfiguration.class
                );

        return context;
    }


    private static ConfigurableApplicationContext initReportContext () {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(
                        TestAppReportConfiguration.class
                );

        return context;
    }
}
