/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.testapp;

import org.springframework.stereotype.Component;
import pizzario.service.*;

import javax.inject.Inject;

@Component
public class ReportController
{

    public void run ()
    {
        ModelReporter reporter = new ModelReporter( System.out );
        reporter.reportIngredients( ingredientService );
        reporter.reportSizes( productSizeService );
        reporter.reportProducts( productService, productSizeService );
        reporter.reportCarts( shoppingCartService );
        reporter.reportOrders( orderService );
        reporter.reportAccounts( accountService );
        reporter.reportCookings( cookingAssignmentService );
        reporter.reportDeliveries( deliveryService );
    }

    @Inject
    private IIngredientService ingredientService;

    @Inject
    private IProductSizeService productSizeService;

    @Inject
    private IProductService productService;

    @Inject
    private IShoppingCartService shoppingCartService;

    @Inject
    private IOrderService orderService;

    @Inject
    private IAccountService accountService;

    @Inject
    private ICookingAssignmentService cookingAssignmentService;

    @Inject
    private IDeliveryService deliveryService;
}
