/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.testapp;

import pizzario.exceptions.DomainLogicException;
import pizzario.service.*;

import java.math.BigDecimal;
import java.util.UUID;


public class TestModelGenerator {


    public void generateSizes ( IProductSizeService service )
            throws DomainLogicException
    {
        Small  = service.create( "Small",  "/resources/images/pizza_size_small.png",  25, 300 );
        Medium = service.create( "Medium", "/resources/images/pizza_size_medium.png", 30, 600 );
        Large  = service.create( "Large",  "/resources/images/pizza_size_large.png",  35, 900 );
    }


    public void generateIngredients ( IIngredientService service )
            throws DomainLogicException
    {
        // ----

        Alfredo                 = service.create( "Alfredo" );
        Bacon                   = service.create( "Bacon" );
        Chicken                 = service.create( "Chicken" );
        ChickenGrill            = service.create( "Chicken Grill" );
        ChickenSmoked           = service.create( "Chicken Smoked" );
        Curry                   = service.create( "Curry" );
        Eggplant                = service.create( "Eggplant" );
        Ham                     = service.create( "Ham" );
        Jalapeno                = service.create( "Jalapeno" );
        Kebab                   = service.create( "Kebab" );
        Mozzarella              = service.create( "Mozzarella" );
        Mushroom                = service.create( "Mushroom" );
        Mussel                  = service.create( "Mussel" );
        Olive                   = service.create( "Olive" );
        Onion                   = service.create( "Onion" );
        Paprika                 = service.create( "Paprika" );
        Parmesan                = service.create( "Parmesan" );
        Parsley                 = service.create( "Parsley" );
        Pepperoni               = service.create( "Pepperoni" );
        PickledCucumber         = service.create( "Pickled Cucumber" );
        Pineapple               = service.create( "Pineapple" );
        Saliami                 = service.create( "Saliami" );
        Shrimp                  = service.create( "Shrimp" );
        Squid                   = service.create( "Squid" );
        Tomato                  = service.create( "Tomato" );
    }


    public void generateProducts ( IProductService service )
            throws DomainLogicException
    {
        // ----

        Mafia = service.create( "Mafia", "/resources/images/pizza_mafia.png" );

        service.defineRecipeIngredient( Mafia, Small,  Mozzarella, 25.0 );
        service.defineRecipeIngredient( Mafia, Small,  Pineapple,  25.0 );
        service.defineRecipeIngredient( Mafia, Small,  Chicken,    25.0 );
        service.defineRecipeIngredient( Mafia, Small,  Saliami,    25.0 );
        service.defineRecipeIngredient( Mafia, Small,  Tomato,     25.0 );

        service.defineRecipeIngredient( Mafia, Medium, Mozzarella, 50.0 );
        service.defineRecipeIngredient( Mafia, Medium, Pineapple,  50.0 );
        service.defineRecipeIngredient( Mafia, Medium, Chicken,    50.0 );
        service.defineRecipeIngredient( Mafia, Medium, Saliami,    50.0 );
        service.defineRecipeIngredient( Mafia, Medium, Tomato,     50.0 );

        service.defineRecipeIngredient( Mafia, Large,  Mozzarella, 75.0 );
        service.defineRecipeIngredient( Mafia, Large,  Pineapple,  75.0 );
        service.defineRecipeIngredient( Mafia, Large,  Chicken,    75.0 );
        service.defineRecipeIngredient( Mafia, Large,  Saliami,    75.0 );
        service.defineRecipeIngredient( Mafia, Large,  Tomato,     75.0 );

        service.definePrice( Mafia, Small,  BigDecimal.valueOf(  73.00 ) );
        service.definePrice( Mafia, Medium, BigDecimal.valueOf(  99.00 ) );
        service.definePrice( Mafia, Large,  BigDecimal.valueOf( 125.00 ) );

        // ----

        Georgia = service.create( "Georgia", "/resources/images/pizza_georgia.jpg" );

        service.defineRecipeIngredient( Georgia, Small, Kebab,      25.0 );
        service.defineRecipeIngredient( Georgia, Small, Mozzarella, 25.0 );
        service.defineRecipeIngredient( Georgia, Small, Eggplant,   25.0 );
        service.defineRecipeIngredient( Georgia, Small, Onion,      25.0 );
        service.defineRecipeIngredient( Georgia, Small, Parsley,    10.0 );
        service.defineRecipeIngredient( Georgia, Small, Tomato,     25.0 );

        service.defineRecipeIngredient( Georgia, Medium, Kebab,      50.0 );
        service.defineRecipeIngredient( Georgia, Medium, Mozzarella, 50.0 );
        service.defineRecipeIngredient( Georgia, Medium, Eggplant,   50.0 );
        service.defineRecipeIngredient( Georgia, Medium, Onion,      50.0 );
        service.defineRecipeIngredient( Georgia, Medium, Parsley,    20.0 );
        service.defineRecipeIngredient( Georgia, Medium, Tomato,     50.0 );

        service.defineRecipeIngredient( Georgia, Large,  Kebab,      75.0 );
        service.defineRecipeIngredient( Georgia, Large,  Mozzarella, 75.0 );
        service.defineRecipeIngredient( Georgia, Large,  Eggplant,   75.0 );
        service.defineRecipeIngredient( Georgia, Large,  Onion,      75.0 );
        service.defineRecipeIngredient( Georgia, Large,  Parsley,    75.0 );
        service.defineRecipeIngredient( Georgia, Large,  Tomato,     75.0 );

        service.definePrice( Georgia, Small,  BigDecimal.valueOf(  81.00 ) );
        service.definePrice( Georgia, Medium, BigDecimal.valueOf( 103.00 ) );
        service.definePrice( Georgia, Large,  BigDecimal.valueOf( 137.00 ) );

        // ----

        Cossack = service.create( "Cossack", "/resources/images/pizza_cossack.jpg" );

        service.defineRecipeIngredient( Cossack, Small,  Mozzarella,      25.0 );
        service.defineRecipeIngredient( Cossack, Small,  Bacon,           25.0 );
        service.defineRecipeIngredient( Cossack, Small,  Ham,             25.0 );
        service.defineRecipeIngredient( Cossack, Small,  Onion,           25.0 );
        service.defineRecipeIngredient( Cossack, Small,  PickledCucumber, 25.0 );
        service.defineRecipeIngredient( Cossack, Small,  Mushroom,        25.0 );

        service.defineRecipeIngredient( Cossack, Medium, Mozzarella,      50.0 );
        service.defineRecipeIngredient( Cossack, Medium, Bacon,           50.0 );
        service.defineRecipeIngredient( Cossack, Medium, Ham,             50.0 );
        service.defineRecipeIngredient( Cossack, Medium, Onion,           50.0 );
        service.defineRecipeIngredient( Cossack, Medium, PickledCucumber, 50.0 );
        service.defineRecipeIngredient( Cossack, Medium, Mushroom,        50.0 );;

        service.defineRecipeIngredient( Cossack, Large,  Mozzarella,      75.0 );
        service.defineRecipeIngredient( Cossack, Large,  Bacon,           75.0 );
        service.defineRecipeIngredient( Cossack, Large,  Ham,             75.0 );
        service.defineRecipeIngredient( Cossack, Large,  Onion,           75.0 );
        service.defineRecipeIngredient( Cossack, Large,  PickledCucumber, 75.0 );
        service.defineRecipeIngredient( Cossack, Large,  Mushroom,        75.0 );

        service.definePrice( Cossack, Small,  BigDecimal.valueOf(  83.00 ) );
        service.definePrice( Cossack, Medium, BigDecimal.valueOf( 107.00 ) );
        service.definePrice( Cossack, Large,  BigDecimal.valueOf( 143.00 ) );

        // ----

        Hawaii = service.create( "Hawaii", "/resources/images/pizza_hawaii.jpg" );

        service.defineRecipeIngredient( Hawaii, Small,   Curry,          25.0 );
        service.defineRecipeIngredient( Hawaii, Small,   Mozzarella,     25.0 );
        service.defineRecipeIngredient( Hawaii, Small,   Pineapple,      25.0 );
        service.defineRecipeIngredient( Hawaii, Small,   Ham,            25.0 );
        service.defineRecipeIngredient( Hawaii, Small,   ChickenGrill,   25.0 );
        service.defineRecipeIngredient( Hawaii, Small,   Tomato,         25.0 );

        service.defineRecipeIngredient( Hawaii, Medium,  Curry,          50.0 );
        service.defineRecipeIngredient( Hawaii, Medium,  Mozzarella,     50.0 );
        service.defineRecipeIngredient( Hawaii, Medium,  Pineapple,      50.0 );
        service.defineRecipeIngredient( Hawaii, Medium,  Ham,            50.0 );
        service.defineRecipeIngredient( Hawaii, Medium,  ChickenGrill,   50.0 );
        service.defineRecipeIngredient( Hawaii, Medium,  Tomato,         50.0 );

        service.defineRecipeIngredient( Hawaii, Large,   Curry,          75.0 );
        service.defineRecipeIngredient( Hawaii, Large,   Mozzarella,     75.0 );
        service.defineRecipeIngredient( Hawaii, Large,   Pineapple,      75.0 );
        service.defineRecipeIngredient( Hawaii, Large,   Ham,            75.0 );
        service.defineRecipeIngredient( Hawaii, Large,   ChickenGrill,   75.0 );
        service.defineRecipeIngredient( Hawaii, Large,   Tomato,         75.0 );

        service.definePrice( Hawaii, Small,  BigDecimal.valueOf(  83.00 ) );
        service.definePrice( Hawaii, Medium, BigDecimal.valueOf( 107.00 ) );
        service.definePrice( Hawaii, Large,  BigDecimal.valueOf( 143.00 ) );

        // ----

        FourSeasons = service.create( "4 Seasons", "/resources/images/pizza_4seasons.jpg" );

        service.defineRecipeIngredient( FourSeasons, Small,   Mozzarella,     20.0 );
        service.defineRecipeIngredient( FourSeasons, Small,   Bacon,          20.0 );
        service.defineRecipeIngredient( FourSeasons, Small,   Ham,            20.0 );
        service.defineRecipeIngredient( FourSeasons, Small,   ChickenSmoked,  20.0 );
        service.defineRecipeIngredient( FourSeasons, Small,   Olive,          10.0 );
        service.defineRecipeIngredient( FourSeasons, Small,   Paprika,        20.0 );
        service.defineRecipeIngredient( FourSeasons, Small,   Tomato,         20.0 );
        service.defineRecipeIngredient( FourSeasons, Small,   Pepperoni,      20.0 );

        service.defineRecipeIngredient( FourSeasons, Medium,   Mozzarella,     40.0 );
        service.defineRecipeIngredient( FourSeasons, Medium,   Bacon,          40.0 );
        service.defineRecipeIngredient( FourSeasons, Medium,   Ham,            40.0 );
        service.defineRecipeIngredient( FourSeasons, Medium,   ChickenSmoked,  40.0 );
        service.defineRecipeIngredient( FourSeasons, Medium,   Olive,          20.0 );
        service.defineRecipeIngredient( FourSeasons, Medium,   Paprika,        40.0 );
        service.defineRecipeIngredient( FourSeasons, Medium,   Tomato,         40.0 );
        service.defineRecipeIngredient( FourSeasons, Medium,   Pepperoni,      40.0 );

        service.defineRecipeIngredient( FourSeasons, Large,   Mozzarella,     60.0 );
        service.defineRecipeIngredient( FourSeasons, Large,   Bacon,          60.0 );
        service.defineRecipeIngredient( FourSeasons, Large,   Ham,            60.0 );
        service.defineRecipeIngredient( FourSeasons, Large,   ChickenSmoked,  60.0 );
        service.defineRecipeIngredient( FourSeasons, Large,   Olive,          60.0 );
        service.defineRecipeIngredient( FourSeasons, Large,   Paprika,        60.0 );
        service.defineRecipeIngredient( FourSeasons, Large,   Tomato,         60.0 );
        service.defineRecipeIngredient( FourSeasons, Large,   Pepperoni,      60.0 );

        service.definePrice( FourSeasons, Small,  BigDecimal.valueOf(  92.00 ) );
        service.definePrice( FourSeasons, Medium, BigDecimal.valueOf( 115.00 ) );
        service.definePrice( FourSeasons, Large,  BigDecimal.valueOf( 153.00 ) );

        // ----

        Margharita = service.create( "Margharita", "/resources/images/pizza_margharita.jpg" );

        service.defineRecipeIngredient( Margharita, Small,   Parmesan,    50.0 );
        service.defineRecipeIngredient( Margharita, Small,   Mozzarella,  50.0 );
        service.defineRecipeIngredient( Margharita, Small,   Tomato,      50.0 );

        service.defineRecipeIngredient( Margharita, Medium,  Parmesan,   100.0 );
        service.defineRecipeIngredient( Margharita, Medium,  Mozzarella, 100.0 );
        service.defineRecipeIngredient( Margharita, Medium,  Tomato,     100.0 );

        service.defineRecipeIngredient( Margharita, Large,   Parmesan,   150.0 );
        service.defineRecipeIngredient( Margharita, Large,   Mozzarella, 150.0 );
        service.defineRecipeIngredient( Margharita, Large,   Tomato,     150.0 );

        service.definePrice( Margharita, Small,  BigDecimal.valueOf(  61.00 ) );
        service.definePrice( Margharita, Medium, BigDecimal.valueOf(  85.00 ) );
        service.definePrice( Margharita, Large,  BigDecimal.valueOf( 101.00 ) );

        // ----

        Marinara = service.create( "Marinara", "/resources/images/pizza_marinara.jpg" );

        service.defineRecipeIngredient( Marinara, Small,   Mozzarella,  25.0 );
        service.defineRecipeIngredient( Marinara, Small,   Squid,       25.0 );
        service.defineRecipeIngredient( Marinara, Small,   Shrimp,      25.0 );
        service.defineRecipeIngredient( Marinara, Small,   Olive,       25.0 );
        service.defineRecipeIngredient( Marinara, Small,   Mussel,      25.0 );
        service.defineRecipeIngredient( Marinara, Small,   Alfredo,     25.0 );

        service.defineRecipeIngredient( Marinara, Medium,  Mozzarella,  50.0 );
        service.defineRecipeIngredient( Marinara, Medium,  Squid,       50.0 );
        service.defineRecipeIngredient( Marinara, Medium,  Shrimp,      50.0 );
        service.defineRecipeIngredient( Marinara, Medium,  Olive,       50.0 );
        service.defineRecipeIngredient( Marinara, Medium,  Mussel,      50.0 );
        service.defineRecipeIngredient( Marinara, Medium,  Alfredo,     50.0 );

        service.defineRecipeIngredient( Marinara, Large,   Mozzarella,  75.0 );
        service.defineRecipeIngredient( Marinara, Large,   Squid,       75.0 );
        service.defineRecipeIngredient( Marinara, Large,   Shrimp,      75.0 );
        service.defineRecipeIngredient( Marinara, Large,   Olive,       75.0 );
        service.defineRecipeIngredient( Marinara, Large,   Mussel,      75.0 );
        service.defineRecipeIngredient( Marinara, Large,   Alfredo,     75.0 );

        service.definePrice( Marinara, Small,  BigDecimal.valueOf(  99.00 ) );
        service.definePrice( Marinara, Medium, BigDecimal.valueOf( 127.00 ) );
        service.definePrice( Marinara, Large,  BigDecimal.valueOf( 161.00 ) );

        // ----

        Mexican = service.create( "Mexican", "/resources/images/pizza_mexican.jpg" );

        service.defineRecipeIngredient( Mexican, Small,   Mozzarella,       25.0 );
        service.defineRecipeIngredient( Mexican, Small,   Ham,              25.0 );
        service.defineRecipeIngredient( Mexican, Small,   ChickenGrill,     25.0 );
        service.defineRecipeIngredient( Mexican, Small,   Onion,            25.0 );
        service.defineRecipeIngredient( Mexican, Small,   Jalapeno,         25.0 );
        service.defineRecipeIngredient( Mexican, Small,   Tomato,           25.0 );

        service.defineRecipeIngredient( Mexican, Medium,  Mozzarella,       50.0 );
        service.defineRecipeIngredient( Mexican, Medium,  Ham,              50.0 );
        service.defineRecipeIngredient( Mexican, Medium,  ChickenGrill,     50.0 );
        service.defineRecipeIngredient( Mexican, Medium,  Onion,            50.0 );
        service.defineRecipeIngredient( Mexican, Medium,  Jalapeno,         50.0 );
        service.defineRecipeIngredient( Mexican, Medium,  Tomato,           50.0 );

        service.defineRecipeIngredient( Mexican, Large,   Mozzarella,       75.0 );
        service.defineRecipeIngredient( Mexican, Large,   Ham,              75.0 );
        service.defineRecipeIngredient( Mexican, Large,   ChickenGrill,     75.0 );
        service.defineRecipeIngredient( Mexican, Large,   Onion,            75.0 );
        service.defineRecipeIngredient( Mexican, Large,   Jalapeno,         75.0 );
        service.defineRecipeIngredient( Mexican, Large,   Tomato,           75.0 );

        service.definePrice( Mexican, Small,  BigDecimal.valueOf(  89.00 ) );
        service.definePrice( Mexican, Medium, BigDecimal.valueOf( 112.00 ) );
        service.definePrice( Mexican, Large,  BigDecimal.valueOf( 139.00 ) );

        // ----

    }


    public void generateCarts ( IShoppingCartService service )
            throws DomainLogicException
    {
        cart = service.createNew();

        service.setItem( cart, Mafia,   Large,  1 );
        service.setItem( cart, Georgia, Small,  2 );
        service.setItem( cart, Cossack, Medium, 1 );

        service.lock( cart );
    }


    public void generateOrders ( IOrderService service )
            throws DomainLogicException
    {
        order = service.createNew(
                    "Ivan Kuziakin",
                    "Sumskaya 1",
                    "(050)123-45-67",
                    "ivan.kuziakin@ukr.net",
                    "",
                    cart
        );

        service.setDiscount( order, BigDecimal.valueOf( 20.00 ) );
    }


    public void generateAccounts ( IAccountService service )
            throws DomainLogicException
    {
        Wasya = service.createOperator( "Wasya Pupkin", "wasya.pupkin@pizzario.com", "12345" );
    }


    public void confirmOrders ( IOrderService service )
            throws DomainLogicException
    {
        for ( UUID orderId : service.viewUnconfirmed() )
            service.confirm( orderId, Wasya );
    }


    public void startCooking ( ICookingAssignmentService service )
            throws DomainLogicException
    {
        for ( UUID assignmentId : service.viewWaiting() )
            service.markCookingStarted( assignmentId );
    }


    public void finishCooking ( ICookingAssignmentService service )
            throws DomainLogicException
    {
        for ( UUID assignmentId : service.viewCookedRightNow() )
            service.markCookingFinished( assignmentId );
    }


    public void deliver ( IDeliveryService service )
            throws DomainLogicException
    {
        for ( UUID deliveryId : service.viewWaiting() )
        {
            service.markStarted( deliveryId, "Wasya" );
            service.markDelivered( deliveryId );
        }
    }


    private UUID Large, Medium, Small;

    private UUID Chicken, ChickenGrill, ChickenSmoked, Saliami, Pepperoni, Kebab;
    private UUID Mozzarella, Pineapple, Tomato, Olive , Eggplant, Curry, Paprika ;
    private UUID Onion, Parsley, Bacon, Ham, PickledCucumber, Mushroom, Parmesan;
    private UUID Squid, Shrimp, Mussel, Alfredo, Jalapeno;

    private UUID Mafia, Georgia, Cossack, Hawaii, FourSeasons, Margharita, Marinara, Mexican;

    private UUID cart;
    private UUID order;
    private UUID Wasya;
}
