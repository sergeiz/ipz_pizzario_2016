<%--(C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine --%>

<%--@elvariable id="products" type="java.util.List<pizzario.dto.ProductDto>"--%>
<%--@elvariable id="sizes" type="java.util.List<pizzario.dto.ProductSozeDto>"--%>
<%--@elvariable id="product_prices" type="java.util.Map<java.util.UUID,java.util.Map<java.util.UUID,java.math.BigDecimal>>"--%>
<spring:message code="title.menu" var="menuTitle" />
<template:main htmlTitle="${menuTitle}" >

    <jsp:attribute name="activeLink">menu</jsp:attribute>

    <jsp:body>

        <div class="container content-wrap">

            <div class="row">

                <c:forEach items="${products}" var="product">

                    <div class="col-sm-6 col-md-4 col-lg-3" >

                        <div class="thumbnail">

                            <img src="<c:url value="${product.imageUrl}" />" class="img-responsive img-circle center-block" alt="${product.name}" >

                            <div class="caption">
                                <h3>${product.name}</h3>
                                <p class="small">${product.description}</p>
                            </div>

                            <div class="container-fluid prices">
                                <div class="row">

                                    <c:forEach items="${sizes}" var="size">

                                        <div class="col-xs-4 prices-concrete"  >
                                            <img src="<c:url value="${size.imageUrl}" />" alt="${size.name}">
                                            <p>${size.diameter}<spring:message code="menu.unit.cm" /></p>
                                            <p>${size.weight}<spring:message code="menu.unit.g" /></p>
                                            <p><strong>${product_prices[product.domainId][size.domainId]}&#x20b4;</strong></p>

                                            <div class="submit bottom"  >
                                                <p>
                                                    <button type="button" class="btn btn-success btn-block"
                                                            onclick="addToCart( '${product.domainId}', '${size.domainId}')" >
                                                        <span class="glyphicon glyphicon-shopping-cart"></span> <spring:message code="menu.button.buy" />
                                                    </button>
                                                </p>
                                            </div>

                                        </div>

                                   </c:forEach>

                                </div>
                            </div>

                        </div>

                    </div>

                </c:forEach>

            </div>

        </div>

    </jsp:body>

</template:main>