<%--(C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine --%>

<%--@elvariable id="cart" type="pizzario.dto.ShoppingCartDto"--%>
<c:choose>
    <c:when test="${cart.items.size() == 0}" >
        <p class="text-muted"><spring:message code="cart.noitems" /></p>
    </c:when>
    <c:otherwise>

        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <th class="table-column-productname"><spring:message code="cart.column.product" /></th>
                <th><spring:message code="cart.column.size" /></th>
                <th><spring:message code="cart.column.price" /></th>
                <th><spring:message code="cart.column.count" /></th>
                <th><spring:message code="cart.column.cost" /></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${cart.items}" var="item" >
                <tr>
                    <td class="table-column-productname">
                            ${item.productName}
                    </td>
                    <td>
                            ${item.sizeName}
                    </td>
                    <td>
                            ${item.fixedPrice} &#x20b4;
                    </td>
                    <td>
                            <input type="number" min="1" value="${item.quantity}" style="width: 50px"
                                   onchange="updateCartItem( '${item.productId}', '${item.sizeId}', this.value  );"
                            />
                    </td>
                    <td>
                            ${item.fixedCost} &#x20b4;
                    </td>
                    <td>
                        <button type="button" class="btn btn-warning"
                                onclick="removeCartItem( '${item.productId}', '${item.sizeId}' )"
                        >
                            <span class="glyphicon glyphicon-remove"></span> <spring:message code="cart.button.remove" />
                        </button>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>

        <p><spring:message code="cart.total" />: <strong>${cart.totalCost}</strong> &#x20b4;</p>

    </c:otherwise>
</c:choose>