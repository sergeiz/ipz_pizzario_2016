﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace Pizzario.Service.Validators
{

    [ AttributeUsage( AttributeTargets.Property  | 
                      AttributeTargets.Field     | 
                      AttributeTargets.Parameter ) 
    ]
    public class DiscountValidator : ValidatorAttribute
    {
        protected override Validator DoCreateValidator ( Type targetType )
        {
           return new RangeValidator( 
                            0.0M, 
                            RangeBoundaryType.Inclusive, 
                            100.00M, 
                            RangeBoundaryType.Inclusive
                      );
        }
    }
}
