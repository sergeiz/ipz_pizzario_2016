﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Dto;
using Pizzario.Utils.Validators;
using Pizzario.Service.Validators;

using System;

namespace Pizzario.Service
{
    public interface IProductSizeService : IDomainEntityService< ProductSizeDto >
    {
        Guid Create (
            [ NonEmptyStringValidator ] string name,
            [ NonEmptyStringValidator ] string imageUrl,
            [ DiameterValidator ] int diameter,
            [ WeightValidator ] int weight
        );

        void Rename (
            Guid sizeId,
            [ NonEmptyStringValidator ] string newName 
        );

        void UpdateImageUrl (
            Guid sizeId,
            [ NonEmptyStringValidator ] string newImageUrl
        );

        void ChangeDiameter ( 
            Guid sizeId,
            [ DiameterValidator ] int newDiameter 
        );

        void ChangeWeight ( 
            Guid sizeId,
            [ WeightValidator ] int weight
        );
    }
}
