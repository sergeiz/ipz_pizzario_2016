﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Dto;
using Pizzario.Utils.Validators;

using System;
using System.Collections.Generic;


namespace Pizzario.Service
{
    public interface IAccountService : IDomainEntityService< AccountDto >
    {
        AccountDto Identify (
            [ EmailValidator ] string email,
            [ NonEmptyStringValidator ] string password 
        );

        IList< Guid > ViewAssociatedOrders ( Guid accountId );

        Guid CreateOperator (
            [ NonEmptyStringValidator ] string name,
            [ EmailValidator ] string email,
            [ NonEmptyStringValidator ] string password 
        );

        void ChangeName ( 
            Guid accountId,
            [ NonEmptyStringValidator ] string newName 
        );

        void ChangeEmail ( 
            Guid accountId,
            [ EmailValidator ] string newEmail 
        );

        void ChangePassword (
            Guid accountId,
            [ NonEmptyStringValidator ] string newPassword 
        );
    }
}
