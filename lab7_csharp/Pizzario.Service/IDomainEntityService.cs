﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Dto;
using System;
using System.Collections.Generic;

namespace Pizzario.Service
{
    public interface IDomainEntityService< TDto > where TDto : DomainEntityDto< TDto >
    {
        IList< Guid > ViewAll ();

        TDto View ( Guid domainId );
    }
}
