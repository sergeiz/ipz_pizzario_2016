﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Dto;
using Pizzario.Utils.Validators;

using System;


namespace Pizzario.Service
{
    public interface IIngredientService : IDomainEntityService< IngredientDto >
    {
        Guid Create ( [ NonEmptyStringValidator ] string name  );

        void Rename (
            Guid ingredientId,
            [ NonEmptyStringValidator ] string newName 
        );
    }
}
