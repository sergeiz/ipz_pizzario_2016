﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Dto;

using System;
using System.Collections.Generic;

namespace Pizzario.Service
{
    public interface ICookingAssignmentService : IDomainEntityService< CookingAssignmentDto >
    {
        IList< Guid > ViewCookedRightNow ();

        IList< Guid > ViewWaiting ();

        IList< CookingAssignmentDto > ViewOrderAssignments ( Guid orderId );

        void MarkCookingStarted ( Guid cookingAssignmentId );

        void MarkCookingFinished ( Guid cookingAssignmentId );
    }
}
