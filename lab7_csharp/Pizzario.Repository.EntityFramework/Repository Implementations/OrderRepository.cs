﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System;
using System.Linq;


namespace Pizzario.Repository.EntityFramework
{
    public class OrderRepository : BasicRepository< Order >, IOrderRepository
    {
        public OrderRepository ( PizzarioDbContext dbContext )
            :   base( dbContext, dbContext.Orders )
        {
        }

        public IQueryable< Guid > SelectUnconfirmedIds ()
        {
            return GetDBSet().Where( o => o.Status == OrderStatus.Placed ).Select( o => o.DomainId );
        }

        public IQueryable< Guid > SelectReady4DeliveryIds ()
        {
            return GetDBSet().Where( o => o.Status == OrderStatus.Delivering ).Select( o => o.DomainId );
        }
    }
}
