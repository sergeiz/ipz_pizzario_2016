﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System;
using System.Linq;

namespace Pizzario.Repository.EntityFramework
{
    public class DeliveryRepository : BasicRepository< Delivery >, IDeliveryRepository
    {
        public DeliveryRepository ( PizzarioDbContext dbContext )
            :   base( dbContext, dbContext.Deliveries )
        {
        }

        public IQueryable< Guid > SelectWaitingIds ()
        {
            return GetDBSet().Where( d => d.Status == DeliveryStatus.Waiting ).Select( d => d.DomainId );
        }

        public IQueryable< Guid > SelectInProgressIds ()
        {
            return GetDBSet().Where( d => d.Status == DeliveryStatus.InProgress ).Select( d => d.DomainId );
        }

        public Delivery FindByOrder ( Order o )
        {
            return GetDBSet().Where( d => d.RelatedOrder == o ).SingleOrDefault();
        }
    }
}
