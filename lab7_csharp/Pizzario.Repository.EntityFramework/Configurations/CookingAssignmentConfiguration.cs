﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

namespace Pizzario.Repository.EntityFramework.Configurations
{
    class CookingAssignmentConfiguration : BasicEntityConfiguration< CookingAssignment >
    {
        public CookingAssignmentConfiguration ()
        {
            HasRequired( ca => ca.RelatedOrder );
            HasRequired( ca => ca.CookedProduct );
            HasRequired( ca => ca.CookedSize );
        }
    }
}
