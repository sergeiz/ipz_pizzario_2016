﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

namespace Pizzario.Repository.EntityFramework.Configurations
{
    class ProductConfiguration : BasicEntityConfiguration< Product >
    {
        public ProductConfiguration ()
        {
            Property( p => p.Name ).IsRequired();
            Property( p => p.ImageUrl ).IsRequired();

            HasMany( p => p.Prices ).WithOptional();
            HasMany( p => p.Recipes ).WithOptional();
        }
    }
}
