﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Repository.EntityFramework.Configurations
{
    class ProductItemConfiguration : BasicEntityConfiguration< ProductItem >
    {
        public ProductItemConfiguration ()
        {
            HasRequired( i => i.SelectedProduct );
            HasRequired( i => i.Size );
        }
    }
}
