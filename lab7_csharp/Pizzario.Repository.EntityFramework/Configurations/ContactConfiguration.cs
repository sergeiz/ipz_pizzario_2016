﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

namespace Pizzario.Repository.EntityFramework.Configurations
{
    class ContactConfiguration : BasicValueConfiguration< Contact >
    {
        public ContactConfiguration ()
        {
            Property( c => c.Address ).IsRequired();
            Property( c => c.Phone ).IsRequired();
        }
    }
}
