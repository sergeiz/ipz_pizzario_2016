﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Repository.EntityFramework.Configurations
{
    class RecipeConfiguration : BasicEntityConfiguration< Recipe >
    {
        public RecipeConfiguration ()
        {
            HasRequired( r => r.RelatedProduct );
            HasRequired( r => r.Size );

            HasMany( r => r.Ingredients ).WithOptional();
        }
    }
}
