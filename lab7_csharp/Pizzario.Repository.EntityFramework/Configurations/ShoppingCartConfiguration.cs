﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

namespace Pizzario.Repository.EntityFramework.Configurations
{
    class ShoppingCartConfiguration : BasicEntityConfiguration< ShoppingCart >
    {
        public ShoppingCartConfiguration ()
        {
            HasMany< ProductItem >( c => c.Items ).WithOptional();
        }
    }
}
