﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System.Data.Entity;

namespace Pizzario.Repository.EntityFramework
{
    public class PizzarioDbContext : DbContext
    {
        public PizzarioDbContext ( bool drop = false )
        {
            if ( drop )
                Database.SetInitializer( new DropCreateDatabaseAlways< PizzarioDbContext >() );

            Database.Log = ( s => System.Diagnostics.Debug.WriteLine( s ) ) ;
        }


        public DbSet< ProductSize > ProductSizes { get; set;  }

        public DbSet< Ingredient > Ingredients { get; set; }

        public DbSet< Product > Products { get; set; }

        public DbSet< ShoppingCart > ShoppingCarts { get; set; }

        public DbSet< Order > Orders { get; set; }

        public DbSet<Account> Accounts { get; set; }

        public DbSet< CookingAssignment > CookingAssignments { get; set; }

        public DbSet< Delivery > Deliveries { get; set; }

        protected override void OnModelCreating ( DbModelBuilder modelBuilder )
        {
            modelBuilder.Configurations.Add( new Configurations.ProductSizeConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.IngredientConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.ProductConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.RecipeConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.ProductItemConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.ShoppingCartConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.OrderConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.AccountConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.OperatorAccountConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.CookingAssignmentConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.DeliveryConfiguration() );

            modelBuilder.Configurations.Add( new Configurations.ProductPriceAssignmentConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.IngredientRecipeInclusionConfiguration() );

            modelBuilder.Configurations.Add( new Configurations.ContactConfiguration() );
            modelBuilder.Configurations.Add( new Configurations.DiscountConfiguration() );
        }
    }
}
