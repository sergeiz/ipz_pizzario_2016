﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;
using Pizzario.Dto;

using System;
using System.Collections.Generic;
using System.Linq;


namespace Pizzario.Service.Impl
{
    static class DtoBuilder
    {
        public static AccountDto ToDto ( this Account account )
        {
            var orderIds = new List< Guid >();
            account.Accept( new OrdersExtractAccountVisitor( orderIds ) );

            return new AccountDto( account.DomainId, account.Name, account.Email, orderIds );
        }

        public static CookingAssignmentDto ToDto ( this CookingAssignment ca )
        {
            return new CookingAssignmentDto(
                ca.DomainId,
                ca.RelatedOrder.DomainId,
                ca.CookedProduct.DomainId,
                ca.CookedProduct.Name,
                ca.CookedSize.DomainId,
                ca.CookedSize.Name,
                ca.Status.ToString()
            );
        }

        public static DeliveryDto ToDto ( this Delivery delivery )
        {
            return new DeliveryDto(
                delivery.DomainId,
                delivery.RelatedOrder.DomainId,
                delivery.DriverName,
                delivery.Status.ToString(),
                delivery.Cash2Collect
            );
        }

        public static IngredientDto ToDto ( this Ingredient ingredient )
        {
            return new IngredientDto( ingredient.DomainId, ingredient.Name );
        }

        public static ProductDto ToDto ( this Product product )
        {
            return new ProductDto( product.DomainId, product.Name, product.ImageUrl, product.BuildDescription() );
        }

        public static ProductSizeDto ToDto ( this ProductSize size )
        {
            return new ProductSizeDto( size.DomainId, size.Name, size.Diameter, size.Weight, size.ImageUrl );
        }

        public static ProductItemDto ToDto ( this ProductItem item )
        {
            return new ProductItemDto(
                    item.DomainId, 
                    item.SelectedProduct.DomainId,
                    item.SelectedProduct.Name,
                    item.Size.DomainId,
                    item.Size.Name,
                    item.Quantity,
                    item.FixedPrice,
                    item.Cost
            );
        }

        public static ShoppingCartDto ToDto ( this ShoppingCart cart )
        {
            var itemDtos = new List< ProductItemDto >();
            foreach ( var item in cart.Items )
                itemDtos.Add( item.ToDto() );

            return new ShoppingCartDto(
                cart.DomainId,
                itemDtos,
                ! cart.Modifiable,
                cart.Cost
            );
        }

        public static OrderDto ToDto ( this Order order )
        {
            var itemDtos = new List< ProductItemDto >();
            foreach ( var item in order.Items )
                itemDtos.Add( item.ToDto() );

            return new OrderDto(
                order.DomainId,
                order.CustomerContact.Name,
                order.CustomerContact.Address,
                order.CustomerContact.Phone,
                order.CustomerContact.Email,
                order.PlacementTime,
                order.Status.ToString(),
                itemDtos,
                order.BasicCost,
                order.TotalCost,
                order.AssignedDiscount.Percent,
                order.Comment
            );
        }
    }
}
