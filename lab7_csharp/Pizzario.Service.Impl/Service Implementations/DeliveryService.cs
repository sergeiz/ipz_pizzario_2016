﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;
using Pizzario.Repository;
using Pizzario.Dto;

using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Practices.Unity;


namespace Pizzario.Service.Impl
{
    public class DeliveryService : IDeliveryService
    {
        public IList< Guid > ViewAll ()
        {
            return deliveryRepository.SelectAllDomainIds();
        }

        public IList< Guid > ViewWaiting ()
        {
            return deliveryRepository.SelectWaitingIds().ToList();
        }

        public IList< Guid > ViewInProgress ()
        {
            return deliveryRepository.SelectInProgressIds().ToList();
        }

        public DeliveryDto View ( Guid deliveryId )
        {
            Delivery delivery = ResolveDelivery( deliveryId );
            return delivery.ToDto();
        }

        public DeliveryDto FindOrderDelivery ( Guid orderId )
        {
            Order o = ResolveOrder( orderId );
            Delivery delivery = deliveryRepository.FindByOrder( o );
            return delivery.ToDto();
        }

        public void MarkStarted ( Guid deliveryId, string driverName )
        {
            Delivery delivery = ResolveDelivery( deliveryId );
            delivery.StartDelivery( driverName );
        }


        public void MarkDelivered ( Guid deliveryId )
        {
            Delivery delivery = ResolveDelivery( deliveryId );
            delivery.FinishDelivery();
        }


        private Delivery ResolveDelivery ( Guid deliveryId )
        {
            return ServiceUtils.ResolveEntity( deliveryRepository, deliveryId );
        }


        private Order ResolveOrder ( Guid orderId )
        {
            return ServiceUtils.ResolveEntity( orderRepository, orderId );
        }


        [ Dependency ]
        protected IDeliveryRepository deliveryRepository { get; set; }

        [ Dependency ]
        protected IOrderRepository orderRepository { get; set; }
    }
}
