﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;
using Pizzario.Repository;
using Pizzario.Dto;
using Pizzario.Exceptions;

using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Practices.Unity;

namespace Pizzario.Service.Impl
{
    public class ShoppingCartService : IShoppingCartService
    {
        public IList< Guid > ViewAll ()
        {
            return shoppingCartRepository.SelectAllDomainIds();
        }

        public IList< Guid > ViewOpen ()
        {
            return shoppingCartRepository.SelectAllOpenCartIds().ToList();
        }

        public ShoppingCartDto View ( Guid cartId )
        {
            ShoppingCart cart = ResolveCart( cartId );
            return cart.ToDto();
        }

        public Guid CreateNew ()
        {
            ShoppingCart cart = new ShoppingCart( Guid.NewGuid() );
            shoppingCartRepository.Add( cart );
            return cart.DomainId;
        }

        public void AddItem ( Guid cartId, Guid productId, Guid sizeId )
        {
            ShoppingCart cart = ResolveCart( cartId );
            Product product   = ResolveProduct( productId );
            ProductSize size  = ResolveSize( sizeId );

            int currentQuantity = cart.GetItemQuantity( product, size );
            cart.PlaceItem(
                new ProductItem( Guid.NewGuid(), product, size, currentQuantity + 1 )
            );
        }

        public void SetItem ( Guid cartId, Guid productId, Guid sizeId, int quantity )
        {
            ShoppingCart cart = ResolveCart( cartId );
            Product product = ResolveProduct( productId );
            ProductSize size = ResolveSize( sizeId );

            ProductItem item = new ProductItem( Guid.NewGuid(), product, size, quantity );
            cart.PlaceItem( item );
        }

        public void RemoveItem ( Guid cartId, Guid productId, Guid sizeId )
        {
            ShoppingCart cart = ResolveCart( cartId );
            Product product = ResolveProduct( productId );
            ProductSize size = ResolveSize( sizeId );

            int itemIndex = cart.FindItemIndex( product, size );
            if ( itemIndex != -1 )
                cart.DropItem( itemIndex );

            else
                throw new RemovingUnexistingCartItemException( cartId, product.Name, size.Name );
        }

        public void ClearItems ( Guid cartId )
        {
            ShoppingCart cart = ResolveCart( cartId );
            cart.ClearItems();
        }

        public void LockCart ( Guid cartId ) {
            ShoppingCart cart = ResolveCart( cartId );
            cart.Checkout();
        }

        public void Remove ( Guid cartId )
        {
            ShoppingCart cart = ResolveCart( cartId );
            shoppingCartRepository.Delete( cart );
        }

        private ShoppingCart ResolveCart ( Guid cartId )
        {
            return ServiceUtils.ResolveEntity( shoppingCartRepository, cartId );
        }

        private Product ResolveProduct ( Guid productId )
        {
            return ServiceUtils.ResolveEntity( productRepository, productId );
        }

        private ProductSize ResolveSize ( Guid sizeId )
        {
            return ServiceUtils.ResolveEntity( productSizeRepository, sizeId );
        }


        [ Dependency ]
        protected IShoppingCartRepository shoppingCartRepository { get; set; }

        [ Dependency ]
        protected IProductRepository productRepository { get; set; }

        [ Dependency ]
        protected IProductSizeRepository productSizeRepository { get; set; }

    }
}
