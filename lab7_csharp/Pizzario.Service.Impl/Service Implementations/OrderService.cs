﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;
using Pizzario.Repository;
using Pizzario.Dto;
using Pizzario.Exceptions;

using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Practices.Unity;

namespace Pizzario.Service.Impl
{
    public class OrderService : IOrderService
    {
        public IList< Guid > ViewAll ()
        {
            return orderRepository.SelectAllDomainIds();
        }

        public IList< Guid > ViewUnconfirmed ()
        {
            return orderRepository.SelectUnconfirmedIds().ToList();
        }

        public IList< Guid > ViewReady4Delivery ()
        {
            return orderRepository.SelectReady4DeliveryIds().ToList();
        }

        public OrderDto View ( Guid orderId )
        {
            Order o = ResolveOrder( orderId );
            return o.ToDto();
        }

        public OrderDto FindOrder ( Guid orderID )
        {
            Order o = orderRepository.FindByDomainId( orderID );
            return ( o != null ) ? o.ToDto() : null;
        }

        public Guid CreateNew ( String customerName, 
                                String deliveryAddress, 
                                String contactPhone, 
                                String contactEmail,
                                String comment,
                                Guid cartId )
        {
            ShoppingCart cart = ResolveCart( cartId );
            Order o = new Order(
                    Guid.NewGuid(),
                    cart,
                    new Contact( customerName, deliveryAddress, contactPhone, contactEmail ),
                    comment,
                    DateTime.Now
            );

            orderRepository.Add( o );

            shoppingCartRepository.Delete( cart );

            return o.DomainId;
        }

        public void SetDiscount ( Guid orderId, decimal discountPercent )
        {
            Order o = ResolveOrder( orderId );
            o.SetDiscount( new Discount( discountPercent ) );
        }

        public void Confirm ( Guid orderId, Guid operatorId )
        {
            Order order = ResolveOrder( orderId );
            OperatorAccount operatorAccount = ResolveOperator( operatorId );

            order.Confirm();

            operatorAccount.TrackOrder( order );

            foreach ( var ca in order.GenerateCookingAssignments() )
                cookingAssignmentRepository.Add( ca );

        }

        public void Cancel ( Guid orderId )
        {
            Order o = ResolveOrder( orderId );
            o.Cancel();
        }

        private Order ResolveOrder ( Guid orderId )
        {
            return ServiceUtils.ResolveEntity( orderRepository, orderId );
        }

        private ShoppingCart ResolveCart ( Guid cartId )
        {
            return ServiceUtils.ResolveEntity( shoppingCartRepository, cartId );
        }

        private OperatorAccount ResolveOperator ( Guid operatorId )
        {
            OperatorAccount account = accountRepository.FindOperatorByDomainId( operatorId );
            if ( account == null )
                throw new ServiceUnresolvedEntityException( typeof( OperatorAccount ), operatorId );

            return account;
        }


        [ Dependency ]
        protected IOrderRepository orderRepository { get; set; }

        [ Dependency ]
        protected IAccountRepository accountRepository { get; set; }

        [ Dependency ]
        protected IShoppingCartRepository shoppingCartRepository { get; set; }

        [ Dependency ]
        protected ICookingAssignmentRepository cookingAssignmentRepository { get; set; }

    }
}
