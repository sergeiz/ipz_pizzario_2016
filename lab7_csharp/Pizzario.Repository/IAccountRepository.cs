﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System;

namespace Pizzario.Repository
{
    public interface IAccountRepository : IRepository< Account >
    {
        Account FindByEmail ( string email );

        OperatorAccount FindOperatorByDomainId ( Guid domainId );
    }
}
