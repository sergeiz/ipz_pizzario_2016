﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;


namespace Pizzario.Repository
{
    public interface IRepository< T > where T : Utils.Entity
    {
        int Count ();

        T Load ( int id );

        void Add ( T t );

        void Delete ( T t );

        T FindByDomainId ( Guid domainId );

        IList< Guid > SelectAllDomainIds ();
    }
}
