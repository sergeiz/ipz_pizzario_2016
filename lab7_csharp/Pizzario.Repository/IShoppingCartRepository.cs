﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System;
using System.Linq;

namespace Pizzario.Repository
{
    public interface IShoppingCartRepository : IRepository< ShoppingCart >
    {
        IQueryable< Guid > SelectAllOpenCartIds ();
    }
}
