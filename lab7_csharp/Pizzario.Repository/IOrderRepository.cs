﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System;
using System.Linq;

namespace Pizzario.Repository
{
    public interface IOrderRepository : IRepository< Order >
    {
        IQueryable< Guid > SelectUnconfirmedIds ();

        IQueryable< Guid > SelectReady4DeliveryIds ();
    }
}
