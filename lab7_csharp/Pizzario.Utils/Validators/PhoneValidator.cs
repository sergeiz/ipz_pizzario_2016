﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace Pizzario.Utils.Validators
{

    [ AttributeUsage( AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter ) ]
    public class PhoneValidator : ValidatorAttribute
    {
        protected override Validator DoCreateValidator ( Type targetType )
        {
            const string PhoneRegex = @"^((1-?)|(\+1 ?))?\(?(\d{3})[)\-.]?(\d{3})[\-.]?(\d{2})[\-.]?(\d{2})$";
            return new RegexValidator( PhoneRegex );
        }
    }
}
