﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

namespace Pizzario.Model
{
    public class IngredientRecipeInclusion
    {
        public long InclusionId { get; set; }

        public virtual Recipe RelatedRecipe { get; private set; }

        public virtual Ingredient IncludedIngredient { get; private set; }

        public int Weight { get; set; }

        protected IngredientRecipeInclusion() { }

        public IngredientRecipeInclusion ( Recipe recipe, Ingredient ingredient, int weight )
        {
            this.RelatedRecipe = recipe;
            this.IncludedIngredient = ingredient;
            this.Weight = weight;
        }
    }
}
