﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Model
{
    public class OperatorAccount : Account
    {
        public virtual ICollection< Order > Orders { get; private set; }

        protected OperatorAccount() {}

        public OperatorAccount ( Guid id, string name, string email, string passwordHash )
            :   base( id, name, email, passwordHash )
        {
            this.Orders = new List< Order >();
        }


        public void TrackOrder ( Order order )
        {
            Orders.Add( order );
        }


        public override void Accept ( AccountVisitor visitor )
        {
            visitor.Visit( this );
        }
    }
}
