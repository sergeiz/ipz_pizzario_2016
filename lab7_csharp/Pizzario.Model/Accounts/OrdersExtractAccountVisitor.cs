﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Model
{
    public class OrdersExtractAccountVisitor : AccountVisitor
    {
        public OrdersExtractAccountVisitor ( IList< Guid > orderIds )
        {
            this.orderIds = orderIds;
        }

        public void Visit ( Account account )
        {
            // No orders..
        }

        public void Visit ( OperatorAccount account )
        {
            foreach ( var o in account.Orders )
                orderIds.Add( o.DomainId );
        }

        private IList<Guid> orderIds;
    }
}
