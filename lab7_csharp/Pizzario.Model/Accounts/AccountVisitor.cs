﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

namespace Pizzario.Model
{
    public interface AccountVisitor
    {
        void Visit ( Account account );

        void Visit ( OperatorAccount account );
    }

}
