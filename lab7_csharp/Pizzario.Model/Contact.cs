﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System.Collections.Generic;

namespace Pizzario.Model
{
    public class Contact : Utils.Value< Contact >
    {
        public string Name { get; private set; }

        public string Address { get; private set; }

        public string Phone { get; private set; }

        public string Email { get; private set; }

        protected Contact() {}

        public Contact ( string name, string address, string phone, string email )
        {
            this.Name    = name;
            this.Address = address;
            this.Phone   = phone;
            this.Email   = email;
        }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new object[] { Name, Address, Phone, Email };
        }
    }
}
