﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Exceptions;
using System;
using System.Collections.Generic;

namespace Pizzario.Model
{
    public class Order : Utils.Entity
    {
        public virtual ICollection< ProductItem > Items { get; private set; }

        public decimal BasicCost { get; private set; }

        public Discount AssignedDiscount { get; private set; }

        public decimal TotalCost
        {
            get
            {
                return AssignedDiscount.GetDiscountedPrice( BasicCost );
            }
        }

        public OrderStatus Status { get; private set; }

        public DateTime PlacementTime { get; private set; }

        public Contact CustomerContact { get; private set; }

        public int UnfinishedCookingsCount { get; private set; }

        public string Comment { get; private set; }

        protected Order () {}

        public Order ( Guid domainId, ShoppingCart cart, Contact contact, String comment, DateTime time )
            :   base( domainId )
        {
            if ( cart.Modifiable )
                throw new ModifiableCartException( cart.DomainId );

            this.Items = new List< ProductItem >( cart.Items );
            this.CustomerContact = contact;

            this.BasicCost = cart.Cost;
            this.AssignedDiscount = new Discount();

            this.Status = OrderStatus.Placed;
            this.PlacementTime = time;

            this.Comment = comment;

            this.UnfinishedCookingsCount = 0;
            foreach ( ProductItem item in Items )
                this.UnfinishedCookingsCount += item.Quantity;
        }


        public void SetDiscount ( Discount discount )
        {
            AssignedDiscount = discount;
        }

        public void Confirm ()
        {
            if ( Status != OrderStatus.Placed )
                throw new OrderLifecycleException(
                            this.DomainId,
                            Status.ToString(),
                            OrderStatus.Placed.ToString()
                          );

            Status = OrderStatus.Confirmed;
        }

        public void Cancel ()
        {
            if ( Status != OrderStatus.Placed )
                throw new OrderLifecycleException(
                            this.DomainId, 
                            Status.ToString(),
                            OrderStatus.Placed.ToString()
                          );

            Status = OrderStatus.Cancelled;
        }

        public void CookingAssignmentCompleted ()
        {
            if ( Status == OrderStatus.Confirmed )
            {
                -- UnfinishedCookingsCount;
                if ( UnfinishedCookingsCount == 0 )
                    Status = OrderStatus.Delivering;
            }
            else
                throw new OrderLifecycleException(
                            this.DomainId, 
                            Status.ToString(),
                            OrderStatus.Confirmed.ToString()
                          );

        }

        public void DeliveryCompleted ()
        {
            if ( Status != OrderStatus.Delivering )
                throw new OrderLifecycleException(
                            this.DomainId, 
                            Status.ToString(),
                            OrderStatus.Delivering.ToString()
                          );

            Status = OrderStatus.Completed;
        }


        public IList< CookingAssignment > GenerateCookingAssignments ()
        {
            var cookingAssignments = new List< CookingAssignment >();

            foreach ( ProductItem item in Items )
            {
                for ( int i = 0; i < item.Quantity; i++ )
                    cookingAssignments.Add( 
                        new CookingAssignment( 
                            Guid.NewGuid(), 
                            this, 
                            item.SelectedProduct, 
                            item.Size 
                        )
                    );
            }

            return cookingAssignments;
        }


        public Delivery GenerateDelivery ()
        {
            return new Delivery( Guid.NewGuid(), this );
        }
    }
}
