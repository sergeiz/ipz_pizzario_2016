﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace Pizzario.Model
{
    public class ProductSize : Utils.Entity
    {
        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public int Diameter { get; set; }

        public int Weight { get; set; }


        protected ProductSize() {}

        public ProductSize ( Guid domainId, string name, string imageUrl, int diameter, int weight )
            :   base( domainId )
        {
            this.Name = name;
            this.ImageUrl = imageUrl;
            this.Diameter = diameter;
            this.Weight = weight;
        }
    }
}