﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Pizzario.Model
{
    public class Product : Utils.Entity
    {
        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public virtual ICollection< Recipe > Recipes { get; private set; }

        public virtual ICollection< ProductPriceAssignment > Prices { get; private set; }

        protected Product() {} 

        public Product ( Guid domainId, string name )
            :   base( domainId )
        {
            this.Name = name;
            this.ImageUrl = "";
            this.Prices = new List< ProductPriceAssignment >();
            this.Recipes = new HashSet< Recipe >();
        }


        public IEnumerable< ProductSize > ListAvailableSizes ()
        {
            return Prices.Select( p => p.Size );
        }

        public decimal GetPrice ( ProductSize size )
        {
            var assignment = Prices.Where( p => p.Size == size ).FirstOrDefault();
            if ( assignment != null )
                return assignment.Price;

            throw new PriceUndefinedException( this.Name, size.Name );
        }

        public void SetPrice ( ProductSize size, decimal price )
        {
            var assignment = Prices.Where(p => p.Size == size).FirstOrDefault();
            if ( assignment != null )
            {
                assignment.Price = price;
                return;
            }

            Prices.Add( new ProductPriceAssignment( this, size, price ) );
        }

        public void RemovePrice ( ProductSize size )
        {
            var priceAssignment = Prices.Where( a => a.Size == size ).FirstOrDefault();
            if ( priceAssignment != null )
                Prices.Remove( priceAssignment );

            else
                throw new PriceUndefinedException( this.Name, size.Name );
        }

        public Recipe GetRecipe ( ProductSize size )
        {
            var recipe = Recipes.Where( r => r.Size == size ).FirstOrDefault();
            if ( recipe != null )
                return recipe;

            throw new RecipeUndefinedException( this.Name, size.Name );
        }

        public Recipe DefineRecipe ( ProductSize size )
        {
            var recipe = Recipes.Where( r => r.Size == size ).FirstOrDefault();
            if ( recipe != null )
                return recipe;

            recipe = new Recipe( Guid.NewGuid(), this, size );
            Recipes.Add( recipe );
            return recipe;
        }

        public void RemoveRecipe ( ProductSize size )
        {
            var recipe = Recipes.Where( r => r.Size == size ).FirstOrDefault();
            if ( recipe == null )
                throw new RecipeUndefinedException( this.Name, size.Name );

            Recipes.Remove( recipe );
        }

        public string BuildDescription ()
        {
            if ( Recipes.Count == 0 )
                return "No ingredients defined yet";

            else
            {
                Recipe r = Recipes.ElementAt( 0 );
                IList< string > ingredientNames =
                    r.Ingredients
                        .Select( i => i.IncludedIngredient.Name )
                        .OrderBy( name => name )
                        .ToList();

                return String.Join( " • ", ingredientNames );
            }
        }
    }
}
