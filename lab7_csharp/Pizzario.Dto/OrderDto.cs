﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Dto
{
    public class OrderDto : DomainEntityDto< OrderDto >
    {
        public string CustomerName { get; private set; }

        public string CustomerAddress { get; private set; }

        public string ContactPhone { get; private set; }

        public string ContactEmail { get; private set; }

        public DateTime PlacementTime { get; private set; }

        public string Status { get; private set; }

        public IList< ProductItemDto > Items { get; private set; }

        public decimal BasicCost { get; private set; }

        public decimal TotalCost { get; private set; }

        public decimal DiscountPercentage { get; private set; }

        public String Comment { get; private set; }

        public OrderDto ( Guid domainId,
                          string customerName,
                          string customerAddress,
                          string contactPhone,
                          string contactEmail,
                          DateTime placementTime,
                          string status,
                          IList< ProductItemDto > items,
                          decimal basicCost,
                          decimal totalCost,
                          decimal discountPercentage,
                          String comment )

            :   base( domainId )
        {
            this.CustomerName = customerName;
            this.CustomerAddress = customerAddress;
            this.ContactPhone = contactPhone;
            this.ContactEmail = contactEmail;
            this.PlacementTime = placementTime;
            this.Status = status;
            this.Items = items;
            this.BasicCost = basicCost;
            this.TotalCost = totalCost;
            this.DiscountPercentage = discountPercentage;
            this.Comment = comment;
        }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            var list = new List< object >()
            {
                DomainId,
                BasicCost, TotalCost, DiscountPercentage,
                ContactPhone, ContactEmail, CustomerName, CustomerAddress,
                Status, PlacementTime
            };

            foreach ( var item in Items )
                list.Add( item );

            return list;
        }

        public override string ToString ()
        {
            return string.Format(
                  "OrderId = {0}\nItems:\n{1}Name = {2}\nAddress = {3}\nPhone = {4}\nEmail = {5}\nStatus = {6}\nPlaced = {7}\nBasic Cost = {8}\nTotal Cost = {9}\nDiscount = {10}\nComment = {11}\n",
                  DomainId,
                  string.Join( "\n", Items ),
                  CustomerName,
                  CustomerAddress,
                  ContactPhone,
                  ContactEmail,
                  Status,
                  PlacementTime,
                  BasicCost,
                  TotalCost,
                  DiscountPercentage,
                  Comment
            );
        }
    }
}
