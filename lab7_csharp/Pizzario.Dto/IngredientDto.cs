﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Dto
{
    public class IngredientDto : DomainEntityDto< IngredientDto >
    {
        public string Name { get; private set; }

        public IngredientDto ( Guid domainId, string name )
            :   base( domainId )
        {
            this.Name = name;
        }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List< object > { DomainId, Name };
        }

        public override string ToString ()
        {
            return string.Format( "IngredientId = {0}\nName = {1}\n", DomainId, Name );
        }
    }
}
