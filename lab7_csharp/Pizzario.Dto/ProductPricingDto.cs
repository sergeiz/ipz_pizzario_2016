﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;
using System.Text;

namespace Pizzario.Dto
{
    public class ProductPricingDto : Utils.Value< ProductPricingDto >
    {
        public Guid ProductId { get; private set; }

        public IDictionary< Guid, decimal > PricesBySizeId { get; private set; }

        public IDictionary< Guid, string > SizeNames { get; private set; }

        public ProductPricingDto ( Guid productId, 
                                   IDictionary< Guid, decimal > pricesBySizeId,
                                   IDictionary< Guid, string > sizeNames )
        {
            this.ProductId = productId;
            this.PricesBySizeId = pricesBySizeId;
            this.SizeNames = sizeNames;
        }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            var list = new List< object > { ProductId };
            foreach ( var priceEntry in PricesBySizeId )
            {
                list.Add( priceEntry.Key );
                list.Add( priceEntry.Value );
            }

            return list;
        }

        public override string ToString ()
        {
            StringBuilder pricesAsString = new StringBuilder();
            pricesAsString.Append( "Prices: " );

            foreach ( var priceEntry in PricesBySizeId )
                pricesAsString.Append(
                    string.Format(
                        "{0} = ${1}  ",
                        SizeNames[ priceEntry.Key ],
                        priceEntry.Value
                    )
                );

            return pricesAsString.ToString();
        }
    }
}
