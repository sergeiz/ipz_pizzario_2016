﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Dto
{
    public class ShoppingCartDto : DomainEntityDto< ShoppingCartDto >
    {
        public IList< ProductItemDto > Items { get; private set; }

        public bool Locked { get; private set; }

        public decimal TotalCost { get; private set; }

        public ShoppingCartDto ( Guid domainId, IList< ProductItemDto > items, bool locked, decimal totalCost )
            :   base( domainId )
        {
            this.Items = items;
            this.Locked = locked;
            this.TotalCost = totalCost;
        }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            var list = new List< object >() { DomainId, Locked, TotalCost };

            foreach ( var item in Items )
                list.Add( item );

            return list;
        }

        public override string ToString ()
        {
            return string.Format(
                  "CartId = {0}\nItems:\n{1}Locked = {2}\nCost = {3}\n",
                  DomainId,
                  string.Join( "\n", Items ),
                  Locked,
                  TotalCost
            );
        }
    }
}
