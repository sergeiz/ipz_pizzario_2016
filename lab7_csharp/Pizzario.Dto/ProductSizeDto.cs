﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Dto
{
    public class ProductSizeDto : DomainEntityDto< ProductSizeDto >
    {
        public string Name { get; private set; }

        public int Diameter { get; private set; }

        public int Weight { get; private set; }

        public string ImageUrl { get; private set; }

        public ProductSizeDto ( Guid domainId, string name, int diameter, int weight, string imageUrl )
            :   base( domainId )
        {
            this.Name = name;
            this.Diameter = diameter;
            this.Weight = weight;
            this.ImageUrl = imageUrl;
        }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List< object > { DomainId, Name, Diameter, Weight, ImageUrl };
        }

        public override string ToString ()
        {
            return string.Format( "ProductSizeId = {0}\nName = {1}\nDiameter = {2}\nWeight = {3}\nImage = {4}\n", DomainId, Name, Diameter, Weight, ImageUrl );
        }
    }
}