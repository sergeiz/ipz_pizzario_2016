﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Dto
{
    public class ProductDto : DomainEntityDto< ProductDto >
    {
        public string Name { get; private set; }

        public string ImageUrl { get; private set; }

        public string Description { get; private set; }

        public ProductDto ( Guid domainId, string name, string imageUrl, string description )
            :   base( domainId )
        {
            this.Name = name;
            this.ImageUrl = imageUrl;
            this.Description = description;
        }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List< object > { DomainId, Name, ImageUrl, Description };
        }

        public override string ToString ()
        {
            return string.Format(
                       "ProductId = {0}\nName = {1}\nImage = {2}\nDescription = {3}",
                       DomainId,
                       Name,
                       ImageUrl,
                       Description
                   );
        }
    }
}
