﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Dto
{
    public class AccountDto : DomainEntityDto< AccountDto >
    {
        public string Name { get; private set; }

        public string Email { get; private set; }

        public IList< Guid > ManagedOrderIds { get; private set; }


        public AccountDto ( Guid domainId, string name, string email, IList< Guid > managedOrderIds )
            :   base( domainId )
        {
            this.Name = name;
            this.Email = email;
            this.ManagedOrderIds = managedOrderIds;
        }


        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            var list = new List< object> { DomainId, Name, Email };

            foreach ( var orderId in ManagedOrderIds )
                list.Add( orderId );

            return list;
        }


        public override string ToString ()
        {
            return string.Format( "AccountId = {0}\nName = {1}\nEmail = {2}\nOrders:\n{3}", 
                                  DomainId, 
                                  Name, 
                                  Email,
                                  string.Join( "\n", ManagedOrderIds ) 
            );
        }
    }
}
