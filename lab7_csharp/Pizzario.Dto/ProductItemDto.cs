﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Dto
{
    public class ProductItemDto : DomainEntityDto< ProductItemDto >
    {
        public Guid ProductId { get; private set; }

        public string ProductName { get; private set; }

        public Guid SizeId { get; private set; }

        public string SizeName { get; private set; }

        public int Quantity { get; private set; }

        public decimal FixedPrice { get; private set; }

        public decimal FixedCost { get; private set; }

        public ProductItemDto ( Guid domainId,
                                Guid productId,
                                string productName,
                                Guid sizeId,
                                string sizeName, 
                                int quantity, 
                                decimal fixedPrice,
                                decimal fixedCost )

            :   base( domainId )
        {
            this.ProductId = productId;
            this.ProductName = productName;

            this.SizeId = sizeId;
            this.SizeName = sizeName;

            this.Quantity = quantity;
            this.FixedPrice = fixedPrice;
            this.FixedCost = fixedCost;
        }


        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List<object>() { DomainId, ProductId, ProductName, SizeId, SizeName, Quantity, FixedPrice, FixedCost };
        }


        public override string ToString ()
        {
            return string.Format( "\tItemId = {0}\n\tFixedPrice = {1}\n\tFixedCost = {2}\n\tProduct = {3}\n\tQuantity = {4}\n\tSize = {5}\n",
                                  DomainId, FixedPrice, FixedCost, ProductName, Quantity, SizeName );
        }
    }
}
