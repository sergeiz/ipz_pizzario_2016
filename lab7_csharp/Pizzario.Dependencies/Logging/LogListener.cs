﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Diagnostics.Tracing;

using Microsoft.Practices.EnterpriseLibrary.SemanticLogging;
using Microsoft.Practices.Unity;

namespace Pizzario.Dependencies
{
    public class LogListener : IDisposable
    {
        internal void OnStartup ()
        {
            listenerInfo = FlatFileLog.CreateListener( "pizzario_services.log" );
            listenerInfo.EnableEvents( Log, EventLevel.LogAlways, PizzarioEventSource.Keywords.ServiceTracing );

            listenerErrors = FlatFileLog.CreateListener( "pizzario_diagnostic.log" );
            listenerErrors.EnableEvents( Log, EventLevel.LogAlways, PizzarioEventSource.Keywords.Diagnostic );

            Log.StartupSucceeded();
        }

        public void Dispose ()
        {
            listenerInfo.DisableEvents( Log );
            listenerErrors.DisableEvents( Log );

            listenerInfo.Dispose();
            listenerErrors.Dispose();
        }


        [ Dependency ]
        protected PizzarioEventSource Log { get; set; }

        private EventListener listenerInfo;
        private EventListener listenerErrors;
    }
}
