﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Service;

using System.IO;
using Microsoft.Practices.Unity;


namespace Pizzario.TestApp
{
    class ModelReporter
    {
        public ModelReporter ( IUnityContainer unityContainer, TextWriter output )
        {
            this.unityContainer = unityContainer;
            this.output = output;
        }

        public void GenerateReport ()
        {
            ReportCollection( "Ingredients", unityContainer.Resolve< IIngredientService >() );
            ReportCollection( "Sizes", unityContainer.Resolve< IProductSizeService >() );
            ReportProducts( "Products", unityContainer.Resolve< IProductService >() );
            ReportCollection( "Carts", unityContainer.Resolve< IShoppingCartService >() );
            ReportCollection( "Orders", unityContainer.Resolve< IOrderService >() );
            ReportCollection( "Accounts", unityContainer.Resolve< IAccountService >() );
            ReportCollection( "Cookings", unityContainer.Resolve< ICookingAssignmentService >() );
            ReportCollection( "Deliveries", unityContainer.Resolve< IDeliveryService>() );
        }

        private void ReportProducts ( string title, IProductService service )
        {
            output.WriteLine( "==== {0} ==== ", title );
            output.WriteLine();

            foreach ( var productId in service.ViewAll() )
            {
                output.WriteLine( service.View( productId ) );
                output.WriteLine( service.ViewPrices( productId ) );

                foreach ( var sizeView in service.AvailableSizes( productId ) )
                    output.WriteLine( service.ViewRecipe( productId, sizeView.DomainId ) );

                output.WriteLine();
                output.WriteLine();
            }

            output.WriteLine();
        }

        private void ReportCollection< TDto > ( string title, IDomainEntityService< TDto > service )
            where TDto : Dto.DomainEntityDto< TDto >
        {
            output.WriteLine("==== {0} ==== ", title );
            output.WriteLine();

            foreach ( var entityId in service.ViewAll() )
            {
                output.Write( service.View( entityId ) );

                output.WriteLine();
                output.WriteLine();
            }

            output.WriteLine();
        }

        private IUnityContainer unityContainer;
        private TextWriter output;
    }
}
