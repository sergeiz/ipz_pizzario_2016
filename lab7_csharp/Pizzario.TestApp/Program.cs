﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Dependencies;
using Pizzario.Repository.EntityFramework;

using Microsoft.Practices.Unity;


using System;

namespace Pizzario.TestApp
{
    class Program
    {
        static void Main ( string[] args )
        {
            try
            {
                using ( var dbContext = new PizzarioDbContext( true ) )
                using ( var unityContainer = new UnityContainer() )
                {
                    dbContext.Database.Initialize( true );

                    ContainerBoostraper.RegisterTypes( unityContainer, dbContext );

                    TestModelGenerator generator = new TestModelGenerator( unityContainer );
                    generator.GenerateTestData();
                }

                using ( var dbContext = new PizzarioDbContext() )
                using ( var unityContainer = new UnityContainer() )
                {
                    ContainerBoostraper.RegisterTypes( unityContainer, dbContext );

                    ModelReporter reportGenerator = new ModelReporter( unityContainer, Console.Out );
                    reportGenerator.GenerateReport();
                }
            }
            catch ( Exception e )
            {
                while ( e != null )
                {
                    Console.WriteLine( "{0}: {1}", e.GetType().FullName, e.Message );
                    //Console.WriteLine( e.StackTrace );

                    e = e.InnerException;
                }
            }
        }
    }
}
