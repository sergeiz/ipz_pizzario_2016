﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Service;

using System;
using Microsoft.Practices.Unity;


namespace Pizzario.TestApp
{
    class TestModelGenerator
    {
        public TestModelGenerator ( IUnityContainer unityContainer )
        {
            this.unityContainer = unityContainer;
        }

        public void GenerateTestData ()
        {
            GenerateSizes();
            GenerateIngredients();
            GenerateProducts();
            GenerateCarts();
            GenerateOrders();
            GenerateAccounts();
            ProcessOrders();
            StartCooking();
            FinishCooking();
            Deliver();
        }


        private void GenerateSizes ()
        {
            // ----

            var service = unityContainer.Resolve< IProductSizeService >();

            // ----

            Small  = service.Create( "Small",  "images/pizza_size_small.png",  25, 300 );
            Medium = service.Create( "Medium", "images/pizza_size_medium.png", 30, 600 );
            Large  = service.Create( "Large",  "images/pizza_size_large.png",  35, 900 );

            // ----
        }


        private void GenerateIngredients ()
        {
            // ----

            var service = unityContainer.Resolve< IIngredientService >();

            // ----

            Alfredo                 = service.Create( "Alfredo" );
            Bacon                   = service.Create( "Bacon" );
            Chicken                 = service.Create( "Chicken" );
            ChickenGrill            = service.Create( "Chicken Grill" );
            ChickenSmoked           = service.Create( "Chicken Smoked" );
            Curry                   = service.Create( "Curry" );
            Eggplant                = service.Create( "Eggplant" );
            Ham                     = service.Create( "Ham" );
            Jalapeno                = service.Create( "Jalapeno" );
            Kebab                   = service.Create( "Kebab" );
            Mozzarella              = service.Create( "Mozzarella" );
            Mushroom                = service.Create( "Mushroom" );
            Mussel                  = service.Create( "Mussel" );
            Olive                   = service.Create( "Olive" );
            Onion                   = service.Create( "Onion" );
            Paprika                 = service.Create( "Paprika" );
            Parmesan                = service.Create( "Parmesan" );
            Parsley                 = service.Create( "Parsley" );
            Pepperoni               = service.Create( "Pepperoni" );
            PickledCucumber         = service.Create( "Pickled Cucumber" );
            Pineapple               = service.Create( "Pineapple" );
            Saliami                 = service.Create( "Saliami" );
            Shrimp                  = service.Create( "Shrimp" );
            Squid                   = service.Create( "Squid" );
            Tomato                  = service.Create( "Tomato" );

            // ----
        }

        private void GenerateProducts ()
        {
            // ----

            var service = unityContainer.Resolve< IProductService >();

            // ----

            Mafia = service.Create( "Mafia", "images/pizza_mafia.png" );

            service.DefineRecipeIngredient( Mafia, Small, Mozzarella,   25 );
            service.DefineRecipeIngredient( Mafia, Small, Pineapple,    25 );
            service.DefineRecipeIngredient( Mafia, Small, Chicken,      25 );
            service.DefineRecipeIngredient( Mafia, Small, Saliami,      25 );
            service.DefineRecipeIngredient( Mafia, Small, Tomato,       25 );

            service.DefineRecipeIngredient( Mafia, Medium, Mozzarella,  50 );
            service.DefineRecipeIngredient( Mafia, Medium, Pineapple,   50 );
            service.DefineRecipeIngredient( Mafia, Medium, Chicken,     50 );
            service.DefineRecipeIngredient( Mafia, Medium, Saliami,     50 );
            service.DefineRecipeIngredient( Mafia, Medium, Tomato,      50 );

            service.DefineRecipeIngredient( Mafia, Large, Mozzarella,   75 );
            service.DefineRecipeIngredient( Mafia, Large, Pineapple,    75 );
            service.DefineRecipeIngredient( Mafia, Large, Chicken,      75 );
            service.DefineRecipeIngredient( Mafia, Large, Saliami,      75 );
            service.DefineRecipeIngredient( Mafia, Large, Tomato,       75 );

            service.DefinePrice( Mafia, Small,   73.00M );
            service.DefinePrice( Mafia, Medium,  99.00M );
            service.DefinePrice( Mafia, Large,  125.00M );

            // ----

            Georgia = service.Create( "Georgia", "images/pizza_georgia.jpg" );

            service.DefineRecipeIngredient( Georgia, Small, Kebab,          25 );
            service.DefineRecipeIngredient( Georgia, Small, Mozzarella,     25 );
            service.DefineRecipeIngredient( Georgia, Small, Eggplant,       25 );
            service.DefineRecipeIngredient( Georgia, Small, Onion,          25 );
            service.DefineRecipeIngredient( Georgia, Small, Parsley,        10 );
            service.DefineRecipeIngredient( Georgia, Small, Tomato,         25 );

            service.DefineRecipeIngredient( Georgia, Medium, Kebab,         50 );
            service.DefineRecipeIngredient( Georgia, Medium, Mozzarella,    50 );
            service.DefineRecipeIngredient( Georgia, Medium, Eggplant,      50 );
            service.DefineRecipeIngredient( Georgia, Medium, Onion,         50 );
            service.DefineRecipeIngredient( Georgia, Medium, Parsley,       20 );
            service.DefineRecipeIngredient( Georgia, Medium, Tomato,        50 );

            service.DefineRecipeIngredient( Georgia, Large, Kebab,          75 );
            service.DefineRecipeIngredient( Georgia, Large, Mozzarella,     75 );
            service.DefineRecipeIngredient( Georgia, Large, Eggplant,       75 );
            service.DefineRecipeIngredient( Georgia, Large, Onion,          75 );
            service.DefineRecipeIngredient( Georgia, Large, Parsley,        75 );
            service.DefineRecipeIngredient( Georgia, Large, Tomato,         75 );

            service.DefinePrice( Georgia, Small,     81.00M );
            service.DefinePrice( Georgia, Medium,   103.00M );
            service.DefinePrice( Georgia, Large,    137.00M );

            // ----

            Cossack = service.Create( "Cossack", "images/pizza_cossack.jpg" );

            service.DefineRecipeIngredient( Cossack, Small, Mozzarella,         25 );
            service.DefineRecipeIngredient( Cossack, Small, Bacon,              25 );
            service.DefineRecipeIngredient( Cossack, Small, Ham,                25 );
            service.DefineRecipeIngredient( Cossack, Small, Onion,              25 );
            service.DefineRecipeIngredient( Cossack, Small, PickledCucumber,    25 );
            service.DefineRecipeIngredient( Cossack, Small, Mushroom,           25 );

            service.DefineRecipeIngredient( Cossack, Medium, Mozzarella,        50 );
            service.DefineRecipeIngredient( Cossack, Medium, Bacon,             50 );
            service.DefineRecipeIngredient( Cossack, Medium, Ham,               50 );
            service.DefineRecipeIngredient( Cossack, Medium, Onion,             50 );
            service.DefineRecipeIngredient( Cossack, Medium, PickledCucumber,   50 );
            service.DefineRecipeIngredient( Cossack, Medium, Mushroom,          50 );

            service.DefineRecipeIngredient( Cossack, Large, Mozzarella,         75 );
            service.DefineRecipeIngredient( Cossack, Large, Bacon,              75 );
            service.DefineRecipeIngredient( Cossack, Large, Ham,                75 );
            service.DefineRecipeIngredient( Cossack, Large, Onion,              75 );
            service.DefineRecipeIngredient( Cossack, Large, PickledCucumber,    75 );
            service.DefineRecipeIngredient( Cossack, Large, Mushroom,           75 );

            service.DefinePrice( Cossack, Small,     83.00M );
            service.DefinePrice( Cossack, Medium,   107.00M );
            service.DefinePrice( Cossack, Large,    143.00M );

            // ----

            Hawaii = service.Create( "Hawaii", "images/pizza_hawaii.jpg" );

            service.DefineRecipeIngredient( Hawaii, Small, Curry,           25 );
            service.DefineRecipeIngredient( Hawaii, Small, Mozzarella,      25 );
            service.DefineRecipeIngredient( Hawaii, Small, Pineapple,       25 );
            service.DefineRecipeIngredient( Hawaii, Small, Ham,             25 );
            service.DefineRecipeIngredient( Hawaii, Small, ChickenGrill,    25 );
            service.DefineRecipeIngredient( Hawaii, Small, Tomato,          25 );

            service.DefineRecipeIngredient( Hawaii, Medium, Curry,          50 );
            service.DefineRecipeIngredient( Hawaii, Medium, Mozzarella,     50 );
            service.DefineRecipeIngredient( Hawaii, Medium, Pineapple,      50 );
            service.DefineRecipeIngredient( Hawaii, Medium, Ham,            50 );
            service.DefineRecipeIngredient( Hawaii, Medium, ChickenGrill,   50 );
            service.DefineRecipeIngredient( Hawaii, Medium, Tomato,         50 );

            service.DefineRecipeIngredient( Hawaii, Large, Curry,           75 );
            service.DefineRecipeIngredient( Hawaii, Large, Mozzarella,      75 );
            service.DefineRecipeIngredient( Hawaii, Large, Pineapple,       75 );
            service.DefineRecipeIngredient( Hawaii, Large, Ham,             75 );
            service.DefineRecipeIngredient( Hawaii, Large, ChickenGrill,    75 );
            service.DefineRecipeIngredient( Hawaii, Large, Tomato,          75 );

            service.DefinePrice( Hawaii, Small,   83.00M );
            service.DefinePrice( Hawaii, Medium, 107.00M );
            service.DefinePrice( Hawaii, Large,  143.00M );

            // ----

            FourSeasons = service.Create( "4 Seasons", "images/pizza_4seasons.jpg" );

            service.DefineRecipeIngredient( FourSeasons, Small, Mozzarella,     20 );
            service.DefineRecipeIngredient( FourSeasons, Small, Bacon,          20 );
            service.DefineRecipeIngredient( FourSeasons, Small, Ham,            20 );
            service.DefineRecipeIngredient( FourSeasons, Small, ChickenSmoked,  20 );
            service.DefineRecipeIngredient( FourSeasons, Small, Olive,          10 );
            service.DefineRecipeIngredient( FourSeasons, Small, Paprika,        20 );
            service.DefineRecipeIngredient( FourSeasons, Small, Tomato,         20 );
            service.DefineRecipeIngredient( FourSeasons, Small, Pepperoni,      20 );

            service.DefineRecipeIngredient( FourSeasons, Medium, Mozzarella,    40 );
            service.DefineRecipeIngredient( FourSeasons, Medium, Bacon,         40 );
            service.DefineRecipeIngredient( FourSeasons, Medium, Ham,           40 );
            service.DefineRecipeIngredient( FourSeasons, Medium, ChickenSmoked, 40 );
            service.DefineRecipeIngredient( FourSeasons, Medium, Olive,         20 );
            service.DefineRecipeIngredient( FourSeasons, Medium, Paprika,       40 );
            service.DefineRecipeIngredient( FourSeasons, Medium, Tomato,        40 );
            service.DefineRecipeIngredient( FourSeasons, Medium, Pepperoni,     40 );

            service.DefineRecipeIngredient( FourSeasons, Large, Mozzarella,     60 );
            service.DefineRecipeIngredient( FourSeasons, Large, Bacon,          60 );
            service.DefineRecipeIngredient( FourSeasons, Large, Ham,            60 );
            service.DefineRecipeIngredient( FourSeasons, Large, ChickenSmoked,  60 );
            service.DefineRecipeIngredient( FourSeasons, Large, Olive,          60 );
            service.DefineRecipeIngredient( FourSeasons, Large, Paprika,        60 );
            service.DefineRecipeIngredient( FourSeasons, Large, Tomato,         60 );
            service.DefineRecipeIngredient( FourSeasons, Large, Pepperoni,      60 );

            service.DefinePrice( FourSeasons, Small,     92.00M );
            service.DefinePrice( FourSeasons, Medium,   115.00M );
            service.DefinePrice( FourSeasons, Large,    153.00M );

            // ----

            Margharita = service.Create( "Margharita", "images/pizza_margharita.jpg" );

            service.DefineRecipeIngredient( Margharita, Small, Parmesan,    50 );
            service.DefineRecipeIngredient( Margharita, Small, Mozzarella,  50 );
            service.DefineRecipeIngredient( Margharita, Small, Tomato,      50 );

            service.DefineRecipeIngredient( Margharita, Medium, Parmesan,   100 );
            service.DefineRecipeIngredient( Margharita, Medium, Mozzarella, 100 );
            service.DefineRecipeIngredient( Margharita, Medium, Tomato,     100 );

            service.DefineRecipeIngredient( Margharita, Large, Parmesan,    150 );
            service.DefineRecipeIngredient( Margharita, Large, Mozzarella,  150 );
            service.DefineRecipeIngredient( Margharita, Large, Tomato,      150 );

            service.DefinePrice( Margharita, Small,     61.00M );
            service.DefinePrice( Margharita, Medium,    85.00M );
            service.DefinePrice( Margharita, Large,    101.00M );

            // ----

            Marinara = service.Create( "Marinara", "images/pizza_marinara.jpg" );

            service.DefineRecipeIngredient( Marinara, Small, Mozzarella,    25 );
            service.DefineRecipeIngredient( Marinara, Small, Squid,         25 );
            service.DefineRecipeIngredient( Marinara, Small, Shrimp,        25 );
            service.DefineRecipeIngredient( Marinara, Small, Olive,         25 );
            service.DefineRecipeIngredient( Marinara, Small, Mussel,        25 );
            service.DefineRecipeIngredient( Marinara, Small, Alfredo,       25 );

            service.DefineRecipeIngredient( Marinara, Medium, Mozzarella,   50 );
            service.DefineRecipeIngredient( Marinara, Medium, Squid,        50 );
            service.DefineRecipeIngredient( Marinara, Medium, Shrimp,       50 );
            service.DefineRecipeIngredient( Marinara, Medium, Olive,        50 );
            service.DefineRecipeIngredient( Marinara, Medium, Mussel,       50 );
            service.DefineRecipeIngredient( Marinara, Medium, Alfredo,      50 );

            service.DefineRecipeIngredient( Marinara, Large, Mozzarella,    75 );
            service.DefineRecipeIngredient( Marinara, Large, Squid,         75 );
            service.DefineRecipeIngredient( Marinara, Large, Shrimp,        75 );
            service.DefineRecipeIngredient( Marinara, Large, Olive,         75 );
            service.DefineRecipeIngredient( Marinara, Large, Mussel,        75 );
            service.DefineRecipeIngredient( Marinara, Large, Alfredo,       75 );

            service.DefinePrice( Marinara, Small,       99.00M );
            service.DefinePrice( Marinara, Medium,     127.00M );
            service.DefinePrice( Marinara, Large,      161.00M );

            // ----

            Mexican = service.Create( "Mexican", "images/pizza_mexican.jpg" );

            service.DefineRecipeIngredient( Mexican, Small, Mozzarella,     25 );
            service.DefineRecipeIngredient( Mexican, Small, Ham,            25 );
            service.DefineRecipeIngredient( Mexican, Small, ChickenGrill,   25 );
            service.DefineRecipeIngredient( Mexican, Small, Onion,          25 );
            service.DefineRecipeIngredient( Mexican, Small, Jalapeno,       25 );
            service.DefineRecipeIngredient( Mexican, Small, Tomato,         25 );

            service.DefineRecipeIngredient( Mexican, Medium, Mozzarella,    50 );
            service.DefineRecipeIngredient( Mexican, Medium, Ham,           50 );
            service.DefineRecipeIngredient( Mexican, Medium, ChickenGrill,  50 );
            service.DefineRecipeIngredient( Mexican, Medium, Onion,         50 );
            service.DefineRecipeIngredient( Mexican, Medium, Jalapeno,      50 );
            service.DefineRecipeIngredient( Mexican, Medium, Tomato,        50 );

            service.DefineRecipeIngredient( Mexican, Large, Mozzarella,     75 );
            service.DefineRecipeIngredient( Mexican, Large, Ham,            75 );
            service.DefineRecipeIngredient( Mexican, Large, ChickenGrill,   75 );
            service.DefineRecipeIngredient( Mexican, Large, Onion,          75 );
            service.DefineRecipeIngredient( Mexican, Large, Jalapeno,       75 );
            service.DefineRecipeIngredient( Mexican, Large, Tomato,         75 );

            service.DefinePrice( Mexican, Small,     89.00M );
            service.DefinePrice( Mexican, Medium,   112.00M );
            service.DefinePrice( Mexican, Large,    139.00M );

            // ----
        }


        private void GenerateCarts ()
        {
            // ----

            var service = unityContainer.Resolve< IShoppingCartService >();

            // ----

            Cart = service.CreateNew();

            service.SetItem( Cart, Mafia,   Large,  1 );
            service.SetItem( Cart, Georgia, Small,  2 );
            service.SetItem( Cart, Cossack, Medium, 1 );

            service.LockCart( Cart );

            // ----
        }


        private void GenerateOrders ()
        {
            // ----

            var service = unityContainer.Resolve< IOrderService >();

            // ----

            Order = service.CreateNew(
                "Ivan Kuziakin",
                "Sumskaya 1",
                "(050)123-45-67",
                "ivan.kuziakin@ukr.net",
                "<no comment>",
                Cart
            );

            service.SetDiscount( Order, 20.00M );

            // ----
        }


        private void GenerateAccounts ()
        {
            // ----

            var service = unityContainer.Resolve< IAccountService >();

            // ----

            Wasya = service.CreateOperator( "Wasya Pupkin", "wasya.pupkin@pizzario.com", "12345");

            // ----
        }


        private void ProcessOrders ()
        {
            // ----

            var service = unityContainer.Resolve< IOrderService >();

            // ----

            foreach ( var orderId in service.ViewUnconfirmed() )
                service.Confirm( orderId, Wasya );

            // ----
        }


        private void StartCooking ()
        {
            // ----

            var service = unityContainer.Resolve< ICookingAssignmentService >();

            // ----

            foreach ( var assignmentId in service.ViewWaiting() )
                service.MarkCookingStarted( assignmentId );

            // ----
        }


        private void FinishCooking ()
        {
            // ----

            var service = unityContainer.Resolve< ICookingAssignmentService >();

            // ----

            foreach ( var assignmentId in service.ViewCookedRightNow() )
                service.MarkCookingFinished( assignmentId );

            // ----
        }


        private void Deliver ()
        {
            // ----

            var service = unityContainer.Resolve< IDeliveryService >();

            // ----

            foreach ( var deliveryId in service.ViewWaiting() )
            {
                service.MarkStarted( deliveryId, "Wasya" );
                service.MarkDelivered( deliveryId );
            }

            // ----
        }

        private Guid Large, Medium, Small;

        private Guid Chicken, ChickenGrill, ChickenSmoked, Saliami, Pepperoni, Kebab;
        private Guid Mozzarella, Pineapple, Tomato, Olive, Eggplant, Curry, Paprika;
        private Guid Onion, Parsley, Bacon, Ham, PickledCucumber, Mushroom, Parmesan;
        private Guid Squid, Shrimp, Mussel, Alfredo, Jalapeno;

        private Guid Mafia, Georgia, Cossack, Hawaii, FourSeasons, Margharita, Marinara, Mexican;

        private Guid Cart;
        private Guid Order;
        private Guid Wasya;

        private IUnityContainer unityContainer;
    }

}
