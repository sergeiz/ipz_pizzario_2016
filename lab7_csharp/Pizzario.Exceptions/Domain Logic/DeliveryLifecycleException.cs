﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace Pizzario.Exceptions
{
    public class DeliveryLifecycleException : DomainLogicException
    {
        private DeliveryLifecycleException ( string message )
            : base( message )
        { }

        public static DeliveryLifecycleException MakeStartWhenNotWaiting ( Guid deliveryId )
        {
            return new DeliveryLifecycleException( 
                string.Format(
                    "Delivery #{0} may be started in Waiting state only",
                    deliveryId
                )
            );
        }

        public static DeliveryLifecycleException MakeFinishWhenNotInProgress ( Guid deliveryId )
        {
            return new DeliveryLifecycleException( 
                string.Format(
                    "Delivery #{0} may be finished in InProgress state only",
                    deliveryId
                )
            );
        }
    }
}
