﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

namespace Pizzario.Exceptions
{
    public class RemovingUnusedIngredientException : DomainLogicException
    {
        public RemovingUnusedIngredientException ( string productName, string ingredientName )

            : base( string.Format( "Removing unregistered ingredient \"{0}\" from product \"{1}\"",
                                    ingredientName, productName ) )
        { }
    }
}
