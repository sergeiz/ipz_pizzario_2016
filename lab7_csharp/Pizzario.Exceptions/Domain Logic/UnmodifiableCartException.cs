﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace Pizzario.Exceptions
{
    public class UnmodifiableCartException : DomainLogicException
    {
        public UnmodifiableCartException ( Guid cartId )
            :   base( string.Format( "Attempted to modify a locked shopping cart #{0}", cartId ) )
        { }
    }
}
