﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

using Pizzario.Dto;

namespace Pizzario.Web.Models
{
    public class MenuViewModel
    {
        public IList< ProductDto > Products { get; set; }

        public IList< ProductSizeDto > Sizes { get; set; }

        public IDictionary< Guid, IDictionary< Guid, decimal > > ProductPrices { get; set; }
    }
}
