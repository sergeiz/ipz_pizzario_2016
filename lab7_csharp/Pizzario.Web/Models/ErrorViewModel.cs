﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

namespace Pizzario.Web.Models
{
    public class ErrorViewModel
    {
        public string ErrorTitle { get; set; }

        public string ErrorMessage { get; set; }
    }
}
