﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Dto;

namespace Pizzario.Web.Models
{
    public class OrderViewModel
    {
        public OrderDto Order { get; set; }
    }
}
