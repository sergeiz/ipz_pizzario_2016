﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Dto;

namespace Pizzario.Web.Models
{
    public class CartViewModel
    {
        public ShoppingCartDto Cart { get; set; }
    }
}
