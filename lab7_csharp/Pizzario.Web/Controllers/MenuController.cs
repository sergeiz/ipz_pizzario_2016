﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Practices.Unity;

using Pizzario.Dto;
using Pizzario.Service;
using Pizzario.Web.Models;


namespace Pizzario.Web.Controllers
{
    public class MenuController : Controller
    {
        public IActionResult Index ()
        {
            var viewModel = new MenuViewModel();
            viewModel.Products = GetProductslist();
            viewModel.Sizes = GetProductSizesList();
            viewModel.ProductPrices = GetProductPricesMap();

            return View( viewModel );
        }

        private IList< ProductDto > GetProductslist ()
        {
            IList< Guid > productIds = productService.ViewAll();

            IList< ProductDto > result = new List< ProductDto >();
            foreach ( var productId in productIds )
                result.Add( productService.View( productId ) );

            return result.OrderBy( p => p.Name ).ToList();
        }


        private List< ProductSizeDto > GetProductSizesList ()
        {
            IList< Guid > productSizeIds = productSizeService.ViewAll();

            IList< ProductSizeDto > result = new List< ProductSizeDto >();
            foreach ( var productSizeId in productSizeIds )
                result.Add( productSizeService.View( productSizeId ) );

            return result.OrderBy( sz => sz.Diameter ).ToList();
        }


        private IDictionary< Guid, IDictionary< Guid, decimal > > GetProductPricesMap ()
        {
            var result = new Dictionary< Guid, IDictionary< Guid, decimal > >();

            foreach ( Guid productId in productService.ViewAll() )
            {
                ProductPricingDto pricingDto = productService.ViewPrices( productId );
                result.Add( productId, pricingDto.PricesBySizeId );
            }

            return result;
        }


        [ Dependency ]
        protected IProductService productService { get; set; }

        [ Dependency ]
        protected IProductSizeService productSizeService { get; set; }
    }
}
