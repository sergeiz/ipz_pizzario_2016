﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Practices.Unity;

using Pizzario.Dto;
using Pizzario.Service;
using Pizzario.Web.Models;
using Pizzario.Web.Utils;


namespace Pizzario.Web.Controllers
{
    public class CheckoutController : Controller
    {
        public IActionResult Index ()
        {
            ShoppingCartDto cartDto = CurrentCart();

            var viewModel = new CartViewModel();
            viewModel.Cart = cartDto;

            return View( viewModel );
        }


        [ HttpPost ]
        public IActionResult Index ( 
            string inputName,
            string inputAddress,
            string inputEmail,
            string inputPhone,
            string inputComment 
        )
        {
            ShoppingCartDto cartDto = CurrentCart();
            if ( cartDto == null || cartDto.Items.Count == 0 )
                return new StatusCodeResult( 400 );

            cartService.LockCart( cartDto.DomainId );

            Guid orderId = orderService.CreateNew(
                                inputName,
                                inputAddress,
                                inputPhone,
                                inputEmail,
                                inputComment,
                                cartDto.DomainId
                           );

            var sessionHelper = new SessionHelper( HttpContext.Session );
            sessionHelper.SetOrderId( orderId );
            sessionHelper.ResetCartId();

            return new RedirectToActionResult( "Index", "Order", null );
        }


        private ShoppingCartDto CurrentCart ()
        {
            SessionHelper helper = new SessionHelper( HttpContext.Session );
            Guid? cartId = helper.GetCartId();
            return ( cartId.HasValue ) ? cartService.View( cartId.Value ) : null;
        }
   

        [ Dependency ]
        protected IShoppingCartService cartService { get; set; }

        [ Dependency ]
        protected IOrderService orderService { get; set; }
    }
}
