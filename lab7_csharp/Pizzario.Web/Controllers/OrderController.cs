﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Practices.Unity;

using Pizzario.Service;
using Pizzario.Web.Models;
using Pizzario.Web.Utils;

namespace Pizzario.Web.Controllers
{
    public class OrderController : Controller
    {
        public IActionResult Index()
        {
            var sessionHelper = new SessionHelper( HttpContext.Session );
            Guid? orderId = sessionHelper.GetOrderId();
            if ( ! orderId.HasValue )
            {
                var errorViewModel = new ErrorViewModel();
                errorViewModel.ErrorTitle = "No Current Order";
                errorViewModel.ErrorMessage = "Current session is not associated with any order yet";

                return View( "Error", errorViewModel );
            }


            var viewModel = new OrderViewModel();
            viewModel.Order = orderService.View( orderId.Value );

            return View( viewModel );
        }


        [ Dependency ]
        protected IOrderService orderService { get; set; }
    }
}
