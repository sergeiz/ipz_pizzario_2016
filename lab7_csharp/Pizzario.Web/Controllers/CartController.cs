﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Practices.Unity;

using Pizzario.Dto;
using Pizzario.Service;
using Pizzario.Web.Models;
using Pizzario.Web.Utils;

namespace Pizzario.Web.Controllers
{
    public class CartController : Controller
    {
        public IActionResult Index ()
        {
            ShoppingCartDto cartDto = CurrentCart();

            var viewModel = new CartViewModel();
            viewModel.Cart = cartDto;

            return PartialView( viewModel ) ;
        }


        [ HttpPost ]
        public void AddItem ( Guid productId, Guid sizeId )
        {
            Guid cartId = CurrentCartId();
            cartService.AddItem( cartId, productId, sizeId );
        }


        [ HttpPost ]
        public void SetItem ( Guid productId, Guid sizeId, int newQuantity )
        {
            Guid cartId = CurrentCartId();
            cartService.SetItem( cartId, productId, sizeId, newQuantity );
        }


        [ HttpPost ]
        public void RemoveItem ( Guid productId, Guid sizeId )
        {
            Guid cartId = CurrentCartId();
            cartService.RemoveItem( cartId, productId, sizeId );
        }


        [ HttpPost ]
        public void Clear ()
        {
            Guid cartId = CurrentCartId();
            cartService.ClearItems( cartId );
        }


        private Guid CurrentCartId ()
        {
            SessionHelper helper = new SessionHelper( HttpContext.Session );
            return helper.GetCartId().Value;
        }


        private ShoppingCartDto CurrentCart ()
        {
            SessionHelper helper = new SessionHelper( HttpContext.Session );
            Guid? cartId = helper.GetCartId();
            if ( cartId == null )
            {
                cartId = cartService.CreateNew();
                helper.SetCartId( cartId.Value );
            }

            return cartService.View( cartId.Value );
        }


        [ Dependency ]
        protected IShoppingCartService cartService { get; set; }
    }
}
