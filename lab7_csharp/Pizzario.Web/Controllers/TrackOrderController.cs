﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Practices.Unity;

using Pizzario.Dto;
using Pizzario.Service;
using Pizzario.Web.Models;
using Pizzario.Web.Utils;


namespace Pizzario.Web.Controllers
{
    public class TrackOrderController : Controller
    {
        [ HttpPost ]
        public IActionResult Index ( Guid inputTrackOrderId,
                                     string inputTrackOrderEmail )
        {
            OrderDto orderDto = orderService.FindOrder( inputTrackOrderId );
            if ( orderDto == null )
            {
                var errorViewModel = new ErrorViewModel();
                errorViewModel.ErrorTitle = "Order Not Found";
                errorViewModel.ErrorMessage = string.Format( "Order #{0} is unknown", inputTrackOrderId );

                return View( "Error", errorViewModel );
            }

            else if ( orderDto.ContactEmail != inputTrackOrderEmail )
            {
                var errorViewModel = new ErrorViewModel();
                errorViewModel.ErrorTitle = "Order Access Violation";
                errorViewModel.ErrorMessage = "Order does not belong to " + inputTrackOrderEmail;

                return View( "Error", errorViewModel );
            }

            var sessionHelper = new SessionHelper( HttpContext.Session );
            sessionHelper.SetOrderId( inputTrackOrderId );

            return new RedirectToActionResult( "Index", "Order", null );
        }


        [ Dependency ]
        protected IOrderService orderService { get; set; }
    }
}
