﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;
using System.Globalization;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Controllers;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Practices.Unity;

using Pizzario.Web.Utils;
using Pizzario.Repository.EntityFramework;
using Pizzario.Dependencies;

namespace Pizzario.Web
{
    public class Startup : IDisposable
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public IServiceProvider ConfigureServices ( IServiceCollection services )
        {
            dbContext = new PizzarioDbContext();

            services.AddLocalization( options => options.ResourcesPath = "Resources" );

            services.AddMvc()
                .AddViewLocalization()
                .AddDataAnnotationsLocalization();

            services.AddSession();

            services.Configure< RequestLocalizationOptions >(
                options =>
                {
                    var supportedCultures = new List< CultureInfo >
                        {
                            new CultureInfo( "en-US" ),
                            new CultureInfo( "ru-RU" ),
                            new CultureInfo( "uk-UA" )
                        };

                    options.DefaultRequestCulture = new RequestCulture( culture: "en-US", uiCulture: "en-US" );
                    options.SupportedCultures = supportedCultures;
                    options.SupportedUICultures = supportedCultures;
                } );

            var unityServiceProvider = new UnityServiceProvider();
            IUnityContainer container = unityServiceProvider.UnityContainer;
            services.AddSingleton< IControllerActivator >( new UnityControllerActivator( container ) );

            var defaultProvider = services.BuildServiceProvider();
            container.AddExtension( new UnityFallbackProviderExtension( defaultProvider ) );

            ContainerBoostraper.RegisterTypes( container, dbContext );

            return unityServiceProvider;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure ( IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory )
        {
            app.Use( async ( context, next ) =>
            {
                await next();
                if ( context.Response.StatusCode == 404 )
                {
                    context.Request.QueryString = QueryString.Create( "errorPath", context.Request.Path );
                    context.Request.Path = "/Home/Error404";
                    await next();
                }
            } );

            app.UseStaticFiles();

            var locOptions = app.ApplicationServices.GetService< IOptions< RequestLocalizationOptions > >();
            app.UseRequestLocalization( locOptions.Value );

            app.UseSession();

            app.UseExceptionHandler( "/Home/FatalError" );

            app.UseMvcWithDefaultRoute();

            loggerFactory.AddConsole();
        }

        public void Dispose ()
        {
            dbContext.Dispose();
        }

        private PizzarioDbContext dbContext;
    }
}
