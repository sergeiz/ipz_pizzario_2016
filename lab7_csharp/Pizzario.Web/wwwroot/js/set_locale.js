/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

function setLocale ( lang ) {
    $.post(
        "/Home/SetLocale",
        {
            language: lang
        },
        function () {
            location.reload();
        }
    ).fail( function() {
        alert( "Failed to change locale" );
    });
}