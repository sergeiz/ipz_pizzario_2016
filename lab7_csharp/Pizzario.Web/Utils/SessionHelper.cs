﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

using Microsoft.AspNetCore.Http;


namespace Pizzario.Web.Utils
{
    public class SessionHelper
    {
        public SessionHelper ( ISession session )
        {
            this.session = session;
        }

        public Guid? GetCartId ()
        {
            string result = session.GetString( CartId );
            if ( result == null || result.Length == 0 )
                return null;

            return Guid.Parse( result );
        }

        public void SetCartId ( Guid cartId )
        {
            session.SetString( CartId, cartId.ToString() );
        }

        public void ResetCartId ()
        {
            session.Remove( CartId );
        }

        public Guid? GetOrderId ()
        {
            string result = session.GetString( OrderId );
            if ( result == null || result.Length == 0 )
                return null;

            return Guid.Parse( result );
        }

        public void SetOrderId ( Guid orderId )
        {
            session.SetString( OrderId, orderId.ToString() );
        }


        private static readonly string CartId = "CART_ID";
        private static readonly string OrderId = "ORDER_ID";

        private ISession session;
    }
}
