/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.testapp;

import pizzario.repository.jpa.PizzarioJPAContext;

public class Program
{

    public static void main ( String[] args )
    {
        try
        {
            try ( PizzarioJPAContext jpaContext = new PizzarioJPAContext( "PizzarioCreate" ) )
            {
                ServiceProvider serviceProvider = new ServiceProvider( jpaContext.getEntityManager() );

                TestModelGenerator generator = new TestModelGenerator( serviceProvider );
                generator.generateTestData();
            }

            try ( PizzarioJPAContext jpaContext = new PizzarioJPAContext( "PizzarioRead" ) )
            {
                ServiceProvider serviceProvider = new ServiceProvider( jpaContext.getEntityManager() );

                ModelReporter reportGenerator = new ModelReporter( serviceProvider, System.out );
                reportGenerator.generateReport();
            }
        } catch ( Exception e )
        {
            System.out.println( e.getClass().getName() );
            System.out.println( e.getMessage() );
            e.printStackTrace( System.out );
        }
    }
}
