/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.testapp;

import pizzario.dto.DomainEntityDto;
import pizzario.dto.ProductSizeDto;
import pizzario.service.IDomainEntityService;
import pizzario.service.IProductService;

import java.io.PrintStream;
import java.util.UUID;


public class ModelReporter
{

    public ModelReporter ( ServiceProvider serviceProvider, PrintStream output )
    {
        this.serviceProvider = serviceProvider;
        this.output = output;
    }

    public void generateReport ()
    {
        reportCollection( "Ingredients", serviceProvider.provideIngredientService() );
        reportCollection( "Sizes", serviceProvider.provideProductSizeService() );
        reportProducts( "Products", serviceProvider.provideProductService() );
        reportCollection( "Carts", serviceProvider.provideShoppingCartService() );
        reportCollection( "Orders", serviceProvider.provideOrderService() );
        reportCollection( "Accounts", serviceProvider.provideAccountService() );
        reportCollection( "Cookings", serviceProvider.provideCookingAssignmentService() );
        reportCollection( "Deliveries", serviceProvider.provideDeliveryService() );
    }


    private void reportProducts ( String title, IProductService service )
    {
        output.format( "==== %s ====\n\n", title );

        for ( UUID productId : service.viewAll() )
        {
            output.println( service.view( productId ) );
            output.println( service.viewPrices( productId ) );

            for ( ProductSizeDto sizeView : service.availableSizes( productId ) )
            {
                output.format( "\tSize = %s\n\t", sizeView.getName() );
                output.println( service.viewRecipe( productId, sizeView.getDomainId() ) );
            }

            output.print( "\n\n" );
        }

        output.println();
    }


    private < TViewModel extends DomainEntityDto >
    void reportCollection ( String title, IDomainEntityService< TViewModel > service )
    {
        output.format( "==== %s ====\n\n", title );

        for ( UUID itemUuid : service.viewAll() )
        {
            output.print( service.view( itemUuid ) );
            output.print( "\n\n" );
        }

        output.println();
    }

    private ServiceProvider serviceProvider;
    private PrintStream output;

}
