/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.testapp;

import pizzario.service.*;

import java.math.BigDecimal;
import java.util.UUID;


public class TestModelGenerator
{

    public TestModelGenerator ( ServiceProvider serviceProvider )
    {
        this.serviceProvider = serviceProvider;
    }

    public void generateTestData ()
    {
        generateSizes();
        generateIngredients();
        generateProducts();
        generateCarts();
        generateOrders();
        generateAccounts();
        startCooking();
        finishCooking();
        deliver();
    }


    private void generateSizes ()
    {
        // ----

        IProductSizeService service = serviceProvider.provideProductSizeService();

        // ----

        Small = service.create( "Small", 25, 300.0 );
        Medium = service.create( "Medium", 30, 600.0 );
        Large = service.create( "Large", 35, 900.0 );
    }


    private void generateIngredients ()
    {
        // ----

        IIngredientService service = serviceProvider.provideIngredientService();

        // ----

        Mozarella = service.create( "Mozarella" );
        Pineaple = service.create( "Pineaple" );
        Chicken = service.create( "Chicken" );
        Saliami = service.create( "Saliami" );
        Tomato = service.create( "Tomato" );
        Kebab = service.create( "Kebab" );
        Eggplant = service.create( "Eggplant" );
        Onion = service.create( "Onion" );
        Parsley = service.create( "Parsley" );
        Bacon = service.create( "Bacon" );
        Ham = service.create( "Ham" );
        PickledCucumber = service.create( "Pickled Cucumber" );
        Mushroom = service.create( "Mushroom" );
    }

    private void generateProducts ()
    {
        // ----

        IProductService service = serviceProvider.provideProductService();

        // ----

        Mafia = service.create( "Mafia", "http://pizzario.com/images/mafia.jpg" );

        service.defineRecipeIngredient( Mafia, Small, Mozarella, 25.0 );
        service.defineRecipeIngredient( Mafia, Small, Pineaple, 25.0 );
        service.defineRecipeIngredient( Mafia, Small, Chicken, 25.0 );
        service.defineRecipeIngredient( Mafia, Small, Saliami, 25.0 );
        service.defineRecipeIngredient( Mafia, Small, Tomato, 25.0 );

        service.defineRecipeIngredient( Mafia, Medium, Mozarella, 50.0 );
        service.defineRecipeIngredient( Mafia, Medium, Pineaple, 50.0 );
        service.defineRecipeIngredient( Mafia, Medium, Chicken, 50.0 );
        service.defineRecipeIngredient( Mafia, Medium, Saliami, 50.0 );
        service.defineRecipeIngredient( Mafia, Medium, Tomato, 50.0 );

        service.defineRecipeIngredient( Mafia, Large, Mozarella, 75.0 );
        service.defineRecipeIngredient( Mafia, Large, Pineaple, 75.0 );
        service.defineRecipeIngredient( Mafia, Large, Chicken, 75.0 );
        service.defineRecipeIngredient( Mafia, Large, Saliami, 75.0 );
        service.defineRecipeIngredient( Mafia, Large, Tomato, 75.0 );

        service.definePrice( Mafia, Small, BigDecimal.valueOf( 73.00 ) );
        service.definePrice( Mafia, Medium, BigDecimal.valueOf( 99.00 ) );
        service.definePrice( Mafia, Large, BigDecimal.valueOf( 125.00 ) );

        // ----

        Georgia = service.create( "Georgia", "http://pizzario.com/images/georgia.jpg" );

        service.defineRecipeIngredient( Georgia, Small, Kebab, 25.0 );
        service.defineRecipeIngredient( Georgia, Small, Mozarella, 25.0 );
        service.defineRecipeIngredient( Georgia, Small, Eggplant, 25.0 );
        service.defineRecipeIngredient( Georgia, Small, Onion, 25.0 );
        service.defineRecipeIngredient( Georgia, Small, Parsley, 10.0 );
        service.defineRecipeIngredient( Georgia, Small, Tomato, 25.0 );

        service.defineRecipeIngredient( Georgia, Medium, Kebab, 50.0 );
        service.defineRecipeIngredient( Georgia, Medium, Mozarella, 50.0 );
        service.defineRecipeIngredient( Georgia, Medium, Eggplant, 50.0 );
        service.defineRecipeIngredient( Georgia, Medium, Onion, 50.0 );
        service.defineRecipeIngredient( Georgia, Medium, Parsley, 20.0 );
        service.defineRecipeIngredient( Georgia, Medium, Tomato, 50.0 );

        service.defineRecipeIngredient( Georgia, Large, Kebab, 75.0 );
        service.defineRecipeIngredient( Georgia, Large, Mozarella, 75.0 );
        service.defineRecipeIngredient( Georgia, Large, Eggplant, 75.0 );
        service.defineRecipeIngredient( Georgia, Large, Onion, 75.0 );
        service.defineRecipeIngredient( Georgia, Large, Parsley, 75.0 );
        service.defineRecipeIngredient( Georgia, Large, Tomato, 75.0 );

        service.definePrice( Georgia, Small, BigDecimal.valueOf( 81.00 ) );
        service.definePrice( Georgia, Medium, BigDecimal.valueOf( 103.00 ) );
        service.definePrice( Georgia, Large, BigDecimal.valueOf( 137.00 ) );

        // ----

        Cossack = service.create( "Cossack", "http://pizzario.com/images/cosscack.jpg" );

        service.defineRecipeIngredient( Cossack, Small, Mozarella, 25.0 );
        service.defineRecipeIngredient( Cossack, Small, Bacon, 25.0 );
        service.defineRecipeIngredient( Cossack, Small, Ham, 25.0 );
        service.defineRecipeIngredient( Cossack, Small, Onion, 25.0 );
        service.defineRecipeIngredient( Cossack, Small, PickledCucumber, 25.0 );
        service.defineRecipeIngredient( Cossack, Small, Mushroom, 25.0 );

        service.defineRecipeIngredient( Cossack, Medium, Mozarella, 50.0 );
        service.defineRecipeIngredient( Cossack, Medium, Bacon, 50.0 );
        service.defineRecipeIngredient( Cossack, Medium, Ham, 50.0 );
        service.defineRecipeIngredient( Cossack, Medium, Onion, 50.0 );
        service.defineRecipeIngredient( Cossack, Medium, PickledCucumber, 50.0 );
        service.defineRecipeIngredient( Cossack, Medium, Mushroom, 50.0 );

        service.defineRecipeIngredient( Cossack, Large, Mozarella, 75.0 );
        service.defineRecipeIngredient( Cossack, Large, Bacon, 75.0 );
        service.defineRecipeIngredient( Cossack, Large, Ham, 75.0 );
        service.defineRecipeIngredient( Cossack, Large, Onion, 75.0 );
        service.defineRecipeIngredient( Cossack, Large, PickledCucumber, 75.0 );
        service.defineRecipeIngredient( Cossack, Large, Mushroom, 75.0 );

        service.definePrice( Cossack, Small, BigDecimal.valueOf( 83.00 ) );
        service.definePrice( Cossack, Medium, BigDecimal.valueOf( 107.00 ) );
        service.definePrice( Cossack, Large, BigDecimal.valueOf( 143.00 ) );

        // ----
    }


    private void generateCarts ()
    {
        // ----

        IShoppingCartService service = serviceProvider.provideShoppingCartService();

        // ----

        cart = service.createNew();

        service.setItem( cart, Mafia, Large, 1 );
        service.setItem( cart, Georgia, Small, 2 );
        service.setItem( cart, Cossack, Medium, 1 );

        service.lock( cart );

        // ----
    }

    private void generateOrders ()
    {
        // ----

        IOrderService service = serviceProvider.provideOrderService();

        // ----

        order = service.createNew( "Sumskaya 1", "123-45-67", cart );

        service.setDiscount( order, BigDecimal.valueOf( 20.00 ) );

        service.confirm( order );

        // ----
    }


    private void generateAccounts ()
    {
        // ----

        IAccountService service = serviceProvider.provideAccountService();

        // ----

        UUID wasya = service.createOperator( "Wasya Pupkin", "wasya.pupkin@pizzario.com", "12345" );

        service.trackOrder( wasya, order );

        // ----
    }

    private void startCooking ()
    {
        // ----

        ICookingAssignmentService service = serviceProvider.provideCookingAssignmentService();

        // ----

        for ( UUID assignmentId : service.viewWaiting() )
            service.markCookingStarted( assignmentId );

        // ----
    }


    private void finishCooking ()
    {
        // ----

        ICookingAssignmentService service = serviceProvider.provideCookingAssignmentService();

        // ----

        for ( UUID assignmentId : service.viewCookedRightNow() )
            service.markCookingFinished( assignmentId );

        // ----
    }


    private void deliver ()
    {
        // ----

        IDeliveryService service = serviceProvider.provideDeliveryService();

        // ----

        for ( UUID deliveryId : service.viewWaiting() )
        {
            service.markStarted( deliveryId, "Wasya" );
            service.markDelivered( deliveryId );
        }

        // ----
    }


    private UUID Large, Medium, Small;
    private UUID Mozarella, Pineaple, Chicken, Saliami, Tomato, Kebab, Eggplant;
    private UUID Onion, Parsley, Bacon, Ham, PickledCucumber, Mushroom;
    private UUID Mafia, Georgia, Cossack;
    private UUID cart;
    private UUID order;


    private ServiceProvider serviceProvider;

}
