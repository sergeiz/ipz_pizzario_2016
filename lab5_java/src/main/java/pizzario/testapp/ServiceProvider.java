/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.testapp;

import pizzario.repository.*;
import pizzario.repository.jpa.RepositoryFactory;
import pizzario.service.*;
import pizzario.service.impl.ServiceFactory;

import javax.persistence.EntityManager;

public class ServiceProvider
{

    public ServiceProvider ( EntityManager entityManager )
    {
        this.entityManager = entityManager;
    }

    public IIngredientService provideIngredientService ()
    {

        IIngredientRepository ingredientRepository = RepositoryFactory.makeIngredientRepository( entityManager );
        return ServiceFactory.makeIngredientService( ingredientRepository );
    }

    public IProductSizeService provideProductSizeService ()
    {

        IProductSizeRepository sizeRepository = RepositoryFactory.makeProductSizeRepository( entityManager );
        return ServiceFactory.makeProductSizeService( sizeRepository );
    }

    public IProductService provideProductService ()
    {

        IProductRepository productRepository = RepositoryFactory.makeProductRepository( entityManager );
        IProductSizeRepository sizeRepository = RepositoryFactory.makeProductSizeRepository( entityManager );
        IIngredientRepository ingredientRepository = RepositoryFactory.makeIngredientRepository( entityManager );

        return ServiceFactory.makeProductService(
                productRepository,
                sizeRepository,
                ingredientRepository
        );
    }

    public IShoppingCartService provideShoppingCartService ()
    {

        IShoppingCartRepository cartRepository = RepositoryFactory.makeShoppingCartRepository( entityManager );
        IProductRepository productRepository = RepositoryFactory.makeProductRepository( entityManager );
        IProductSizeRepository sizeRepository = RepositoryFactory.makeProductSizeRepository( entityManager );

        return ServiceFactory.makeShoppingCartService(
                cartRepository,
                productRepository,
                sizeRepository
        );
    }

    public IOrderService provideOrderService ()
    {

        IOrderRepository orderRepository = RepositoryFactory.makeOrderRepository( entityManager );
        IShoppingCartRepository cartRepository = RepositoryFactory.makeShoppingCartRepository( entityManager );
        ICookingAssignmentRepository cookingRepository = RepositoryFactory.makeCookingAssignmentRepository( entityManager );

        return ServiceFactory.makeOrderService(
                orderRepository,
                cartRepository,
                cookingRepository
        );
    }

    public IAccountService provideAccountService ()
    {

        IAccountRepository accountRepository = RepositoryFactory.makeAccountRepository( entityManager );
        IOrderRepository orderRepository = RepositoryFactory.makeOrderRepository( entityManager );

        return ServiceFactory.makeAccountService(
                accountRepository,
                orderRepository
        );
    }

    public ICookingAssignmentService provideCookingAssignmentService ()
    {

        ICookingAssignmentRepository cookingRepository = RepositoryFactory.makeCookingAssignmentRepository( entityManager );
        IDeliveryRepository deliveryRepository = RepositoryFactory.makeDeliveryRepository( entityManager );
        IOrderRepository orderRepository = RepositoryFactory.makeOrderRepository( entityManager );

        return ServiceFactory.makeCookingAssignmentService(
                cookingRepository,
                deliveryRepository,
                orderRepository
        );
    }

    public IDeliveryService provideDeliveryService ()
    {

        IDeliveryRepository deliveryRepository = RepositoryFactory.makeDeliveryRepository( entityManager );
        IOrderRepository orderRepository = RepositoryFactory.makeOrderRepository( entityManager );

        return ServiceFactory.makeDeliveryService(
                deliveryRepository,
                orderRepository
        );
    }


    private EntityManager entityManager;

}
