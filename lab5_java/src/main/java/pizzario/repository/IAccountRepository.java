/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository;

import pizzario.model.Account;
import pizzario.model.Order;

public interface IAccountRepository extends IRepository< Account >
{

    Account findByOrder ( Order o );

    Account findByEmail ( String newEmail );
}
