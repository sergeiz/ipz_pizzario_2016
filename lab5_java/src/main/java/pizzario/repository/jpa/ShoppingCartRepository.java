/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import pizzario.model.ShoppingCart;
import pizzario.repository.IShoppingCartRepository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.UUID;

class ShoppingCartRepository
        extends BasicRepository< ShoppingCart >
        implements IShoppingCartRepository
{

    public ShoppingCartRepository ( EntityManager entityManager )
    {
        super( entityManager, ShoppingCart.class );

        queryOpen = getEntityManager().createQuery(
                "SELECT cart.domainId " +
                        "FROM ShoppingCart cart " +
                        "WHERE cart.modifiable = false",
                UUID.class
        );
    }

    @Override
    public List< UUID > selectAllOpenCartIds ()
    {
        return queryOpen.getResultList();
    }

    private TypedQuery< UUID > queryOpen;
}
