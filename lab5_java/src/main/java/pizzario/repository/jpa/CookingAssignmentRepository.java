/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import pizzario.model.CookingAssignment;
import pizzario.model.Order;
import pizzario.repository.ICookingAssignmentRepository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.UUID;

class CookingAssignmentRepository
        extends BasicRepository< CookingAssignment >
        implements ICookingAssignmentRepository
{

    public CookingAssignmentRepository ( EntityManager entityManager )
    {
        super( entityManager, CookingAssignment.class );

        queryCooking = getEntityManager().createQuery(
                "SELECT ca.domainId " +
                        "FROM CookingAssignment ca " +
                        "WHERE ca.status = pizzario.model.CookingStatus.InProgress",
                UUID.class
        );

        queryWaiting = getEntityManager().createQuery(
                "SELECT ca.domainId " +
                        "FROM CookingAssignment ca " +
                        "WHERE ca.status = pizzario.model.CookingStatus.Waiting",
                UUID.class
        );

        queryByOrder = getEntityManager().createQuery(
                "SELECT ca " +
                        "FROM CookingAssignment ca " +
                        "WHERE ca.order.databaseId = :orderId",

                getEntityClass()
        );
    }

    @Override
    public List< UUID > selectCookingIds ()
    {
        return queryCooking.getResultList();
    }

    @Override
    public List< UUID > selectWaitingIds ()
    {
        return queryWaiting.getResultList();
    }

    @Override
    public List< CookingAssignment > findOrderAssignments ( Order o )
    {
        queryByOrder.setParameter( "orderId", o.getDatabaseId() );
        return queryByOrder.getResultList();
    }


    private TypedQuery< UUID > queryCooking;
    private TypedQuery< UUID > queryWaiting;

    private TypedQuery< CookingAssignment > queryByOrder;
}
