/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import pizzario.model.Account;
import pizzario.model.Order;
import pizzario.repository.IAccountRepository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

class AccountRepository
        extends BasicRepository< Account >
        implements IAccountRepository
{

    public AccountRepository ( EntityManager entityManager )
    {
        super( entityManager, Account.class );

        queryByOrder = getEntityManager().createQuery(
                "SELECT a " +
                        "FROM OperatorAccount a " +
                        "WHERE EXISTS ( " +
                        "    SELECT 1 " +
                        "    FROM a.orders o " +
                        "    WHERE o.databaseId = :orderId" +
                        ")",

                Account.class
        );

        queryByEmail = getEntityManager().createQuery(
                "SELECT a " +
                        "FROM Account a " +
                        "WHERE a.email = :email",

                Account.class
        );
    }

    @Override
    public Account findByOrder ( Order o )
    {
        queryByOrder.setParameter( "orderId", o.getDatabaseId() );
        List< Account > results = queryByOrder.getResultList();
        return ( results.size() == 1 ) ? results.get( 0 ) : null;
    }

    @Override
    public Account findByEmail ( String email )
    {
        queryByEmail.setParameter( "email", email );
        List< Account > results = queryByEmail.getResultList();
        return ( results.size() == 1 ) ? results.get( 0 ) : null;
    }

    private TypedQuery< Account > queryByOrder;
    private TypedQuery< Account > queryByEmail;
}
