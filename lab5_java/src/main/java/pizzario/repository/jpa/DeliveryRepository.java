/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import pizzario.model.Delivery;
import pizzario.model.Order;
import pizzario.repository.IDeliveryRepository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.UUID;

class DeliveryRepository
        extends BasicRepository< Delivery >
        implements IDeliveryRepository
{

    public DeliveryRepository ( EntityManager entityManager )
    {
        super( entityManager, Delivery.class );

        queryWaiting = getEntityManager().createQuery(
                "SELECT d.domainId " +
                        "FROM Delivery d " +
                        "WHERE d.status = pizzario.model.DeliveryStatus.Waiting",

                UUID.class
        );

        queryInProgress = getEntityManager().createQuery(
                "SELECT d.domainId " +
                        "FROM Delivery d " +
                        "WHERE d.status = pizzario.model.DeliveryStatus.InProgress",

                UUID.class
        );

        queryByOrder = getEntityManager().createQuery(
                "SELECT d " +
                        "FROM Delivery d " +
                        "WHERE d.order.databaseId = :orderId",

                getEntityClass()
        );
    }

    @Override
    public List< UUID > selectWaitingIds ()
    {
        return queryWaiting.getResultList();
    }

    @Override
    public List< UUID > selectInProgressIds ()
    {
        return queryInProgress.getResultList();
    }

    @Override
    public Delivery findByOrder ( Order o )
    {
        queryByOrder.setParameter( "orderId", o.getDatabaseId() );
        List< Delivery > results = queryByOrder.getResultList();
        return ( results.size() == 1 ) ? results.get( 0 ) : null;
    }

    private TypedQuery< UUID > queryWaiting;
    private TypedQuery< UUID > queryInProgress;

    private TypedQuery< Delivery > queryByOrder;
}
