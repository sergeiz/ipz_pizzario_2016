/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import pizzario.model.ProductSize;
import pizzario.repository.IProductSizeRepository;

import javax.persistence.EntityManager;


class ProductSizeRepository
        extends NamedObjectRepository< ProductSize >
        implements IProductSizeRepository
{

    public ProductSizeRepository ( EntityManager entityManager )
    {
        super( entityManager, ProductSize.class );
    }
}
