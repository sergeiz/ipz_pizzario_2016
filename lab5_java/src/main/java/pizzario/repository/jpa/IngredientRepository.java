/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import pizzario.model.Ingredient;
import pizzario.repository.IIngredientRepository;

import javax.persistence.EntityManager;

class IngredientRepository
        extends NamedObjectRepository< Ingredient >
        implements IIngredientRepository
{

    public IngredientRepository ( EntityManager entityManager )
    {
        super( entityManager, Ingredient.class );
    }
}
