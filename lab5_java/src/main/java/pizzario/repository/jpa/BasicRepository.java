/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import pizzario.repository.IRepository;
import pizzario.utils.DomainEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.UUID;

abstract class BasicRepository< T extends DomainEntity > implements IRepository< T >
{

    protected BasicRepository ( EntityManager entityManager, Class< T > theClass )
    {
        this.entityManager = entityManager;
        this.theClass = theClass;

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        {
            CriteriaQuery< T > criteria = cb.createQuery( theClass );
            criteria.select( criteria.from( theClass ) );
            queryAllEntities = entityManager.createQuery( criteria );
        }

        {
            CriteriaQuery< UUID > criteria = cb.createQuery( UUID.class );
            Root< T > root = criteria.from( theClass );
            criteria.select( root.get( "domainId" ) );
            queryAllIds = entityManager.createQuery( criteria );
        }

        {
            CriteriaQuery< T > criteria = cb.createQuery( theClass );
            Root< T > root = criteria.from( theClass );
            criteria.select( root )
                    .where(
                            cb.equal(
                                    root.get( "domainId" ),
                                    cb.parameter( UUID.class, "domainId" )
                            )
                    );

            queryByDomainId = entityManager.createQuery( criteria );
        }
    }

    @Override
    public void startTransaction ()
    {
        this.entityManager.getTransaction().begin();
    }

    @Override
    public void commit ()
    {
        this.entityManager.getTransaction().commit();
    }

    @Override
    public void rollback ()
    {
        this.entityManager.getTransaction().rollback();
    }

    @Override
    public void add ( T obj )
    {
        this.entityManager.persist( obj );
    }

    @Override
    public void delete ( T obj )
    {
        this.entityManager.remove( obj );
    }

    @Override
    public Iterable< T > loadAll ()
    {
        return queryAllEntities.getResultList();
    }

    @Override
    public T load ( long id )
    {
        return entityManager.find( theClass, id );
    }


    @Override
    public T findByDomainId ( UUID domainId )
    {
        queryByDomainId.setParameter( "domainId", domainId );
        List< T > results = queryByDomainId.getResultList();
        return ( results.size() == 1 ) ? results.get( 0 ) : null;
    }


    @Override
    public List< UUID > selectAllDomainIds ()
    {
        return queryAllIds.getResultList();
    }

    protected EntityManager getEntityManager ()
    {
        return this.entityManager;
    }

    protected Class< T > getEntityClass ()
    {
        return this.theClass;
    }


    private EntityManager entityManager;
    private Class< T > theClass;

    private TypedQuery< T > queryAllEntities;
    private TypedQuery< UUID > queryAllIds;
    private TypedQuery< T > queryByDomainId;
}
