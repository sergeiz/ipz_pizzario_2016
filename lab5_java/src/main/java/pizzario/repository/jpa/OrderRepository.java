/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import pizzario.model.Order;
import pizzario.repository.IOrderRepository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.UUID;

class OrderRepository
        extends BasicRepository< Order >
        implements IOrderRepository
{

    public OrderRepository ( EntityManager entityManager )
    {
        super( entityManager, Order.class );

        queryUnconfirmed = getEntityManager().createQuery(
                "SELECT o.domainId " +
                        "FROM CustomerOrder o " +
                        "WHERE o.status = pizzario.model.OrderStatus.Placed",
                UUID.class
        );

        queryReady4Delivery = getEntityManager().createQuery(
                "SELECT o.domainId " +
                        "FROM CustomerOrder o " +
                        "WHERE o.status = pizzario.model.OrderStatus.Delivering",
                UUID.class
        );
    }

    @Override
    public List< UUID > selectUnconfirmedIds ()
    {
        return queryUnconfirmed.getResultList();
    }

    @Override
    public List< UUID > selectReady4DeliveryIds ()
    {
        return queryReady4Delivery.getResultList();
    }

    private TypedQuery< UUID > queryUnconfirmed;
    private TypedQuery< UUID > queryReady4Delivery;
}
