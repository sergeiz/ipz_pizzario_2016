/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.repository.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PizzarioJPAContext implements AutoCloseable
{

    public PizzarioJPAContext ( String persistenceUnitName )
    {
        this.entityManagerFactory = Persistence.createEntityManagerFactory( persistenceUnitName );
        this.entityManager = this.entityManagerFactory.createEntityManager();
    }

    public EntityManager getEntityManager ()
    {
        return this.entityManager;
    }

    @Override
    public void close ()
    {
        entityManager.close();
        entityManagerFactory.close();
    }

    private EntityManagerFactory entityManagerFactory;

    private EntityManager entityManager;
}
