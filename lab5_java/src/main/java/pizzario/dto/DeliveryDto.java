/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.dto;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class DeliveryDto extends DomainEntityDto< DeliveryDto >
{

    public DeliveryDto ( UUID domainId,
                         UUID orderId,
                         String driverName,
                         String status,
                         BigDecimal cash2Collect )
    {
        super( domainId );

        this.orderId = orderId;
        this.driverName = driverName;
        this.status = status;
        this.cash2Collect = cash2Collect;
    }

    public UUID getOrderId ()
    {
        return orderId;
    }

    public String getDriverName ()
    {
        return driverName;
    }

    public String getStatus ()
    {
        return status;
    }

    public BigDecimal getCash2Collect ()
    {
        return cash2Collect;
    }

    @Override
    public String toString ()
    {
        return String.format(
                "ID = %s\nDriver = %s\nOrder # = %s\nStatus = %s\nCash2Collect = %s",
                getDomainId(),
                getDriverName(),
                getOrderId(),
                getStatus(),
                getCash2Collect()
        );
    }

    @Override
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList(
                getDomainId(),
                getOrderId(),
                getDriverName(),
                getStatus(),
                getCash2Collect()
        );
    }

    private final UUID orderId;
    private final String driverName;
    private final String status;
    private final BigDecimal cash2Collect;

}
