/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.dto;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class AccountDto extends DomainEntityDto< AccountDto >
{

    public AccountDto ( UUID domainId, String name, String email )
    {
        super( domainId );
        this.name = name;
        this.email = email;
    }

    public String getName ()
    {
        return name;
    }

    public String getEmail ()
    {
        return email;
    }

    @Override
    public String toString ()
    {
        return String.format(
                "ID = %s\nName = %s\nEmail = %s\n",
                getDomainId(),
                getName(),
                getEmail()
        );
    }

    @Override
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList( getDomainId(), getName(), getEmail() );
    }

    private final String name;
    private final String email;
}
