/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.dto;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class OrderDto extends DomainEntityDto< OrderDto >
{

    public OrderDto (
            UUID domainId,
            String customerAddress,
            String contactPhone,
            LocalDateTime placementTime,
            String status,
            List< ProductItemDto > items,
            BigDecimal basicCost,
            BigDecimal totalCost,
            BigDecimal discountPercentage
    )
    {
        super( domainId );

        this.customerAddress = customerAddress;
        this.contactPhone = contactPhone;
        this.placementTime = placementTime;
        this.status = status;
        this.items = items;
        this.basicCost = basicCost;
        this.totalCost = totalCost;
        this.discountPercentage = discountPercentage;
    }

    public String getCustomerAddress ()
    {
        return customerAddress;
    }

    public String getContactPhone ()
    {
        return contactPhone;
    }

    public LocalDateTime getPlacementTime ()
    {
        return placementTime;
    }

    public String getStatus ()
    {
        return status;
    }

    public List< ProductItemDto > viewItems ()
    {
        return items;
    }

    public BigDecimal getBasicCost ()
    {
        return basicCost;
    }

    public BigDecimal getTotalCost ()
    {
        return totalCost;
    }

    public BigDecimal getDiscountPercentage ()
    {
        return discountPercentage;
    }

    @Override
    public String toString ()
    {
        StringBuilder itemsAsString = new StringBuilder();
        for ( ProductItemDto item : items )
        {
            itemsAsString.append( item );
            itemsAsString.append( "\n\n" );
        }

        return String.format(
                "ID = %s\nContent:\n%sBasic cost = %s\nDiscount = %s\nTotal cost = %s\nStatus = %s\nPlaced = %s",
                getDomainId(),
                itemsAsString.toString(),
                NumberFormat.getCurrencyInstance().format( getBasicCost() ),
                NumberFormat.getPercentInstance().format( getDiscountPercentage() ),
                NumberFormat.getCurrencyInstance().format( getTotalCost() ),
                getStatus(),
                getPlacementTime()
        );
    }

    @Override
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        List< Object > list = Arrays.asList(
                getDomainId(),
                getCustomerAddress(),
                getContactPhone(),
                getPlacementTime(),
                getStatus(),
                getBasicCost(),
                getTotalCost(),
                getDiscountPercentage()
        );
        list.addAll( items );
        return list;
    }


    private final String customerAddress;
    private final String contactPhone;
    private final LocalDateTime placementTime;
    private final String status;
    private final List< ProductItemDto > items;
    private final BigDecimal basicCost;
    private final BigDecimal totalCost;
    private final BigDecimal discountPercentage;
}
