/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.dto;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class ProductDto extends DomainEntityDto< ProductDto >
{

    public ProductDto ( UUID domainId, String name, String imageUrl )
    {
        super( domainId );

        this.name = name;
        this.imageUrl = imageUrl;
    }

    public String getName ()
    {
        return name;
    }

    public String getImageUrl ()
    {
        return imageUrl;
    }


    @Override
    public String toString ()
    {

        return String.format(
                "ID = %s\nName = %s\nImageUrl = %s",
                getDomainId(),
                getName(),
                getImageUrl()
        );
    }

    @Override
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList( getDomainId(), getName(), getImageUrl() );
    }

    private final String name;
    private final String imageUrl;

}
