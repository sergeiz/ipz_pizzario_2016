/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.dto;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class ProductSizeDto extends DomainEntityDto< ProductSizeDto >
{

    public ProductSizeDto ( UUID domainId, String name, int diameter, double weight )
    {
        super( domainId );

        this.name = name;
        this.diameter = diameter;
        this.weight = weight;
    }

    public String getName ()
    {
        return name;
    }

    public int getDiameter ()
    {
        return diameter;
    }

    public double getWeight ()
    {
        return weight;
    }

    @Override
    public String toString ()
    {
        return String.format(
                "ID = %s\nName = %s\nDiameter = %d\nWeight = %f",
                getDomainId(),
                getName(),
                getDiameter(),
                getWeight()
        );
    }

    @Override
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList(
                getDomainId(),
                getName(),
                getDiameter(),
                getWeight()
        );
    }

    private final String name;
    private final int diameter;
    private final double weight;
}
