/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.dto;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class ShoppingCartDto extends DomainEntityDto< ShoppingCartDto >
{

    public ShoppingCartDto (
            UUID domainId,
            List< ProductItemDto > items,
            boolean locked
    )
    {
        super( domainId );

        this.items = items;
        this.locked = locked;
    }

    public List< ProductItemDto > viewItems ()
    {
        return items;
    }

    public boolean isLocked ()
    {
        return locked;
    }

    @Override
    public String toString ()
    {
        StringBuilder b = new StringBuilder();

        b.append( String.format( "Id = %s\n", getDomainId() ) );

        b.append( "Content:\n" );
        for ( ProductItemDto item : items )
        {
            b.append( item );
            b.append( "\n\n" );
        }

        b.append( String.format( "Locked = %s\n", isLocked() ) );

        return b.toString();
    }

    @Override
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        List< Object > list = Arrays.asList( getDomainId(), isLocked() );
        list.addAll( items );
        return list;
    }

    private List< ProductItemDto > items;
    private boolean locked;

}
