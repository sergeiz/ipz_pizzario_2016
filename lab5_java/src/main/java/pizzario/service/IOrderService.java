/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service;

import pizzario.dto.OrderDto;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public interface IOrderService extends IDomainEntityService< OrderDto >
{

    List< UUID > viewUnconfirmed ();

    List< UUID > viewReady4Delivery ();

    UUID createNew ( String deliveryAddress, String contactPhone, UUID cartId );

    void setDiscount ( UUID orderId, BigDecimal discountPercent );

    void confirm ( UUID orderId );

    void cancel ( UUID orderID );

}
