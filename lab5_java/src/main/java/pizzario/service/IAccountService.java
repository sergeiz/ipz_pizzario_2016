/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service;

import pizzario.dto.AccountDto;

import java.util.List;
import java.util.UUID;

public interface IAccountService extends IDomainEntityService< AccountDto >
{

    AccountDto identify ( String email, String password );

    List< UUID > viewAssociatedOrders ( UUID accountId );

    UUID createOperator ( String name, String email, String password );

    void changeName ( UUID accountId, String newName );

    void changeEmail ( UUID accountId, String newEmail );

    void changePassword ( UUID accountId, String oldPassword, String newPassword );

    void trackOrder ( UUID accountId, UUID orderId );
}
