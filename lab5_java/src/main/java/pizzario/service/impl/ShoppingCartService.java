/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import pizzario.dto.ShoppingCartDto;
import pizzario.model.Product;
import pizzario.model.ProductItem;
import pizzario.model.ProductSize;
import pizzario.model.ShoppingCart;
import pizzario.repository.IProductRepository;
import pizzario.repository.IProductSizeRepository;
import pizzario.repository.IShoppingCartRepository;
import pizzario.service.IShoppingCartService;

import java.util.List;
import java.util.UUID;

public class ShoppingCartService implements IShoppingCartService
{

    public ShoppingCartService ( IShoppingCartRepository shoppingCartRepository,
                                 IProductRepository productRepository,
                                 IProductSizeRepository productSizeRepository )
    {

        this.shoppingCartRepository = shoppingCartRepository;
        this.productRepository = productRepository;
        this.productSizeRepository = productSizeRepository;
    }


    @Override
    public List< UUID > viewAll ()
    {
        return shoppingCartRepository.selectAllDomainIds();
    }

    @Override
    public List< UUID > viewOpen ()
    {
        return shoppingCartRepository.selectAllOpenCartIds();
    }

    @Override
    public ShoppingCartDto view ( UUID cartId )
    {
        ShoppingCart cart = resolveCart( cartId );
        return DtoBuilder.toDto( cart );
    }

    @Override
    public UUID createNew ()
    {
        shoppingCartRepository.startTransaction();

        ShoppingCart cart = new ShoppingCart( UUID.randomUUID() );
        shoppingCartRepository.add( cart );

        shoppingCartRepository.commit();

        return cart.getDomainId();
    }

    @Override
    public void setItem ( UUID cartId, UUID productId, UUID sizeId, int quantity )
    {
        shoppingCartRepository.startTransaction();

        ShoppingCart cart = resolveCart( cartId );
        Product product = resolveProduct( productId );
        ProductSize size = resolveSize( sizeId );

        ProductItem item = new ProductItem( UUID.randomUUID(), product, size, quantity );
        int itemIndex = cart.findItemIndex( product, size );
        if ( itemIndex == -1 )
            cart.addItem( item );
        else
            cart.updateItem( itemIndex, item );

        shoppingCartRepository.commit();
    }

    @Override
    public void removeItem ( UUID cartId, UUID productId, UUID sizeId )
    {
        shoppingCartRepository.startTransaction();

        ShoppingCart cart = resolveCart( cartId );
        Product product = resolveProduct( productId );
        ProductSize size = resolveSize( sizeId );

        int itemIndex = cart.findItemIndex( product, size );
        if ( itemIndex != -1 )
            cart.dropItem( itemIndex );

        else
            throw new IllegalStateException( "Item not found in cart" );

        shoppingCartRepository.commit();
    }

    @Override
    public void clearItems ( UUID cartId )
    {
        shoppingCartRepository.startTransaction();

        ShoppingCart cart = resolveCart( cartId );
        cart.clearItems();

        shoppingCartRepository.commit();
    }

    @Override
    public void lock ( UUID cartId )
    {
        shoppingCartRepository.startTransaction();

        ShoppingCart cart = resolveCart( cartId );
        cart.checkout();

        shoppingCartRepository.commit();
    }


    private ShoppingCart resolveCart ( UUID cartId )
    {
        return ServiceUtils.resolveEntity( shoppingCartRepository, cartId );
    }

    private Product resolveProduct ( UUID productId )
    {
        return ServiceUtils.resolveEntity( productRepository, productId );
    }

    private ProductSize resolveSize ( UUID sizeId )
    {
        return ServiceUtils.resolveEntity( productSizeRepository, sizeId );
    }


    private IShoppingCartRepository shoppingCartRepository;
    private IProductRepository productRepository;
    private IProductSizeRepository productSizeRepository;
}
