/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import pizzario.dto.CookingAssignmentDto;
import pizzario.model.CookingAssignment;
import pizzario.model.Delivery;
import pizzario.model.Order;
import pizzario.model.OrderStatus;
import pizzario.repository.ICookingAssignmentRepository;
import pizzario.repository.IDeliveryRepository;
import pizzario.repository.IOrderRepository;
import pizzario.service.ICookingAssignmentService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

class CookingAssignmentService implements ICookingAssignmentService
{

    public CookingAssignmentService (
            ICookingAssignmentRepository cookingAssignmentRepository,
            IDeliveryRepository deliveryRepository,
            IOrderRepository orderRepository )
    {

        this.cookingAssignmentRepository = cookingAssignmentRepository;
        this.deliveryRepository = deliveryRepository;
        this.orderRepository = orderRepository;
    }

    @Override
    public List< UUID > viewAll ()
    {
        return cookingAssignmentRepository.selectAllDomainIds();
    }

    @Override
    public List< UUID > viewCookedRightNow ()
    {
        return cookingAssignmentRepository.selectCookingIds();
    }

    @Override
    public List< UUID > viewWaiting ()
    {
        return cookingAssignmentRepository.selectWaitingIds();
    }

    @Override
    public CookingAssignmentDto view ( UUID cookingAssignmentId )
    {
        CookingAssignment cooking = resolveCooking( cookingAssignmentId );
        return DtoBuilder.toDto( cooking );
    }

    @Override
    public List< CookingAssignmentDto > viewOrderAssignments ( UUID orderId )
    {
        List< CookingAssignmentDto > views = new ArrayList<>();
        Order o = resolveOrder( orderId );
        for ( CookingAssignment ca : cookingAssignmentRepository.findOrderAssignments( o ) )
        {
            views.add(
                    new CookingAssignmentDto(
                            ca.getDomainId(),
                            ca.getOrder().getDomainId(),
                            ca.getProduct().getDomainId(),
                            ca.getProduct().getName(),
                            ca.getSize().getDomainId(),
                            ca.getSize().getName(),
                            ca.getStatus().toString()
                    ) );
        }
        return views;
    }

    @Override
    public void markCookingStarted ( UUID cookingAssignmentId )
    {
        cookingAssignmentRepository.startTransaction();

        CookingAssignment cooking = resolveCooking( cookingAssignmentId );
        cooking.startCooking();

        cookingAssignmentRepository.commit();
    }

    @Override
    public void markCookingFinished ( UUID cookingAssignmentId )
    {
        cookingAssignmentRepository.startTransaction();

        CookingAssignment cooking = resolveCooking( cookingAssignmentId );
        OrderStatus previousOrderStatus = cooking.getOrder().getStatus();

        cooking.finishCooking();
        cookingAssignmentRepository.commit();

        OrderStatus newOrderStatus = cooking.getOrder().getStatus();
        if ( previousOrderStatus != newOrderStatus )
        {
            deliveryRepository.startTransaction();
            Delivery d = cooking.getOrder().generateDelivery();
            deliveryRepository.add( d );
            deliveryRepository.commit();
        }
    }

    private CookingAssignment resolveCooking ( UUID cookingID )
    {
        return ServiceUtils.resolveEntity( cookingAssignmentRepository, cookingID );
    }

    private Order resolveOrder ( UUID orderId )
    {
        return ServiceUtils.resolveEntity( orderRepository, orderId );
    }

    private ICookingAssignmentRepository cookingAssignmentRepository;
    private IDeliveryRepository deliveryRepository;
    private IOrderRepository orderRepository;
}
