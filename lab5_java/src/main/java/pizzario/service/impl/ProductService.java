/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import pizzario.dto.*;
import pizzario.model.Ingredient;
import pizzario.model.Product;
import pizzario.model.ProductSize;
import pizzario.model.Recipe;
import pizzario.repository.IIngredientRepository;
import pizzario.repository.IProductRepository;
import pizzario.repository.IProductSizeRepository;
import pizzario.service.IProductService;

import java.math.BigDecimal;
import java.util.*;

class ProductService implements IProductService
{

    ProductService ( IProductRepository productRepository,
                     IProductSizeRepository productSizeRepository,
                     IIngredientRepository ingredientRepository )
    {

        this.productRepository = productRepository;
        this.productSizeRepository = productSizeRepository;
        this.ingredientRepository = ingredientRepository;
    }

    @Override
    public List< UUID > viewAll ()
    {
        return productRepository.selectAllDomainIds();
    }

    @Override
    public ProductDto view ( UUID productId )
    {
        Product product = resolveProduct( productId );
        return DtoBuilder.toDto( product );
    }

    @Override
    public List< ProductSizeDto > availableSizes ( UUID productId )
    {
        Product product = resolveProduct( productId );

        List< ProductSizeDto > sizes = new ArrayList<>();
        for ( ProductSize size : product.listAvailableSizes() )
            sizes.add( DtoBuilder.toDto( size ) );

        return sizes;
    }

    @Override
    public ProductPricingDto viewPrices ( UUID productId )
    {

        Product product = resolveProduct( productId );

        Map< ProductSizeDto, BigDecimal > prices = new HashMap<>();

        for ( ProductSize size : product.listAvailableSizes() )
            prices.put(
                    DtoBuilder.toDto( size ),
                    product.getPrice( size )
            );

        return new ProductPricingDto( productId, prices );
    }

    @Override
    public ProductRecipeDto viewRecipe ( UUID productId, UUID sizeId )
    {
        Product product = resolveProduct( productId );
        ProductSize size = resolveSize( sizeId );

        Recipe recipe = product.getRecipe( size );

        Map< IngredientDto, Double > usedIngredients = new HashMap<>();
        for ( Ingredient i : recipe.listUsedIngredients() )
        {
            usedIngredients.put(
                    DtoBuilder.toDto( i ),
                    recipe.getIngredientWeight( i )
            );
        }
        return new ProductRecipeDto( productId, sizeId, usedIngredients );
    }

    @Override
    public UUID create ( String name, String imageUrl )
    {
        productRepository.startTransaction();

        if ( productRepository.findByName( name ) != null )
            throw new IllegalStateException( "Duplicate product " + name );

        Product p = new Product( UUID.randomUUID(), name, imageUrl );
        productRepository.add( p );
        productRepository.commit();

        return p.getDomainId();
    }

    @Override
    public void rename ( UUID productId, String newName )
    {

        productRepository.startTransaction();

        if ( productRepository.findByName( newName ) != null )
            throw new IllegalStateException( "Duplicate product " + newName );

        Product p = resolveProduct( productId );
        p.setName( newName );

        productRepository.commit();
    }

    @Override
    public void updateImageUrl ( UUID productId, String imageUrl )
    {
        productRepository.startTransaction();

        Product p = resolveProduct( productId );
        p.setImageUrl( imageUrl );

        productRepository.commit();
    }

    @Override
    public void definePrice ( UUID productId, UUID sizeId, BigDecimal price )
    {
        productRepository.startTransaction();

        Product p = resolveProduct( productId );
        ProductSize sz = resolveSize( sizeId );
        p.setPrice( sz, price );

        productRepository.commit();
    }

    @Override
    public void undefinePrice ( UUID productId, UUID sizeId )
    {
        productRepository.startTransaction();

        Product p = resolveProduct( productId );
        ProductSize sz = resolveSize( sizeId );
        p.removePrice( sz );

        productRepository.commit();
    }

    @Override
    public void defineRecipeIngredient ( UUID productId, UUID sizeId, UUID ingredientId, double weight )
    {
        productRepository.startTransaction();

        Product p = resolveProduct( productId );
        ProductSize sz = resolveSize( sizeId );
        Ingredient i = resolveIngredient( ingredientId );

        p.defineRecipe( sz ).useIngredient( i, weight );

        productRepository.commit();
    }

    @Override
    public void removeRecipeIngredient ( UUID productId, UUID sizeId, UUID ingredientId )
    {
        productRepository.startTransaction();

        Product p = resolveProduct( productId );
        ProductSize sz = resolveSize( sizeId );
        Ingredient i = resolveIngredient( ingredientId );
        p.defineRecipe( sz ).removeIngredient( i );

        productRepository.commit();
    }

    private Product resolveProduct ( UUID productId )
    {
        return ServiceUtils.resolveEntity( productRepository, productId );
    }

    private ProductSize resolveSize ( UUID sizeId )
    {
        return ServiceUtils.resolveEntity( productSizeRepository, sizeId );
    }

    private Ingredient resolveIngredient ( UUID ingredientId )
    {
        return ServiceUtils.resolveEntity( ingredientRepository, ingredientId );
    }

    private IProductRepository productRepository;
    private IProductSizeRepository productSizeRepository;
    private IIngredientRepository ingredientRepository;
}
