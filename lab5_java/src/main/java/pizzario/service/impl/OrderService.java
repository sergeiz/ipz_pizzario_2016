/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import pizzario.dto.OrderDto;
import pizzario.model.*;
import pizzario.repository.ICookingAssignmentRepository;
import pizzario.repository.IOrderRepository;
import pizzario.repository.IShoppingCartRepository;
import pizzario.service.IOrderService;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

class OrderService implements IOrderService
{

    public OrderService ( IOrderRepository orderRepository,
                          IShoppingCartRepository shoppingCartRepository,
                          ICookingAssignmentRepository cookingAssignmentRepository )
    {

        this.orderRepository = orderRepository;
        this.shoppingCartRepository = shoppingCartRepository;
        this.cookingAssignmentRepository = cookingAssignmentRepository;
    }

    @Override
    public List< UUID > viewAll ()
    {
        return orderRepository.selectAllDomainIds();
    }

    @Override
    public List< UUID > viewUnconfirmed ()
    {
        return orderRepository.selectUnconfirmedIds();
    }

    @Override
    public List< UUID > viewReady4Delivery ()
    {
        return orderRepository.selectReady4DeliveryIds();
    }

    @Override
    public OrderDto view ( UUID orderId )
    {
        Order o = resolveOrder( orderId );
        return DtoBuilder.toDto( o );
    }

    @Override
    public UUID createNew ( String deliveryAddress, String contactPhone, UUID cartId )
    {
        orderRepository.startTransaction();

        ShoppingCart cart = resolveCart( cartId );
        Order o = new Order(
                UUID.randomUUID(),
                cart,
                new Contact( deliveryAddress, contactPhone ),
                LocalDateTime.now()
        );

        orderRepository.add( o );

        orderRepository.commit();

        return o.getDomainId();
    }

    @Override
    public void setDiscount ( UUID orderId, BigDecimal discountPercent )
    {
        orderRepository.startTransaction();

        Order o = resolveOrder( orderId );
        o.setDiscount( new Discount( discountPercent ) );

        orderRepository.commit();
    }

    @Override
    public void confirm ( UUID orderId )
    {
        orderRepository.startTransaction();

        Order o = resolveOrder( orderId );
        o.confirm();

        orderRepository.commit();

        cookingAssignmentRepository.startTransaction();

        for ( CookingAssignment ca : o.generateCookingAssignments() )
        {
            cookingAssignmentRepository.add( ca );
        }

        cookingAssignmentRepository.commit();
    }

    @Override
    public void cancel ( UUID orderId )
    {
        orderRepository.startTransaction();

        Order o = resolveOrder( orderId );
        o.cancel();

        orderRepository.commit();
    }

    private Order resolveOrder ( UUID orderId )
    {
        return ServiceUtils.resolveEntity( orderRepository, orderId );
    }

    private ShoppingCart resolveCart ( UUID cartId )
    {
        return ServiceUtils.resolveEntity( shoppingCartRepository, cartId );
    }


    private IOrderRepository orderRepository;
    private IShoppingCartRepository shoppingCartRepository;
    private ICookingAssignmentRepository cookingAssignmentRepository;

}
