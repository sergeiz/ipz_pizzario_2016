/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import pizzario.dto.ProductSizeDto;
import pizzario.model.ProductSize;
import pizzario.repository.IProductSizeRepository;
import pizzario.service.IProductSizeService;

import java.util.List;
import java.util.UUID;

class ProductSizeService implements IProductSizeService
{

    public ProductSizeService ( IProductSizeRepository productSizeRepository )
    {
        this.productSizeRepository = productSizeRepository;
    }

    @Override
    public List< UUID > viewAll ()
    {
        return productSizeRepository.selectAllDomainIds();
    }

    @Override
    public ProductSizeDto view ( UUID sizeId )
    {
        ProductSize size = resolveSize( sizeId );
        return DtoBuilder.toDto( size );
    }

    @Override
    public UUID create ( String name, int diameter, double weight )
    {

        productSizeRepository.startTransaction();

        if ( productSizeRepository.findByName( name ) != null )
            throw new IllegalStateException( "Duplicate ingredient " + name );

        ProductSize size = new ProductSize( UUID.randomUUID(), name, diameter, weight );

        productSizeRepository.add( size );
        productSizeRepository.commit();

        return size.getDomainId();
    }

    @Override
    public void rename ( UUID sizeId, String newName )
    {
        if ( productSizeRepository.findByName( newName ) != null )
            throw new IllegalStateException( "Duplicate size " + newName );

        productSizeRepository.startTransaction();
        ProductSize size = resolveSize( sizeId );
        size.setName( newName );
        productSizeRepository.commit();
    }

    @Override
    public void changeDiameter ( UUID sizeId, int newDiameter )
    {
        productSizeRepository.startTransaction();
        ProductSize size = resolveSize( sizeId );
        size.setDiameter( newDiameter );
        productSizeRepository.commit();
    }

    @Override
    public void changeWeight ( UUID sizeId, double weight )
    {
        productSizeRepository.startTransaction();
        ProductSize size = resolveSize( sizeId );
        size.setWeight( weight );
        productSizeRepository.commit();
    }

    private ProductSize resolveSize ( UUID sizeId )
    {
        return ServiceUtils.resolveEntity( productSizeRepository, sizeId );
    }

    private IProductSizeRepository productSizeRepository;
}
