/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import pizzario.repository.*;
import pizzario.service.*;

public final class ServiceFactory
{
    public static IIngredientService makeIngredientService ( IIngredientRepository repository )
    {
        return new IngredientService( repository );
    }

    public static IProductSizeService makeProductSizeService ( IProductSizeRepository repository )
    {
        return new ProductSizeService( repository );
    }

    public static IProductService makeProductService (
            IProductRepository productRepository,
            IProductSizeRepository productSizeRepository,
            IIngredientRepository ingredientRepository )
    {

        return new ProductService(
                productRepository,
                productSizeRepository,
                ingredientRepository
        );
    }

    public static IShoppingCartService makeShoppingCartService (
            IShoppingCartRepository shoppingCartRepository,
            IProductRepository productRepository,
            IProductSizeRepository productSizeRepository )
    {

        return new ShoppingCartService(
                shoppingCartRepository,
                productRepository,
                productSizeRepository
        );
    }

    public static IOrderService makeOrderService (
            IOrderRepository orderRepository,
            IShoppingCartRepository shoppingCartRepository,
            ICookingAssignmentRepository cookingAssignmentRepository )
    {

        return new OrderService(
                orderRepository,
                shoppingCartRepository,
                cookingAssignmentRepository
        );
    }

    public static IAccountService makeAccountService (
            IAccountRepository accountRepository,
            IOrderRepository orderRepository )
    {

        return new AccountService(
                accountRepository,
                orderRepository
        );
    }

    public static ICookingAssignmentService makeCookingAssignmentService (
            ICookingAssignmentRepository cookingAssignmentRepository,
            IDeliveryRepository deliveryRepository,
            IOrderRepository orderRepository )
    {

        return new CookingAssignmentService(
                cookingAssignmentRepository,
                deliveryRepository,
                orderRepository
        );
    }

    public static IDeliveryService makeDeliveryService (
            IDeliveryRepository deliveryRepository,
            IOrderRepository orderRepository )
    {

        return new DeliveryService(
                deliveryRepository,
                orderRepository
        );
    }

}
