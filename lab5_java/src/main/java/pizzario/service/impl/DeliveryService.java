/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import pizzario.dto.DeliveryDto;
import pizzario.model.Delivery;
import pizzario.model.Order;
import pizzario.repository.IDeliveryRepository;
import pizzario.repository.IOrderRepository;
import pizzario.service.IDeliveryService;

import java.util.List;
import java.util.UUID;

class DeliveryService implements IDeliveryService
{

    public DeliveryService (
            IDeliveryRepository deliveryRepository,
            IOrderRepository orderRepository )
    {

        this.deliveryRepository = deliveryRepository;
        this.orderRepository = orderRepository;
    }

    @Override
    public List< UUID > viewAll ()
    {
        return deliveryRepository.selectAllDomainIds();
    }

    @Override
    public List< UUID > viewWaiting ()
    {
        return deliveryRepository.selectWaitingIds();
    }

    @Override
    public List< UUID > viewInProgress ()
    {
        return deliveryRepository.selectInProgressIds();
    }

    @Override
    public DeliveryDto view ( UUID deliveryId )
    {
        Delivery delivery = resolveDelivery( deliveryId );
        return new DeliveryDto(
                delivery.getDomainId(),
                delivery.getOrder().getDomainId(),
                delivery.getDriverName(),
                delivery.getStatus().toString(),
                delivery.getCash2Collect()
        );
    }

    @Override
    public DeliveryDto findOrderDelivery ( UUID orderId )
    {
        Order o = resolveOrder( orderId );
        Delivery delivery = deliveryRepository.findByOrder( o );
        return new DeliveryDto(
                delivery.getDomainId(),
                delivery.getOrder().getDomainId(),
                delivery.getDriverName(),
                delivery.getStatus().toString(),
                delivery.getCash2Collect()
        );
    }


    @Override
    public void markStarted ( UUID deliveryId, String driverName )
    {
        deliveryRepository.startTransaction();

        Delivery delivery = resolveDelivery( deliveryId );
        delivery.startDelivery( driverName );

        deliveryRepository.commit();
    }

    @Override
    public void markDelivered ( UUID deliveryId )
    {
        deliveryRepository.startTransaction();

        Delivery delivery = resolveDelivery( deliveryId );
        delivery.finishDelivery();

        deliveryRepository.commit();
    }

    private Delivery resolveDelivery ( UUID deliveryId )
    {
        return ServiceUtils.resolveEntity( deliveryRepository, deliveryId );
    }

    private Order resolveOrder ( UUID orderId )
    {
        return ServiceUtils.resolveEntity( orderRepository, orderId );
    }

    private IDeliveryRepository deliveryRepository;
    private IOrderRepository orderRepository;
}
