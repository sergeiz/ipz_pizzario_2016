/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import pizzario.dto.AccountDto;
import pizzario.model.Account;
import pizzario.model.AccountVisitor;
import pizzario.model.OperatorAccount;
import pizzario.model.Order;
import pizzario.repository.IAccountRepository;
import pizzario.repository.IOrderRepository;
import pizzario.service.IAccountService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

class AccountService implements IAccountService
{

    public AccountService ( IAccountRepository accountRepository,
                            IOrderRepository orderRepository )
    {

        this.accountRepository = accountRepository;
        this.orderRepository = orderRepository;
    }

    @Override
    public List< UUID > viewAll ()
    {
        return accountRepository.selectAllDomainIds();
    }

    @Override
    public AccountDto view ( UUID accountId )
    {
        Account a = resolveAccount( accountId );
        return DtoBuilder.toDto( a );
    }

    @Override
    public AccountDto identify ( String email, String password )
    {

        Account a = accountRepository.findByEmail( email );
        if ( a == null )
            return null;

        if ( !a.checkPassword( password ) )
            return null;

        return new AccountDto( a.getDomainId(), a.getName(), a.getEmail() );
    }

    @Override
    public List< UUID > viewAssociatedOrders ( UUID accountId )
    {

        Account a = resolveAccount( accountId );

        List< UUID > orderIds = new ArrayList<>();
        a.accept(
                new AccountVisitor()
                {
                    @Override
                    public void visit ( Account account )
                    {
                        // No orders..
                    }

                    @Override
                    public void visit ( OperatorAccount account )
                    {
                        for ( Order o : account.getOrders() )
                            orderIds.add( o.getDomainId() );
                    }
                }
        );

        return orderIds;
    }

    @Override
    public UUID createOperator ( String name, String email, String password )
    {

        Account a = accountRepository.findByEmail( email );
        if ( a != null )
            throw new IllegalArgumentException( "Duplicate account" );

        accountRepository.startTransaction();

        OperatorAccount operator = new OperatorAccount( UUID.randomUUID(), name, email, password );
        accountRepository.add( operator );

        accountRepository.commit();

        return operator.getDomainId();
    }

    @Override
    public void changeName ( UUID accountId, String newName )
    {

        accountRepository.startTransaction();

        Account a = resolveAccount( accountId );
        a.setName( newName );

        accountRepository.commit();
    }

    @Override
    public void changeEmail ( UUID accountId, String newEmail )
    {

        accountRepository.startTransaction();

        Account a = accountRepository.findByEmail( newEmail );
        if ( a != null )
            throw new IllegalArgumentException( "Duplicate account" );

        a.setEmail( newEmail );

        accountRepository.commit();
    }

    @Override
    public void changePassword ( UUID accountId, String oldPassword, String newPassword )
    {

        accountRepository.startTransaction();

        Account a = resolveAccount( accountId );

        if ( !a.checkPassword( oldPassword ) )
            throw new IllegalArgumentException( "Password failed" );

        a.setPassword( newPassword );

        accountRepository.commit();
    }

    @Override
    public void trackOrder ( UUID accountId, UUID orderId )
    {

        accountRepository.startTransaction();

        Account a = resolveAccount( accountId );
        Order o = resolveOrder( orderId );

        Account otherAccount = accountRepository.findByOrder( o );
        if ( otherAccount != null )
            throw new IllegalArgumentException( "Order was already served by different account" );

        a.accept( new AccountVisitor()
        {

            @Override
            public void visit ( Account account )
            {
                throw new IllegalArgumentException( "Not an operator account" );
            }

            @Override
            public void visit ( OperatorAccount account )
            {
                account.trackOrder( o );
            }
        } );

        accountRepository.commit();
    }

    private Account resolveAccount ( UUID accountId )
    {
        return ServiceUtils.resolveEntity( accountRepository, accountId );
    }

    private Order resolveOrder ( UUID orderId )
    {
        return ServiceUtils.resolveEntity( orderRepository, orderId );
    }

    private IAccountRepository accountRepository;
    private IOrderRepository orderRepository;
}
