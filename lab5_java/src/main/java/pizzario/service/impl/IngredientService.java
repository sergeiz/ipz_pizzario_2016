/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service.impl;

import pizzario.dto.IngredientDto;
import pizzario.model.Ingredient;
import pizzario.repository.IIngredientRepository;
import pizzario.service.IIngredientService;

import java.util.List;
import java.util.UUID;

class IngredientService implements IIngredientService
{

    public IngredientService ( IIngredientRepository ingredientRepository )
    {
        this.ingredientRepository = ingredientRepository;
    }

    @Override
    public List< UUID > viewAll ()
    {
        return ingredientRepository.selectAllDomainIds();
    }

    @Override
    public IngredientDto view ( UUID ingredientId )
    {
        Ingredient i = resolveIngredient( ingredientId );
        return DtoBuilder.toDto( i );
    }

    @Override
    public UUID create ( String name )
    {

        ingredientRepository.startTransaction();

        if ( ingredientRepository.findByName( name ) != null )
            throw new IllegalStateException( "Duplicate ingredient " + name );

        Ingredient i = new Ingredient( UUID.randomUUID(), name );

        ingredientRepository.add( i );
        ingredientRepository.commit();

        return i.getDomainId();
    }

    @Override
    public void rename ( UUID ingredientId, String newName )
    {
        if ( ingredientRepository.findByName( newName ) != null )
            throw new IllegalStateException( "Duplicate ingredient " + newName );

        ingredientRepository.startTransaction();

        Ingredient i = resolveIngredient( ingredientId );
        i.setName( newName );

        ingredientRepository.commit();
    }

    private Ingredient resolveIngredient ( UUID ingredientId )
    {
        return ServiceUtils.resolveEntity( ingredientRepository, ingredientId );
    }

    private IIngredientRepository ingredientRepository;
}
