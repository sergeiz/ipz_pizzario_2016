/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service;

import pizzario.dto.IngredientDto;

import java.util.UUID;

public interface IIngredientService extends IDomainEntityService< IngredientDto >
{

    UUID create ( String name );

    void rename ( UUID ingredientId, String newName );

}
