/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service;

import pizzario.dto.DomainEntityDto;

import java.util.List;
import java.util.UUID;

public interface IDomainEntityService< TViewModel extends DomainEntityDto >
{

    List< UUID > viewAll ();

    TViewModel view ( UUID domainId );

}
