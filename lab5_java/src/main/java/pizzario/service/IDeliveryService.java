/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service;

import pizzario.dto.DeliveryDto;

import java.util.List;
import java.util.UUID;

public interface IDeliveryService extends IDomainEntityService< DeliveryDto >
{

    List< UUID > viewWaiting ();

    List< UUID > viewInProgress ();

    DeliveryDto findOrderDelivery ( UUID orderId );

    void markStarted ( UUID deliveryId, String driverName );

    void markDelivered ( UUID deliveryId );

}
