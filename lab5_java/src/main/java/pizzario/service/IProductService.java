/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service;

import pizzario.dto.ProductDto;
import pizzario.dto.ProductPricingDto;
import pizzario.dto.ProductRecipeDto;
import pizzario.dto.ProductSizeDto;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public interface IProductService extends IDomainEntityService< ProductDto >
{

    List< ProductSizeDto > availableSizes ( UUID productId );

    ProductPricingDto viewPrices ( UUID productId );

    ProductRecipeDto viewRecipe ( UUID productId, UUID sizeId );

    UUID create ( String name, String imageUrl );

    void rename ( UUID productId, String newName );

    void updateImageUrl ( UUID productId, String imageUrl );

    void definePrice ( UUID productId, UUID sizeId, BigDecimal price );

    void undefinePrice ( UUID productd, UUID sizeId );

    void defineRecipeIngredient ( UUID productId, UUID sizeId, UUID ingredientId, double weight );

    void removeRecipeIngredient ( UUID productId, UUID sizeId, UUID ingredientId );

}
