/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service;

import pizzario.dto.ProductSizeDto;

import java.util.UUID;

public interface IProductSizeService extends IDomainEntityService< ProductSizeDto >
{

    UUID create ( String name, int diameter, double weight );

    void rename ( UUID sizeId, String newName );

    void changeDiameter ( UUID sizeId, int newDiameter );

    void changeWeight ( UUID sizeId, double weight );

}
