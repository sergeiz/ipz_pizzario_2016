/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service;

import pizzario.dto.CookingAssignmentDto;

import java.util.List;
import java.util.UUID;

public interface ICookingAssignmentService extends IDomainEntityService< CookingAssignmentDto >
{

    List< UUID > viewCookedRightNow ();

    List< UUID > viewWaiting ();

    List< CookingAssignmentDto > viewOrderAssignments ( UUID orderId );

    void markCookingStarted ( UUID cookingAssignmentId );

    void markCookingFinished ( UUID cookingAssignmentId );

}
