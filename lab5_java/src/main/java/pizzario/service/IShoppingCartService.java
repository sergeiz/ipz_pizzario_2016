/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.service;

import pizzario.dto.ShoppingCartDto;

import java.util.List;
import java.util.UUID;

public interface IShoppingCartService extends IDomainEntityService< ShoppingCartDto >
{

    List< UUID > viewOpen ();

    UUID createNew ();

    void setItem ( UUID cartId, UUID productId, UUID sizeId, int quantity );

    void removeItem ( UUID cartId, UUID productId, UUID sizeId );

    void clearItems ( UUID cartId );

    void lock ( UUID cartId );

}
