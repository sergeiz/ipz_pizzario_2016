/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.utils;

public class NonEmptyString extends AbstractProperty
{

    public NonEmptyString ( String paramName )
    {
        super( paramName );
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue ( String value )
    {
        checkValue( value );
        this.value = value;
    }

    protected void checkValue ( String value )
    {

        if ( value == null )
            throw new IllegalArgumentException( getParamName() );

        else if ( value.length() == 0 )
            throw new IllegalArgumentException( "Empty string not allowed for " + getParamName() );
    }

    private String value;
}
