/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.DomainEntity;
import pizzario.utils.NonEmptyString;
import pizzario.utils.RangeProperty;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import java.util.UUID;

@Entity
@Access( AccessType.PROPERTY )
public class ProductSize extends DomainEntity
{

    protected ProductSize ()
    {
    }

    public ProductSize ( UUID domainId, String name, int diameter, double weight )
    {
        super( domainId );
        setName( name );
        setDiameter( diameter );
        setWeight( weight );
    }


    public String getName ()
    {
        return this.name.getValue();
    }

    public void setName ( String name )
    {
        this.name.setValue( name );
    }

    public int getDiameter ()
    {
        return this.diameter.getValue();
    }

    public void setDiameter ( int diameter )
    {
        this.diameter.setValue( diameter );
    }

    public double getWeight ()
    {
        return this.weight.getValue();
    }

    public void setWeight ( double weight )
    {
        this.weight.setValue( weight );
    }


    private NonEmptyString name = new NonEmptyString( "name" );

    private RangeProperty< Integer > diameter =
            new RangeProperty<>( "diameter", 0, false, Integer.MAX_VALUE, false );

    private RangeProperty< Double > weight =
            new RangeProperty<>( "weight", 0.0, false, Double.MAX_VALUE, false );
}
