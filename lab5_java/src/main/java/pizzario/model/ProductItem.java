/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.DomainEntity;
import pizzario.utils.RangeProperty;
import pizzario.utils.RequiredProperty;

import javax.persistence.*;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.UUID;

@Entity
@Access( AccessType.PROPERTY )
public class ProductItem extends DomainEntity
{

    protected ProductItem ()
    {
    }

    public ProductItem ( UUID domainId, Product product, ProductSize size, int quantity )
    {

        super( domainId );

        setProduct( product );
        setSize( size );
        setQuantity( quantity );
        setFixedPrice( product.getPrice( size ) );
    }

    @ManyToOne
    public Product getProduct ()
    {
        return product.getValue();
    }

    private void setProduct ( Product product )
    {
        this.product.setValue( product );
    }

    @ManyToOne
    public ProductSize getSize ()
    {
        return size.getValue();
    }

    private void setSize ( ProductSize size )
    {
        this.size.setValue( size );
    }

    public int getQuantity ()
    {
        return this.quantity.getValue();
    }

    public void setQuantity ( int quantity )
    {
        this.quantity.setValue( quantity );
    }

    public BigDecimal getFixedPrice ()
    {
        return this.fixedPrice.getValue();
    }

    public void setFixedPrice ( BigDecimal fixedPrice )
    {
        this.fixedPrice.setValue( fixedPrice );
    }

    @Transient
    public BigDecimal getCost ()
    {
        return getFixedPrice().multiply( BigDecimal.valueOf( getQuantity() ) );
    }

    private RequiredProperty< Product > product = new RequiredProperty<>( "product" );

    private RequiredProperty< ProductSize > size = new RequiredProperty<>( "size" );

    private RangeProperty< Integer > quantity =
            new RangeProperty<>( "quantity", 0, false, Integer.MAX_VALUE, false );

    private RangeProperty< BigDecimal > fixedPrice =
            new RangeProperty<>(
                    "fixedPrice", BigDecimal.ZERO, true, BigDecimal.valueOf( Long.MAX_VALUE ), false
            );

}
