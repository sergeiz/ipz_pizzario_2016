/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.DomainEntity;
import pizzario.utils.RequiredProperty;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.UUID;

@Entity
@Access( AccessType.PROPERTY )
public class CookingAssignment extends DomainEntity
{

    protected CookingAssignment ()
    {
    }

    public CookingAssignment ( UUID domainID, Order order, Product product, ProductSize size )
    {
        super( domainID );

        setOrder( order );
        setProduct( product );
        setSize( size );

        status = CookingStatus.Waiting;
    }

    @ManyToOne
    public Order getOrder ()
    {
        return this.order.getValue();
    }

    private void setOrder ( Order order )
    {
        this.order.setValue( order );
    }

    @ManyToOne
    public Product getProduct ()
    {
        return this.product.getValue();
    }

    private void setProduct ( Product product )
    {
        this.product.setValue( product );
    }

    @ManyToOne
    public ProductSize getSize ()
    {
        return this.size.getValue();
    }

    private void setSize ( ProductSize size )
    {
        this.size.setValue( size );
    }

    public CookingStatus getStatus ()
    {
        return this.status;
    }

    private void setStatus ( CookingStatus status )
    {
        this.status = status;
    }

    public void startCooking ()
    {
        if ( this.status != CookingStatus.Waiting )
            throw new IllegalStateException( "CookingAssignment: cooking may be started in Waiting state only" );

        this.status = CookingStatus.InProgress;
    }

    public void finishCooking ()
    {
        if ( this.status != CookingStatus.InProgress )
            throw new IllegalStateException( "CookingAssignment: cooking may be finished in InProgress state only" );

        this.status = CookingStatus.Finished;

        getOrder().cookingAssignmentCompleted();
    }


    private RequiredProperty< Order > order = new RequiredProperty<>( "order" );

    private RequiredProperty< Product > product = new RequiredProperty<>( "product" );

    private RequiredProperty< ProductSize > size = new RequiredProperty<>( "size" );

    private CookingStatus status;

}
