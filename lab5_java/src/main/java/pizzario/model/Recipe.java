/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.DomainEntity;
import pizzario.utils.RequiredProperty;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Entity
@Access( AccessType.PROPERTY )
public class Recipe extends DomainEntity
{

    protected Recipe ()
    {
    }

    public Recipe ( UUID domainId, Product product, ProductSize size )
    {
        super( domainId );
        setProduct( product );
        setSize( size );
    }

    @ManyToOne
    public Product getProduct ()
    {
        return this.product.getValue();
    }

    private void setProduct ( Product product )
    {
        this.product.setValue( product );
    }

    @ManyToOne
    public ProductSize getSize ()
    {
        return this.size.getValue();
    }

    private void setSize ( ProductSize size )
    {
        this.size.setValue( size );
    }

    @Transient
    public double getTotalIngredientsWeight ()
    {
        return ingredients.values().stream()
                .mapToDouble( value -> value.doubleValue() )
                .sum()
                ;
    }

    public Iterable< Ingredient > listUsedIngredients ()
    {
        return ingredients.keySet();
    }

    public boolean usesIngredient ( Ingredient i )
    {
        return ingredients.containsKey( i );
    }

    public double getIngredientWeight ( Ingredient i )
    {
        Double weight = ingredients.get( i );
        if ( weight != null )
            return weight;

        throw new IllegalStateException( "Ingredient " + i.getName() + " not included to the recipe" );
    }

    public Recipe useIngredient ( Ingredient i, double weight )
    {
        ingredients.put( i, weight );
        return this;
    }

    public void removeIngredient ( Ingredient i )
    {
        if ( usesIngredient( i ) )
            ingredients.remove( i );

        else
            throw new IllegalArgumentException( "Ingredient" + i.getName() + " was not used" );
    }

    private RequiredProperty< Product > product = new RequiredProperty<>( "product" );

    private RequiredProperty< ProductSize > size = new RequiredProperty<>( "size" );

    @ElementCollection
    @Access( AccessType.FIELD )
    private Map< Ingredient, Double > ingredients = new HashMap<>();

}
