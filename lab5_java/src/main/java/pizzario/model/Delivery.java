/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.DomainEntity;
import pizzario.utils.RequiredProperty;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Access( AccessType.PROPERTY )
public class Delivery extends DomainEntity
{

    protected Delivery ()
    {
    }

    public Delivery ( UUID domainId, Order order )
    {
        super( domainId );

        setOrder( order );

        status = DeliveryStatus.Waiting;
    }

    public String getDriverName ()
    {
        return this.driverName;
    }

    protected void setDriverName ( String driverName )
    {
        this.driverName = driverName;
    }

    @ManyToOne
    public Order getOrder ()
    {
        return order.getValue();
    }

    private void setOrder ( Order order )
    {
        this.order.setValue( order );
    }

    @Transient
    public BigDecimal getCash2Collect ()
    {
        return getOrder().getTotalCost();
    }

    public DeliveryStatus getStatus ()
    {
        return status;
    }

    private void setStatus ( DeliveryStatus status )
    {
        this.status = status;
    }

    public void startDelivery ( String driverName )
    {
        if ( this.status != DeliveryStatus.Waiting )
            throw new IllegalStateException( "Delivery: can start in Waiting state only" );

        this.driverName = driverName;
        this.status = DeliveryStatus.InProgress;
    }

    public void finishDelivery ()
    {
        if ( this.status != DeliveryStatus.InProgress )
            throw new IllegalStateException( "Delivery: can finish in InProgress state only" );

        this.status = DeliveryStatus.Delivered;

        getOrder().deliveryCompleted();
    }

    private String driverName;

    private RequiredProperty< Order > order = new RequiredProperty<>( "order" );

    private DeliveryStatus status;
}
