/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.RangeProperty;
import pizzario.utils.Value;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@Embeddable
@Access( AccessType.PROPERTY )
public class Discount extends Value< Discount >
{

    public Discount ()
    {
        setPercent( BigDecimal.ZERO );
    }

    public Discount ( BigDecimal discount )
    {
        setPercent( discount );
    }

    public BigDecimal getPercent ()
    {
        return percent.getValue();
    }

    private void setPercent ( BigDecimal percent )
    {
        this.percent.setValue( percent );
        this.derivedCoeffecient = HUNDRED.subtract( percent ).divide( HUNDRED );
    }

    @Override
    @Transient
    protected List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList( getPercent() );
    }

    public BigDecimal getDiscountedPrice ( BigDecimal price )
    {
        return price.multiply( derivedCoeffecient );
    }

    private static final BigDecimal HUNDRED = BigDecimal.valueOf( 100.00 );

    private RangeProperty< BigDecimal > percent =
            new RangeProperty<>(
                    "value", BigDecimal.ZERO, true, BigDecimal.valueOf( 100 ), true
            );

    private BigDecimal derivedCoeffecient;
}
