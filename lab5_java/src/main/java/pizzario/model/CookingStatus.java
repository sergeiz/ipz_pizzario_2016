/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

public enum CookingStatus
{

    Waiting,
    InProgress,
    Finished

}
