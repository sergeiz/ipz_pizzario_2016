/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.DomainEntity;
import pizzario.utils.NonEmptyString;
import pizzario.utils.RegexString;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import java.util.UUID;

@Entity
@Access( AccessType.PROPERTY )
public class Account extends DomainEntity
{

    protected Account ()
    {
    }

    public Account ( UUID domainId, String name, String email, String password )
    {
        super( domainId );
        setName( name );
        setEmail( email );
        setPassword( password );
    }

    public String getName ()
    {
        return this.name.getValue();
    }

    public void setName ( String name )
    {
        this.name.setValue( name );
    }

    public String getEmail ()
    {
        return this.email.getValue();
    }

    public void setEmail ( String email )
    {
        this.email.setValue( email );
    }

    public String getPassword ()
    {
        return this.password.getValue();
    }

    public void setPassword ( String password )
    {
        this.password.setValue( password );
    }


    public boolean checkPassword ( String password )
    {
        if ( password == null )
            throw new IllegalArgumentException( "password" );

        return this.getPassword() == password;
    }

    public void accept ( AccountVisitor visitor )
    {
        visitor.visit( this );
    }

    private NonEmptyString name = new NonEmptyString( "name" );

    private RegexString email = new RegexString( "email", "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$" );

    private NonEmptyString password = new NonEmptyString( "password" );
}
