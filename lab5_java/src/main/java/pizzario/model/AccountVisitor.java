/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

public interface AccountVisitor
{

    void visit ( Account account );

    void visit ( OperatorAccount account );

}
