/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.model;

import pizzario.utils.DomainEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class ShoppingCart extends DomainEntity
{

    protected ShoppingCart ()
    {
    }

    public ShoppingCart ( UUID domainId )
    {
        super( domainId );
        this.modifiable = true;
    }

    public List< ProductItem > getItems ()
    {
        return items;
    }

    public int findItemIndex ( Product p, ProductSize sz )
    {
        for ( int i = 0; i < items.size(); i++ )
        {
            ProductItem item = items.get( i );
            if ( item.getProduct() == p && item.getSize() == sz )
                return i;
        }

        return -1;
    }

    public void addItem ( ProductItem item )
    {
        if ( !isModifiable() )
            throw new IllegalStateException( "ShoppingCart.addItem: unmodifiable cart" );

        int existingItemIndex = findItemIndex( item.getProduct(), item.getSize() );
        if ( existingItemIndex != -1 )
            throw new IllegalStateException( "ShoppingCart.addItem: duplicate product-size pair added" );

        items.add( item );
    }

    public void updateItem ( int index, ProductItem item )
    {
        if ( !isModifiable() )
            throw new IllegalStateException( "ShoppingCart.updateItem: unmodifiable cart" );

        items.set( index, item );
    }

    public void dropItem ( int index )
    {
        if ( !isModifiable() )
            throw new IllegalStateException( "ShoppingCart.dropItem: unmodifiable cart" );

        items.remove( index );
    }

    public void clearItems ()
    {
        if ( !isModifiable() )
            throw new IllegalStateException( "ShoppingCart.clearItems: unmodifiable cart" );

        items.clear();
    }

    public boolean isModifiable ()
    {
        return this.modifiable;
    }

    public BigDecimal cost ()
    {
        return items.stream()
                .map( item -> item.getCost() )
                .reduce( BigDecimal.ZERO, BigDecimal::add );
    }

    public void checkout ()
    {
        if ( items.isEmpty() )
            throw new IllegalStateException( "ShoppingCart.checkout: locking empty cart" );

        this.modifiable = false;
    }


    @OneToMany( cascade = CascadeType.PERSIST )
    private List< ProductItem > items = new ArrayList<>();

    private boolean modifiable;
}
