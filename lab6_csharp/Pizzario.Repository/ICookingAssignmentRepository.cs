﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System;
using System.Linq;

namespace Pizzario.Repository
{
    public interface ICookingAssignmentRepository : IRepository< CookingAssignment >
    {
        IQueryable< Guid > SelectCookingIds ();

        IQueryable< Guid > SelectWaitingIds ();

        IQueryable< CookingAssignment > FindOrderAssignments ( Order o );
    }
}
