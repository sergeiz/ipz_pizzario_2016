﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

namespace Pizzario.Repository
{
    public interface INamedObjectRepository< T > : IRepository< T > where T : Utils.Entity
    {
        T FindByName  ( string name );
    }
}
