﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Dto;
using Pizzario.Utils.Validators;
using Pizzario.Service.Validators;

using System;
using System.Collections.Generic;


namespace Pizzario.Service
{
    public interface IProductService : IDomainEntityService< ProductDto >
    {
        IList< ProductSizeDto > AvailableSizes ( Guid productId );

        ProductPricingDto ViewPrices ( Guid productId );

        ProductRecipeDto ViewRecipe ( Guid productId, Guid sizeId );

        Guid Create (
            [ NonEmptyStringValidator ] string name,
            string imageUrl 
        );

        void Rename ( 
            Guid productId,
            [ NonEmptyStringValidator ] string newName 
        );

        void UpdateImageUrl ( Guid productId, string imageUrl );

        void DefinePrice ( 
            Guid productId, 
            Guid sizeId,
            [ PriceValidator ] decimal price 
        );

        void UndefinePrice ( Guid productd, Guid sizeId );

        void DefineRecipeIngredient ( 
            Guid productId, 
            Guid sizeId, 
            Guid ingredientId, 
            [ WeightValidator ] double weight
        );

        void RemoveRecipeIngredient ( Guid productId, Guid sizeId, Guid ingredientId ) ;
    }
}
