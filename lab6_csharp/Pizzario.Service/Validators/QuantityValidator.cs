﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace Pizzario.Service.Validators
{

    [ AttributeUsage( AttributeTargets.Property  | 
                      AttributeTargets.Field     | 
                      AttributeTargets.Parameter ) 
    ]
    public class QuantityValidator : ValidatorAttribute
    {
        protected override Validator DoCreateValidator ( Type targetType )
        {
           return new RangeValidator( 
                            0, 
                            RangeBoundaryType.Exclusive, 
                            int.MaxValue,
                            RangeBoundaryType.Ignore
                      );
        }
    }
}
