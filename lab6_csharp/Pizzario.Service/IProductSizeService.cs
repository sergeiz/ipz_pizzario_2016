﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Dto;
using Pizzario.Utils.Validators;
using Pizzario.Service.Validators;

using System;

namespace Pizzario.Service
{
    public interface IProductSizeService : IDomainEntityService< ProductSizeDto >
    {
        Guid Create (
            [ NonEmptyStringValidator ] string name,
            [ DiameterValidator ] int diameter,
            [ WeightValidator ] double weight
        );

        void Rename (
            Guid sizeId,
            [ NonEmptyStringValidator ] string newName 
        );

        void ChangeDiameter ( 
            Guid sizeId,
            [ DiameterValidator ] int newDiameter 
        );

        void ChangeWeight ( 
            Guid sizeId,
            [ WeightValidator ] double weight
        );
    }
}
