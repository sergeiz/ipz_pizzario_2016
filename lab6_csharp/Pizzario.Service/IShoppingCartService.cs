﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Dto;
using Pizzario.Service.Validators;

using System;
using System.Collections.Generic;

namespace Pizzario.Service
{
    public interface IShoppingCartService : IDomainEntityService< ShoppingCartDto >
    {
        IList< Guid > ViewOpen ();

        Guid CreateNew ();

        void SetItem ( 
            Guid cartId, 
            Guid productId,
            Guid sizeId,
            [ QuantityValidator ]   int quantity 
        );

        void RemoveItem ( Guid cartId, Guid productId, Guid sizeId );

        void ClearItems ( Guid cartId );

        void LockCart ( Guid cartId );

        void Remove ( Guid cartId );
    }
}
