﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Dto;
using Pizzario.Utils.Validators;
using Pizzario.Service.Validators;

using System;
using System.Collections.Generic;


namespace Pizzario.Service
{
    public interface IOrderService : IDomainEntityService< OrderDto >
    {
        IList< Guid > ViewUnconfirmed ();

        IList< Guid > ViewReady4Delivery ();

        Guid CreateNew ( 
            [ NonEmptyStringValidator ] string deliveryAddress,
            [ PhoneValidator ] string contactPhone, 
            Guid cartId 
        );

        void SetDiscount ( 
            Guid orderId,
            [ DiscountValidator ] decimal discountPercent 
        );

        void Confirm ( Guid orderId, Guid operatorId );

        void Cancel ( Guid orderID );
    }
}
