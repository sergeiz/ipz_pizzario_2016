﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System.Collections.Generic;

namespace Pizzario.Model
{
    public class Discount : Utils.Value< Discount >
    {

        public decimal Percent { get; private set; }

        public Discount ()
        {
            this.Percent = 0.0M;
        }

        public Discount ( decimal value )
        {
            this.Percent = value;
        }


        public decimal GetDiscountedPrice ( decimal price )
        {
            return price * ( 100.00M - Percent) / 100.00M;
        }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new object[] { Percent };
        }
    }
}
