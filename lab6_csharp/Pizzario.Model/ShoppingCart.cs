﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Exceptions;
using System;
using System.Collections.Generic;

namespace Pizzario.Model
{
    public class ShoppingCart : Utils.Entity
    {
        public virtual IList< ProductItem > Items { get; private set; } 

        public bool Modifiable { get; private set; }

        public decimal Cost
        {
            get
            {
                decimal totalCost = 0;
                foreach ( var item in Items )
                    totalCost += item.Cost;
                return totalCost;
            }
        }

        protected ShoppingCart() {}


        public ShoppingCart ( Guid domainId )
            :   base( domainId )
        {
            this.Items = new List< ProductItem >();
            this.Modifiable = true;
        }

        public int FindItemIndex ( Product product, ProductSize size )
        {
            for ( int i = 0; i < Items.Count; i++ )
            {
                ProductItem item = Items[i];
                if ( item.SelectedProduct == product && item.Size == size )
                    return i;
            }

            return -1;
        }


        public void PlaceItem ( ProductItem item )
        { 
            if ( ! Modifiable )
                throw new UnmodifiableCartException( this.DomainId );

            int existingItemIndex = FindItemIndex( item.SelectedProduct, item.Size);
            if ( existingItemIndex != -1 )
                Items[ existingItemIndex ] = item;

            else
                Items.Add( item );
        }


        public void DropItem ( int index )
        {
            if ( ! Modifiable )
                throw new UnmodifiableCartException( this.DomainId );

            Items.RemoveAt( index );
        }


        public void ClearItems ()
        {
            if ( ! Modifiable )
                throw new UnmodifiableCartException( this.DomainId );

            Items.Clear();
        }


        public void Checkout ()
        {
            if ( Items.Count == 0 )
                throw new LockingEmptyCartException( this.DomainId );

            Modifiable = false;
        }
    }
}
