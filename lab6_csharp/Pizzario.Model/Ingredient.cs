﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace Pizzario.Model
{
    public class Ingredient : Utils.Entity
    {
        public string Name { get; set; }

        protected Ingredient () {}

        public Ingredient ( Guid domainId, string name )
            :   base( domainId )
        {
            this.Name = name;
        }
    }
}
