﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace Pizzario.Model
{
    public class ProductItem : Utils.Entity
    {
        public virtual Product SelectedProduct { get; private set; }
    
        public virtual ProductSize Size { get; private set; }

        public decimal FixedPrice { get; private set; }

        public int Quantity { get; private set; }

        public decimal Cost
        {
            get
            {
                return FixedPrice * Quantity;
            }
        }

        protected ProductItem () {}

        public ProductItem ( Guid domainId, Product product, ProductSize size, int quantity )
            :   base( domainId )
        {
            this.SelectedProduct = product;
            this.Size = size;

            this.FixedPrice = product.GetPrice( size );
            this.Quantity = quantity;
        }
    }
}
