﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Exceptions;
using System;

namespace Pizzario.Model
{
    public class Delivery : Utils.Entity
    {
        public string DriverName { get; private set; }

        public virtual Order RelatedOrder { get; private set; }

        public decimal Cash2Collect
        {
            get { return RelatedOrder.TotalCost; }
        } 

        public DeliveryStatus Status { get; private set; }

        protected Delivery () {}

        public Delivery ( Guid domainId, Order order )
            :   base( domainId )
        {
            this.RelatedOrder = order;
            this.Status = DeliveryStatus.Waiting;
        }


        public void StartDelivery ( string driverName )
        {
            if ( this.Status != DeliveryStatus.Waiting )
                throw DeliveryLifecycleException.MakeStartWhenNotWaiting( this.DomainId );

            DriverName = driverName;
            Status = DeliveryStatus.InProgress;
        }

        public void FinishDelivery ()
        {
            if ( this.Status != DeliveryStatus.InProgress )
                throw DeliveryLifecycleException.MakeFinishWhenNotInProgress( this.DomainId );

            Status = DeliveryStatus.Delivered;

            RelatedOrder.DeliveryCompleted();
        }
    }
}
