﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System.Collections.Generic;

namespace Pizzario.Model
{
    public class Contact : Utils.Value< Contact >
    {
        public string Address { get; private set; }

        public string Phone { get; private set; }

        protected Contact() {}

        public Contact ( string address, string phone )
        {
            this.Address = address;
            this.Phone   = phone;
        }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new object[] { Address, Phone };
        }
    }
}
