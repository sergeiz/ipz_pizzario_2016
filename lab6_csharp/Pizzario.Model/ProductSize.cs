﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace Pizzario.Model
{
    public class ProductSize : Utils.Entity
    {
        public string Name { get; set; }

        public int Diameter { get; set; }

        public double Weight { get; set; }


        protected ProductSize() {}

        public ProductSize ( Guid domainId, string name, int diameter, double weight )
            :   base( domainId )
        {
            this.Name = name;
            this.Diameter = diameter;
            this.Weight = weight;
        }
    }
}