﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Exceptions;
using System;

namespace Pizzario.Model
{
    public class CookingAssignment :  Utils.Entity
    {
        public virtual Order RelatedOrder { get; private set; }

        public virtual Product CookedProduct { get; private set; }

        public virtual ProductSize CookedSize { get; private set; }

        public CookingStatus Status { get; private set; }


        protected CookingAssignment () {}

        public CookingAssignment ( Guid domainId, Order order, Product product, ProductSize size )
            :   base( domainId )
        {
            this.Status = CookingStatus.Waiting;

            this.RelatedOrder = order;
            this.CookedProduct = product;
            this.CookedSize = size;
        }


        public void StartCooking ()
        {
            if ( Status != CookingStatus.Waiting )
                throw CookingLifecycleException.MakeStartWhenNotWaiting( this.DomainId );

            Status = CookingStatus.InProgress;
        }

        public void FinishCooking ()
        {
            if ( Status != CookingStatus.InProgress )
                throw CookingLifecycleException.MakeFinishWhenNotInProgress( this.DomainId );

            Status = CookingStatus.Finished;

            RelatedOrder.CookingAssignmentCompleted();
        }
    }
}
