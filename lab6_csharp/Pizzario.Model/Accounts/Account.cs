﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace Pizzario.Model
{
    public class Account : Utils.Entity
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        protected Account () {}

        public Account ( Guid domainId, string name, string email, string password )
            :   base( domainId )
        {
            this.Name = name;
            this.Email = email;
            this.Password = password;
        }

        public bool CheckPassword ( string password )
        {
            return this.Password == password;
        }

        public virtual void Accept ( AccountVisitor visitor )
        {
            visitor.Visit( this );
        }
    }
}
