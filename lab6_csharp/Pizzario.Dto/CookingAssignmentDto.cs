﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Dto
{
    public class CookingAssignmentDto : DomainEntityDto< CookingAssignmentDto >
    {
        public Guid OrderId { get; private set; }

        public Guid ProductId { get; private set; }

        public string ProductName { get; private set; }

        public Guid SizeId { get; private set; }

        public string SizeName { get; private set; }

        public string Status { get; private set; }


        public CookingAssignmentDto ( Guid domainId,
                                      Guid orderId, 
                                      Guid productId, 
                                      string productName, 
                                      Guid sizeId,
                                      string sizeName,
                                      string status )
            :   base( domainId )
        {
            this.OrderId = orderId;
            this.ProductId = productId;
            this.ProductName = productName;
            this.SizeId = sizeId;
            this.SizeName = sizeName;
            this.Status = status;
        }


        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List< object >{ DomainId, OrderId, ProductId, ProductName, SizeId, SizeName, Status };
        }


        public override string ToString ()
        {
            return string.Format( "CookingId = {0}\nOrderId = {1}\nProduct = {2}\nSize = {3}\nStatus = {4}\n",
                                  DomainId, OrderId, ProductName, SizeName, Status );
        }
    }
}
