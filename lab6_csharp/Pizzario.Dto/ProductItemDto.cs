﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Dto
{
    public class ProductItemDto : DomainEntityDto< ProductItemDto >
    {
        public Guid ProductId { get; private set; }

        public string ProductName { get; private set; }

        public Guid SizeId { get; private set; }

        public string SizeName { get; private set; }

        public int Quantity { get; private set; }

        public decimal FixedPrice { get; private set; }

        public ProductItemDto ( Guid domainId,
                                Guid productId,
                                string productName,
                                Guid sizeId,
                                string sizeName, 
                                int quantity, 
                                decimal fixedPrice )

            :   base( domainId )
        {
            this.ProductId = productId;
            this.ProductName = productName;

            this.SizeId = sizeId;
            this.SizeName = sizeName;

            this.Quantity = quantity;
            this.FixedPrice = fixedPrice;
        }


        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List<object>() { DomainId, ProductId, ProductName, SizeId, SizeName, Quantity, FixedPrice };
        }


        public override string ToString ()
        {
            return string.Format( "\tItemId = {0}\n\tFixedPrice = {1}\n\tProduct = {2}\n\tQuantity = {3}\n\tSize = {4}\n",
                                  DomainId, FixedPrice, ProductName, Quantity, SizeName );
        }
    }
}
