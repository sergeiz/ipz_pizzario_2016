﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Dto
{
    public class ProductDto : DomainEntityDto< ProductDto >
    {
        public string Name { get; private set; }

        public string ImageUrl { get; private set; }

        public ProductDto ( Guid domainId, string name, string imageUrl )
            :   base( domainId )
        {
            this.Name = name;
            this.ImageUrl = imageUrl;
        }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List< object > { DomainId, Name, ImageUrl };
        }

        public override string ToString ()
        {
            return string.Format(
                       "ProductId = {0}\nName = {1}\nImage = {2}\n",
                       DomainId,
                       Name,
                       ImageUrl
                   );
        }
    }
}
