﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

namespace Pizzario.Exceptions
{
    public class PriceUndefinedException : DomainLogicException
    {
        public PriceUndefinedException ( string productName, string sizeName )

            :   base( string.Format( "Price for size \"{0}\" is undefined for product \"{1}\"",
                                     sizeName, productName ) )
        { }
    }
}
