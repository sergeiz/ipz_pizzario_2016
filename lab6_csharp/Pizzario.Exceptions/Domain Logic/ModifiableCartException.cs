﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace Pizzario.Exceptions
{
    public class ModifiableCartException : DomainLogicException
    {
        public ModifiableCartException ( Guid cartId )
            : base( string.Format( "Shopping cart #{0} must be locked by this moment", cartId ) )
        { }
    }
}
