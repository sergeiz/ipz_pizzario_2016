﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

namespace Pizzario.Exceptions
{
    public class RecipeUndefinedException : DomainLogicException
    {
        public RecipeUndefinedException ( string productName, string sizeName )

            : base( string.Format( "Recipe for size \"{0}\" is undefined for product \"{1}\"",
                                     sizeName, productName ) )
        { }
    }
}
