﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace Pizzario.Exceptions
{
    public class CookingLifecycleException : DomainLogicException
    {
        private CookingLifecycleException ( string message )
            : base( message )
        { }

        public static CookingLifecycleException MakeStartWhenNotWaiting ( Guid cookingAssignmentId )
        {
            return new CookingLifecycleException( 
                string.Format( 
                    "Cooking #{0} may be started in Waiting state only" ,
                    cookingAssignmentId
                )
            );
        }

        public static CookingLifecycleException MakeFinishWhenNotInProgress ( Guid cookingAssignmentId )
        {
            return new CookingLifecycleException( 
                string.Format(
                    "Сooking #{0} may be finished in InProgress state only",
                    cookingAssignmentId
                )
            );
        }
    }
}
