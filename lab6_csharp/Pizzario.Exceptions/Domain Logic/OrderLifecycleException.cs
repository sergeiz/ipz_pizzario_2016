﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace Pizzario.Exceptions
{
    public class OrderLifecycleException : DomainLogicException
    {
        public OrderLifecycleException ( Guid orderId, string currentStatus, string expectedStatus )
            : base( 
                  string.Format( 
                      "Order #{0} must have {1} status, while {2} is a current status",
                      orderId,
                      expectedStatus,
                      currentStatus
                  )
             )
        { }
    }
}
