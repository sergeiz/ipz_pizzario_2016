﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace Pizzario.Exceptions
{
    public class LockingEmptyCartException : DomainLogicException
    {
        public LockingEmptyCartException ( Guid cartId )
            : base( string.Format( "Attempted to lock empty cart #{0}", cartId ) )
        { }
    }
}
