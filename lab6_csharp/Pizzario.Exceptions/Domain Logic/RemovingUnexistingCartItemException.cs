﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace Pizzario.Exceptions
{
    public class RemovingUnexistingCartItemException : DomainLogicException
    {
        public RemovingUnexistingCartItemException ( Guid cartId, string productname, string sizeName )

            : base( string.Format( "Removing item \"{0}\" ({1}) that does not exist in cart #{2}",
                                    productname, sizeName, cartId ) )
        { }
    }
}
