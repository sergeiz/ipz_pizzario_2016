﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System;
using System.Linq;

namespace Pizzario.Repository.EntityFramework
{
    public class CookingAssignmentRepository : BasicRepository< CookingAssignment >, ICookingAssignmentRepository
    {
        public CookingAssignmentRepository ( PizzarioDbContext dbContext )
            :   base( dbContext, dbContext.CookingAssignments )
        {
        }

        public IQueryable< Guid > SelectCookingIds ()
        {
            return GetDBSet().Where( ca => ca.Status == CookingStatus.InProgress ).Select( ca => ca.DomainId );
        }

        public IQueryable< Guid > SelectWaitingIds ()
        {
            return GetDBSet().Where( ca => ca.Status == CookingStatus.Waiting ).Select( ca => ca.DomainId );
        }

        public IQueryable< CookingAssignment > FindOrderAssignments ( Order o )
        {
            return GetDBSet().Where( ca => ca.RelatedOrder == o );
        }
    }
}
