﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System.Linq;

namespace Pizzario.Repository.EntityFramework
{
    public class IngredientRepository : BasicRepository< Ingredient >, IIngredientRepository
    {
        public IngredientRepository ( PizzarioDbContext dbContext )
            :   base( dbContext, dbContext.Ingredients )
        {
        }

        public Ingredient FindByName ( string name )
        {
            return GetDBSet().Where( i => i.Name == name ).SingleOrDefault();
        }
    }
}
