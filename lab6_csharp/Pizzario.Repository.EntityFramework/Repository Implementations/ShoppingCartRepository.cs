﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System;
using System.Linq;

namespace Pizzario.Repository.EntityFramework
{
    public class ShoppingCartRepository : BasicRepository< ShoppingCart >, IShoppingCartRepository
    {
        public ShoppingCartRepository ( PizzarioDbContext dbContext )
            :   base( dbContext, dbContext.ShoppingCarts )
        {
        }

        public IQueryable< Guid > SelectAllOpenCartIds ()
        {
            return GetDBSet().Where( c => c.Modifiable == true ).Select( c => c.DomainId );
        }
    }
}
