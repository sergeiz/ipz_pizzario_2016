﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System.Linq;

namespace Pizzario.Repository.EntityFramework
{
    public class ProductSizeRepository : BasicRepository< ProductSize >, IProductSizeRepository
    {
        public ProductSizeRepository( PizzarioDbContext dbContext )
            :   base( dbContext, dbContext.ProductSizes )
        {
        }

        public ProductSize FindByName ( string name )
        {
            return GetDBSet().Where( s => s.Name == name ).SingleOrDefault();
        }
    }
}
