﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System;
using System.Linq;

namespace Pizzario.Repository.EntityFramework
{
    public class AccountRepository : BasicRepository< Account >, IAccountRepository
    {
        public AccountRepository ( PizzarioDbContext dbContext )
            :   base( dbContext, dbContext.Accounts )
        {
        }

        public Account FindByEmail ( string email )
        {
            return GetDBSet().Where( a => a.Email == email ).SingleOrDefault();
        }

        public OperatorAccount FindOperatorByDomainId ( Guid domainId )
        {
            return GetDBSet()
                    .Where( a => a.DomainId == domainId )
                    .OfType< OperatorAccount>()
                    .SingleOrDefault();
        }
    }
}
