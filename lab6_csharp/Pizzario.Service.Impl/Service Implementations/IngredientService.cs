﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;
using Pizzario.Repository;
using Pizzario.Dto;
using Pizzario.Exceptions;

using System;
using System.Collections.Generic;

using Microsoft.Practices.Unity;

namespace Pizzario.Service.Impl
{

    public class IngredientService : IIngredientService
    {
        public IList< Guid > ViewAll ()
        {
            return ingredientRepository.SelectAllDomainIds();
        }

        public IngredientDto View ( Guid ingredientId )
        {
            Ingredient i = ResolveIngredient( ingredientId );
            return i.ToDto();
        }


        public Guid Create ( string name )
        {
            if ( ingredientRepository.FindByName( name ) != null)
                throw new DuplicateNamedEntityException( typeof( Ingredient ), name );

            Ingredient i = new Ingredient( Guid.NewGuid(), name );
            ingredientRepository.Add(i);
            return i.DomainId;
        }

        public void Rename ( Guid ingredientId, string newName )
        {
            if ( ingredientRepository.FindByName( newName ) != null )
                throw new DuplicateNamedEntityException( typeof( Ingredient ), newName );

            Ingredient i = ResolveIngredient(ingredientId);
            i.Name = newName;
        }

        private Ingredient ResolveIngredient ( Guid ingredientId )
        {
            return ServiceUtils.ResolveEntity( ingredientRepository, ingredientId );
        }

        [ Dependency ]
        protected IIngredientRepository ingredientRepository { get; set; }
    }
}
