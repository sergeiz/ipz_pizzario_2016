﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;
using Pizzario.Repository;
using Pizzario.Dto;

using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Practices.Unity;

namespace Pizzario.Service.Impl
{
    public class CookingAssignmentService : ICookingAssignmentService
    {
        public IList< Guid > ViewAll ()
        {
            return cookingAssignmentRepository.SelectAllDomainIds();
        }

        public IList< Guid > ViewCookedRightNow ()
        {
            return cookingAssignmentRepository.SelectCookingIds().ToList();
        }

        public IList< Guid > ViewWaiting ()
        {
            return cookingAssignmentRepository.SelectWaitingIds().ToList();
        }

        public CookingAssignmentDto View ( Guid cookingAssignmentId )
        {
            CookingAssignment cooking = ResolveCooking( cookingAssignmentId );
            return cooking.ToDto();
        }

        public IList< CookingAssignmentDto > ViewOrderAssignments ( Guid orderId )
        {
            var views = new List< CookingAssignmentDto >();

            Order o = ResolveOrder( orderId );
            foreach ( var ca in cookingAssignmentRepository.FindOrderAssignments( o ) )
                views.Add( ca.ToDto() );

            return views;
        }

        public void MarkCookingStarted ( Guid cookingAssignmentId )
        {
            CookingAssignment cooking = ResolveCooking( cookingAssignmentId );
            cooking.StartCooking();
        }

        public void MarkCookingFinished ( Guid cookingAssignmentId )
        {
            CookingAssignment cooking = ResolveCooking( cookingAssignmentId );
            OrderStatus previousOrderStatus = cooking.RelatedOrder.Status;

            cooking.FinishCooking();

            OrderStatus newOrderStatus = cooking.RelatedOrder.Status;
            if ( previousOrderStatus != newOrderStatus )
            {
                Delivery d = cooking.RelatedOrder.GenerateDelivery();
                deliveryRepository.Add( d );
            }
        }

        private CookingAssignment ResolveCooking ( Guid cookingID )
        {
            return ServiceUtils.ResolveEntity( cookingAssignmentRepository, cookingID );
        }

        private Order ResolveOrder ( Guid orderId )
        {
            return ServiceUtils.ResolveEntity( orderRepository, orderId );
        }
        
        [ Dependency ]
        protected ICookingAssignmentRepository cookingAssignmentRepository { get; set; }

        [ Dependency ]
        protected IDeliveryRepository deliveryRepository { get; set; }

        [ Dependency ]
        protected IOrderRepository orderRepository { get; set; }
    }
}
