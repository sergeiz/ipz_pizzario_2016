﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;
using Pizzario.Repository;
using Pizzario.Dto;
using Pizzario.Exceptions;

using System;
using System.Collections.Generic;

using Microsoft.Practices.Unity;


namespace Pizzario.Service.Impl
{
    public class AccountService : IAccountService
    {
        public IList< Guid > ViewAll ()
        {
            return accountRepository.SelectAllDomainIds();
        }


        public AccountDto View ( Guid accountId )
        {
            Account a = ResolveAccount( accountId );
            return a.ToDto();
        }


        public AccountDto Identify ( string email, string password )
        {
            Account a = accountRepository.FindByEmail( email );
            if ( a == null )
                return null;

            if ( !a.CheckPassword( password ) )
                return null;

            return a.ToDto();
        }

        public IList< Guid > ViewAssociatedOrders ( Guid accountId )
        {
            Account a = ResolveAccount( accountId );

            var orderIds = new List< Guid > { };
            a.Accept( new OrdersExtractAccountVisitor( orderIds ) );
            return orderIds;
        }


        public Guid CreateOperator ( string name, string email, string password )
        {
            Account a = accountRepository.FindByEmail( email );
            if ( a != null )
                throw new DuplicateNamedEntityException( typeof( Account ), email );

            OperatorAccount operatorAccount = new OperatorAccount( Guid.NewGuid(), name, email, password );
            accountRepository.Add( operatorAccount );

            return operatorAccount.DomainId;
        }


        public void ChangeName ( Guid accountId, string newName )
        {
            Account a = ResolveAccount( accountId );
            a.Name = newName;
        }


        public void ChangeEmail ( Guid accountId, string newEmail )
        {
            Account a = accountRepository.FindByEmail( newEmail );
            if ( a != null )
                throw new DuplicateNamedEntityException( typeof( Account ), newEmail );

            a.Email = newEmail;
        }


        public void ChangePassword ( Guid accountId, string newPassword )
        {
            Account a = ResolveAccount( accountId );
            a.Password = newPassword;
        }


        private Account ResolveAccount ( Guid accountId )
        {
            return ServiceUtils.ResolveEntity( accountRepository, accountId );
        }


        [ Dependency ]
        protected IAccountRepository accountRepository { get; set; }
    }
}
