﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;
using Pizzario.Repository;
using Pizzario.Dto;

using System;
using System.Collections.Generic;

using Microsoft.Practices.Unity;
using Pizzario.Exceptions;

namespace Pizzario.Service.Impl
{
    public class ProductSizeService : IProductSizeService
    {
        public IList< Guid > ViewAll ()
        {
            return productSizeRepository.SelectAllDomainIds();
        }

        public ProductSizeDto View ( Guid sizeId )
        {
            ProductSize size = ResolveSize( sizeId );
            return size.ToDto();
        }

        public Guid Create ( String name, int diameter, double weight )
        {
            if ( productSizeRepository.FindByName( name ) != null )
                throw new DuplicateNamedEntityException( typeof( ProductSize ), name );

            ProductSize size = new ProductSize( Guid.NewGuid(), name, diameter, weight );
            productSizeRepository.Add( size );
            return size.DomainId;
        }

        public void Rename ( Guid sizeId, String newName )
        {
            if ( productSizeRepository.FindByName( newName ) != null )
                throw new DuplicateNamedEntityException( typeof( ProductSize ), newName );

            ProductSize size = ResolveSize( sizeId );
            size.Name = newName;
        }

        public void ChangeDiameter ( Guid sizeId, int newDiameter )
        {
            ProductSize size = ResolveSize( sizeId );
            size.Diameter = newDiameter;
        }

        public void ChangeWeight ( Guid sizeId, double weight )
        {
            ProductSize size = ResolveSize( sizeId );
            size.Weight = weight;
        }


        private ProductSize ResolveSize ( Guid sizeId )
        {
            return ServiceUtils.ResolveEntity( productSizeRepository, sizeId );
        }


        [ Dependency ]
        protected IProductSizeRepository productSizeRepository { get; set; }
    }
}
