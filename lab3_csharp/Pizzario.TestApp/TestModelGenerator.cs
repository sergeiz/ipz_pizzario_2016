﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System;


namespace Pizzario.TestApp
{
    static class TestModelGenerator
    {
        public static PizzarioModel GenerateTestData ()
        {
            PizzarioModel m = new PizzarioModel();

            GenerateAccounts( m );
            GenerateSizes( m );
            GenerateProducts( m );
            GenerateCarts( m );
            GenerateOrders( m );
            GenerateCookingAssignments( m );
            GenerateDeliveries( m );

            return m;
        }


        private static void GenerateAccounts ( PizzarioModel m )
        {
            OperatorAccount wasya = new OperatorAccount( Guid.NewGuid(), "Wasya Pupkin", "wasya.pupkin@pizzario.com", "12345");
            m.Accounts.Add( wasya );
        }

        private static void GenerateSizes ( PizzarioModel m )
        {
            // ----

            Small = new ProductSize( Guid.NewGuid(), "Small", 25, 300.0 );
            Medium = new ProductSize( Guid.NewGuid(), "Medium", 30, 600.0 );
            Large = new ProductSize( Guid.NewGuid(), "Large", 35, 900.0 );

            // ----

            m.ProductSizes.Add( Small );
            m.ProductSizes.Add( Medium );
            m.ProductSizes.Add( Large );
        }

        private static void GenerateProducts ( PizzarioModel m )
        {
            // ----

            Mafia = new Product( Guid.NewGuid(), "Mafia" );
            Mafia.ImageUrl = "http://pizzario.com/images/mafia.jpg";

            Mafia.Recipe.Add( new Ingredient( "Mozarella" ) );
            Mafia.Recipe.Add( new Ingredient( "Pineaple" ) );
            Mafia.Recipe.Add( new Ingredient( "Chicken" ) );
            Mafia.Recipe.Add( new Ingredient( "Saliami" ) );
            Mafia.Recipe.Add( new Ingredient( "Tomato" ) );

            Mafia.Prices[ Small  ] = 73.00M;
            Mafia.Prices[ Medium ] = 99.00M;
            Mafia.Prices[ Large  ] = 125.00M;

            // ----

            Georgia = new Product( Guid.NewGuid(), "Georgia" );
            Georgia.ImageUrl = "http://pizzario.com/images/georgia.jpg";

            Georgia.Recipe.Add( new Ingredient( "Kebab" ) );
            Georgia.Recipe.Add( new Ingredient( "Mozarella" ) );
            Georgia.Recipe.Add( new Ingredient( "Eggplant" ) );
            Georgia.Recipe.Add( new Ingredient( "Onion" ) );
            Georgia.Recipe.Add( new Ingredient( "Parsley" ) );
            Georgia.Recipe.Add( new Ingredient( "Tomato" ) );

            Georgia.Prices[ Small  ]  = 81.00M;
            Georgia.Prices[ Medium ] = 103.00M;
            Georgia.Prices[ Large  ] = 137.00M;

            // ----

            Cossack = new Product( Guid.NewGuid(), "Cossack" );
            Cossack.ImageUrl = "http://pizzario.com/images/cosscack.jpg";

            Cossack.Recipe.Add( new Ingredient( "Mozarella" ) );
            Cossack.Recipe.Add( new Ingredient( "Bacon" ) );
            Cossack.Recipe.Add( new Ingredient( "Ham" ) );
            Cossack.Recipe.Add( new Ingredient( "Onion" ) ) ;
            Cossack.Recipe.Add( new Ingredient( "Pickled Cucumber" ) );
            Cossack.Recipe.Add( new Ingredient( "Mushroom" ) );

            Cossack.Prices[ Small ]  = 83.00M;
            Cossack.Prices[ Medium ] = 107.00M;
            Cossack.Prices[ Large  ] = 143.00M;

            // ----

            m.Products.Add( Mafia );
            m.Products.Add( Georgia );
            m.Products.Add( Cossack );
        }


        private static void GenerateCarts ( PizzarioModel m )
        {
            cart = new ShoppingCart( Guid.NewGuid() );
            cart.AddItem( new ProductItem( Mafia,   Large,  1 ) );
            cart.AddItem( new ProductItem( Georgia, Small,  2 ) );
            cart.AddItem( new ProductItem( Cossack, Medium, 1 ) );

            cart.Checkout();

            m.ShoppingCarts.Add( cart );
        }


        private static void GenerateOrders ( PizzarioModel m )
        {
            order = new Order( 
                Guid.NewGuid(), 
                cart,
                new Contact( "Sumskaya 1", "123-45-67" ), 
                DateTime.Now 
            );

            order.SetDiscount( new Discount( 20.00M ) );
            order.Confirm();

            m.Orders.Add( order );
        }


        private static void GenerateCookingAssignments ( PizzarioModel m )
        {
            var orderAssignments = order.GenerateCookingAssignments();
            foreach ( CookingAssignment ca in orderAssignments )
                m.Cookings.Add( ca );
        }


        private static void GenerateDeliveries ( PizzarioModel m )
        {
            m.Deliveries.Add( order.GenerateDelivery( "Ivan Vodilkin" ) );
        }


        private static ProductSize Large, Medium, Small;
        private static Product Mafia, Georgia, Cossack;
        private static ShoppingCart cart;
        private static Order order;
    }

}
