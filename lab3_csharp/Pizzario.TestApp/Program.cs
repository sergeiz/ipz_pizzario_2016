﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace Pizzario.TestApp
{
    class Program
    {
        static void Main ( string[] args )
        {
            try
            {
                PizzarioModel model = TestModelGenerator.GenerateTestData();

                ModelReporter reporter = new ModelReporter( Console.Out );
                reporter.GenerateReport( model );
            }
            catch ( Exception e )
            {
                Console.WriteLine( e.GetType().FullName );
                Console.WriteLine( e.Message );
                Console.WriteLine( e.StackTrace );
            }
        }
    }
}
