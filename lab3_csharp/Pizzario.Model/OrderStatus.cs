﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

namespace Pizzario.Model
{
    public enum OrderStatus
    {
        Placed,
        Confirmed,
        Cancelled,
        Delivering,
        Completed
    }
}
