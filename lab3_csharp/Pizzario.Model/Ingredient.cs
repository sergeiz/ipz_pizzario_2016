﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Model
{
    public class Ingredient : Utils.Value< Ingredient >
    {
        public string Name
        {
            get { return _name.Value; }
        }


        public Ingredient ( string name )
        {
            this._name.Value = name;
        }

        public override string ToString()
        {
            return Name;
        }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck()
        {
            return new object[] { Name };
        }

        private readonly Utils.NonEmptyString _name = new Utils.NonEmptyString( "name" );
    }
}
