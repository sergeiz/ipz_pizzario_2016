﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace Pizzario.Model
{
    public class CookingAssignment :  Utils.Entity
    {
        public Order RelatedOrder
        {
            get { return _order.Value; }
        }

        public Product CookedProduct
        {
            get { return _product.Value; }
        }

        public ProductSize CookedSize
        {
            get { return _size.Value; }
        }

        public CookingStatus Status { get; private set; }


        public CookingAssignment ( Guid id, Order order, Product product, ProductSize size )
            :   base( id )
        {
            this.Status = CookingStatus.Waiting;

            this._order.Value = order;
            this._product.Value = product;
            this._size.Value = size;
        }

        public override string ToString()
        {
            return string.Format(
                       "ID = {0}\nOrder # = {1}\nProduct = {2}\nSize = {3}\nStatus = {4}",
                       Id,
                       RelatedOrder.Id,
                       CookedProduct.Name,
                       CookedSize.Name,
                       Status
                   );
        }

        public void StartCooking ()
        {
            if ( Status != CookingStatus.Waiting )
                throw new InvalidOperationException( "CookingAssignment: cooking may be started in Waiting state only" );

            Status = CookingStatus.InProgress;
        }

        public void FinishCooking ()
        {
            if ( Status != CookingStatus.InProgress )
                throw new InvalidOperationException( "CookingAssignment: cooking may be finished in InProgress state only" );

            Status = CookingStatus.Finished;

            RelatedOrder.CookingAssignmentCompleted();
        }


        private readonly Utils.RequiredProperty< Order > _order
            = new Utils.RequiredProperty< Order >( "order" );

        private readonly Utils.RequiredProperty< Product > _product
            = new Utils.RequiredProperty< Product >( "product" );

        private readonly Utils.RequiredProperty< ProductSize > _size
            = new Utils.RequiredProperty< ProductSize >( "size" );
    }
}
