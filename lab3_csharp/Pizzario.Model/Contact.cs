﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Model
{
    public class Contact : Utils.Value< Contact >
    {
        public string Address {
            get { return _address.Value; }
        }

        public string Phone {
            get { return _phone.Value; }
        }

        public Contact ( string address, string phone )
        {
            _address.Value = address;
            _phone.Value   = phone;
        }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new object[] { Address, Phone };
        }

        private readonly Utils.NonEmptyString _address = new Utils.NonEmptyString( "address" );
        private readonly Utils.NonEmptyString _phone   = new Utils.NonEmptyString( "phone" );
    }
}
