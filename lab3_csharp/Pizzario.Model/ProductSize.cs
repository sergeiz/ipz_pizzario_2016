﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace Pizzario.Model
{
    public class ProductSize : Utils.Entity
    {
        public string Name
        {
            get { return _name.Value; }
            set { _name.Value = value; }
        }

        public int Diameter
        {
            get { return _diameter.Value; }
            set { _diameter.Value = value; }
        }

        public double Weight
        {
            get { return _weight.Value; }
            set {  _weight.Value = value; }
        }


        public ProductSize ( Guid id, string name, int diameter, double weight )
            :   base( id )
        {
            this.Name = name;
            this.Diameter = diameter;
            this.Weight = weight;
        }

        public override string ToString()
        {
            return string.Format(
                       "ID = {0}\nName = {1}\nDiameter = {2}\nWeight = {3}",
                       Id,
                       Name,
                       Diameter,
                       Weight
                   );
        }

        private Utils.NonEmptyString _name = new Utils.NonEmptyString( "name" );

        private Utils.RangeProperty< int > _diameter = 
           new Utils.RangeProperty< int >( "diameter", 0, false, int.MaxValue, true );

        private Utils.RangeProperty< double > _weight =
            new Utils.RangeProperty< double >( "weight", 0, false, double.MaxValue, true );
    }
}