﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Model
{
    public class OperatorAccount : Account
    {
        public ICollection< Order > Orders
        {
            get
            {
                return _orders.AsReadOnly();
            }
        }


        public OperatorAccount ( Guid id, string name, string email, string passwordHash )
            :   base( id, name, email, passwordHash )
        {
            this._orders = new List< Order >();
        }


        public void TrackOrder ( Order order )
        {
            if ( order == null )
                throw new ArgumentNullException( "order" );

            _orders.Add( order );
        }


        private readonly List< Order > _orders;
    }
}
