﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;
using System.Text;

namespace Pizzario.Model
{
    public class ShoppingCart : Utils.Entity
    {
        public IList< ProductItem > Items
        {
            get { return _items.AsReadOnly(); }
        }

        public bool Modifiable { get; private set; }

        public decimal Cost
        {
            get
            {
                decimal totalCost = 0;
                for ( int i = 0; i < Items.Count; i++ )
                    totalCost += Items[i].Cost;
                return totalCost;
            }
        }


        public ShoppingCart ( Guid id )
            :   base( id )
        {
            this._items = new List< ProductItem >();
            this.Modifiable = true;
        }


        public override string ToString ()
        {
            StringBuilder b = new StringBuilder();

            b.AppendFormat( "Id = {0}\n", Id );

            b.AppendLine( "Content:" );
            foreach( var item in Items )
            {
                b.Append( "\t" );
                b.Append( item );
                b.AppendLine();
            }

            b.AppendFormat( "Modifiable = {0}\n", Modifiable );

            return b.ToString();
        }


        public int FindItemIndex ( Product product, ProductSize size )
        {
            for ( int i = 0; i < Items.Count; i++ )
            {
                ProductItem item = Items[i];
                if ( item.SelectedProduct == product && item.Size == size )
                    return i;
            }

            return -1;
        }


        public void AddItem ( ProductItem item )
        { 
            if ( ! Modifiable )
                throw new InvalidOperationException( "ShoppingCart.AddItem: unmodifiable cart" );

            int existingItemIndex = FindItemIndex( item.SelectedProduct, item.Size);
            if ( existingItemIndex != -1 )
                throw new InvalidOperationException( "ShoppingCart.AddItem: duplicate product-size pair added" );

            _items.Add( item );
        }


        public void UpdateItem ( int index, ProductItem item )
        {
            if ( ! Modifiable )
                throw new InvalidOperationException( "ShoppingCart.UpdateItem: unmodifiable cart" );

            _items[ index ] = item;
        }


        public void DropItem ( int index )
        {
            if ( ! Modifiable )
                throw new InvalidOperationException( "ShoppingCart.DropItem: unmodifiable cart" );

            _items.RemoveAt( index );
        }


        public void ClearItems ()
        {
            if ( ! Modifiable )
                throw new InvalidOperationException( "ShoppingCart.ClearItems: unmodifiable cart" );

            _items.Clear();
        }


        public void Checkout ()
        {
            if ( Items.Count == 0 )
                throw new InvalidOperationException( "ShoppingCart.Lock: locking empty cart" );

            Modifiable = false;
        }


        private readonly List< ProductItem > _items;
    }
}
