﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;
using System.Text;


namespace Pizzario.Model
{
    public class Product : Utils.Entity
    {
        public string Name
        {
            get {  return _name.Value; }
            set { _name.Value = value; }
        }

        public string ImageUrl { get; set; }

        public ICollection< Ingredient > Recipe
        {
            get { return _recipe; }
        }

        public IDictionary< ProductSize, decimal > Prices
        {
            get { return _prices; }
        }


        public Product ( Guid id, string name )
            :   base( id )
        {
            this.Name = name;
            this.ImageUrl = "";

            this._recipe = new HashSet< Ingredient >();
            this._prices = new Dictionary< ProductSize, decimal >();
        }

        public override string ToString()
        {
            StringBuilder pricesAsString = new StringBuilder();
            foreach ( var entry in Prices )
                pricesAsString.AppendFormat( "{0} = {1}  ", entry.Key.Name, entry.Value );

            return string.Format(
                       "ID = {0}\nName = {1}\nImageUrl = {2}\nRecipe: {3}\nPrices: {4}",
                       Id,
                       Name,
                       ImageUrl,
                       String.Join( ", ", _recipe ),
                       pricesAsString.ToString()
                   );
        }

        private Utils.NonEmptyString _name = new Utils.NonEmptyString( "name" );

        private HashSet< Ingredient > _recipe;
        private Dictionary< ProductSize, decimal > _prices;
    }
}
