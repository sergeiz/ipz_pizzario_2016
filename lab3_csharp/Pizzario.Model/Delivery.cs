﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace Pizzario.Model
{
    public class Delivery : Utils.Entity
    {
        public string DriverName
        {
            get {  return _driverName.Value; }
            set { _driverName.Value = value; }
        }

        public Order RelatedOrder
        {
            get { return _order.Value; }
        }

        public decimal Cash2Collect
        {
            get { return RelatedOrder.TotalCost; }
        } 

        public DeliveryStatus Status { get; private set; }

        public Delivery ( Guid id, string driverName, Order order )
            :   base( id )
        {
            this.DriverName = driverName;
            this._order.Value = order;

            this.Status = DeliveryStatus.Waiting;
        }

        public override string ToString()
        {
            return string.Format(
                       "ID = {0}\nDriver = {1}\nOrder # = {2}\nStatus = {3}",
                       Id,
                       DriverName,
                       RelatedOrder.Id,
                       Status
                   );
        }


        public void StartDelivery ()
        {
            if ( this.Status != DeliveryStatus.Waiting )
                throw new InvalidOperationException( "Delivery: can start in Waiting state only" );

            Status = DeliveryStatus.InProgress;
        }

        public void FinishDelivery ()
        {
            if ( this.Status != DeliveryStatus.InProgress )
                throw new InvalidOperationException( "Delivery: can finish in InProgress state only" );

            Status = DeliveryStatus.Delivered;

            RelatedOrder.DeliveryCompleted();
        }

        private Utils.NonEmptyString _driverName = new Utils.NonEmptyString( "driverName" );

        private readonly Utils.RequiredProperty< Order > _order = new Utils.RequiredProperty< Order >( "order" );
    }
}
