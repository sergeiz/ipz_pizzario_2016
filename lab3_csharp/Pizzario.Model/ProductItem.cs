﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Model
{
    public class ProductItem : Utils.Value< ProductItem >
    {
        public Product SelectedProduct
        {
            get { return _product.Value; }
        }
    
        public ProductSize Size
        {
            get { return _size.Value; }
        }

        public decimal FixedPrice {
            get {  return _fixedPrice.Value; }
        }

        public int Quantity
        {
            get { return _quantity.Value; }
        }

        public decimal Cost
        {
            get
            {
                return FixedPrice * Quantity;
            }
        }

        public ProductItem ( Product product, ProductSize size, int quantity )
        {
            if ( quantity <= 0 )
                throw new Exception( "ProductItem: non-positive quantity" );

            this._product.Value = product;
            this._size.Value = size;

            this._fixedPrice.Value = product.Prices[ size ];
            this._quantity.Value = quantity;
        }

        public override string ToString()
        {
            return string.Format(
                       "Product = {0}, Size = {1}, Price = {2}, Quantity = {3}",
                       SelectedProduct.Name,
                       Size.Name,
                       FixedPrice,
                       Quantity
                   );
        }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new object[] { SelectedProduct, Size, FixedPrice, Quantity };
        }


        private readonly Utils.RequiredProperty< Product > _product =
            new Utils.RequiredProperty< Product >( "product" );

        private readonly Utils.RequiredProperty< ProductSize > _size =
            new Utils.RequiredProperty< ProductSize >( "size" );

        private readonly Utils.RangeProperty< int > _quantity =
            new Utils.RangeProperty< int >( "quantity", 0, false, int.MaxValue, true );

        private readonly Utils.RangeProperty< decimal > _fixedPrice =
            new Utils.RangeProperty< decimal >( "fixedPrice", 0, true, decimal.MaxValue, true );
    }
}
