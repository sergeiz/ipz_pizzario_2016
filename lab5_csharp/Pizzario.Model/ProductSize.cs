﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace Pizzario.Model
{
    public class ProductSize : Utils.Entity
    {
        public string Name
        {
            get { return _name.Value; }
            set { _name.Value = value; }
        }

        public int Diameter
        {
            get { return _diameter.Value; }
            set { _diameter.Value = value; }
        }

        public double Weight
        {
            get { return _weight.Value; }
            set {  _weight.Value = value; }
        }


        protected ProductSize() {}

        public ProductSize ( Guid domainId, string name, int diameter, double weight )
            :   base( domainId )
        {
            this.Name = name;
            this.Diameter = diameter;
            this.Weight = weight;
        }


        private Utils.NonEmptyString _name = new Utils.NonEmptyString( "name" );

        private Utils.RangeProperty< int > _diameter = 
           new Utils.RangeProperty< int >( "diameter", 0, false, int.MaxValue, true );

        private Utils.RangeProperty< double > _weight =
            new Utils.RangeProperty< double >( "weight", 0, false, double.MaxValue, true );
    }
}