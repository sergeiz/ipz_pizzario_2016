﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Text.RegularExpressions;

namespace Pizzario.Model
{
    public class Account : Utils.Entity
    {
        public string Name {
            get { return _name.Value; }
            set { _name.Value = value; }
        }

        public string Email {
            get { return _email.Value; }
            set {  _email.Value = value; }
        }

        public string Password {
            get { return _password.Value; }
            set {  _password.Value = value; }
        }

        protected Account () {}

        public Account ( Guid domainId, string name, string email, string password )
            :   base( domainId )
        {
            this.Name = name;
            this.Email = email;
            this.Password = password;
        }

        public bool CheckPassword ( string password )
        {
            if ( password == null )
                throw new ArgumentNullException( "password" );

            return this._password.Value == password;
        }

        public virtual void Accept ( AccountVisitor visitor )
        {
            visitor.Visit( this );
        }


        private Utils.NonEmptyString _name = new Utils.NonEmptyString("name");
        private Utils.RegexString _email = new Utils.RegexString( "email", "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$" );
        private Utils.NonEmptyString _password = new Utils.NonEmptyString( "password" );

    }
}
