﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pizzario.Model
{
    public class Recipe : Utils.Entity
    {
        public virtual Product RelatedProduct { get; private set; }

        public virtual ProductSize Size { get; private set; }

        public virtual ICollection< IngredientRecipeInclusion > Ingredients { get; private set; }

        public double TotalIngredientsWeight
        {
            get
            {
                double total = 0;
                foreach ( var inclusion in Ingredients )
                    total += inclusion.Weight;

                return total;
            }
        }

        protected Recipe () {}

        public Recipe ( Guid domainId, Product product, ProductSize size )
            :   base( domainId )
        {
            this.Ingredients = new List< IngredientRecipeInclusion >();

            this.RelatedProduct = product;
            this.Size = size;
        }

        public bool UsesIngredient ( Ingredient i )
        {
            var inclusion = Ingredients.Where( incl => incl.IncludedIngredient == i ).FirstOrDefault();
            return ( inclusion != null );
        }

        public double GetIngredientWeight ( Ingredient i )
        {
            var inclusion = Ingredients.Where(incl => incl.IncludedIngredient == i).FirstOrDefault();
            if ( inclusion != null )
                return inclusion.Weight;

            throw new InvalidOperationException( "Ingredient " + i.Name + " not included to the recipe" );
        }

        public Recipe UseIngredient ( Ingredient i, double weight )
        {
            var inclusion = Ingredients.Where(incl => incl.IncludedIngredient == i).FirstOrDefault();
            if ( inclusion != null )
                inclusion.Weight = weight;

            else
                Ingredients.Add( new IngredientRecipeInclusion( this, i, weight ) );

            return this;
        }

        public void RemoveIngredient ( Ingredient i )
        {
            var inclusion = Ingredients.Where( incl => incl.IncludedIngredient == i ).FirstOrDefault();
            if ( inclusion != null )
                Ingredients.Remove( inclusion );

            else
                throw new ArgumentException( "Ingredient" + i.Name + " was not used" );
        }

    }
}
