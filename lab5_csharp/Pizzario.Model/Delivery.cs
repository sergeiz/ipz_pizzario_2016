﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace Pizzario.Model
{
    public class Delivery : Utils.Entity
    {
        public string DriverName { get; private set; }

        public virtual Order RelatedOrder
        {
            get { return _order.Value; }
            private set { _order.Value = value; }
        }

        public decimal Cash2Collect
        {
            get { return RelatedOrder.TotalCost; }
        } 

        public DeliveryStatus Status { get; private set; }

        protected Delivery () {}

        public Delivery ( Guid domainId, Order order )
            :   base( domainId )
        {
            this._order.Value = order;

            this.Status = DeliveryStatus.Waiting;
        }


        public void StartDelivery ( string driverName )
        {
            if ( this.Status != DeliveryStatus.Waiting )
                throw new InvalidOperationException( "Delivery: can start in Waiting state only" );

            if ( driverName == null )
                throw new ArgumentNullException( "driverName" );

            if ( driverName.Length == 0 )
                throw new ArgumentException( "Empty value not allowed", "driverName" );

            DriverName = driverName;
            Status = DeliveryStatus.InProgress;
        }

        public void FinishDelivery ()
        {
            if ( this.Status != DeliveryStatus.InProgress )
                throw new InvalidOperationException( "Delivery: can finish in InProgress state only" );

            Status = DeliveryStatus.Delivered;

            RelatedOrder.DeliveryCompleted();
        }

        private string _driverName;

        private readonly Utils.RequiredProperty< Order > _order = new Utils.RequiredProperty< Order >( "order" );
    }
}
