﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Model
{
    public class Ingredient : Utils.Entity
    {
        public string Name
        {
            get { return _name.Value; }
            set { _name.Value = value; }
        }

        protected Ingredient () {}

        public Ingredient ( Guid domainId, string name )
            :   base( domainId )
        {
            this._name.Value = name;
        }


        private readonly Utils.NonEmptyString _name = new Utils.NonEmptyString( "name" );
    }
}
