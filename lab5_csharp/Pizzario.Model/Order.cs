﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;
using System.Text;

namespace Pizzario.Model
{
    public class Order : Utils.Entity
    {
        public virtual ICollection< ProductItem > Items { get; private set; }

        public decimal BasicCost { get; private set; }

        public Discount AssignedDiscount { get; private set; }

        public decimal TotalCost
        {
            get
            {
                return AssignedDiscount.GetDiscountedPrice( BasicCost );
            }
        }

        public OrderStatus Status { get; private set; }

        public DateTime PlacementTime { get; private set; }

        public Contact CustomerContact
        {
            get { return _contact.Value; }
            private set {  _contact.Value = value; }
        }

        public int UnfinishedCookingsCount { get; private set; }

        protected Order () {}

        public Order ( Guid domainId, ShoppingCart cart, Contact contact, DateTime time )
            :   base( domainId )
        {
            if ( cart.Modifiable )
                throw new InvalidOperationException( "Order: initializing with a modifiable cart" );

            this.Items = new List< ProductItem >( cart.Items );
            this._contact.Value = contact;

            this.BasicCost = cart.Cost;
            this.AssignedDiscount = new Discount();

            this.Status = OrderStatus.Placed;
            this.PlacementTime = time;

            this.UnfinishedCookingsCount = 0;
            foreach ( ProductItem item in Items )
                this.UnfinishedCookingsCount += item.Quantity;
        }


        public void SetDiscount ( Discount discount )
        {
            if ( discount == null )
                throw new ArgumentNullException( "Order.SetDiscount: null discount object" );

            AssignedDiscount = discount;
        }

        public void Confirm ()
        {
            if ( Status != OrderStatus.Placed )
                throw new InvalidOperationException("Order.Confirm - can only run in Placed state");

            Status = OrderStatus.Confirmed;
        }

        public void Cancel ()
        {
            if ( Status != OrderStatus.Placed )
                throw new InvalidOperationException("Order.Cancel - can only run in Placed state");

            Status = OrderStatus.Cancelled;
        }

        public void CookingAssignmentCompleted ()
        {
            if ( Status == OrderStatus.Confirmed )
            {
                -- UnfinishedCookingsCount;
                if ( UnfinishedCookingsCount == 0 )
                    Status = OrderStatus.Delivering;
            }
            else
                throw new InvalidOperationException( "Order.CookingAssignmentCompleted - can only happen in Confirmed state" );

        }

        public void DeliveryCompleted ()
        {
            if ( Status != OrderStatus.Delivering )
                throw new InvalidOperationException("Order.DeliveryCompleted - can only happen in Delivering state");

            Status = OrderStatus.Completed;
        }


        public IList< CookingAssignment > GenerateCookingAssignments ()
        {
            var cookingAssignments = new List< CookingAssignment >();

            foreach ( ProductItem item in Items )
            {
                for ( int i = 0; i < item.Quantity; i++ )
                    cookingAssignments.Add( 
                        new CookingAssignment( 
                            Guid.NewGuid(), 
                            this, 
                            item.SelectedProduct, 
                            item.Size 
                        )
                    );
            }

            return cookingAssignments;
        }


        public Delivery GenerateDelivery ()
        {
            return new Delivery( Guid.NewGuid(), this );
        }

        private Utils.RequiredProperty< Contact > _contact = new Utils.RequiredProperty< Contact >( "contact" );
    }
}
