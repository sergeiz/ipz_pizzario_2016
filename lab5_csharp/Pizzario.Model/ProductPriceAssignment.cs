﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

namespace Pizzario.Model
{
    public class ProductPriceAssignment
    {
        public long AssignmentId { get; set; }

        public virtual Product RelatedProduct { get; private set; }

        public virtual ProductSize Size { get; private set; }

        public decimal Price
        {
            get { return _price.Value; }
            set {  _price.Value = value; }
        }

        protected ProductPriceAssignment () {}

        public ProductPriceAssignment ( Product product, ProductSize size, decimal price )
        {
            this.RelatedProduct = product;
            this.Size = size;
            this.Price = price;
        }

        private Utils.RangeProperty< decimal > _price =
            new Utils.RangeProperty< decimal >( "price", 0.00M, true, decimal.MaxValue, false );
    }
}
