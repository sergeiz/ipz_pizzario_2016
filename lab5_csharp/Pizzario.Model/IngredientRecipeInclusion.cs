﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

namespace Pizzario.Model
{
    public class IngredientRecipeInclusion
    {
        public long InclusionId { get; set; }

        public virtual Recipe RelatedRecipe { get; private set; }

        public virtual Ingredient IncludedIngredient { get; private set; }

        public double Weight
        {
            get { return _weight.Value; }
            set { _weight.Value = value; }
        }

        protected IngredientRecipeInclusion() { }

        public IngredientRecipeInclusion ( Recipe recipe, Ingredient ingredient, double weight )
        {
            this.RelatedRecipe = recipe;
            this.IncludedIngredient = ingredient;
            this.Weight = weight;
        }

        private Utils.RangeProperty< double > _weight =
            new Utils.RangeProperty< double >( "weight", 0.00, false, double.MaxValue, false );
    }
}
