﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Dto
{
    public class AccountDto : DomainEntityDto< AccountDto >
    {
        public string Name { get; private set; }

        public string Email { get; private set; }


        public AccountDto ( Guid domainId, string name, string email )
            :   base( domainId )
        {
            this.Name = name;
            this.Email = email;
        }


        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List<object> { DomainId, Name, Email };
        }


        public override string ToString ()
        {
            return string.Format( "AccountId = {0}\nName = {1}\nEmail = {2}\n", DomainId, Name, Email );
        }
    }
}
