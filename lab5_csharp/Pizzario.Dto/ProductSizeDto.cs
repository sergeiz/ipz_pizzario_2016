﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Dto
{
    public class ProductSizeDto : DomainEntityDto< ProductSizeDto >
    {
        public string Name { get; private set; }

        public int Diameter { get; private set; }

        public double Weight { get; private set; }

        public ProductSizeDto ( Guid domainId, string name, int diameter, double weight )
            :   base( domainId )
        {
            this.Name = name;
            this.Diameter = diameter;
            this.Weight = weight;
        }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List< object > { DomainId, Name, Diameter, Weight };
        }

        public override string ToString ()
        {
            return string.Format( "ProductSizeId = {0}\nName = {1}\nDiameter = {2}\nWeight = {3}\n", DomainId, Name, Diameter, Weight );
        }
    }
}