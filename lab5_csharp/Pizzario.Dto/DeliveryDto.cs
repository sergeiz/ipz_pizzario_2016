﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Dto
{
    public class DeliveryDto : DomainEntityDto< DeliveryDto >
    {
        public Guid OrderId { get; private set; }

        public string DriverName { get; private set; }

        public string Status { get; private set; }

        public decimal Cash2Collect { get; private set; }


        public DeliveryDto ( Guid domainId, Guid orderId, string driverName, string status, decimal cash2Collect )
            :   base( domainId )
        {
            this.OrderId = orderId;
            this.DriverName = driverName;
            this.Status = status;
            this.Cash2Collect = cash2Collect;
        }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            return new List< object > { DomainId, OrderId, Cash2Collect, DriverName, Status };
        }

        public override string ToString ()
        {
            return string.Format( "DeliveryId = {0}\nOrderId = {1}\nDriverName = {2}\nCash = {3}\nStatus = {4}\n",
                                  DomainId, OrderId, ( DriverName != null ) ? DriverName : "<unassigned>", Cash2Collect, Status );
        }
    }
}
