﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;
using System.Text;

namespace Pizzario.Dto
{
    public class ProductRecipeDto : Utils.Value< ProductRecipeDto >
    {
        public Guid ProductId { get; private set; }

        public Guid SizeId { get; private set; }

        public IDictionary< IngredientDto, double > UsedIngredients { get; private set; }

        public ProductRecipeDto ( Guid productId,
                                  Guid sizeId, 
                                  IDictionary< IngredientDto, double > usedIngredients )
        {
            this.ProductId = productId;
            this.SizeId = sizeId;
            this.UsedIngredients = usedIngredients;
        }


        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            var list = new List< object >() { ProductId, SizeId };
            foreach ( var ingredientEntry in UsedIngredients )
            {
                list.Add( ingredientEntry.Key.DomainId );
                list.Add( ingredientEntry.Value );
            }

            return list;
        }

        public override string ToString ()
        {
            StringBuilder b = new StringBuilder();

            bool first = true;
            foreach ( var ingredientEntry in UsedIngredients )
            {
                if ( !first )
                    b.Append( ',' );
                else
                    first = false;

                b.Append( ' ' );
                b.Append( ingredientEntry.Key.Name );
                b.Append( " = " );
                b.Append( ingredientEntry.Value );
            }

            return b.ToString();
        }
    }
}
