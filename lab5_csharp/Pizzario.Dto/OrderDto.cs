﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;

namespace Pizzario.Dto
{
    public class OrderDto : DomainEntityDto< OrderDto >
    {
        public string CustomerAddress { get; private set; }

        public string ContactPhone { get; private set; }

        public DateTime PlacementTime { get; private set; }

        public string Status { get; private set; }

        public IList< ProductItemDto > Items { get; private set; }

        public decimal BasicCost { get; private set; }

        public decimal TotalCost { get; private set; }

        public decimal DiscountPercentage { get; private set; }

        public OrderDto ( Guid domainId, 
                          string customerAddress,
                          string contactPhone,
                          DateTime placementTime,
                          string status,
                          IList< ProductItemDto > items,
                          decimal basicCost,
                          decimal totalCost,
                          decimal discountPercentage )

            :   base( domainId )
        {
            this.CustomerAddress = customerAddress;
            this.ContactPhone = contactPhone;
            this.PlacementTime = placementTime;
            this.Status = status;
            this.Items = items;
            this.BasicCost = basicCost;
            this.TotalCost = totalCost;
            this.DiscountPercentage = discountPercentage;
        }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            var list = new List< object >()
            {
                DomainId,
                BasicCost, TotalCost, DiscountPercentage,
                ContactPhone, CustomerAddress,
                Status, PlacementTime
            };

            foreach ( var item in Items )
                list.Add( item );

            return list;
        }

        public override string ToString ()
        {
            return string.Format(
                  "OrderId = {0}\nItems:\n{1}Address = {2}\nPhone = {3}\nStatus = {4}\nPlaced = {5}\nBasic Cost = {6}\nTotal Cost = {7}\nDiscount = {8}\n",
                  DomainId,
                  string.Join( "\n", Items ),
                  CustomerAddress,
                  ContactPhone,
                  Status,
                  PlacementTime,
                  BasicCost,
                  TotalCost,
                  DiscountPercentage
            );
        }
    }
}
