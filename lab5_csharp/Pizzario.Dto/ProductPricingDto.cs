﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Collections.Generic;
using System.Text;

namespace Pizzario.Dto
{
    public class ProductPricingDto : Utils.Value< ProductPricingDto >
    {
        public Guid ProductId { get; private set; }

        public IDictionary< ProductSizeDto, decimal > Prices { get; private set; }

        public ProductPricingDto ( Guid productId, IDictionary< ProductSizeDto, decimal > prices )
        {
            this.ProductId = productId;
            this.Prices = prices;
        }

        protected override IEnumerable< object > GetAttributesToIncludeInEqualityCheck ()
        {
            var list = new List< object > { ProductId };
            foreach ( var priceEntry in Prices )
            {
                list.Add( priceEntry.Key.DomainId );
                list.Add( priceEntry.Value );
            }

            return list;
        }

        public override string ToString ()
        {
            StringBuilder pricesAsString = new StringBuilder();
            pricesAsString.Append( "Prices: " );

            foreach ( var priceEntry in Prices )
                pricesAsString.Append(
                    string.Format(
                        "{0} = ${1}  ",
                        priceEntry.Key.Name,
                        priceEntry.Value
                    )
                );

            return pricesAsString.ToString();
        }
    }
}
