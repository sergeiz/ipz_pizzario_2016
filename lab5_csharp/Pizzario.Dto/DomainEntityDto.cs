﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;

namespace Pizzario.Dto
{
    public abstract class DomainEntityDto< TConcreteDto > : Utils.Value< DomainEntityDto< TConcreteDto > >
    {
        public Guid DomainId { get; private set; }

        protected DomainEntityDto ( Guid domainId )
        {
            this.DomainId = domainId;
        }
    }
}