﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System.IO;
using Pizzario.Service;

namespace Pizzario.TestApp
{
    class ModelReporter
    {
        public ModelReporter ( ServiceProvider serviceProvider, TextWriter output )
        {
            this.serviceProvider = serviceProvider;
            this.output = output;
        }

        public void GenerateReport ()
        {
            ReportCollection( "Ingredients", serviceProvider.ProvideIngredientService() );
            ReportCollection( "Sizes", serviceProvider.ProvideProductSizeService() );
            ReportProducts( "Products", serviceProvider.ProvideProductService() );
            ReportCollection( "Carts", serviceProvider.ProvideShoppingCartService() );
            ReportCollection( "Orders", serviceProvider.ProvideOrderService() );
            ReportCollection( "Accounts", serviceProvider.ProvideAccountService() );
            ReportCollection( "Cookings", serviceProvider.ProvideCookingAssignmentService() );
            ReportCollection( "Deliveries", serviceProvider.ProvideDeliveryService() );
        }

        private void ReportProducts ( string title, IProductService service )
        {
            output.WriteLine( "==== {0} ==== ", title );
            output.WriteLine();

            foreach ( var productId in service.ViewAll() )
            {
                output.WriteLine( service.View( productId ) );
                output.WriteLine( service.ViewPrices( productId ) );

                foreach ( var sizeView in service.AvailableSizes( productId ) )
                    output.WriteLine( service.ViewRecipe( productId, sizeView.DomainId ) );

                output.WriteLine();
                output.WriteLine();
            }

            output.WriteLine();
        }

        private void ReportCollection< TDto > ( string title, IDomainEntityService< TDto > service )
            where TDto : Dto.DomainEntityDto< TDto >
        {
            output.WriteLine("==== {0} ==== ", title );
            output.WriteLine();

            foreach ( var entityId in service.ViewAll() )
            {
                output.Write( service.View( entityId ) );

                output.WriteLine();
                output.WriteLine();
            }

            output.WriteLine();
        }

        private ServiceProvider serviceProvider;
        private TextWriter output;
    }
}
