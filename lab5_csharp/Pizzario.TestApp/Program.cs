﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Repository.EntityFramework;

using System;

namespace Pizzario.TestApp
{
    class Program
    {
        static void Main ( string[] args )
        {
            try
            {
                using ( var dbContext = new PizzarioDbContext() )
                {
                    ServiceProvider serviceProvider = new ServiceProvider( dbContext );

                    TestModelGenerator generator = new TestModelGenerator( serviceProvider );
                    generator.GenerateTestData();
                }

                using ( var dbContext = new PizzarioDbContext() )
                {
                    ServiceProvider serviceProvider = new ServiceProvider( dbContext );

                    ModelReporter reportGenerator = new ModelReporter( serviceProvider, Console.Out );
                    reportGenerator.GenerateReport();
                }
            }
            catch ( Exception e )
            {
                Console.WriteLine( e.GetType().FullName );
                Console.WriteLine( e.Message );
                Console.WriteLine( e.StackTrace );//////////////////////////////
            }
        }
    }
}
