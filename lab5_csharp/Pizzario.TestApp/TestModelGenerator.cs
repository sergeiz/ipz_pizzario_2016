﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Service;

using System;


namespace Pizzario.TestApp
{
    class TestModelGenerator
    {
        public TestModelGenerator ( ServiceProvider serviceProvider )
        {
            this.serviceProvider = serviceProvider;
        }

        public void GenerateTestData ()
        {
            GenerateSizes();
            GenerateIngredients();
            GenerateProducts();
            GenerateCarts();
            GenerateOrders();
            GenerateAccounts();
            StartCooking();
            FinishCooking();
            Deliver();
        }


        private void GenerateSizes ()
        {
            // ----

            IProductSizeService service = serviceProvider.ProvideProductSizeService();

            // ----

            Small  = service.Create( "Small", 25, 300.0 );
            Medium = service.Create( "Medium", 30, 600.0 );
            Large  = service.Create( "Large", 35, 900.0 );

            // ----
        }


        private void GenerateIngredients ()
        {
            // ----

            IIngredientService service = serviceProvider.ProvideIngredientService();

            // ----

            Mozarella       = service.Create( "Mozarella" );
            Pineaple        = service.Create( "Pineaple" );
            Chicken         = service.Create( "Chicken" );
            Saliami         = service.Create( "Saliami" );
            Tomato          = service.Create( "Tomato" );
            Kebab           = service.Create( "Kebab" );
            Eggplant        = service.Create( "Eggplant" );
            Onion           = service.Create( "Onion" );
            Parsley         = service.Create( "Parsley" );
            Bacon           = service.Create( "Bacon" );
            Ham             = service.Create( "Ham" );
            PickledCucumber = service.Create( "Pickled Cucumber" );
            Mushroom        = service.Create( "Mushroom" );

            // ----
        }

        private void GenerateProducts ()
        {
            // ----

            IProductService service = serviceProvider.ProvideProductService();

            // ----

            Mafia = service.Create( "Mafia", "http://pizzario.com/images/mafia.jpg" );

            service.DefineRecipeIngredient( Mafia, Small, Mozarella, 25.0 );
            service.DefineRecipeIngredient( Mafia, Small, Pineaple,  25.0 );
            service.DefineRecipeIngredient( Mafia, Small, Chicken,   25.0 );
            service.DefineRecipeIngredient( Mafia, Small, Saliami,   25.0 );
            service.DefineRecipeIngredient( Mafia, Small, Tomato,    25.0 );

            service.DefineRecipeIngredient( Mafia, Medium, Mozarella, 50.0 );
            service.DefineRecipeIngredient( Mafia, Medium, Pineaple,  50.0 );
            service.DefineRecipeIngredient( Mafia, Medium, Chicken,   50.0 );
            service.DefineRecipeIngredient( Mafia, Medium, Saliami,   50.0 );
            service.DefineRecipeIngredient( Mafia, Medium, Tomato,    50.0 );

            service.DefineRecipeIngredient( Mafia, Large, Mozarella, 75.0 );
            service.DefineRecipeIngredient( Mafia, Large, Pineaple,  75.0 );
            service.DefineRecipeIngredient( Mafia, Large, Chicken,   75.0 );
            service.DefineRecipeIngredient( Mafia, Large, Saliami,   75.0 );
            service.DefineRecipeIngredient( Mafia, Large, Tomato,    75.0 );

            service.DefinePrice( Mafia, Small,   73.00M );
            service.DefinePrice( Mafia, Medium,  99.00M );
            service.DefinePrice( Mafia, Large,  125.00M );

            // ----

            Georgia = service.Create( "Georgia", "http://pizzario.com/images/georgia.jpg" );

            service.DefineRecipeIngredient( Georgia, Small, Kebab,     25.0 );
            service.DefineRecipeIngredient( Georgia, Small, Mozarella, 25.0 );
            service.DefineRecipeIngredient( Georgia, Small, Eggplant,  25.0 );
            service.DefineRecipeIngredient( Georgia, Small, Onion,     25.0 );
            service.DefineRecipeIngredient( Georgia, Small, Parsley,   10.0 );
            service.DefineRecipeIngredient( Georgia, Small, Tomato,    25.0 );

            service.DefineRecipeIngredient( Georgia, Medium, Kebab,     50.0 );
            service.DefineRecipeIngredient( Georgia, Medium, Mozarella, 50.0 );
            service.DefineRecipeIngredient( Georgia, Medium, Eggplant,  50.0 );
            service.DefineRecipeIngredient( Georgia, Medium, Onion,     50.0 );
            service.DefineRecipeIngredient( Georgia, Medium, Parsley,   20.0 );
            service.DefineRecipeIngredient( Georgia, Medium, Tomato,    50.0 );

            service.DefineRecipeIngredient( Georgia, Large, Kebab,     75.0 );
            service.DefineRecipeIngredient( Georgia, Large, Mozarella, 75.0 );
            service.DefineRecipeIngredient( Georgia, Large, Eggplant,  75.0 );
            service.DefineRecipeIngredient( Georgia, Large, Onion,     75.0 );
            service.DefineRecipeIngredient( Georgia, Large, Parsley,   75.0 );
            service.DefineRecipeIngredient( Georgia, Large, Tomato,    75.0 );

            service.DefinePrice( Georgia, Small,   81.00M );
            service.DefinePrice( Georgia, Medium, 103.00M );
            service.DefinePrice( Georgia, Large,  137.00M );

            // ----

            Cossack = service.Create( "Cossack", "http://pizzario.com/images/cosscack.jpg" );

            service.DefineRecipeIngredient( Cossack, Small, Mozarella,       25.0 );
            service.DefineRecipeIngredient( Cossack, Small, Bacon,           25.0 );
            service.DefineRecipeIngredient( Cossack, Small, Ham,             25.0 );
            service.DefineRecipeIngredient( Cossack, Small, Onion,           25.0 );
            service.DefineRecipeIngredient( Cossack, Small, PickledCucumber, 25.0 );
            service.DefineRecipeIngredient( Cossack, Small, Mushroom,        25.0 );

            service.DefineRecipeIngredient( Cossack, Medium, Mozarella,       50.0 );
            service.DefineRecipeIngredient( Cossack, Medium, Bacon,           50.0 );
            service.DefineRecipeIngredient( Cossack, Medium, Ham,             50.0 );
            service.DefineRecipeIngredient( Cossack, Medium, Onion,           50.0 );
            service.DefineRecipeIngredient( Cossack, Medium, PickledCucumber, 50.0 );
            service.DefineRecipeIngredient( Cossack, Medium, Mushroom,        50.0 );

            service.DefineRecipeIngredient( Cossack, Large, Mozarella,       75.0 );
            service.DefineRecipeIngredient( Cossack, Large, Bacon,           75.0 );
            service.DefineRecipeIngredient( Cossack, Large, Ham,             75.0 );
            service.DefineRecipeIngredient( Cossack, Large, Onion,           75.0 );
            service.DefineRecipeIngredient( Cossack, Large, PickledCucumber, 75.0 );
            service.DefineRecipeIngredient( Cossack, Large, Mushroom,        75.0 );

            service.DefinePrice( Cossack, Small,   83.00M );
            service.DefinePrice( Cossack, Medium, 107.00M );
            service.DefinePrice( Cossack, Large,  143.00M );

            // ----
        }


        private void GenerateCarts ()
        {
            // ----

            IShoppingCartService service = serviceProvider.ProvideShoppingCartService();

            // ----

            cart = service.CreateNew();
            service.SetItem( cart, Mafia,   Large,  1 );
            service.SetItem( cart, Georgia, Small,  2 );
            service.SetItem( cart, Cossack, Medium, 1 );

            service.LockCart( cart );

            // ----
        }


        private void GenerateOrders ()
        {
            // ----

            IOrderService service = serviceProvider.ProvideOrderService();

            // ----

            order = service.CreateNew( 
                "Sumskaya 1",
                "123-45-67",
                cart
            );

            service.SetDiscount( order, 20.00M );
            service.Confirm( order );

            // ----
        }


        private void GenerateAccounts ()
        {
            // ----

            IAccountService service = serviceProvider.ProvideAccountService();

            // ----

            Guid wasya = service.CreateOperator( "Wasya Pupkin", "wasya.pupkin@pizzario.com", "12345");
            service.TrackOrder( wasya, order );

            // ----
        }


        private void StartCooking ()
        {
            // ----

            ICookingAssignmentService service = serviceProvider.ProvideCookingAssignmentService();

            // ----

            foreach ( var assignmentId in service.ViewWaiting() )
                service.MarkCookingStarted( assignmentId );

            // ----
        }


        private void FinishCooking ()
        {
            // ----

            ICookingAssignmentService service = serviceProvider.ProvideCookingAssignmentService();

            // ----

            foreach ( var assignmentId in service.ViewCookedRightNow() )
                service.MarkCookingFinished( assignmentId );

            // ----
        }


        private void Deliver ()
        {
            // ----

            IDeliveryService service = serviceProvider.ProvideDeliveryService();

            // ----

            foreach ( var deliveryId in service.ViewWaiting() )
            {
                service.MarkStarted( deliveryId, "Wasya" );
                service.MarkDelivered( deliveryId );
            }

            // ----
        }


        private Guid Large, Medium, Small;
        private Guid Mozarella, Pineaple, Chicken, Saliami, Tomato, Kebab, Eggplant;
        private Guid Onion, Parsley, Bacon, Ham, PickledCucumber, Mushroom;
        private Guid Mafia, Georgia, Cossack;
        private Guid cart;
        private Guid order;

        private ServiceProvider serviceProvider;
    }

}
