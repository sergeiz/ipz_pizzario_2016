﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Service;
using Pizzario.Service.Impl;
using Pizzario.Repository;
using Pizzario.Repository.EntityFramework;

namespace Pizzario.TestApp
{
    class ServiceProvider
    {
        public ServiceProvider ( PizzarioDbContext dbContext )
        {
            this.dbContext = dbContext;
        }

        public IIngredientService ProvideIngredientService ()
        {
            IIngredientRepository ingredientRepository = RepositoryFactory.MakeIngredientRepository( dbContext );
            return ServiceFactory.MakeIngredientService( ingredientRepository );
        }

        public IProductSizeService ProvideProductSizeService ()
        {
            IProductSizeRepository sizeRepository = RepositoryFactory.MakeProductSizeRepository( dbContext );
            return ServiceFactory.MakeProductSizeService( sizeRepository );
        }

        public IProductService ProvideProductService ()
        {
            IProductRepository productRepository = RepositoryFactory.MakeProductRepository( dbContext );
            IProductSizeRepository sizeRepository = RepositoryFactory.MakeProductSizeRepository( dbContext );
            IIngredientRepository ingredientRepository = RepositoryFactory.MakeIngredientRepository( dbContext );

            return ServiceFactory.MakeProductService(
                    productRepository,
                    sizeRepository,
                    ingredientRepository
            );
        }

        public IShoppingCartService ProvideShoppingCartService ()
        {
            IShoppingCartRepository cartRepository = RepositoryFactory.MakeShoppingCartRepository( dbContext );
            IProductRepository productRepository = RepositoryFactory.MakeProductRepository( dbContext );
            IProductSizeRepository sizeRepository = RepositoryFactory.MakeProductSizeRepository( dbContext );

            return ServiceFactory.MakeShoppingCartService(
                    cartRepository,
                    productRepository,
                    sizeRepository
            );
        }

        public IOrderService ProvideOrderService ()
        {
            IOrderRepository orderRepository = RepositoryFactory.MakeOrderRepository( dbContext );
            IShoppingCartRepository cartRepository = RepositoryFactory.MakeShoppingCartRepository( dbContext );
            ICookingAssignmentRepository cookingRepository = RepositoryFactory.MakeCookingAssignmentRepository( dbContext );

            return ServiceFactory.MakeOrderService(
                    orderRepository,
                    cartRepository,
                    cookingRepository
            );
        }

        public IAccountService ProvideAccountService ()
        {
            IAccountRepository accountRepository = RepositoryFactory.MakeAccountRepository( dbContext );
            IOrderRepository orderRepository = RepositoryFactory.MakeOrderRepository( dbContext );

            return ServiceFactory.MakeAccountService(
                    accountRepository,
                    orderRepository
            );
        }

        public ICookingAssignmentService ProvideCookingAssignmentService ()
        {
            ICookingAssignmentRepository cookingRepository = RepositoryFactory.MakeCookingAssignmentRepository( dbContext );
            IDeliveryRepository deliveryRepository = RepositoryFactory.MakeDeliveryRepository( dbContext );
            IOrderRepository orderRepository = RepositoryFactory.MakeOrderRepository( dbContext );

            return ServiceFactory.MakeCookingAssignmentService(
                    cookingRepository,
                    deliveryRepository,
                    orderRepository
            );
        }

        public IDeliveryService ProvideDeliveryService ()
        {
            IDeliveryRepository deliveryRepository = RepositoryFactory.MakeDeliveryRepository( dbContext );
            IOrderRepository orderRepository = RepositoryFactory.MakeOrderRepository( dbContext );

            return ServiceFactory.MakeDeliveryService(
                    deliveryRepository,
                    orderRepository
            );
        }


        private PizzarioDbContext dbContext;
    }
}
