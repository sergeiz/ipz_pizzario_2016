﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Repository;

namespace Pizzario.Service.Impl
{
    public static class ServiceFactory
    {
        public static IIngredientService MakeIngredientService ( IIngredientRepository repository )
        {
            return new IngredientService( repository );
        }

        public static IProductSizeService MakeProductSizeService ( IProductSizeRepository repository )
        {
            return new ProductSizeService( repository );
        }

        public static IProductService MakeProductService (
                IProductRepository productRepository,
                IProductSizeRepository productSizeRepository,
                IIngredientRepository ingredientRepository )
        {

            return new ProductService(
                    productRepository,
                    productSizeRepository,
                    ingredientRepository
            );
        }

        public static IShoppingCartService MakeShoppingCartService (
                IShoppingCartRepository shoppingCartRepository,
                IProductRepository productRepository,
                IProductSizeRepository productSizeRepository )
        {

            return new ShoppingCartService(
                    shoppingCartRepository,
                    productRepository,
                    productSizeRepository
            );
        }

        public static IOrderService MakeOrderService (
                IOrderRepository orderRepository,
                IShoppingCartRepository shoppingCartRepository,
                ICookingAssignmentRepository cookingAssignmentRepository )
        {

            return new OrderService(
                    orderRepository,
                    shoppingCartRepository,
                    cookingAssignmentRepository
            );
        }

        public static IAccountService MakeAccountService (
                IAccountRepository accountRepository,
                IOrderRepository orderRepository )
        {

            return new AccountService(
                    accountRepository,
                    orderRepository
            );
        }

        public static ICookingAssignmentService MakeCookingAssignmentService (
                ICookingAssignmentRepository cookingAssignmentRepository,
                IDeliveryRepository deliveryRepository,
                IOrderRepository orderRepository )
        {

            return new CookingAssignmentService(
                    cookingAssignmentRepository,
                    deliveryRepository,
                    orderRepository
            );
        }

        public static IDeliveryService MakeDeliveryService (
                IDeliveryRepository deliveryRepository,
                IOrderRepository orderRepository )
        {

            return new DeliveryService(
                    deliveryRepository,
                    orderRepository
            );
        }

    }

}
