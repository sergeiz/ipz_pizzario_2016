﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;
using Pizzario.Repository;
using Pizzario.Dto;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Pizzario.Service.Impl
{
    public class OrderService : IOrderService
    {
        public OrderService ( IOrderRepository orderRepository,
                              IShoppingCartRepository shoppingCartRepository,
                              ICookingAssignmentRepository cookingAssignmentRepository )
        {

            this.orderRepository = orderRepository;
            this.shoppingCartRepository = shoppingCartRepository;
            this.cookingAssignmentRepository = cookingAssignmentRepository;
        }

        public IList< Guid > ViewAll ()
        {
            return orderRepository.SelectAllDomainIds().ToList();
        }

        public IList< Guid > ViewUnconfirmed ()
        {
            return orderRepository.SelectUnconfirmedIds().ToList();
        }

        public IList< Guid > ViewReady4Delivery ()
        {
            return orderRepository.SelectReady4DeliveryIds().ToList();
        }

        public OrderDto View ( Guid orderId )
        {
            Order o = ResolveOrder( orderId );
            return o.ToDto();
        }

        public Guid CreateNew ( String deliveryAddress, String contactPhone, Guid cartId )
        {
            orderRepository.StartTransaction();

            ShoppingCart cart = ResolveCart( cartId );
            Order o = new Order(
                    Guid.NewGuid(),
                    cart,
                    new Contact( deliveryAddress, contactPhone ),
                    DateTime.Now
            );

            orderRepository.Add( o );

            orderRepository.Commit();

            return o.DomainId;
        }

        public void SetDiscount ( Guid orderId, decimal discountPercent )
        {
            orderRepository.StartTransaction();

            Order o = ResolveOrder( orderId );
            o.SetDiscount( new Discount( discountPercent ) );

            orderRepository.Commit();
        }

        public void Confirm ( Guid orderId )
        {
            orderRepository.StartTransaction();

            Order o = ResolveOrder( orderId );
            o.Confirm();

            orderRepository.Commit();

            cookingAssignmentRepository.StartTransaction();

            foreach ( var ca in o.GenerateCookingAssignments() )
                cookingAssignmentRepository.Add( ca );

            cookingAssignmentRepository.Commit();
        }

        public void Cancel ( Guid orderId )
        {
            orderRepository.StartTransaction();

            Order o = ResolveOrder( orderId );
            o.Cancel();

            orderRepository.Commit();
        }

        private Order ResolveOrder ( Guid orderId )
        {
            return ServiceUtils.ResolveEntity( orderRepository, orderId );
        }

        private ShoppingCart ResolveCart ( Guid cartId )
        {
            return ServiceUtils.ResolveEntity( shoppingCartRepository, cartId );
        }


        private IOrderRepository orderRepository;
        private IShoppingCartRepository shoppingCartRepository;
        private ICookingAssignmentRepository cookingAssignmentRepository;

    }
}
