﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;
using Pizzario.Repository;
using Pizzario.Dto;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Pizzario.Service.Impl
{
    public class AccountService : IAccountService
    {
        public AccountService ( 
            IAccountRepository accountRepository,
            IOrderRepository orderRepository 
        )
        {
            this.accountRepository = accountRepository;
            this.orderRepository = orderRepository;
        }

        public IList< Guid > ViewAll ()
        {
            return accountRepository.SelectAllDomainIds().ToList();
        }


        public AccountDto View ( Guid accountId )
        {
            Account a = ResolveAccount( accountId );
            return a.ToDto();
        }


        public AccountDto Identify ( string email, string password )
        {
            Account a = accountRepository.FindByEmail( email );
            if ( a == null )
                return null;

            if ( !a.CheckPassword( password ) )
                return null;

            return a.ToDto();
        }

        public IList< Guid > ViewAssociatedOrders ( Guid accountId )
        {
            Account a = ResolveAccount( accountId );

            var orderIds = new List< Guid > { };
            a.Accept( new OrdersExtractVisitor( orderIds ) );

            return orderIds;
        }

        private class OrdersExtractVisitor : AccountVisitor
        {
            public OrdersExtractVisitor ( IList< Guid > orderIds )
            {
                this.orderIds = orderIds;
            }

            public void Visit ( Account account )
            {
                // No orders..
            }

            public void Visit ( OperatorAccount account )
            {
                foreach ( var o in account.Orders )
                    orderIds.Add( o.DomainId );
            }

            private IList< Guid > orderIds;
        };

        public Guid CreateOperator ( string name, string email, string password )
        {
            Account a = accountRepository.FindByEmail( email );
            if ( a != null )
                throw new ArgumentException( "Duplicate account" );

            accountRepository.StartTransaction();

            OperatorAccount operatorAccount = new OperatorAccount( Guid.NewGuid(), name, email, password );
            accountRepository.Add( operatorAccount );

            accountRepository.Commit();

            return operatorAccount.DomainId;
        }


        public void ChangeName ( Guid accountId, string newName )
        {
            accountRepository.StartTransaction();

            Account a = ResolveAccount( accountId );
            a.Name = newName;

            accountRepository.Commit();
        }


        public void ChangeEmail ( Guid accountId, string newEmail )
        {
            accountRepository.StartTransaction();

            Account a = accountRepository.FindByEmail( newEmail );
            if ( a != null )
                throw new ArgumentException( "Duplicate account" );

            a.Email = newEmail;

            accountRepository.Commit();
        }


        public void ChangePassword ( Guid accountId, string oldPassword, string newPassword )
        {
            accountRepository.StartTransaction();

            Account a = ResolveAccount( accountId );

            if ( !a.CheckPassword( oldPassword ) )
                throw new ArgumentException( "Password failed" );

            a.Password = newPassword;

            accountRepository.Commit();
        }


        public void TrackOrder ( Guid accountId, Guid orderId )
        {
            accountRepository.StartTransaction();

            Account a = ResolveAccount( accountId );
            Order o = ResolveOrder( orderId );

            Account otherAccount = accountRepository.FindByOrder( o );
            if ( otherAccount != null )
                throw new ArgumentException( "Order was already served by different account" );

            a.Accept( new OrderTrackingVisitor( o ) );
            accountRepository.Commit();
        }


        private class OrderTrackingVisitor : AccountVisitor
        {
            public OrderTrackingVisitor ( Order o )
            {
                this.o = o;
            }

            public void Visit ( Account account )
            {
                throw new ArgumentException( "Not an operator account" );
            }

            public void Visit ( OperatorAccount account )
            {
                account.TrackOrder( o );
            }

            private Order o;
        }


        private Account ResolveAccount ( Guid accountId )
        {
            return ServiceUtils.ResolveEntity( accountRepository, accountId );
        }


        private Order ResolveOrder ( Guid orderId )
        {
            return ServiceUtils.ResolveEntity( orderRepository, orderId );
        }


        private IAccountRepository accountRepository;
        private IOrderRepository orderRepository;

    }
}
