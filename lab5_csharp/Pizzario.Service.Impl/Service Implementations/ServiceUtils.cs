﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;
using Pizzario.Dto;
using Pizzario.Repository;

using System;

namespace Pizzario.Service.Impl
{
    sealed class ServiceUtils
    {
        private ServiceUtils() { }

        public static TEntity ResolveEntity< TEntity > ( 
            IRepository< TEntity > repository, 
            Guid domainId 
        ) where TEntity : Utils.Entity
        {
            TEntity entity = repository.FindByDomainId( domainId );
            if (entity != null)
                return entity;

            throw new ArgumentException( "Unresolved entity" );
        }
    }
}
