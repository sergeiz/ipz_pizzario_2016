﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;
using Pizzario.Repository;
using Pizzario.Dto;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Pizzario.Service.Impl
{
    public class CookingAssignmentService : ICookingAssignmentService
    {
        public CookingAssignmentService (
                ICookingAssignmentRepository cookingAssignmentRepository,
                IDeliveryRepository deliveryRepository,
                IOrderRepository orderRepository )
        {
            this.cookingAssignmentRepository = cookingAssignmentRepository;
            this.deliveryRepository = deliveryRepository;
            this.orderRepository = orderRepository;
        }

        public IList< Guid > ViewAll ()
        {
            return cookingAssignmentRepository.SelectAllDomainIds().ToList();
        }

        public IList< Guid > ViewCookedRightNow ()
        {
            return cookingAssignmentRepository.SelectCookingIds().ToList();
        }

        public IList< Guid > ViewWaiting ()
        {
            return cookingAssignmentRepository.SelectWaitingIds().ToList();
        }

        public CookingAssignmentDto View ( Guid cookingAssignmentId )
        {
            CookingAssignment cooking = ResolveCooking( cookingAssignmentId );
            return cooking.ToDto();
        }

        public IList< CookingAssignmentDto > ViewOrderAssignments ( Guid orderId )
        {
            var views = new List< CookingAssignmentDto >();

            Order o = ResolveOrder( orderId );
            foreach ( var ca in cookingAssignmentRepository.FindOrderAssignments( o ) )
                views.Add( ca.ToDto() );

            return views;
        }

        public void MarkCookingStarted ( Guid cookingAssignmentId )
        {
            cookingAssignmentRepository.StartTransaction();

            CookingAssignment cooking = ResolveCooking( cookingAssignmentId );
            cooking.StartCooking();

            cookingAssignmentRepository.Commit();
        }

        public void MarkCookingFinished ( Guid cookingAssignmentId )
        {
            cookingAssignmentRepository.StartTransaction();

            CookingAssignment cooking = ResolveCooking( cookingAssignmentId );
            OrderStatus previousOrderStatus = cooking.RelatedOrder.Status;

            cooking.FinishCooking();
            cookingAssignmentRepository.Commit();

            OrderStatus newOrderStatus = cooking.RelatedOrder.Status;
            if ( previousOrderStatus != newOrderStatus )
            {
                deliveryRepository.StartTransaction();
                Delivery d = cooking.RelatedOrder.GenerateDelivery();
                deliveryRepository.Add( d );
                deliveryRepository.Commit();
            }
        }

        private CookingAssignment ResolveCooking ( Guid cookingID )
        {
            return ServiceUtils.ResolveEntity( cookingAssignmentRepository, cookingID );
        }

        private Order ResolveOrder ( Guid orderId )
        {
            return ServiceUtils.ResolveEntity( orderRepository, orderId );
        }

        private ICookingAssignmentRepository cookingAssignmentRepository;
        private IDeliveryRepository deliveryRepository;
        private IOrderRepository orderRepository;
    }
}
