﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;
using Pizzario.Repository;
using Pizzario.Dto;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Pizzario.Service.Impl
{
    public class DeliveryService : IDeliveryService
    {
        public DeliveryService (
                IDeliveryRepository deliveryRepository,
                IOrderRepository orderRepository )
        {
            this.deliveryRepository = deliveryRepository;
            this.orderRepository = orderRepository;
        }

        public IList< Guid > ViewAll ()
        {
            return deliveryRepository.SelectAllDomainIds().ToList();
        }

        public IList< Guid > ViewWaiting ()
        {
            return deliveryRepository.SelectWaitingIds().ToList();
        }

        public IList< Guid > ViewInProgress ()
        {
            return deliveryRepository.SelectInProgressIds().ToList();
        }

        public DeliveryDto View ( Guid deliveryId )
        {
            Delivery delivery = ResolveDelivery( deliveryId );
            return delivery.ToDto();
        }

        public DeliveryDto FindOrderDelivery ( Guid orderId )
        {
            Order o = ResolveOrder( orderId );
            Delivery delivery = deliveryRepository.FindByOrder( o );
            return delivery.ToDto();
        }

        public void MarkStarted ( Guid deliveryId, string driverName )
        {
            deliveryRepository.StartTransaction();

            Delivery delivery = ResolveDelivery( deliveryId );
            delivery.StartDelivery( driverName );

            deliveryRepository.Commit();
        }


        public void MarkDelivered ( Guid deliveryId )
        {
            deliveryRepository.StartTransaction();

            Delivery delivery = ResolveDelivery( deliveryId );
            delivery.FinishDelivery();

            deliveryRepository.Commit();
        }


        private Delivery ResolveDelivery ( Guid deliveryId )
        {
            return ServiceUtils.ResolveEntity( deliveryRepository, deliveryId );
        }


        private Order ResolveOrder ( Guid orderId )
        {
            return ServiceUtils.ResolveEntity( orderRepository, orderId );
        }


        private IDeliveryRepository deliveryRepository;
        private IOrderRepository orderRepository;
    }
}
