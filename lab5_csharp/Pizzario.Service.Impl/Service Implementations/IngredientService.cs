﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;
using Pizzario.Repository;
using Pizzario.Dto;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Pizzario.Service.Impl
{
    public class IngredientService : IIngredientService
    {
        public IngredientService( IIngredientRepository ingredientRepository )
        {
            this.ingredientRepository = ingredientRepository;
        }

        public IList< Guid > ViewAll ()
        {
            return ingredientRepository.SelectAllDomainIds().ToList();
        }

        public IngredientDto View ( Guid ingredientId )
        {
            Ingredient i = ResolveIngredient( ingredientId );
            return i.ToDto();
        }

        public Guid Create ( string name )
        {
            ingredientRepository.StartTransaction();

            if ( ingredientRepository.FindByName( name ) != null)
                throw new InvalidOperationException( "Duplicate ingredient " + name );

            Ingredient i = new Ingredient( Guid.NewGuid(), name );

            ingredientRepository.Add(i);
            ingredientRepository.Commit();

            return i.DomainId;
        }

        public void Rename ( Guid ingredientId, string newName )
        {
            if ( ingredientRepository.FindByName( newName ) != null )
                throw new InvalidOperationException( "Duplicate ingredient " + newName );

            ingredientRepository.StartTransaction();

            Ingredient i = ResolveIngredient(ingredientId);
            i.Name = newName;

            ingredientRepository.Commit();
        }

        private Ingredient ResolveIngredient ( Guid ingredientId )
        {
            return ServiceUtils.ResolveEntity( ingredientRepository, ingredientId );
        }

        private IIngredientRepository ingredientRepository;

    }
}
