﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;
using Pizzario.Repository;
using Pizzario.Dto;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Pizzario.Service.Impl
{
    public class ProductSizeService : IProductSizeService
    {
        public ProductSizeService ( IProductSizeRepository productSizeRepository )
        {
            this.productSizeRepository = productSizeRepository;
        }

        public IList< Guid > ViewAll ()
        {
            return productSizeRepository.SelectAllDomainIds().ToList();
        }

        public ProductSizeDto View ( Guid sizeId )
        {
            ProductSize size = ResolveSize( sizeId );
            return size.ToDto();
        }

        public Guid Create ( String name, int diameter, double weight )
        {
            productSizeRepository.StartTransaction();

            if ( productSizeRepository.FindByName( name ) != null )
                throw new InvalidOperationException( "Duplicate ingredient " + name );

            ProductSize size = new ProductSize( Guid.NewGuid(), name, diameter, weight );

            productSizeRepository.Add( size );
            productSizeRepository.Commit();

            return size.DomainId;
        }

        public void Rename ( Guid sizeId, String newName )
        {
            if ( productSizeRepository.FindByName( newName ) != null )
                throw new InvalidOperationException( "Duplicate size " + newName );

            productSizeRepository.StartTransaction();
            ProductSize size = ResolveSize( sizeId );
            size.Name = newName;
            productSizeRepository.Commit();
        }

        public void ChangeDiameter ( Guid sizeId, int newDiameter )
        {
            productSizeRepository.StartTransaction();
            ProductSize size = ResolveSize( sizeId );
            size.Diameter = newDiameter;
            productSizeRepository.Commit();
        }

        public void ChangeWeight ( Guid sizeId, double weight )
        {
            productSizeRepository.StartTransaction();
            ProductSize size = ResolveSize( sizeId );
            size.Weight = weight;
            productSizeRepository.Commit();
        }


        private ProductSize ResolveSize ( Guid sizeId )
        {
            return ServiceUtils.ResolveEntity( productSizeRepository, sizeId );
        }

        private IProductSizeRepository productSizeRepository;

    }
}
