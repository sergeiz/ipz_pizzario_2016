﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;
using Pizzario.Repository;
using Pizzario.Dto;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Pizzario.Service.Impl
{
    public class ShoppingCartService : IShoppingCartService
    {
        public ShoppingCartService ( IShoppingCartRepository shoppingCartRepository,
                                     IProductRepository productRepository,
                                     IProductSizeRepository productSizeRepository )
        {
            this.shoppingCartRepository = shoppingCartRepository;
            this.productRepository = productRepository;
            this.productSizeRepository = productSizeRepository;
        }


        public IList< Guid > ViewAll ()
        {
            return shoppingCartRepository.SelectAllDomainIds().ToList();
        }

        public IList< Guid > ViewOpen ()
        {
            return shoppingCartRepository.SelectAllOpenCartIds().ToList();
        }

        public ShoppingCartDto View ( Guid cartId )
        {
            ShoppingCart cart = ResolveCart( cartId );
            return cart.ToDto();
        }

        public Guid CreateNew ()
        {
            shoppingCartRepository.StartTransaction();

            ShoppingCart cart = new ShoppingCart( Guid.NewGuid() );
            shoppingCartRepository.Add( cart );

            shoppingCartRepository.Commit();

            return cart.DomainId;
        }

        public void SetItem ( Guid cartId, Guid productId, Guid sizeId, int quantity )
        {
            shoppingCartRepository.StartTransaction();

            ShoppingCart cart = ResolveCart( cartId );
            Product product = ResolveProduct( productId );
            ProductSize size = ResolveSize( sizeId );

            ProductItem item = new ProductItem( Guid.NewGuid(), product, size, quantity );
            int itemIndex = cart.FindItemIndex( product, size );
            if ( itemIndex == -1 )
                cart.AddItem( item );
            else
                cart.UpdateItem( itemIndex, item );

            shoppingCartRepository.Commit();
        }

        public void RemoveItem ( Guid cartId, Guid productId, Guid sizeId )
        {
            shoppingCartRepository.StartTransaction();

            ShoppingCart cart = ResolveCart( cartId );
            Product product = ResolveProduct( productId );
            ProductSize size = ResolveSize( sizeId );

            int itemIndex = cart.FindItemIndex( product, size );
            if ( itemIndex != -1 )
                cart.DropItem( itemIndex );

            else
                throw new InvalidOperationException( "Item not found in cart" );

            shoppingCartRepository.Commit();
        }

        public void ClearItems ( Guid cartId )
        {
            shoppingCartRepository.StartTransaction();

            ShoppingCart cart = ResolveCart( cartId );
            cart.ClearItems();

            shoppingCartRepository.Commit();
        }

        public void LockCart ( Guid cartId ) {
            shoppingCartRepository.StartTransaction();

            ShoppingCart cart = ResolveCart( cartId );
            cart.Checkout();

            shoppingCartRepository.Commit();
        }

        public void Remove ( Guid cartId )
        {
            shoppingCartRepository.StartTransaction();

            ShoppingCart cart = ResolveCart( cartId );
            shoppingCartRepository.Delete( cart );

            shoppingCartRepository.Commit();
        }

        private ShoppingCart ResolveCart ( Guid cartId )
        {
            return ServiceUtils.ResolveEntity( shoppingCartRepository, cartId );
        }

        private Product ResolveProduct ( Guid productId )
        {
            return ServiceUtils.ResolveEntity( productRepository, productId );
        }

        private ProductSize ResolveSize ( Guid sizeId )
        {
            return ServiceUtils.ResolveEntity( productSizeRepository, sizeId );
        }


        private IShoppingCartRepository shoppingCartRepository;
        private IProductRepository productRepository;
        private IProductSizeRepository productSizeRepository;

    }
}
