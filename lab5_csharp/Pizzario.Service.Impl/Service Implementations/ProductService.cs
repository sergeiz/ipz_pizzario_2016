﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;
using Pizzario.Repository;
using Pizzario.Dto;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Pizzario.Service.Impl
{
    public class ProductService : IProductService
    {
        public ProductService ( 
            IProductRepository productRepository,
            IProductSizeRepository productSizeRepository,
            IIngredientRepository ingredientRepository 
        )
        {
            this.productRepository = productRepository;
            this.productSizeRepository = productSizeRepository;
            this.ingredientRepository = ingredientRepository;
        }

        public IList< Guid > ViewAll ()
        {
            return productRepository.SelectAllDomainIds().ToList();
        }

        public ProductDto View ( Guid productId )
        {
            Product product = ResolveProduct( productId );
            return product.ToDto();
        }

        public IList< ProductSizeDto > AvailableSizes ( Guid productId )
        {
            Product product = ResolveProduct( productId );

            var sizes = new List< ProductSizeDto >();
            foreach ( var size in product.ListAvailableSizes() )
                sizes.Add( size.ToDto() );

            return sizes;
        }

        public ProductPricingDto ViewPrices ( Guid productId )
        {
            Product product = ResolveProduct( productId );

            var pricesDto = new Dictionary< ProductSizeDto, decimal >();
            foreach ( var size in product.ListAvailableSizes() )
                pricesDto.Add( size.ToDto(), product.GetPrice( size ) );

            return new ProductPricingDto( product.DomainId, pricesDto );
        }

        public ProductRecipeDto ViewRecipe ( Guid productId, Guid sizeId )
        {
            Product product = ResolveProduct( productId );
            ProductSize size = ResolveSize( sizeId );

            Recipe recipe = product.GetRecipe( size );

            var usedIngredientsDto = new Dictionary< IngredientDto, double >();
            foreach ( var inclusion in recipe.Ingredients )
                usedIngredientsDto.Add( 
                    inclusion.IncludedIngredient.ToDto(), 
                    inclusion.Weight 
                );

            return new ProductRecipeDto(
                        product.DomainId,
                        size.DomainId,
                        usedIngredientsDto
                   );
        }

        public Guid Create ( String name, String imageUrl )
        {
            productRepository.StartTransaction();

            if ( productRepository.FindByName( name ) != null )
                throw new InvalidOperationException( "Duplicate product " + name );

            Product p = new Product( Guid.NewGuid(), name );
            p.ImageUrl = imageUrl;

            productRepository.Add( p );
            productRepository.Commit();

            return p.DomainId;
        }

        public void Rename ( Guid productId, String newName )
        {
            productRepository.StartTransaction();

            if ( productRepository.FindByName( newName ) != null )
                throw new InvalidOperationException( "Duplicate product " + newName );

            Product p = ResolveProduct( productId );
            p.Name = newName;

            productRepository.Commit();
        }

       public void UpdateImageUrl ( Guid productId, String imageUrl )
        {
            productRepository.StartTransaction();

            Product p = ResolveProduct( productId );
            p.ImageUrl = imageUrl;

            productRepository.Commit();
        }

        public void DefinePrice ( Guid productId, Guid sizeId, decimal price )
        {
            productRepository.StartTransaction();

            Product p = ResolveProduct( productId );
            ProductSize sz = ResolveSize( sizeId );
            p.SetPrice( sz, price );

            productRepository.Commit();
        }

       public void UndefinePrice ( Guid productId, Guid sizeId )
        {
            productRepository.StartTransaction();

            Product p = ResolveProduct( productId );
            ProductSize sz = ResolveSize( sizeId );
            p.RemovePrice( sz );

            productRepository.Commit();
        }

        public void DefineRecipeIngredient ( Guid productId, Guid sizeId, Guid ingredientId, double weight )
        {
            productRepository.StartTransaction();

            Product p = ResolveProduct( productId );
            ProductSize sz = ResolveSize( sizeId );
            Ingredient i = ResolveIngredient( ingredientId );

            p.DefineRecipe( sz ).UseIngredient( i, weight );

            productRepository.Commit();
        }

        public void RemoveRecipeIngredient ( Guid productId, Guid sizeId, Guid ingredientId )
        {
            productRepository.StartTransaction();

            Product p = ResolveProduct( productId );
            ProductSize sz = ResolveSize( sizeId );
            Ingredient i = ResolveIngredient( ingredientId );
            p.DefineRecipe( sz ).RemoveIngredient( i );

            productRepository.Commit();
        }

        private Product ResolveProduct ( Guid productId )
        {
            return ServiceUtils.ResolveEntity( productRepository, productId );
        }

        private ProductSize ResolveSize ( Guid sizeId )
        {
            return ServiceUtils.ResolveEntity( productSizeRepository, sizeId );
        }

        private Ingredient ResolveIngredient ( Guid ingredientId )
        {
            return ServiceUtils.ResolveEntity( ingredientRepository, ingredientId );
        }
        

        private IProductRepository productRepository;
        private IProductSizeRepository productSizeRepository;
        private IIngredientRepository ingredientRepository;
    }
}
