﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Dto;
using System;

namespace Pizzario.Service
{
    public interface IIngredientService : IDomainEntityService< IngredientDto >
    {
        Guid Create ( string name );

        void Rename ( Guid ingredientId, string newName );
    }
}
