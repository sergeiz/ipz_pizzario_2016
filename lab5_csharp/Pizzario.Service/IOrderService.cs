﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Dto;

using System;
using System.Collections.Generic;

namespace Pizzario.Service
{
    public interface IOrderService : IDomainEntityService< OrderDto >
    {
        IList< Guid > ViewUnconfirmed ();

        IList< Guid > ViewReady4Delivery ();

        Guid CreateNew ( string deliveryAddress, string contactPhone, Guid cartId );

        void SetDiscount ( Guid orderId, decimal discountPercent );

        void Confirm ( Guid orderId );

        void Cancel ( Guid orderID );
    }
}
