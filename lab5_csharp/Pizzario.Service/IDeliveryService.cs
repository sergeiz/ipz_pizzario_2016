﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Dto;

using System;
using System.Collections.Generic;

namespace Pizzario.Service
{
    public interface IDeliveryService : IDomainEntityService< DeliveryDto >
    {
        IList< Guid > ViewWaiting ();

        IList< Guid > ViewInProgress ();

        DeliveryDto FindOrderDelivery ( Guid orderId );

        void MarkStarted ( Guid deliveryId, string driverName );

        void MarkDelivered ( Guid deliveryId );
    }
}
