﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Dto;

using System;
using System.Collections.Generic;

namespace Pizzario.Service
{
    public interface IAccountService : IDomainEntityService< AccountDto >
    {
        AccountDto Identify ( string email, string password );

        IList< Guid > ViewAssociatedOrders ( Guid accountId );

        Guid CreateOperator ( string name, string email, string password );

        void ChangeName ( Guid accountId, string newName );

        void ChangeEmail ( Guid accountId, string newEmail );

        void ChangePassword ( Guid accountId, string oldPassword, string newPassword );

        void TrackOrder ( Guid accountId, Guid orderId );
    }
}
