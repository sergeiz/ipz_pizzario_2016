﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Dto;
using System;

namespace Pizzario.Service
{
    public interface IProductSizeService : IDomainEntityService< ProductSizeDto >
    {
        Guid Create ( string name, int diameter, double weight );

        void Rename ( Guid sizeId, string newName );

        void ChangeDiameter ( Guid sizeId, int newDiameter );

        void ChangeWeight ( Guid sizeId, double weight );
    }
}
