﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

namespace Pizzario.Repository.EntityFramework.Configurations
{
    class DiscountConfiguration : BasicValueConfiguration< Discount >
    {
        public DiscountConfiguration ()
        {
            Property( d => d.Percent ).IsRequired();
        }
    }
}
