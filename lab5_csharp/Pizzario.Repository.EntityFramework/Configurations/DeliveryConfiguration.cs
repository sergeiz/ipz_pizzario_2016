﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

namespace Pizzario.Repository.EntityFramework.Configurations
{
    class DeliveryConfiguration : BasicEntityConfiguration< Delivery >
    {
        public DeliveryConfiguration()
        {
            Property( d => d.DriverName ).IsOptional();
            HasRequired( d => d.RelatedOrder );
        }
    }
}
