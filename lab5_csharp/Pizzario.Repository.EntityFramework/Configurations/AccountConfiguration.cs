﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

namespace Pizzario.Repository.EntityFramework.Configurations
{
    class AccountConfiguration : BasicEntityConfiguration< Account >
    {
        public AccountConfiguration ()
        {
            Property(a => a.Name).IsRequired();
            Property(a => a.Email).IsRequired();
            Property(a => a.Password).IsRequired();
        }
    }
}
