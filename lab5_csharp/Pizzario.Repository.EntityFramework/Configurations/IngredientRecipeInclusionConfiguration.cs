﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Repository.EntityFramework.Configurations
{
    class IngredientRecipeInclusionConfiguration : EntityTypeConfiguration< IngredientRecipeInclusion >
    {
        public IngredientRecipeInclusionConfiguration()
        {
            HasKey( i => i.InclusionId );
            HasRequired( i => i.RelatedRecipe );
            HasRequired( i => i.IncludedIngredient );
        }
    }
}
