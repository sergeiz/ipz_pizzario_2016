﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Repository.EntityFramework.Configurations
{
    abstract class BasicEntityConfiguration< T > : EntityTypeConfiguration< T> 
        where T : Pizzario.Utils.Entity
    {
        protected BasicEntityConfiguration ()
        {
            HasKey( e => e.DatabaseId );
            Property( e => e.DomainId ).IsRequired();
        }
    }
}
