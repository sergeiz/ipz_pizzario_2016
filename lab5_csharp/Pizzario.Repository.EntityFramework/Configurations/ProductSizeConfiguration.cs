﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

namespace Pizzario.Repository.EntityFramework.Configurations
{
    class ProductSizeConfiguration : BasicEntityConfiguration< ProductSize >
    {
        public ProductSizeConfiguration()
        {
            Property( s => s.Name ).IsRequired();
        }
    }
}
