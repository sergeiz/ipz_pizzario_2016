﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System.Data.Entity.ModelConfiguration;

namespace Pizzario.Repository.EntityFramework.Configurations
{
    class ProductPriceAssignmentConfiguration : EntityTypeConfiguration< ProductPriceAssignment >
    {
        public ProductPriceAssignmentConfiguration ()
        {
            HasKey( c => c.AssignmentId );
            HasRequired( c => c.RelatedProduct );
            HasRequired( c => c.Size );
        }
    }
}
