﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

namespace Pizzario.Repository.EntityFramework.Configurations
{
    class IngredientConfiguration : BasicEntityConfiguration< Ingredient >
    {
        public IngredientConfiguration ()
        {
            Property( i => i.Name ).IsRequired();
        }
    }
}
