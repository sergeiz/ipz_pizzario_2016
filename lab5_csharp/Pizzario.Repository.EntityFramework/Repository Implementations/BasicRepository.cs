﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using System;
using System.Data.Entity;
using System.Linq;

namespace Pizzario.Repository.EntityFramework
{
    public abstract class BasicRepository< T > where T : Pizzario.Utils.Entity
    {
        protected BasicRepository ( PizzarioDbContext dbContext, DbSet< T > dbSet )
        {
            this.dbContext = dbContext;
            this.dbSet     = dbSet;
        }

        protected PizzarioDbContext GetDBContext ()
        {
            return this.dbContext;
        }

        protected DbSet< T > GetDBSet ()
        {
            return this.dbSet;
        }

        public void Add ( T obj )
        {
            dbSet.Add( obj );
        }

        public void Delete ( T obj )
        {
            dbSet.Remove( obj );
        }

        public IQueryable< T > LoadAll ()
        {
            return dbSet;
        }

        public T Load ( int id )
        {
            return dbSet.Find( id );
        }

        public int Count ()
        {
            return dbSet.Count();
        }

        public T FindByDomainId ( Guid domainId )
        {
            return dbSet.Where( e => e.DomainId == domainId ).SingleOrDefault();
        }


        public IQueryable< Guid > SelectAllDomainIds ()
        {
            return dbSet.Select( e => e.DomainId );
        }

        public void StartTransaction ()
        {
            this.dbContext.Database.BeginTransaction();
        }

        public void Commit ()
        {
            this.dbContext.ChangeTracker.DetectChanges();
            this.dbContext.SaveChanges();
            this.dbContext.Database.CurrentTransaction.Commit();
        }

        public void Rollback ()
        {
            this.dbContext.Database.CurrentTransaction.Rollback();
        }


        private PizzarioDbContext dbContext;
        private DbSet< T > dbSet;
    }
}
