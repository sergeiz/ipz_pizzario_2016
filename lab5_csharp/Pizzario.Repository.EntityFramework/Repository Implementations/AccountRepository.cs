﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System.Linq;

namespace Pizzario.Repository.EntityFramework
{
    public class AccountRepository : BasicRepository< Account >, IAccountRepository
    {
        public AccountRepository ( PizzarioDbContext dbContext )
            :   base( dbContext, dbContext.Accounts )
        {
        }

        public Account FindByOrder ( Order o )
        {
            var operators = GetDBSet().OfType< OperatorAccount >();
            return ( from a in operators where a.Orders.Any( aOrder => aOrder.DatabaseId == o.DatabaseId ) select a ).SingleOrDefault();
        }

        public Account FindByEmail ( string email )
        {
            return GetDBSet().Where( a => a.Email == email ).SingleOrDefault();
        }
    }
}
