﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Pizzario.Model;

using System.Linq;

namespace Pizzario.Repository.EntityFramework
{
    public class ProductRepository : BasicRepository< Product >, IProductRepository
    {
        public ProductRepository ( PizzarioDbContext dbContext )
            :   base( dbContext, dbContext.Products )
        {
        }

        public Product FindByName ( string name )
        {
            return GetDBSet().Where( p => p.Name == name ).SingleOrDefault();
        }
    }
}
